package com.wx.house.core;


import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 * @ProjectName: mybatis-plus
 * @Package: com.citia.mybatisplus.conf
 * @ClassName: CodeGenerator
 * @Author: jiangfeixiang
 * @email: 1016767658@qq.com
 * @Description: 代码生成器
 * @Date: 2019/5/10/0010 21:41
 */
public class CodeGenerator {
	public static String db_ip="192.168.1.200";
	public static String prot="3306";
	public static String db_name="wx_houseing_saas";
	public static String db_acctount="root";
	public static String db_pwd="3jdmshsk@*123&";
    /**
     * 读取控制台内容
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    public static void main(String[] args) throws IOException {
        /**
         * 代码生成器
         */
        AutoGenerator mpg = new AutoGenerator();

        /**
         * 全局配置
         */
        String projectPath = System.getProperty("user.dir");

        mpg.setGlobalConfig(new GlobalConfig()
                .setOutputDir(projectPath + "/src/main/java/")
                .setAuthor("yangxiusen")
                .setSwagger2(true)
                .setFileOverride(true)// 是否覆盖文件
                .setActiveRecord(true)// 开启 activeRecord 模式
                .setEnableCache(false)// XML 二级缓存
                .setBaseResultMap(true)// XML ResultMap
                .setBaseColumnList(true)// XML columList
                .setOpen(false)//生成后打开文件夹
                // 自定义文件命名，注意 %s 会自动填充表实体属性！
                .setMapperName("%sMapper")
                .setXmlName("%sMapper")
                .setServiceName("%sService")
                .setServiceImplName("%sServiceImpl")
                .setControllerName("%sController")
        );

        /**
         * 数据源配置
         */
        //dc.setDbType(DbType.MYSQL);
        mpg.setDataSource(
                new DataSourceConfig()
                        .setTypeConvert(new MySqlTypeConvert())
                        .setDbType(DbType.MYSQL)
                        .setDriverName("com.mysql.jdbc.Driver")
                        //.setUrl("jdbc:mysql://219.153.15.13:3306/citia?useUnicode=true&characterEncoding=UTF8&allowMultiQueries=true&useSSL=false&serverTimezone=GMT%2B8&nullCatalogMeansCurrent=true")
                        //.setUsername("root")
                        //.setPassword("ZX!159#kjpass")
                        .setUrl("jdbc:mysql://"+db_ip+":"+prot+"/"+db_name+"?useUnicode=true&useSSL=false&characterEncoding=utf8")
                        .setDriverName("com.mysql.jdbc.Driver")
                        .setUsername(db_acctount)
                        .setPassword(db_pwd)
        );
        /**
         * 包配置
         */
        mpg.setPackageInfo(new PackageConfig()
                //.setModuleName(scanner("模块名称"))
                .setParent("com.wx.house.core")// 自定义包路径
                .setController("controller")// 这里是控制器包名，默认 web
                .setEntity("entity") // 设置Entity包名，默认entity
                .setMapper("mapper") // 设置Mapper包名，默认mapper
                .setService("service") // 设置Service包名，默认service
                .setServiceImpl("service.impl") // 设置Service Impl包名，默认service.impl
                .setXml("mapper") // 设置Mapper XML包名，默认mapper.xml
        );
        /**
         * 自定义配置
         */
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("cfg", this.getConfig().getGlobalConfig().getAuthor() + "-mp");
                this.setMap(map);
            }
        };
        /**
         * 模板
         */
        //如果模板引擎是 freemarker
        //String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";
        /**
         * 自定义输出配置  xml 生成目录
         */
        List<FileOutConfig> fileOutList = new ArrayList<>();
        // 自定义配置会被优先输出
        fileOutList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/java/com/wx/house/core/"
                        + "/xml/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(fileOutList);
        mpg.setCfg(cfg);
        /**
         * 配置模板
         */
        TemplateConfig templateConfig = new TemplateConfig();
        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // templateConfig.setEntity("templates/entity2.java");
        // templateConfig.setService();
        // templateConfig.setController();
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);
        /**
         * 策略配置
         */
        mpg.setStrategy(new StrategyConfig()
                        // 全局大写命名
                        // .setCapitalMode(true)
                        //.setDbColumnUnderline(true)
                        // 此处可以修改为您的表前缀
                        //.setTablePrefix(new String[]{prefix})
                        .setEntityLombokModel(true)
                        //生成 @RestController 控制器
                        .setRestControllerStyle(true)
                        // 表名生成策略
                        .setNaming(NamingStrategy.underline_to_camel)
                        .setColumnNaming(NamingStrategy.underline_to_camel)
                        //生成表名
                        .setInclude(scanner("表名,多个英文逗号分割").split(","))
                        .setControllerMappingHyphenStyle(true)
                //.setExclude(new String[]{"test"}) // 排除生成的表
                // 自定义实体父类
                // .setSuperEntityClass("com.baomidou.demo.TestEntity")
                // 自定义实体，公共字段
                //.setSuperEntityColumns(new String[]{"test_id"})
                //.setTableFillList(tableFillList)
                // 自定义 mapper 父类 默认BaseMapper
                //.setSuperMapperClass("com.baomidou.mybatisplus.mapper.BaseMapper")
                // 自定义 service 父类 默认IService
                // .setSuperServiceClass("com.baomidou.demo.TestService")
                // 自定义 service 实现类父类 默认ServiceImpl
                // .setSuperServiceImplClass("com.baomidou.demo.TestServiceImpl")
                // 自定义 controller 父类
                //.setSuperControllerClass("com.kichun."+packageName+".controller.AbstractController")
                // 【实体】是否生成字段常量（默认 false）
                // public static final String ID = "test_id";
                // .setEntityColumnConstant(true)
                // 【实体】是否为构建者模型（默认 false）
                // public User setName(String name) {this.name = name; return this;}
                // .setEntityBuilderModel(true)
                // 【实体】是否为lombok模型（默认 false）<a href="https://projectlombok.org/">document</a>
                // Boolean类型字段是否移除is前缀处理
                // .setEntityBooleanColumnRemoveIsPrefix(true)
                // .setRestControllerStyle(true)
                // .setControllerMappingHyphenStyle(true)
        );
        //mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }
}