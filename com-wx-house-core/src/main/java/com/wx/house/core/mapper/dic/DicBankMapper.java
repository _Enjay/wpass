package com.wx.house.core.mapper.dic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.DicBank;
import com.wx.house.core.pojo.vo.DicBankVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 下午 02:00
 * @description：${description}
 * @version: ：V$version
 */
public interface DicBankMapper extends BaseMapper<DicBank> {

List<DicBankVo> selectAll(@Param("keyword") String keyword);

int insertSelective(DicBank dicBank);

int updateById(@Param("updated") DicBank updated, @Param("id") String id);

int deleteById(@Param("id") String id);


}