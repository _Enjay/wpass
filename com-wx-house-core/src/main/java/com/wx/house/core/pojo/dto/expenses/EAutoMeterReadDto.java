package com.wx.house.core.pojo.dto.expenses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.dto.expenses
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 10:44
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class EAutoMeterReadDto {
    /**
     * id
     */
    @ApiModelProperty(value = "ID(用于更新)", required = false, example = "123459652")
    private Long id;
    /**
     * 自动抄表数据频率
     */
    @ApiModelProperty(value = "自动抄表数据频率", required = true, example = "1")
    private Integer meterReadNum;
    /**
     * 自动抄表数据频率
     */
    @ApiModelProperty(value = "提前通知收房租天数", required = true, example = "1")
    private Integer advanceDays;
}
