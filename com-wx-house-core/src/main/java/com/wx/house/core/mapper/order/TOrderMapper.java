package com.wx.house.core.mapper.order;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.aly8246.collectionutil.model.Page;
import com.wx.house.core.pojo.po.TOrder;
import com.wx.house.core.pojo.vo.TOrderCreateVo;
import com.wx.house.core.pojo.vo.TOrderCreateVos;
import com.wx.house.core.pojo.vo.TOrderDetailAllVo;
import com.wx.house.core.pojo.vo.TOrderMonthAllVo;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:23
 * @description：${description}
 * @version: ：V$version
 */
@Component
public interface TOrderMapper extends BaseMapper<TOrder> {

    int insertSelective(TOrder tOrder);

    TOrderCreateVo selectTOrderCreateVo(@Param("orderId") String orderId);

    List<TOrderCreateVo> selectTOrderCreateAllVo(Page page, @Param("orderNumber") String orderNumber, @Param("state") Integer state, String userId);

    //取消订单
    int updateById(@Param("updated") TOrder updated, @Param("id") String id);

    //更新支付结果
    int updateOrderId(Map map);

    //更新微信预付订单id
    int updatePrepayId(@Param("orderNo") String orderNo, @Param("prepayId") String prepayId);

    //根据系统订单号查询
    TOrder selectTorder(@Param("orderNumber") String orderNumber);

    // 查询所有订单信息-list里的数据
    List<TOrderCreateVos> selectTOrderCreateAllVoFY(@Param("state") String state, String userId);

    // 查询所有订单信息-list里todString的数据
    List<TOrderCreateVos> selectDetailAllVo(@Param("todString") String todString);
    
    //根据系统订单编号查询
    TOrder selectNumber(@Param("orderNumber")String orderNumber);
    
    //调取订单查询接口更新数据
    int updateState(@Param("actualPrice")BigDecimal actualPrice,@Param("transactionId")String transactionId,@Param("orderNumber") String OrderNumber);
    
    //根据订单表id查询订单金额
    TOrder selectTorId(@Param("orderNumber")String orderNumber);
}