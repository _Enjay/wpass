package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description 楼宇列表
 * @Author 罗棋
 * @Date 2019/11/01 下午 02:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildingVo {
    @ApiModelProperty(value = "楼栋id", example = "1")
    private String id;

    @ApiModelProperty(value = "楼栋名称", example = "v客部落")
    private String buildName;

    @ApiModelProperty(value = "楼栋图片", example = "http://www.xxx.com/1.png")
    private String buildImg;

    @ApiModelProperty(value = "省份", example = "北京")
    private String province;

    @ApiModelProperty(value = "城市", example = "北京市")
    private String city;

    @ApiModelProperty(value = "区县", example = "东城区")
    private String county;

    @ApiModelProperty(value = "详细地址", example = "d5 601")
    private String address;

    @ApiModelProperty(value = "添加时间", example = "2019-11-01 10:57:11")
    private Date addTime;

    @ApiModelProperty(value = "操作人", example = "小明")
    private String operation;

    @ApiModelProperty(value = "主账号id", example = "12")
    private String mAccId;

    @ApiModelProperty(value = "操作时间", example = "2019-11-01 10:57:17")
    private Date operationTime;

    @ApiModelProperty(value = "经度", example = "37.2303283876")
    private String longitude;

    @ApiModelProperty(value = "纬度", example = "105.6884765625")
    private String latitude;

    @ApiModelProperty(value = "待出租数量", example = "50")
    private Integer waitHireNumber;

    @ApiModelProperty(value = "出租中数量", example = "20")
    private Integer hiredNumber;

    @ApiModelProperty(value = "已停用数量", example = "20")
    private Integer discontinue;

    @ApiModelProperty(value = "省份ID", example = "500000")
    private String provinceId;

    @ApiModelProperty(value = "城市ID", example = "500100")
    private String cityId;

    @ApiModelProperty(value = "区县ID", example = "500101")
    private String countyId;


    @ApiModelProperty(value = "水费 x元/1m³", example = "0.10", required = false)
    private BigDecimal waterRate;
    @ApiModelProperty(value = "电费 x元/1千瓦时", example = "0.88", required = false)
    private BigDecimal powerRate;
    @ApiModelProperty(value = "气费 x元/1m³", example = "0.23", required = false)
    private BigDecimal gasRate;
    @ApiModelProperty(value = "物管费 x元/月", example = "30", required = false)
    private BigDecimal propertyRateRate;
}
