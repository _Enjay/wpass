package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.ECostDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ECostDetailVo {
	@ApiModelProperty(value = "id")
	private String id;
	@ApiModelProperty(value = "合同id")
	private String contrId;
	@ApiModelProperty(value = "合同编号")
	private String contrNumber;
	@ApiModelProperty(value = "合同名称")
	private String contrName;
	@ApiModelProperty(value = "费用记录创建时间")
	private Date creationTime;
	@ApiModelProperty(value = "收费月份")
	private String chargeMonth;
	@ApiModelProperty(value = "备注")
	private String remarks;
	@ApiModelProperty(value = "主账号id")
	private String mAccId;
	@ApiModelProperty("收费类型: 0保证金 1房租 2 物业管理费 3水,电,气费 6其他")
	private String payTypes;
	@ApiModelProperty("费用类型: 0收费 1支出")
	private Integer costType;
	@ApiModelProperty("总计费用")
	private BigDecimal total;
	@ApiModelProperty("实际收费")
	private BigDecimal actualPay;
	@ApiModelProperty(value = "费用记录创建时间")
	private Date payTime;
	@ApiModelProperty(value = "租户姓名")
	private String tenantName;
	@ApiModelProperty(value = "手机号")
	private String tenantPhone;
	@ApiModelProperty(value = "证件号")
	private String tenantCertiNumber;
	@ApiModelProperty(value = "公寓名称")
	private String buildName;
	@ApiModelProperty(value = "房间号")
	private String roomNumber;
	
	private List<ECostDetail> eCostDetails=new ArrayList<ECostDetail>();
}
