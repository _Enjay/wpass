package com.wx.house.core.pojo.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 上午 11:28
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
private String id;
private String accountType;
private String mAccId;
private String level;
private String realName;

private String iss;
private String iat;
private String exp;
}
