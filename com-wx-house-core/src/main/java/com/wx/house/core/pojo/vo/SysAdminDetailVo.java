package com.wx.house.core.pojo.vo;

import java.util.List;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysAdminDetailVo {
	@ApiModelProperty(value = "id")
	private String id;
	@ApiModelProperty(value = "真实姓名")
	private String reallyName;
	@ApiModelProperty(value = "证件号码")
	private String idCardNumber;
	@ApiModelProperty(value = "实名状态0未实名 1待审核 2拒绝 3成功")
	private Integer realState;
	@ApiModelProperty(value = "主账号ID")
	private String maccId;
	@ApiModelProperty(value = "备注")
	private String remark;
	@ApiModelProperty(value = "电话")
	private String phone;
	@ApiModelProperty(value = "账户类型(0免费用户)")
	private Integer accountLevel;
	private List<AttaUpload> attaUploadList;
}
