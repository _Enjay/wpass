package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import lombok.Data;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/11/05 上午 11:03
 * @description：${description}
 * @version:   ：V$version
 */

@Data
@TableName(value = "dic_auto_create_config")
public class DicAutoCreateConfig implements Serializable {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private String id;

    /**
     * 主id
     */
    @TableField(value = "mAccId")
    @ApiModelProperty(value="主id")
    private String maccid;

    /**
     * 时间
     */
    @TableField(value = "time")
    @ApiModelProperty(value="时间")
    private Long time;

    /**
     * 时间单位
     */
    @TableField(value = "timeUnit")
    @ApiModelProperty(value="时间单位")
    private Long timeunit;

    /**
     * 类型: 1生成抄表数据 2生成物管费
     */
    @TableField(value = "auto_type")
    @ApiModelProperty(value="类型: 1生成抄表数据 2生成物管费")
    private Long autoType;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_MACCID = "mAccId";

    public static final String COL_TIME = "time";

    public static final String COL_TIMEUNIT = "timeUnit";

    public static final String COL_AUTO_TYPE = "auto_type";
}