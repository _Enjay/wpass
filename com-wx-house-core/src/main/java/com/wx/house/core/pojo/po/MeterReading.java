package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "meter_reading")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MeterReading implements Serializable {

/**
 * 抄表id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "抄表id")
private String id;

/**
 * 楼栋id
 */
@TableField(value = "building_id")
@ApiModelProperty(value = "楼栋id")
private String buildingId;

/**
 * 门牌号
 */
@TableField(value = "door_number")
@ApiModelProperty(value = "门牌号")
private String doorNumber;

/**
 * 合同编号
 */
@TableField(value = "contr_number")
@ApiModelProperty(value = "合同编号")
private String contrNumber;

/**
 * 租户姓名
 */
@TableField(value = "tenant_name")
@ApiModelProperty(value = "租户姓名")
private String tenantName;

/**
 * 电话号码
 */
@TableField(value = "phone")
@ApiModelProperty(value = "电话号码")
private String phone;

/**
 * 抄表月份
 */
@TableField(value = "month")
@ApiModelProperty(value = "抄表月份")
private String month;

/**
 * 水表读数(立方米)
 */
@TableField(value = "month_water")
@ApiModelProperty(value = "水表读数(立方米)")
private Integer monthWater;

/**
 * 电表读数(千瓦时)
 */
@TableField(value = "month_electric")
@ApiModelProperty(value = "电表读数(千瓦时)")
private Integer monthElectric;

/**
 * 气表读数(立方米)
 */
@TableField(value = "month_gas")
@ApiModelProperty(value = "气表读数(立方米)")
private Integer monthGas;

/**
 * 抄表状态 0待抄表 1已抄表
 */
@TableField(value = "month_state")
@ApiModelProperty(value = "抄表状态 （0待抄表 1已抄表）")
private Integer monthState;

/**
 * 当月水表读数(立方米)
 */
@TableField(value = "same_water")
@ApiModelProperty(value = "当月水表读数(立方米)")
private Integer sameWater;

/**
 * 当月电表读数(千瓦时)
 */
@TableField(value = "same_electric")
@ApiModelProperty(value = "当月电表读数(千瓦时)")
private Integer sameElectric;

/**
 *当月 气表读数(立方米)
 */
@TableField(value = "same_gas")
@ApiModelProperty(value = "当月气表读数(立方米)")
private Integer sameGas;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_BUILDING_ID = "building_id";

public static final String COL_DOOR_NUMBER = "door_number";

public static final String COL_CONTR_NUMBER = "contr_number";

public static final String COL_TENANT_NAME = "tenant_name";

public static final String COL_PHONE = "phone";

public static final String COL_MONTH = "month";

public static final String COL_MONTH_WATER = "month_water";

public static final String COL_MONTH_ELECTRIC = "month_electric";

public static final String COL_MONTH_GAS = "month_gas";
}