package com.wx.house.core.mapper.permission;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdminRole;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface SysAdminRoleMapper extends BaseMapper<SysAdminRole> {
	/***
	 * 删除管理员角色
	 * @param adminid
	 * @return
	 */
	int dateAdmin(@Param("adminid")String adminid);
	/***
	 * 根据用户id查询角色
	 * @param adminId
	 * @return
	 */
	List<SysAdminRole> selectAdminIdRole(@Param("adminId")String adminId);
}