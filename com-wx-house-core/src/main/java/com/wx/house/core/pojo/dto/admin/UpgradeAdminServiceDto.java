package com.wx.house.core.pojo.dto.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 04:13
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpgradeAdminServiceDto {
@ApiModelProperty("id")
private String id;

@ApiModelProperty("账号类型: 0免费用户 1付费用户")
private Integer accountType;

@ApiModelProperty("购买服务月份数量[几个月]")
private Integer month;

@ApiModelProperty("备注")
private String remark;
}
