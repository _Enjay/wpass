package com.wx.house.core.mapper.expenses;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.MeterReading;
import com.wx.house.core.pojo.vo.MeterReadingVo;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface MeterReadingMapper extends BaseMapper<MeterReading> {

int insertList(@Param("list") List<MeterReading> list);


/***
 * 查询全部抄表记录
 * @param MenuName
 * @return
 */
List<MeterReadingVo> AllMeter(
		@Param("maccid") String maccid,
		@Param("buildName") String buildName,
		@Param("month") String month,
		@Param("monthState") String monthState);

/***
 * 完成抄表
 * @param meterId
 * @return
 */
int completeMeter(@Param("meterId") String meterId,
		@Param("sameWater")Integer sameWater,
		@Param("sameElectric")Integer sameElectric,
		@Param("sameGas")Integer sameGas);
/***
 * 根据合同编号，抄表月份返回数据
 * @param number
 * @param month
 * @return
 */
MeterReading selectNumberMonth(@Param("number")String number,
		@Param("month")String month);
}