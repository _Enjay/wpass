package com.wx.house.core.pojo.vo;

import com.wx.house.core.pojo.state.ChargeTypeEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TOrderExpenseVo {
@ApiModelProperty("缴费类型")
private Integer expenseType;

public String getExpenseType() {
	return ChargeTypeEnum.chargeTypeByCode(expenseType);
}

@ApiModelProperty("缴费项需要缴费多少")
private BigDecimal totalExpense;

@ApiModelProperty("缴费项单价")
private BigDecimal price;

@ApiModelProperty("缴费项数量")
private BigDecimal useNumber;
}
