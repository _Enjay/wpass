package com.wx.house.core.mapper.capital;

import java.math.BigDecimal;
import java.util.List;

import com.wx.house.core.pojo.vo.SysAdminBalanceVo;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.vo.SysAdminDetailVo;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 02:31
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminDetailMapper extends BaseMapper<SysAdminDetail> {
SysAdminDetail selectByMaccId(@Param("maccId") String maccId);

int updateById(@Param("updated") SysAdminDetail updated, @Param("id") String id);
int updateByMaccId(@Param("updated")SysAdminDetail updated,@Param("maccId")String maccId);

	
	/***
	 * 提交实名认证
	 * 
	 * @param maccId
	 * @param sysAdminDetail
	 * @return
	 */
	int updateReal(@Param("adminId") String maccId, @Param("reallyName") String reallyName,
			@Param("idCardNumber") String idCardNumber);

	/***
	 * 根据附件来源id查询
	 *
	 * @param resourceId
	 * @return
	 */
	SysAdminDetailVo selectAdminUpload(@Param("resourceId") String resourceId);
	/***
	 * 根据maccId查询
	 * @param maccId
	 * @return
	 */
	SysAdminBalanceVo selectBalance(@Param("maccId") String maccId);
	/***
	 * 更新管理员资金总收入
	 */
	int updateDetail(@Param("maccId") String maccId,@Param("balance") BigDecimal balance ,@Param("totalIncome") BigDecimal totalIncome);
	/***
	 * 判断身份证是否已实名
	 */
	SysAdminDetail selectIdCardNumber(@Param("idCardNumber")String idCardNumber);
	/***
	 * 微信支付成功后更新总收入与余额
	 */
	int updateTotal(@Param("total")BigDecimal total,@Param("mAccId")String mAccId);
}