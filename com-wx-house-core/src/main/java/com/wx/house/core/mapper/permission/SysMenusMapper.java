package com.wx.house.core.mapper.permission;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysMenus;
import com.wx.house.core.pojo.vo.PermissionMenuVo;
import com.wx.house.core.pojo.vo.SysMenusVo;
import com.wx.house.core.pojo.vo.SysPermissionVo;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */
@Mapper
public interface SysMenusMapper extends BaseMapper<SysMenus> {
	/***
	 * 查询全部菜单
	 * @param MenuName
	 * @return
	 */
	List<SysMenus> selectMenus(
			@Param("menuName") String menuName);
	/***
	 * 根据菜单名查询是否重复
	 */
	SysMenus getMenusName(
			@Param("menuName") String menuName);
	/***
	 * 删除菜单（禁用）
	 * @param id
	 * @param state
	 * @return
	 */
	int DeleteMenus(
			@Param("sysMenusId") String sysMenusId,
			@Param("state") Integer state);
	/***
	 * 删除菜单
	 * @param id
	 * @param state
	 * @return
	 */
	int DeleteMenuseId(
			@Param("sysMenusId") String sysMenusId);
	
	/***
	 * 查询全部菜单
	 * @return
	 */
	List<SysMenusVo> sysMenuslist();
	/***
	 * 查询全部菜单(角色)
	 * @return
	 */
	List<SysMenusVo> sysRoleMenuslist(@Param("roleId")String roleId);
	/***
	 * 查询全部菜单
	 * @return
	 */
	List<SysMenusVo> sysMenusId(@Param("adminId")String adminId);
	/***
	 * admin查询所有菜单
	 */
	List<SysMenusVo> sysAdmin();
	/***
	 * 查询请求类型
	 * @param name
	 * @return
	 */
	SysPermissionVo SelectSysPermission(@Param("name")String name);
	
	Set<SysMenusVo> selectByAdmin();
	
	List<SysMenus> selectByAdminlist();
	
	Set<SysMenusVo> selectByUserId(@Param("uid") String uid);
	
	List<SysMenusVo> selectMenuById(@Param("idList") List<String> idList);
	
	List<PermissionMenuVo> selectMenuPermission(@Param("adminId")String adminId);
	
	PermissionMenuVo listPidMenus(@Param("id") String id);
	
	int updateMenu(@Param("sysMenus")SysMenus sysMenus);
}