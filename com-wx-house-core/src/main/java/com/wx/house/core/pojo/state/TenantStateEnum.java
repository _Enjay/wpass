package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 11:31
 * @description：.
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum TenantStateEnum {
	NORMAL(0, "正常"),
	FREEZE(1, "用户被禁用，请联系管理员");
public Integer state;
public String description;
}
