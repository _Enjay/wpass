package com.wx.house.core.pojo.po.building;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "b_charge_template")
public class BChargeTemplate implements Serializable {
/**
 * 收费模板id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "收费模板id")
private String id;

/**
 * 楼栋id
 */
@TableField(value = "build_id")
@ApiModelProperty(value = "楼栋id")
private String buildId;

/**
 * 水费单价
 */
@TableField(value = "build_water")
@ApiModelProperty(value = "水费单价")
private BigDecimal buildWater;

/**
 * 电费单价
 */
@TableField(value = "build_electric")
@ApiModelProperty(value = "电费单价")
private BigDecimal buildElectric;

/**
 * 气费单价
 */
@TableField(value = "build_gas")
@ApiModelProperty(value = "气费单价")
private BigDecimal buildGas;

/**
 * 物管费单价
 */
@TableField(value = "build_property")
@ApiModelProperty(value = "物管费单价")
private BigDecimal buildProperty;

/**
 * 添加时间
 */
@TableField(value = "add_time")
@ApiModelProperty(value = "添加时间")
private Date addTime;

/**
 * 操作人
 */
@TableField(value = "operation")
@ApiModelProperty(value = "操作人")
private String operation;

/**
 * 操作时间
 */
@TableField(value = "operation_time")
@ApiModelProperty(value = "操作时间")
private Date operationTime;

/**
 * 逻辑状态0 启用 1禁用
 */
@TableField(value = "logical_state")
@ApiModelProperty(value = "逻辑状态0 启用 1禁用")
private Integer logicalState;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_BUILD_ID = "build_id";

public static final String COL_BUILD_WATER = "build_water";

public static final String COL_BUILD_ELECTRIC = "build_electric";

public static final String COL_BUILD_GAS = "build_gas";

public static final String COL_BUILD_PROPERTY = "build_property";

public static final String COL_ADD_TIME = "add_time";

public static final String COL_OPERATION = "operation";

public static final String COL_OPERATION_TIME = "operation_time";

public static final String COL_LOGICAL_STATE = "logical_state";
}