package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.wx.house.core.pojo.po.CContract;
import com.wx.house.core.pojo.po.ContractDetail;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class CcontractNextChargeVo {
	/**
	 * 合同详细id
	 */
	@TableId(value = "id", type = IdType.INPUT)
	@ApiModelProperty(value = "合同详细id")
	private String id;
	/**
	 * 合同编号
	 */
	@TableField(value = "contr_number")
	@ApiModelProperty(value = "合同编号")
	private String contrNumber;
	/**
	 * 合同名称
	 */
	@TableField(value = "contr_name")
	@ApiModelProperty(value = "合同名称")
	private String contrName;
	/**
	 * 主账号id
	 */
	@TableField(value = "m_acc_id")
	@ApiModelProperty(value = "主账号id")
	private String mAccId;
	/**
	 * 合同id
	 */
	@TableField(value = "contract_id")
	@ApiModelProperty(value = "合同id")
	private String contractId;
	/**
	 * 合同开始时间
	 */
	@TableField(value = "start_time")
	@ApiModelProperty(value = "合同开始时间")
	private Date startTime;
	/**
	 * 合同结束时间
	 */
	@TableField(value = "end_time")
	@ApiModelProperty(value = "合同结束时间")
	private Date endTime;
	/**
	 * 租金类型
	 */
	@TableField(value = "rent_type")
	@ApiModelProperty(value = "租金类型")
	private Integer rentType;
	/**
	 * 房租
	 */
	@TableField(value = "charge_money")
	@ApiModelProperty(value = "房租")
	private BigDecimal chargeMoney;
	/**
	 * 物管费用
	 */
	@TableField(value = "next_charge_time")
	@ApiModelProperty(value = "下次收费时间")
	private Date nextChargeTime;
	
	
}
