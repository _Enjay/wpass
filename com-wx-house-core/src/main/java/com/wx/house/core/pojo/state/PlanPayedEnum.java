package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 下午 03:51
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum PlanPayedEnum {
	PAYED(0, "已缴清", "费用已缴清，无需再次缴费啦"),
	INCOMPLETE_PAY(1, "未缴清", "未缴清"),
	WAIT_PAY(2, "待缴费", "还未缴费");
private int code;
private String state;
private String msg;
}
