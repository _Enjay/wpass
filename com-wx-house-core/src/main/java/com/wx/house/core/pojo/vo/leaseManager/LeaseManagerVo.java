package com.wx.house.core.pojo.vo.leaseManager;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 10:25
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeaseManagerVo {
@ApiModelProperty("有效租约")
private Integer validLease;
@ApiModelProperty("将要过期")
private Integer willExpire;
@ApiModelProperty("已过期")
private Integer expire;

@ApiModelProperty("过期租约详细信息")
private List<LeaseManagerDetailVo> leaseManagerDetailVoList = new ArrayList<>();

}
