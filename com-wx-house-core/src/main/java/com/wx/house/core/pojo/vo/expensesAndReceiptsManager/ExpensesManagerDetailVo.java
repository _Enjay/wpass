package com.wx.house.core.pojo.vo.expensesAndReceiptsManager;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 09:40
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpensesManagerDetailVo {
@ApiModelProperty("待收费类型: 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
private Integer waitExpensesType;

@ApiModelProperty("待收笔数")
private Integer waitExpensesNumber;

@ApiModelProperty("待收金额")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal waitExpensesMoney;

}
