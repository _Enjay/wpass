package com.wx.house.core.mapper.tenant;


import com.wx.house.core.pojo.state.ExpenseHistoryVo;
import com.wx.house.core.pojo.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:39
 * @description：${description}
 * @version: ：V$version
 */
public interface UserCustomMapper {
    //查询用户id，用户时间和名字，缴费金额，总费用，合同到期时间
    TerminalIndexVo selectTerminalIndex(@Param("tUserId") String tUserId, @Param("thisMonth") String thisMonth);

    //app查询合同列表
    List<IndexContractVo> selectIndexContractVo(@Param("tUserId") String tUserId, @Param("state") String state);

    //查询用户实名信息
    TUserIDCardVo selectTUserIDCardVo(@Param("tUserId") String tUserId);

    //查询用户合同详情
    TUserContractVo selectTenantContractDetail(@Param("contractId") String contractId);

    //app首页-待缴收费基本信息
    List<TenantPendingMonthPaymentVo> selectTenantPendingPaymentVo(@Param("userId") String userId);

    List<TenantPendingPaymentVo> selectTenantPendingPaymentVoByContractIdAndMonthIn(@Param("contractId") String contractId, @Param("monthList") List<String> monthList);

    List<TenantPendingPaymentVo> selectTenantPendingPaymentVoByContractIdAndMonth(@Param("contractId") String contractId, @Param("month") String month);

    /***
     * 根据用户id查询
     * @param tUserId
     * @return
     */
    List<ExpenseHistoryVo> selectPayHistory(@Param("tUserId") String tUserId);


}