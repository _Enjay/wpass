package com.wx.house.core.mapper.capital;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdminWithdrawal;
import com.wx.house.core.pojo.vo.SysAdminWithdrawalVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:12
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminWithdrawalMapper extends BaseMapper<SysAdminWithdrawal> {
List<SysAdminWithdrawalVo> selectByMaccId(
		@Param("maccId") String maccId
		, @Param("state") Integer state
		, @Param("keyword") String keyword);

SysAdminWithdrawal selectById(@Param("id") String id);

int insertSelective(SysAdminWithdrawal sysAdminWithdrawal);


int updateById(@Param("updated") SysAdminWithdrawal updated, @Param("id") String id);

}