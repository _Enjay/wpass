package com.wx.house.core.pojo.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wx.house.core.pojo.po.CContract;
import com.wx.house.core.pojo.po.ContractDetail;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/04 上午 10:52
 * @description：${description}
 * @version: ：V$version
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CContractBo extends CContract implements Serializable {

private ContractDetail contractDetail;

private BuildingRoom buildingRoom;
private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_CONTR_NUMBER = "contr_number";

public static final String COL_CONTR_NAME = "contr_name";

public static final String COL_ROOM_ID = "room_id";

public static final String COL_END_WATER = "end_water";

public static final String COL_END_ELECTRIC = "end_electric";

public static final String COL_END_GAS = "end_gas";

public static final String COL_TENANT_NAME = "tenant_name";

public static final String COL_TENANT_PHONE = "tenant_phone";

public static final String COL_TENANT_CERTIFICATES = "tenant_certificates";

public static final String COL_TENANT_CERTI_NUMBER = "tenant_certi_number";

public static final String COL_REMARKS = "remarks";

public static final String COL_ADD_TIME = "add_time";

public static final String COL_OPERATION = "operation";

public static final String COL_OPERATION_TIME = "operation_time";

public static final String COL_LOGICAL_STATE = "logical_state";

public static final String COL_M_ACC_ID = "m_acc_id";
}