package com.wx.house.core.pojo.dto.expenses;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class ECostDto {
	@ApiModelProperty(value = "待处理费用id",required = true)
	@NotNull
	private String id;
//	@ApiModelProperty(value = "合同编号",required = true)
//	@NotNull
//	private String contrNumber;
//	@ApiModelProperty(value = "合同id",required = true)
//	@NotNull
//	private String contrId;
//	@ApiModelProperty(value = "收费类型（收费类型0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他）")
//	private Integer chargeType;
//	@ApiModelProperty(value = "费用类型(0收费 1支出)")
//	private Integer costType;
//	@ApiModelProperty(value = "创建时间")
//	private Date creationTime;
//	@TableField(value = "收费年月")
//	private String chargeMonth;
	@ApiModelProperty(value = "应收金额")
	private BigDecimal receiveMoney;
	@ApiModelProperty(value = "实收金额")
	private BigDecimal netMoney;
	@ApiModelProperty(value = "备注")
	private String remarks;
}
