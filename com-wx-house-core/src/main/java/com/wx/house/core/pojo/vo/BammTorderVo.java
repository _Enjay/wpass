package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BammTorderVo {

    @ApiModelProperty(value = "缴费类型", example = "1")
    private String chargeType;

    public String getChargeType() {
        // 0保证金 1房租 2 物业管理费 3水费 4电费 5气费
        return "0".equals(chargeType) ? "保证金"
                : "1".equals(chargeType) ? "房租"
                : "2".equals(chargeType) ? "物业管理费"
                : "3".equals(chargeType) ? "水费"
                : "4".equals(chargeType) ? "电费" : "气费";
    }

    @ApiModelProperty("单价")
    private BigDecimal unitPrice;
    @ApiModelProperty("用量")
    private Integer consumption;

    public Integer getConsumption() {
        Integer a = 0;
        if ("3".equals(this.chargeType) || "4".equals(this.chargeType) || "5".equals(this.chargeType)) {
            a = this.receiveMoney.divide(this.unitPrice, BigDecimal.ROUND_HALF_UP).intValue();
        }
        return a;
    }

    @ApiModelProperty("应缴费用")
    private BigDecimal receiveMoney;
}
