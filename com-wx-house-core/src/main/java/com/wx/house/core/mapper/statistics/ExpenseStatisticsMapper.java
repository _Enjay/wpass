package com.wx.house.core.mapper.statistics;

import com.wx.house.core.pojo.vo.expenseStatistics.ExpenseMonthStatisticsVo;
import com.wx.house.core.pojo.vo.expenseStatistics.ExpenseYearStatisticsVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingMonthIntoDetailStatisticsVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingYearIntoStatisticsVo;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 04:09
 * @description：
 * @version: ：V
 */
public interface ExpenseStatisticsMapper {
List<ExpenseYearStatisticsVo> selectExpenseYearStatisticsVo(@Param("maccid") String maccid, @Param("year") String year);

ExpenseMonthStatisticsVo selectExpenseMonthStatisticsVo(@Param("maccid") String maccid, @Param("yearMonth") String yearMonth);

List<BuildingMonthIntoDetailStatisticsVo> selectBuildingIntoStatisticsVo(@Param("maccid") String maccid, @Param("yearMonth") String yearMonth);
/***
 * 根据楼宇，开始，结束，时间查询(查月)
 * @param buildName
 * @param start
 * @param end
 * @return
 */
BuildingMonthIntoDetailStatisticsVo selectRoomNumber(@Param("buildName")String buildName,@Param("start")String start,@Param("end")String end);
/***
 * 根据mAccID查询
 * @param mAccId
 * @param start
 * @param end
 * @return
 */
BuildingYearIntoStatisticsVo selectYeaMRoomNumber(@Param("mAccId")String mAccId,@Param("start")String start,@Param("end")String end);
/***
 * 根据楼宇，开始，结束，时间查询(查年)
 * @param buildName
 * @param start
 * @param end
 * @return
 */
BuildingYearIntoStatisticsVo selectYeaRoomNumber(@Param("buildName")String buildName,@Param("start")String start,@Param("end")String end);
}
