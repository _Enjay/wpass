package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 03:17
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminServiceVo {

@ApiModelProperty(value = "id")
private String id;

@TableField(value = "balance")
@ApiModelProperty(value = "余额")
private BigDecimal balance;


@TableField(value = "total_income")
@ApiModelProperty(value = "总收入")
private BigDecimal totalIncome;

/**
 * 已提现金额
 */
@TableField(value = "withdrawals")
@ApiModelProperty(value = "已提现金额")
private BigDecimal withdrawals;

/**
 * 被冻结余额[ 提现发起以后扣除余额进入冻结状态]
 */
@TableField(value = "freeze_balance")
@ApiModelProperty(value = "被冻结余额[ 提现发起以后扣除余额进入冻结状态]")
private BigDecimal freezeBalance;

@ApiModelProperty(value = "手机号")
private String phone;

/**
 * 真实姓名
 */
@TableField(value = "really_name")
@ApiModelProperty(value = "真实姓名")
private String reallyName;


/**
 * 证件号码
 */
@TableField(value = "id_card_number")
@ApiModelProperty(value = "证件号码")
private String idCardNumber;

@ApiModelProperty(value = "身份证图片")
private String idCardImg;

@ApiModelProperty(value = "身份证图片-反面")
private String idCardImg_r;


/**
 * 实名状态0未实名 1待审核 2拒绝 3成功
 */
@TableField(value = "real_state")
@ApiModelProperty(value = "实名状态0未实名 1待审核 2拒绝 3成功")
private Integer realState;

/**
 * 账号类型: 0免费用户 1付费用户
 */
@TableField(value = "account_level")
@ApiModelProperty(value = "账号类型: 0免费用户 1付费用户")
private Integer accountLevel;

/**
 * 如果是付费用户: 服务结束时间 如果是免费用户:体验结束时间
 */
@TableField(value = "service_end_time")
@ApiModelProperty(value = "如果是付费用户: 服务结束时间 如果是免费用户:体验结束时间")
private Date serviceEndTime;

@ApiModelProperty(value = "子账号个数")
private Integer childAccountNumber;

}
