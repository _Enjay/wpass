package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.wx.house.core.pojo.po.ECostDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 上午 11:23
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantPendingMonthPaymentVo {
    /**
     * 收费时间
     */
    @TableField(value = "charge_month")
    @ApiModelProperty(value = "收费月份")
    private String chargeMonth;

    @ApiModelProperty(value = "收费月份总计金额")
    private BigDecimal monthTotal;

    public BigDecimal getMonthTotal() {
        BigDecimal bigDecimal = new BigDecimal(0);
        for (TenantPendingPaymentVo eCostDetail : pendingPaymentVoList) {
            bigDecimal = bigDecimal.add(eCostDetail.getTotal());
        }
        return bigDecimal;
    }

    private List<TenantPendingPaymentVo> pendingPaymentVoList = new ArrayList<>();
}
