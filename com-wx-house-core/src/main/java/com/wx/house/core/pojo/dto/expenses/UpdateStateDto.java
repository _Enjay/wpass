package com.wx.house.core.pojo.dto.expenses;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.dto.expenses
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 17:13
 * @Version V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStateDto {
    @ApiModelProperty(value = "iD", example = "id", required = true)
    String id;
    @ApiModelProperty(value = "使用状态 1 使用 2 禁用", example = "1", required = true)
    Integer logicalState;
}
