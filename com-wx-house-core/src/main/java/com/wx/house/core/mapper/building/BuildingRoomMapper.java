package com.wx.house.core.mapper.building;

import java.util.Collection;

import com.wx.house.core.pojo.vo.BuildingRoomVo;
import com.wx.house.core.pojo.vo.HiredRoomVo;
import com.wx.house.core.pojo.vo.RoomNumberVo;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.building.BuildingRoom;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 03:45
 * @description：${description}
 * @version: ：V$version
 */
public interface BuildingRoomMapper extends BaseMapper<BuildingRoom> {
int insertList(@Param("list") List<BuildingRoom> list);

int roomWaitHireNumber(@Param("buildingName") String buildingName);

int deleteByBuildingId(@Param("buildingId") String buildingId);

BuildingRoom selectRoom(@Param("floorId") String floorId, @Param("doorNumber") String doorNumber);

List<BuildingRoom> selectByBuildingId(@Param("buildingId") String buildingId);

List<BuildingRoom> selectByIdIn(@Param("idCollection") Collection<String> idCollection);

int deleteByIdIn(@Param("idCollection") Collection<String> idCollection);

int updateById(@Param("updated") BuildingRoom updated, @Param("id") String id);

BuildingRoom selectById(@Param("id") String id);

	int hireBuildingRoom(@Param("updatedBuildType") Integer updatedBuildType, 
			@Param("id") String id,
			@Param("historyWater") Integer historyWater,
			@Param("historyElectric") Integer historyElectric,
			@Param("historyGas") Integer historyGas
			);


List<BuildingRoomVo> selectSimpleWaitHireRoom(@Param("buildingId") String buildingId, @Param("floor") String floor, @Param("door") String door,@Param("buildType")String buildType);

List<BuildingRoomVo> selectWaitHireRoom(@Param("buildingId") String buildingId, @Param("floor") String floor);

void resumeBuildingRoomByContractId(@Param("id") String id);

List<HiredRoomVo> selectHiredRoomVo(@Param("buildingId") String buildingId, @Param("floor") String floor, @Param("door") String door);

List<BuildingRoom> selectByMaccid(@Param("maccid") String maccid, @Param("buildingName") String buildingName);
/***
 * 根据合同编号查询房间信息
 */
BuildingRoom selectNumberRoom(@Param("contrNumber")String contrNumber);
/***
 * 根据id将房间废弃
 */
void discardBuildingRoom(@Param("roomId")String roomId,@Param("buildType")String buildType);
/***
 * 根据楼宇id查询房间号
 */
List<RoomNumberVo> selectRoomNumber(@Param("buildingId") String buildingId);
/***
 * 根据房间id修改数据
 */
int updateBuildingRoom(@Param("room")BuildingRoom room);
/***
 * 根据合同Id查询房间信息
 */
BuildingRoom selectContrIdberRoom(@Param("contrId")String contrId);
/***
 * 根据房间id查询房间信息
 * @param contrId
 * @return
 */
BuildingRoom selectNumberIdRoom(@Param("roomId")String roomId);
/***
 * 根据房间Id修改房间状态（0 待出租 1已出租 2停用）
 * @param id
 * @param buildType
 * @return
 */
int updateRoombuildType(@Param("id")String id,@Param("buildType")String buildType);
}