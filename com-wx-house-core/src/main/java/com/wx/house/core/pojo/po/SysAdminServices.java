package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 03:13
 * @description：${description}
 * @version: ：V$version
 */
@ApiModel(value = "com-wx-house-core-pojo-po-SysAdminServices")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_admin_services")
public class SysAdminServices {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 主账号ID
 */
@TableField(value = "macc_id")
@ApiModelProperty(value = "主账号ID")
private String maccId;

/**
 * 账号类型: 0免费用户 1付费用户
 */
@TableField(value = "account_level")
@ApiModelProperty(value = "账号类型: 0免费用户 1付费用户")
private Integer accountLevel;

/**
 * 如果是付费用户: 服务结束时间 如果是免费用户:体验结束时间
 */
@TableField(value = "service_end_time")
@ApiModelProperty(value = "如果是付费用户: 服务结束时间 如果是免费用户:体验结束时间")
private Date serviceEndTime;

/**
 * 备注
 */
@TableField(value = "remark")
@ApiModelProperty(value = "备注")
private String remark;

public static final String COL_ID = "id";

public static final String COL_MACC_ID = "macc_id";

public static final String COL_ACCOUNT_TYPE = "account_type";

public static final String COL_SERVICE_END_TIME = "service_end_time";
}