package com.wx.house.core.mapper.contract;

import java.util.Date;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.bo.CContractBo;
import com.wx.house.core.pojo.po.CContract;
import com.wx.house.core.pojo.vo.CcontractBuildVo;
import com.wx.house.core.pojo.vo.ContractBaseVo;
import com.wx.house.core.pojo.vo.ContractStatisticsVo;
import com.wx.house.core.pojo.vo.ContractVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import javax.validation.constraints.Pattern;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/04 上午 10:52
 * @description：${description}
 * @version: ：V$version
 */
public interface CContractMapper extends BaseMapper<CContract> {
List<ContractVo> selectAllContract(
		@Param("mAccId") String mAccId
		, @Param("contractKeyword") String contractKeyword
		, @Param("tenantKeyword") String tenantKeyword,
		@Param("state")Integer state,
		@Param("daysTime")Integer daysTime);

int insertSelective(CContract cContract);

CContract selectByContrName(@Param("contrName") String contrName);

CContract selectByContrNumber(@Param("contrNumber") String contrNumber);

/***
 * 查询合同编号
 * @return
 */
List<CcontractBuildVo> selectNumber(@Param("tenantKeyword") String tenantKeyword);

List<CContractBo> selectByMAccId(@Param("mAccId") String mAccId);

/***
 * 查询合同资料，以及房间读数
 */
ContractBaseVo selectIdBase(@Param("contrNumber") String contrNumber);
/***
 * 按月份查询有效合同
 * @param maccid
 * @return
 */
List<ContractStatisticsVo> selectContractStatisticsVo(String maccid);
/***
 * 合同到期过后归档
 * @param contractId
 * @return
 */
int updateFile(@Param("contractId")String contractId);
}