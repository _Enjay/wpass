package com.wx.house.core.pojo.dto.expenses;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.dto.expenses
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 14:08
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class EPaymentDto {
    /**
     * id
     */
    @ApiModelProperty(value = "ID(用于更新)", required = false, example = "123459652")
    private String id;

    /**
     * 支付id
     */
    @ApiModelProperty(value = "支付id", required = false, example = "123459652")
    private String paymentId;

    /**
     * 支付密钥
     */
    @ApiModelProperty(value = "支付密钥", required = false, example = "123459652")
    private String paymentSecretkey;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id", required = false, example = "123")
    private String merchantId;
    
    /**
     * 字典id
     */
    @ApiModelProperty(value = "ID(字典id)", required = false, example = "123")
    private Long dicId;

}
