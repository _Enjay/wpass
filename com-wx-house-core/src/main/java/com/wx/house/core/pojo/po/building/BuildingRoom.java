package com.wx.house.core.pojo.po.building;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 03:45
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "b_building_room")
public class BuildingRoom implements Serializable {

/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 楼栋id
 */
@TableField(value = "building_id")
@ApiModelProperty(value = "楼栋id")
private String buildingId;

/**
 * 楼层
 */
@TableField(value = "floor_id")
@ApiModelProperty(value = "楼层")
private String floorId;

/**
 * 门牌号
 */
@TableField(value = "door_number")
@ApiModelProperty(value = "门牌号")
private String doorNumber;

/**
 * 房间大小（立方米）
 */
@TableField(value = "room_space")
@ApiModelProperty(value = "房间大小（立方米）")
private BigDecimal roomSpace;

/**
 * 添加时间
 */
@TableField(value = "add_time")
@ApiModelProperty(value = "添加时间")
private Date addTime;

/**
 * 状态(0 已出租 1待出租)
 */
@TableField(value = "build_type")
@ApiModelProperty(value = "状态 0待出租 1已出租")
private Integer buildType;

/**
 * 操作人
 */
@TableField(value = "operation")
@ApiModelProperty(value = "操作人")
private String operation;

/**
 * 操作时间
 */
@TableField(value = "operation_time")
@ApiModelProperty(value = "操作时间")
private Date operationTime;

/**
 * 水表读数
 */
@TableField(value = "history_water")
@ApiModelProperty(value = "水表读数")
private Integer historyWater;

/**
 * 电表读数
 */
@TableField(value = "history_electric")
@ApiModelProperty(value = "电表读数")
private Integer historyElectric;

/**
 * 气表读数
 */
@TableField(value = "history_gas")
@ApiModelProperty(value = "气表读数")
private Integer historyGas;

/**
 * 表具信息更新时间
 */
@TableField(value = "update_time")
@ApiModelProperty(value = "表具信息更新时间")
private Date updateTime;
}