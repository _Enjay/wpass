package com.wx.house.core.mapper.permission;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdmin;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface SysAdminMapper extends BaseMapper<SysAdmin> {
	/***
	 * 根据账号查询用户记录
	 */
	SysAdmin loginAdmin(
			@Param("loginName") String loginName);
	/***
	 * 查询电话是否注册
	 */
	SysAdmin getPhone(
			@Param("phone") String phone);
	/***
	 * 根据账号Id查询用户记录
	 */
	SysAdmin loginAdminId(
			@Param("adminId") String adminId);
	/***
	 * 用户登录更新IP，最后登录时间
	 * @param loginIp
	 * @param loginTime
	 * @return
	 */
	int updetaAdminIp(
			@Param("id") String id,
			@Param("loginIp") String loginIp,
			@Param("loginTime") Date loginTime);
	/***
	 * 查询全部(模糊查询账号，状态)
	 * @param id
	 * @param id
	 * @return
	 */
	List<SysAdmin> selctAdmin(
			@Param("level") String level,
			@Param("accountType") String accountType);
	/***
	 * 删除用户(禁用)
	 * @param loginName
	 * @return
	 */
	int DeleteAdmin(
			@Param("id") String id,
			@Param("state") Integer state);
	/***
	 * 删除用户
	 * @param loginName
	 * @return
	 */
	int deleteDelType(
			@Param("id") String id);
	/***
	 * 修改密码
	 * @param loginPwd
	 * @param safeKey
	 * @returns
	 */
	int updateAdminLog(@Param("adminId")String adminId,
						@Param("loginPwd")String loginPwd,
						@Param("safeKey")Integer safeKey);
}