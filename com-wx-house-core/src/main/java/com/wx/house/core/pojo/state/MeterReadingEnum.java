package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/13 上午 11:52
 * @description：
 * @version: ：V
 */
@Getter
@AllArgsConstructor
public enum MeterReadingEnum {
	WAIT_READING(0, "未抄表"),
	READY(1, "已抄表");
public Integer state;
public String description;
}
