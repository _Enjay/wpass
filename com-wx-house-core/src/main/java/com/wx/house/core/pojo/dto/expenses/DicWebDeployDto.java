package com.wx.house.core.pojo.dto.expenses;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.dto.expenses
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 13:48
 * @Version V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class DicWebDeployDto {
    /**
     * id
     */
    @ApiModelProperty(value="id(用于更新)",required = false,example = "123")
    private String id;

    /**
     * 网站名称
     */
    @ApiModelProperty(value="网站名称",required = true,example = "xx某网站")
    private String webName;

    /**
     * 版权信息
     */
    @ApiModelProperty(value="版权信息",required = true,example = "1.23")
    private String copyright;

    /**
     * 附件id
     */
    @ApiModelProperty(value="附件id",required = true,example = "12456")
    private String fileId;

    /**
     * 网站域名
     */
    @ApiModelProperty(value="网站域名",required = true,example = "oppo1.9linked.com")
    private String domainName;

}
