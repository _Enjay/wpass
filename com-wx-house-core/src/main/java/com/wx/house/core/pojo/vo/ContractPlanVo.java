package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/15 上午 09:44
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractPlanVo {
private List<ContractPlanDetailVo> contractPlanDetailVoList;
@ApiModelProperty(value = "公寓名称")
private String buildingName;
@ApiModelProperty(value = "公寓地址")
private String buildingAddress;
@ApiModelProperty(value = "楼层")
private String floorId;
@ApiModelProperty(value = "房间号")
private String doorNumber;
@ApiModelProperty(value = "合同名称")
private String contractName;
@ApiModelProperty(value = "水费")
private String buildWater;
@ApiModelProperty(value = "电费")
private String buildElectric;
@ApiModelProperty(value = "气费")
private String buildGas;
@ApiModelProperty(value = "物管费")
private String buildProperty;
@ApiModelProperty(value = "合同开始时间")
private Date startTime;
@ApiModelProperty(value = "合同结束时间")
private Date endTime;
}
