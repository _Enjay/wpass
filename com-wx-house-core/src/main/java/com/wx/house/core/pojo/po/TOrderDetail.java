package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 11:27
 * @description：${description}
 * @version: ：V$version
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_order_detail")
public class TOrderDetail {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 订单id
     */
    @TableField(value = "order_id")
    @ApiModelProperty(value = "订单id")
    private String orderId;

    @TableField(value = "e_cost_id")
    @ApiModelProperty(value = "待处理费用id")
    private String eCostId;

    /**
     * 合同id[一个月份里多个月份都需要缴费]
     */
    @TableField(value = "c_contract_id")
    @ApiModelProperty(value = "合同id")
    private String cContractId;


}