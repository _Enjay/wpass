package com.wx.house.core.pojo.dto.building;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 03:46
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public
class BuildingRoomDto {
@ApiModelProperty(value = "楼层", example = "6", required = true)
@NotBlank
private String floorId;
@ApiModelProperty(value = "门牌", example = "601", required = true)
@NotBlank
private String doorNumber;

@ApiModelProperty(value = "房屋面积 m³", example = "68.33", required = false)
private BigDecimal roomSpace;
}
