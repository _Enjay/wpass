package com.wx.house.core.pojo.dto.fileload;

import com.wx.house.core.pojo.base.Page;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.dto.fileload
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/28 17:55
 * @Version V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttaUploadDto {
    /**|
     * 文件base64字符串
     */
    @ApiModelProperty(value = "文件base64字符串", required = true, example = "encodedImageStr")
    private String encodedImageStr;
    /**
     * 1 身份证正面 2身份证反面  3 合同 4 网站logo
     */
    @ApiModelProperty(value = "1 身份证正面 2身份证反面  3 合同 4 网站logo", required = false, example = "1")
    private  String type;
    /**
     * 文件上传路径
     */
    @ApiModelProperty(value = "文件上传路径", required = false, example = "1")
    private String fileUploadPath;
    @ApiModelProperty(value = "姓名", required = false, example = "1")
    private String OcrName;
    @ApiModelProperty(value = "身份证", required = false, example = "1")
    private String OcrCode;
}
