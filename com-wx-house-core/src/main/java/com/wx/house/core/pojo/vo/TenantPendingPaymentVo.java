package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.wx.house.core.pojo.po.ECostDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 上午 11:23
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class TenantPendingPaymentVo {
    /**
     * 待处理费用id(=>ecostId)
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 合同id
     */
    @TableField(value = "contr_id")
    @ApiModelProperty(value = "合同id")
    private String contractId;
    /**
     * 合同编号
     */
    @TableField(value = "contr_number")
    @ApiModelProperty(value = "合同编号")
    private String contrNumber;

    /**
     * 合同名称
     */
    @TableField(value = "contr_name")
    @ApiModelProperty(value = "合同名称")
    private String contrName;

    @TableField(value = "build_name")
    @ApiModelProperty(value = "公寓名称")
    private String buildingName;

    @TableField(value = "bbr_floor_id")
    @ApiModelProperty(value = "公寓楼层")
    private String buildingroomfioorid;

    @TableField(value = "bbr_door_number")
    @ApiModelProperty(value = "公寓门牌号")
    private String buildingdoornumber;

    @TableField(value = "buildingAddress")
    @ApiModelProperty(value = "公寓地址")
    private String buildingAddress;

    @ApiModelProperty(value = "公寓图片")
    private String buildingImg;


    /**
     * 收费时间
     */
    @TableField(value = "charge_month")
    @ApiModelProperty(value = "收费月份")
    private String chargeMonth;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    @ApiModelProperty(value = "备注")
    private String remarks;

    private List<ECostDetail> eCostDetails;

    @ApiModelProperty("该合同当月总计")
    private BigDecimal total;

    public BigDecimal getTotal() {
        BigDecimal bigDecimal = new BigDecimal(0);
        for (ECostDetail eCostDetail : eCostDetails) {
            //计算应收金额
            bigDecimal = bigDecimal.add(eCostDetail.getReceiveMoney());
            //计算实收金额
            bigDecimal = bigDecimal.subtract(eCostDetail.getNetMoney());
        }
        return bigDecimal;
    }
}
