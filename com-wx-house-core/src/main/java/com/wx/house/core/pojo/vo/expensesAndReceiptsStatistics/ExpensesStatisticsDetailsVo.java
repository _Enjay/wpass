package com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 11:17
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpensesStatisticsDetailsVo {
@ApiModelProperty("收退费类型:0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
private Integer type;

@ApiModelProperty("费用金额")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal money;
}
