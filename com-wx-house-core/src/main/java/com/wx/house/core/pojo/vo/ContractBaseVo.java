package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ContractBaseVo {
	@ApiModelProperty(value = "合同")
	private String id;

	@TableField(value = "contr_number")
	@ApiModelProperty(value = "合同编号")
	private String contrNumber;

	@TableField(value = "contr_name")
	@ApiModelProperty(value = "合同名称")
	private String contrName;

	@TableField(value = "room_id")
	@ApiModelProperty(value = "房间id")
	private String roomId;

	@ApiModelProperty(value = "截止水表读数(立方米)")
	private Integer endWater;

	@ApiModelProperty(value = "截止电表读数(千瓦时)")
	private Integer endElectric;

	@ApiModelProperty(value = "截止气表读数(立方米)")
	private Integer endGas;

	@ApiModelProperty(value = "租户姓名")
	private String tenantName;

	@ApiModelProperty(value = "手机号")
	private String tenantPhone;

	@ApiModelProperty(value = "证件类型(0  身份证)")
	private Integer tenantCertificates;

	@ApiModelProperty(value = "证件号")
	private String tenantCertiNumber;

	@TableField(value = "remarks")
	@ApiModelProperty(value = "备注")
	private String remarks;

	@TableField(value = "add_time")
	@ApiModelProperty(value = "添加时间")
	private Date addTime;

	@ApiModelProperty(value = "操作人")
	private String operation;

	@ApiModelProperty(value = "操作时间")
	private Date operationTime;

	@ApiModelProperty(value = "逻辑状态0 启用 1禁用")
	private Integer logicalState;

	@ApiModelProperty(value = "主账号id")
	private String mAccId;
	
	@ApiModelProperty(value = "水费单价")
	private BigDecimal buildWater;

	@ApiModelProperty(value = "电费单价")
	private BigDecimal buildElectric;

	@ApiModelProperty(value = "水费单价")
	private BigDecimal buildGas;
	
	@ApiModelProperty(value = "物业费")
	private BigDecimal propertyManager;
	
	
}
