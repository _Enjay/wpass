package com.wx.house.core.pojo.vo.expenseStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 04:07
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseMonthStatisticsVo {
@ApiModelProperty(value = "指定年月")
private String time;

@ApiModelProperty(value = "费用统计详情")
private List<ExpenseMonthDetailStatisticsVo> detailStatisticsVoList = new ArrayList<>();
}
