package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/20 上午 10:02
 * @description：
 * @version: ：V
 */
@Getter
@AllArgsConstructor
public enum CertificationTypeEnum {
	ID_CARD(1);
private Integer type;
}
