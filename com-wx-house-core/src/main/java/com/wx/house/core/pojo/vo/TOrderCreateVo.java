package com.wx.house.core.pojo.vo;

import com.wx.house.core.pojo.po.ECost;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订单管理
 *
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/02 上午 09:22
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TOrderCreateVo {

    @ApiModelProperty("订单id")
    private String id;

    @ApiModelProperty("订单编号")
    private String orderNumber;

    @ApiModelProperty("订单状态: 0待缴费 1已缴费")
    private Integer orderState;

    @ApiModelProperty("订单创建时间")
    private Date createTime;

    @ApiModelProperty("订单总金额")
    private BigDecimal orderPrice;

    public BigDecimal getOrderPrice() {
        BigDecimal bigDecimal = new BigDecimal(0);

        for (TOrderMonthVo tOrderMonthVo : tOrderMonthVoList) {
            bigDecimal = tOrderMonthVo.getTotal().add(bigDecimal);
        }
        return bigDecimal;
    }


    @ApiModelProperty("每个订单多个缴费月份")
    private List<TOrderMonthVo> tOrderMonthVoList = new ArrayList<>();


    @ApiModelProperty("每个订单多个缴费月份")
    private List<TOrderCreateVos> tOrderMonthAllVoList = new ArrayList<>();


}
