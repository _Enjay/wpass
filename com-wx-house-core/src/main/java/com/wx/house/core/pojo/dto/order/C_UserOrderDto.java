package com.wx.house.core.pojo.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:26
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class C_UserOrderDto {

@ApiModelProperty(value = "缴费月份", required = true, example = "2019-11")
@NotBlank
private String payMonth;

@ApiModelProperty("缴费详情")
private List<C_UserOrderDetail> c_userOrderDetail;

}
