package com.wx.house.core.pojo.state;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/21 上午 09:25
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseHistoryVo {
    /*当前时间（年*月）*/
    @TableField(value = "mouth")
    @ApiModelProperty(value = "往期缴费时间（年*月）")
    private String mouth;

    /*实收金额*/
    @TableField(value = "expense")
    @ApiModelProperty(value = "实收金额")
    private String expense;
}
