package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 下午 04:04
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum PlanCreatedEnum {
	CREATED(0, "已生成", "已生成 待处理费用信息"),
	WAIT_CREATE(1, "待生成", "待生成 待处理费用信息");
private Integer code;
private String state;
private String msg;
}
