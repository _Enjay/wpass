package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.ECostDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TOrderMonthVo {

    @TableField(value = "charge_month")
    @ApiModelProperty("缴费月份")
    private String month;
    @ApiModelProperty("该月份总计缴费金额")
    private BigDecimal total= new BigDecimal(0);
    @ApiModelProperty("该月份里多个合同需要缴费")
    private List<TOrderBuildingVo> tOrderBuildingVoList = new ArrayList<>();

    public BigDecimal getTotal() {
        //统计该月，总计需要缴费多少
        if (tOrderBuildingVoList.size() > 0) {
            BigDecimal total = new BigDecimal(0);
            List<BigDecimal> expenseList = tOrderBuildingVoList.stream().map(TOrderBuildingVo::getExpense).collect(Collectors.toList());
            for (BigDecimal e : expenseList) {
                total = total.add(e);
            }
            return total;
        }
        return total;
    }


}
