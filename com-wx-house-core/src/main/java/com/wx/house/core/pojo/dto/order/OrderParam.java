package com.wx.house.core.pojo.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Auther: 杰
 * @Date: 2020/1/10 11:45
 * @Description:
 */
@Data
public class OrderParam {

    @ApiModelProperty("订单总金额")
    private BigDecimal total;

    @ApiModelProperty("待处理Id")
    private List<String> eCostId;
}