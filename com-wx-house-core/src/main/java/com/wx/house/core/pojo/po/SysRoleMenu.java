package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wx.house.core.pojo.po.SysAdminRole.SysAdminRoleBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */

@Data
@TableName(value = "sys_role_menu")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysRoleMenu implements Serializable {
    /**
     * 权限id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="权限id")
    private String id;

    /**
     * 角色id
     */
    @TableField(value = "role_id")
    @ApiModelProperty(value="角色id")
    private String roleId;

    /**
     * 菜单权限id
     */
    @TableField(value = "permission_id")
    @ApiModelProperty(value="菜单权限id")
    private String  permissionId;

    /**
     * 菜单id
     */
    @TableField(value = "menu_id")
    @ApiModelProperty(value="菜单id")
    private String menuId;

    /**
     * 操作者
     */
    @TableField(value = "operation")
    @ApiModelProperty(value="操作者")
    private String operation;

    /**
     * 操作时间
     */
    @TableField(value = "operation_time")
    @ApiModelProperty(value="操作时间")
    private Date operationTime;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_ROLE_ID = "role_id";

    public static final String COL_PERMISSION_ID = "permission_id";

    public static final String COL_MENU_ID = "menu_id";

    public static final String COL_OPERATION = "operation";

    public static final String COL_OPERATION_TIME = "operation_time";
}