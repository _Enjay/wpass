package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version:   ：V$version
 */

@Data
@TableName(value = "sys_admin_role")
@Builder
@AllArgsConstructor
public class SysAdminRole implements Serializable {
    /**
     * 用户关联id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="用户关联id")
    private String id;

    /**
     * 管理员id
     */
    @TableField(value = "admin_id")
    @ApiModelProperty(value="管理员id")
    private String adminId;

    /**
     * 角色id
     */
    @TableField(value = "role_id")
    @ApiModelProperty(value="角色id")
    private String roleId;

    /**
     * 操作人
     */
    @TableField(value = "operation")
    @ApiModelProperty(value="操作人")
    private String operation;

    /**
     * 操作时间
     */
    @TableField(value = "operation_time")
    @ApiModelProperty(value="操作时间")
    private Date operationTime;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_ADMIN_ID = "admin_id";

    public static final String COL_ROLE_ID = "role_id";

    public static final String COL_OPERATION = "operation";

    public static final String COL_OPERATION_TIME = "operation_time";
}