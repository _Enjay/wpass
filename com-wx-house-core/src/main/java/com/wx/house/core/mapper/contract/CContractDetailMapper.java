package com.wx.house.core.mapper.contract;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.ContractDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface CContractDetailMapper extends BaseMapper<ContractDetail> {
	int insertList(@Param("list")List<ContractDetail> list);

	int insertSelective(ContractDetail contractDetail);

	
}