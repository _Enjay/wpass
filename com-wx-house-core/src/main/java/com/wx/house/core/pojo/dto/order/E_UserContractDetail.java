package com.wx.house.core.pojo.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:26
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class E_UserContractDetail {

@ApiModelProperty("缴费合同id")
private String contractId;

@ApiModelProperty(value = "[暂时不传，默认1234] 缴费类型: 1房租 2 物业管理费 3水费 4电费 5气费 6其他", required = false, example = "[1,2,3,4]")
private List<Integer> payType;
}
