package com.wx.house.core.pojo.vo;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class RoomNumberVo {
	@ApiModelProperty("房间号")
	private String roomNumber;
}
