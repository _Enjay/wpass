package com.wx.house.core.pojo.vo;

import com.wx.house.core.pojo.state.ExpenseHistoryVo;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/21 上午 09:22
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TerminalIndexVo {
@ApiModelProperty("用户id")
private String id;

@ApiModelProperty("用户姓名")
private String username;

@ApiModelProperty("本月待缴费总费用")
private String expense;

@ApiModelProperty("合同到期时间")
private Date contractEndTime;

private List<ExpenseHistoryVo> expenseHistoryVoList = new ArrayList<>();

}
