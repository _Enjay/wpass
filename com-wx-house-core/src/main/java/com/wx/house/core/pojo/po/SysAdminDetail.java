package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 02:31
 * @description：${description}
 * @version: ：V$version
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_admin_detail")
public class SysAdminDetail {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 余额
 */
@TableField(value = "balance")
@ApiModelProperty(value = "余额")
private BigDecimal balance;

/**
 * 总收入
 */
@TableField(value = "total_income")
@ApiModelProperty(value = "总收入")
private BigDecimal totalIncome;

/**
 * 已提现金额
 */
@TableField(value = "withdrawals")
@ApiModelProperty(value = "已提现金额")
private BigDecimal withdrawals;

/**
 * 被冻结余额[ 提现发起以后扣除余额进入冻结状态]
 */
@TableField(value = "freeze_balance")
@ApiModelProperty(value = "被冻结余额[ 提现发起以后扣除余额进入冻结状态]")
private BigDecimal freezeBalance;

/**
 * 真实姓名
 */
@TableField(value = "really_name")
@ApiModelProperty(value = "真实姓名")
private String reallyName;

/**
 * 证件号码
 */
@TableField(value = "id_card_number")
@ApiModelProperty(value = "证件号码")
private String idCardNumber;

/**
 * 实名状态0未实名 1待审核 2拒绝 3成功
 */
@TableField(value = "real_state")
@ApiModelProperty(value = "实名状态0未实名 1待审核 2拒绝 3成功")
private Integer realState;

/**
 * 主账号ID
 */
@TableField(value = "macc_id")
@ApiModelProperty(value = "主账号ID")
private String maccId;

/**
 * 备注
 */
@TableField(value = "remark")
@ApiModelProperty(value = "备注")
private String remark;


public static final String COL_ID = "id";

public static final String COL_BALANCE = "balance";

public static final String COL_TOTAL_INCOME = "total_income";

public static final String COL_WITHDRAWALS = "withdrawals";

public static final String COL_FREEZE_BALANCE = "freeze_balance";

public static final String COL_MACC_ID = "macc_id";

public static final String COL_REMARK = "remark";
}