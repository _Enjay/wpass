package com.wx.house.core.pojo.vo;

import lombok.Data;

@Data
public class SysPermissionVo {
	private String id;
	private String name;
}
