package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 02:07
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HiredRoomVo {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 楼栋id
 */
@TableField(value = "building_id")
@ApiModelProperty(value = "楼栋id")
private String buildingId;

/**
 * 楼层
 */
@TableField(value = "floor_id")
@ApiModelProperty(value = "楼层")
private String floorId;

/**
 * 门牌号
 */
@TableField(value = "door_number")
@ApiModelProperty(value = "门牌号")
private String doorNumber;

/**
 * 房间大小（立方米）
 */
@TableField(value = "room_space")
@ApiModelProperty(value = "房间大小（立方米）")
private BigDecimal roomSpace;

/**
 * 添加时间
 */
@TableField(value = "add_time")
@ApiModelProperty(value = "添加时间")
private Date addTime;

/**
 * 状态(0 已出租 1待出租)
 */
@TableField(value = "build_type")
@ApiModelProperty(value = "状态 0待出租 1已出租")
private Integer buildType;

@ApiModelProperty(value = "楼宇名称")
private String buildingName;

@ApiModelProperty(value = "楼宇地址")
private String buildingAddress;
@ApiModelProperty(value = "楼宇图片")
private String buildingImg;

@ApiModelProperty(value = "租户id")
private String tenantId;

@ApiModelProperty(value = "租户名称")
private String tenantName;

@ApiModelProperty(value = "手机号", example = "13996996969")
private String phoneNumber;

@ApiModelProperty(value = "证件号码", example = "500242199911445867")
private String idCardNumber;

@ApiModelProperty(value = "合同ID")
private String contractId;

@ApiModelProperty(value = "合同名称")
private String contractName;

@ApiModelProperty(value = "合同编号")
private String contractNumber;
}
