package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/06 下午 03:46
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BuildingRoomVo {
@ApiModelProperty(value = "房间id")
private String roomId;
@ApiModelProperty(value = "楼层号")
private String floorId;
@ApiModelProperty(value = "房间号")
private String doorNumber;

@ApiModelProperty(value = "公寓名称")
private String buildingName;
@ApiModelProperty(value = "公寓图片")
private String buildingImg;
@ApiModelProperty(value = "公寓地址")
private String address;

/**
 * 房间大小（立方米）
 */
@TableField(value = "room_space")
@ApiModelProperty(value = "房间大小（立方米）")
private BigDecimal roomSpace;

/**
 * 添加时间
 */
@TableField(value = "add_time")
@ApiModelProperty(value = "添加时间")
private Date addTime;

/**
 * 状态(0 已出租 1待出租 2停用)
 */
@TableField(value = "build_type")
@ApiModelProperty(value = "状态 0待出租 1已出租 2停用")
private Integer buildType;

}
