package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/25 上午 09:32
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexContractVo {
@ApiModelProperty("合同id")
private String contractId;

@ApiModelProperty("合同名称")
private String contractName;

@ApiModelProperty("合同编号")
private String contractNumber;

@ApiModelProperty("建筑名称")
private String buildingName;

@ApiModelProperty("楼层号")
private String floor;

@ApiModelProperty("房间号")
private String door;

@ApiModelProperty("合同结束时间")
private String endTime;

@ApiModelProperty("正面图片")
private String idCard;

@ApiModelProperty("反面图片")
private String idCard_r;

@ApiModelProperty("反面图片")
private List<String> contractFileList;
}
