package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "c_contract_detail")
public class ContractDetail implements Serializable {
/**
 * 合同详细id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "合同详细id")
private String id;

/**
 * 合同id
 */
@TableField(value = "contract_id")
@ApiModelProperty(value = "合同id")
private String contractId;

/**
 * 合同期限
 */
@TableField(value = "contract_term")
@ApiModelProperty(value = "合同期限")
private Integer contractTerm;

/**
 * 期限单位
 */
@TableField(value = "contract_term_unit")
@ApiModelProperty(value = "期限单位")
private Integer contractTermUnit;

/**
 * 合同开始时间
 */
@TableField(value = "start_time")
@ApiModelProperty(value = "合同开始时间")
private Date startTime;

/**
 * 合同结束时间
 */
@TableField(value = "end_time")
@ApiModelProperty(value = "合同结束时间")
private Date endTime;

/**
 * 保证金
 */
@TableField(value = "deposit")
@ApiModelProperty(value = "保证金")
private BigDecimal deposit;

/**
 * 租金类型
 */
@TableField(value = "rent_type")
@ApiModelProperty(value = "租金类型")
private Integer rentType;

/**
 * 预付房租
 */
@TableField(value = "prepayment")
@ApiModelProperty(value = "预付房租")
private BigDecimal prepayment;

/**
 * 房租
 */
@TableField(value = "charge_money")
@ApiModelProperty(value = "房租")
private BigDecimal chargeMoney;

/**
 * 物管费用
 */
@TableField(value = "property_manager")
@ApiModelProperty(value = "物管费用")
private BigDecimal propertyManager;

/**
 * 截止水表读数(立方米)
 */
@TableField(value = "end_water")
@ApiModelProperty(value = "截止水表读数(立方米)")
private Integer endWater;

/**
 * 截止电表读数(千瓦时)
 */
@TableField(value = "end_electric")
@ApiModelProperty(value = "截止电表读数(千瓦时)")
private Integer endElectric;

/**
 * 截止气表读数(立方米)
 */
@TableField(value = "end_gas")
@ApiModelProperty(value = "截止气表读数(立方米)")
private Integer endGas;

/**
 * 物管费用
 */
@TableField(value = "next_charge_time")
@ApiModelProperty(value = "下次收费时间")
private Date nextChargeTime;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_CONTRACT_ID = "contract_id";

public static final String COL_CONTRACT_TERM = "contract_term";

public static final String COL_CONTRACT_TERM_UNIT = "contract_term_unit";

public static final String COL_START_TIME = "start_time";

public static final String COL_END_TIME = "end_time";

public static final String COL_DEPOSIT = "deposit";

public static final String COL_RENT_TYPE = "rent_type";

public static final String COL_PREPAYMENT = "prepayment";

public static final String COL_CHARGE_MONEY = "charge_money";
}