package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/04 下午 02:17
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum TimeUnitEnum {
	MILLISECOND(1, "毫秒", 1),
	SECOND(2, "秒", 1000),
	MINUTE(3, "分钟", 1000 * 60),
	HOUR(4, "小时", 1000 * 60 * 60),
	DAY(5, "天", 1000 * 60 * 60 * 24),
	WEEK(6, "周", 1000 * 60 * 60 * 24 * 7),
	MONTH(7, "月", 1000 * 60 * 60 * 24 * 30),
	QUARTER(8, "季度", 1000 * 60 * 60 * 24 * 30 * 3),
	HALF_YEAR(9, "半年", 1000 * 60 * 60 * 24 * 30 * 6),
	YEAR(10, "年", 1000 * 60 * 60 * 24 * 30 * 12);
private int timeCode;
private String timeStr;
private long unit;
}
