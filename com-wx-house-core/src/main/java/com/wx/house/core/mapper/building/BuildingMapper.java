package com.wx.house.core.mapper.building;

import com.wx.house.core.pojo.vo.BuildingSimpleVo;
import com.wx.house.core.pojo.vo.BuildingVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingMonthIntoDetailStatisticsVo;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.building.Building;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:56
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface BuildingMapper extends BaseMapper<Building> {
int insertSelective(Building building);

int insertList(@Param("list") List<Building> list);

int deleteById(@Param("id") String id);

int updateById(@Param("updated") Building updated, @Param("id") String id);

Building selectById(@Param("id") String id);

/**
 * @param mAccId 主账号id
 * @return 楼宇列表
 * @see BuildingVo
 */
List<BuildingVo> selectBuildingVoList(@Param("mAccId") String mAccId,
                                      @Param("buildingName") String buildingName,
                                      @Param("province") Integer province,
                                      @Param("city") Integer city,
                                      @Param("county") Integer county);

List<BuildingSimpleVo> selectBuildingSimpleVoList(@Param("mAccId") String mAccId,
                                                  @Param("buildingName") String buildingName,
                                                  @Param("province") Integer province,
                                                  @Param("city") Integer city,
                                                  @Param("county") Integer county,
                                                  @Param("floor") String floor,
                                                  @Param("door") String door);

/**
 * @param buildingName 楼栋名称
 * @return 楼宇
 */
Building selectByBuildName(@Param("buildingName") String buildingName);
/***
 * 根据maccId查询房东有多少楼宇
 * @param mAccId
 * @return
 */
List<Building> selcetMaccId(@Param("mAccId")String mAccId);
/***
 * 根据房间名字查询下面有多少房间
 * @param maccId
 * @return
 */
BuildingMonthIntoDetailStatisticsVo selcetNumber(@Param("buildName")String buildName);
/***
 * 根据maccId查询当前房东下面多少房间
 * @param maccId
 * @return
 */
BuildingMonthIntoDetailStatisticsVo selcetMaccIdNumber(@Param("mAccId")String mAccId);

}