package com.wx.house.core.mapper.expenses;

import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.vo.ECostDetailVo;
import com.wx.house.core.pojo.vo.ECostVo;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface ECostMapper extends BaseMapper<ECost> {
int insertListRent(@Param("list") List<ECost> list);

/***
 * 查询合同列表
 * @param
 * @param tenantKeyword
 * @return
 */
List<ECostVo> selectAllECost(
		@Param("maccid") String maccid,
		@Param("tenantKeyword") String tenantKeyword,
		@Param("chargeType") String chargeType,
		@Param("startTime") Date startTime,
		@Param("endTime") Date endTime,
		@Param("buildingId") String buildingId,
		@Param("floorId") String floorId,
		@Param("doorNumber") String doorNumber);

ECostDetailVo selectAllECostDetail(@Param("eCostId")String eCostId);

List<ECost> selectByCostStateAndUserId(@Param("costState") Integer costState, @Param("userId") String userId);

/***
 * 查询当前月份是否已经存在
 */
ECost selectMonth(@Param("contrId")String contrId,@Param("month")String month,@Param("payTypes")String payTypes);

List<ECost> selectListId(@Param("eCostIds")String eCostIds);
/***
 * 根据待处理id修改总金额
 */
int updateIdTotal(@Param("eCostId")String eCostId,@Param("total")BigDecimal total);
/***
 * 根据id查询
 */
ECost selectId(@Param("id")String id);
/***
 * 根据商户id查询
 */
List<ECost> selectTransactionId(@Param("order_number")String order_number);
}