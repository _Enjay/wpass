package com.wx.house.core.mapper.tenant;

import java.util.Collection;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.TUser;
import com.wx.house.core.pojo.vo.TUserVo;
import com.wx.house.core.pojo.vo.TenantContractDetailVo;
import com.wx.house.core.pojo.vo.TenantUserVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:39
 * @description：${description}
 * @version: ：V$version
 */
public interface TUserMapper extends BaseMapper<TUser> {
    List<TenantUserVo> selectAll(@Param("keyword") String keyword, @Param("maccId") String maccId);

    TUser selectById(@Param("id") String id);


    int deleteById(@Param("id") String id);

    List<TUser> selectByIdIn(@Param("idCollection") Collection<String> idCollection);

    /**
     * 添加注册用户
     * @param tUser
     * @return
     */
    int insertSelective(TUser tUser);

    /**
     * 更新实名信息
     * 修改账户密码
     * @param updated
     * @param id
     * @return
     */
    //
    int updateById(@Param("updated") TUser updated, @Param("id") String id);

    TenantContractDetailVo selectTenantContractDetail(@Param("tenantId") String tenantId);

    /***
     * 查询登录信息
     * @param phoneNumber
     * @return
     */
    TUserVo selectByPhoneNumber(@Param("phoneNumber") String phoneNumber);

    List<TenantUserVo> selectIDAll(@Param("idNumber") String idNumber, @Param("maccId") String maccId);
    /***
     * 根据手机号查询用户
     * @param phone
     * @return
     */
    TUser selectPhone(@Param("phone")String phone);
    /***
     * 租房绑定房东id
     */
    int updateTuserMaccId(@Param("maccid")String maccid,@Param("userId")String userId);
}