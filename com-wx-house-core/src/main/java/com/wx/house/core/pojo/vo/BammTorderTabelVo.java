package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BammTorderTabelVo {
	 @ApiModelProperty(value = "楼栋名称", example = "v客部落")
	    private String buildName;
	    
	    @ApiModelProperty(value = "房间号")
	    private String plate;
	    
	    @ApiModelProperty("url")
	    private String url;

		@ApiModelProperty("费用类型")
	    private List<BammTorderVo> bammTorderVoList;
	    
	    @ApiModelProperty("该月份总计缴费金额")
	    private BigDecimal total = new BigDecimal(0);
	    
	    public BigDecimal getTotal() {
	        //统计该月，总计需要缴费多少
	        if (bammTorderVoList.size() > 0) {
	            BigDecimal total = new BigDecimal(0);
	            List<BigDecimal> expenseList = bammTorderVoList.stream().map(BammTorderVo::getReceiveMoney).collect(Collectors.toList());
	            for (BigDecimal e : expenseList) {
	                total = total.add(e);
	            }
	            return total;
	        }
	        return total;
	    }
}
