package com.wx.house.core.pojo.vo.expenseStatistics;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 04:07
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseYearStatisticsVo {
@ApiModelProperty("时间")
private String time;

@ApiModelProperty("收入")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal income = new BigDecimal(0);

@ApiModelProperty("支出")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal expenditure = new BigDecimal(0);
}
