package com.wx.house.core.pojo.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/29 上午 09:42
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class Page {
/**
 * @Description 第几页
 */
@ApiModelProperty(value = "第几页,默认第一页", name = "page", example = "1", required = false)
private Integer page = 1;

/**
 * @Description 每页的大小
 */

@ApiModelProperty(value = "每页的大小,默认一页10条数据", name = "pageSize", example = "10", required = false)
private Integer pageSize = 10;


public Integer getPage() {
	return page != null ? page : 1;
}

public Integer getPageSize() {
	return pageSize != null ? pageSize : 10;
}

}
