package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wx.house.core.pojo.po.ECollectFees;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/12 上午 11:38
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ECollectFeesDetailVo {
    @ApiModelProperty(value = "id")
    private String id;


    @TableField(value = "sz_type")
    @ApiModelProperty(value = "收支类型 收费  退费")
    private Integer szType;


    @TableField(value = "fees_type")
    @ApiModelProperty(value = "收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
    private Integer feesType;

    @TableField(value = "payment_method")
    @ApiModelProperty(value = "支付方式： 0支付宝 1微信 2银行卡 3线下")
    private Integer paymentMethod;

    @TableField(value = "m_acc_id")
    @ApiModelProperty(value = "合同id")
    private String mAccId;

    @TableField(value = "contract_id")
    @ApiModelProperty(value = "合同id")
    private String contractId;

    @TableField(value = "fee_month")
    @ApiModelProperty(value = "收费年月")
    private String feeMonth;

    @TableField(value = "receivable")
    @ApiModelProperty(value = "应收费用")
    private BigDecimal receivable;

    @TableField(value = "actual")
    @ApiModelProperty(value = "实收费用")
    private BigDecimal actual;

    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @TableField(value = "charge_time")
    @ApiModelProperty(value = "收费时间")
    private Date chargeTime;

    @TableField(value = "remarks")
    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "水费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildWater;

    @ApiModelProperty(value = "电费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildElectric;

    @ApiModelProperty(value = "气费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildGas;

    @ApiModelProperty(value = "该缴费项的价格")
    private BigDecimal price;

    public BigDecimal getPrice() {
        switch (this.getFeesType()) {
            case 3:
                return this.getBuildWater();
            case 4:
                return this.getBuildElectric();
            case 5:
                return this.getBuildGas();
        }
        return new BigDecimal(0);
    }


    @ApiModelProperty(value = "该缴费项的使用数量")
    private BigDecimal useNumber;

    public BigDecimal getUseNumber() {
        if (this.getPrice().compareTo(new BigDecimal(0)) == 0) return new BigDecimal(0);
        return this.getReceivable().divide(this.getPrice());
    }
}
