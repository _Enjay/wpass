package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.wx.house.core.pojo.po.DicBank;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 下午 02:00
 * @description：${description}
 * @version: ：V$version
 */

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value = "dic_bank")
public class DicBankVo extends DicBank {

/**
 * 附件id
 */
@ApiModelProperty(value = "附件")
private String url;

public static final String COL_ID = "id";

public static final String COL_BANK_NAME = "bank_name";

public static final String COL_ORDER = "order";

public static final String COL_FILE_ID = "file_id";
}