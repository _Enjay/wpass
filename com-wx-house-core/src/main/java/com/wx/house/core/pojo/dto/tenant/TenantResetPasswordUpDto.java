package com.wx.house.core.pojo.dto.tenant;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:00
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantResetPasswordUpDto {
@ApiModelProperty(name = "phone", value = "手机号", required = true)
@NotBlank
private String phone;

@NotBlank
@ApiModelProperty(name = "code1", value = "密码1", required = true)
private String code1;

@NotBlank
@ApiModelProperty(name = "code2", value = "密码2[相同于密码1]", required = true)
private String code2;

@NotBlank
@ApiModelProperty(name = "validCode", value = "短信验证码", required = true)
private String validCode;
}
