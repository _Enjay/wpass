package com.wx.house.core.pojo.dto.contract;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/04 下午 02:13
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SelectContractDto {
@ApiModelProperty("根据合同搜索: 合同编号 合同名称 楼宇名称")
private String contractKeyword;

@ApiModelProperty("根据租户搜索: 租户性名 租户手机 租户证件号码")
private String tenantKeyword;

@ApiModelProperty("合同状态: 0正常 1废弃 2到期 3归档  不传[默认]全部 ")
private Integer state;

@ApiModelProperty("即将到期：5 10 30")
private Integer daysTime;
}
