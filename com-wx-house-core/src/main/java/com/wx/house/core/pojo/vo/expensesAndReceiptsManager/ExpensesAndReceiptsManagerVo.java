package com.wx.house.core.pojo.vo.expensesAndReceiptsManager;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 09:37
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpensesAndReceiptsManagerVo {

@ApiModelProperty("待收总笔数")
private Integer waitExpensesTotalNumber;

@ApiModelProperty("待收总金额")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal waitExpensesTotalMoney;

public Integer getWaitExpensesTotalNumber() {
	return (int) this.expensesManagerDetailVoList
			             .stream()
			             .map(ExpensesManagerDetailVo::getWaitExpensesNumber)
			             .mapToInt(a -> a)
			             .summaryStatistics()
			             .getSum();
}

public BigDecimal getWaitExpensesTotalMoney() {
	return this.expensesManagerDetailVoList
			       .stream()
			       .map(ExpensesManagerDetailVo::getWaitExpensesMoney)
			       .reduce(BigDecimal.ZERO, BigDecimal::add);
}

@ApiModelProperty("待收费详情")
private List<ExpensesManagerDetailVo> expensesManagerDetailVoList = new ArrayList<>();

}
