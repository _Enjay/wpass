package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Description 楼宇列表
 * @Author 罗棋
 * @Date 2019/11/01 下午 02:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildingSimpleVo {
@ApiModelProperty(value = "楼栋id", example = "1")
private String id;

@ApiModelProperty(value = "楼栋名称", example = "v客部落")
private String buildName;


@ApiModelProperty(value = "详细地址", example = "d5 601")
private String address;


@ApiModelProperty(value = "待出租数量", example = "50")
private Integer waitHireNumber;

@ApiModelProperty(value = "出租中数量", example = "20")
private Integer hiredNumber;

@ApiModelProperty(value = "已停用数量", example = "20")
private Integer discontinueNumber;
}
