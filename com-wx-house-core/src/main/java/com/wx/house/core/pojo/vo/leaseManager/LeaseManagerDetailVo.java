package com.wx.house.core.pojo.vo.leaseManager;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 10:34
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeaseManagerDetailVo {
@ApiModelProperty("公寓名称")
private String buildingName;

@ApiModelProperty("过期时间")
private Date expireTime;

@ApiModelProperty("楼层")
private String floor;

@ApiModelProperty("门牌")
private String door;
}
