package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 04:10
 * @description：
 * @version: ：V
 */
@Getter
@AllArgsConstructor
public enum LogicalDeleteEnum {
	NORMAL(0, "正常"),
	DISCARD(1, "废弃"),
	EXPIRE(2, "到期"),
	FILE(3, "归档");
public Integer state;
public String description;
}
