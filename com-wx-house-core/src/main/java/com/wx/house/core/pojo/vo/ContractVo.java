package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wx.house.core.pojo.po.ContractDetail;
import com.wx.house.core.pojo.po.building.BChargeTemplate;
import com.wx.house.core.pojo.po.building.Building;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import com.wx.house.core.pojo.state.TimeUnitEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@Data
public class ContractVo {

@ApiModelProperty(value = "合同")
private String id;

@ApiModelProperty(value = "合同编号")
private String contrNumber;

@ApiModelProperty(value = "合同名称")
private String contrName;

@ApiModelProperty(value = "租房地址")
private String address;


@ApiModelProperty(value = "租户姓名")
private String tenantName;


@ApiModelProperty(value = "手机号")
private String tenantPhone;


@ApiModelProperty(value = "证件类型(0  身份证)")
private Integer tenantCertificates;


@ApiModelProperty(value = "证件号")
private String tenantCertiNumber;

@ApiModelProperty(value = "逻辑状态0 正常 1废弃 2到期 3归档")
private Integer logicalState;


@TableField(value = "remarks")
@ApiModelProperty(value = "备注")
private String remarks;

/**
 * 合同开始时间
 */
@TableField(value = "start_time")
@ApiModelProperty(value = "合同开始时间")
private Date startTime;

/**
 * 合同结束时间
 */
@TableField(value = "end_time")
@ApiModelProperty(value = "合同结束时间")
private Date endTime;


/**
 * 保证金
 */
@TableField(value = "deposit")
@ApiModelProperty(value = "保证金")
private BigDecimal deposit;

/**
 * 租金类型
 */
@TableField(value = "rent_type")
@ApiModelProperty(value = "租金类型: 5[日付] 6[周付] 7[月付] 8[季付] 9[半年付] 10[年付]")
private Integer rentType;

/**
 * 预付房租
 */
@TableField(value = "prepayment")
@ApiModelProperty(value = "预付房租")
private BigDecimal prepayment;

/**
 * 房租
 */
@TableField(value = "charge_money")
@ApiModelProperty(value = "房租")
private BigDecimal chargeMoney;

@JsonIgnore
private Integer contractTerm;
@JsonIgnore
private Integer contractTermUnit;

@ApiModelProperty(value = "合同期限")
private String contractTermStr;


@ApiModelProperty(value = "起止水费")
private BigDecimal endWater;

@ApiModelProperty(value = "起止电费")
private BigDecimal endElectric;

@ApiModelProperty(value = "起止气费")
private BigDecimal endGas;

private ContractDetail contractDetail;
private BuildingRoom buildingRoom;
private Building building;
private BChargeTemplate bChargeTemplate;

public String getContractTermStr() {
	List<TimeUnitEnum> collect = Arrays.stream(TimeUnitEnum.values())
			                             .filter(e -> e.getTimeCode() == this.contractTermUnit)
			                             .collect(Collectors.toList());
	if (collect.size() < 1) return contractTerm + "未知时间单位-" + this.contractTermUnit;
	TimeUnitEnum timeUnitEnum = collect.get(0);
	return contractTerm + timeUnitEnum.getTimeStr();
}

@ApiModelProperty(value = "合同状态: 0正常 1临近到期 2已到期")
private Integer expireState = 0;

private List<AttaUpload> attaUploadList;

public Integer getExpireState() {
	if (new Date().after(this.endTime)) return 2;
	if (getDaysBetween(new Date(), endTime) < 7) return 1;
	return 0;
}

public static Long getDaysBetween(Date startDate, Date endDate) {
	if (startDate == null || endDate == null) return 31L;
	Calendar fromCalendar = Calendar.getInstance();
	fromCalendar.setTime(startDate);
	fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
	fromCalendar.set(Calendar.MINUTE, 0);
	fromCalendar.set(Calendar.SECOND, 0);
	fromCalendar.set(Calendar.MILLISECOND, 0);
	
	Calendar toCalendar = Calendar.getInstance();
	toCalendar.setTime(endDate);
	toCalendar.set(Calendar.HOUR_OF_DAY, 0);
	toCalendar.set(Calendar.MINUTE, 0);
	toCalendar.set(Calendar.SECOND, 0);
	toCalendar.set(Calendar.MILLISECOND, 0);
	
	return (toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24);
}

}