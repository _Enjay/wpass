/*
 * Copyright (C) 2017 Zhejiang BYCDAO Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.bycdao.com.
 * Developer Web Site: http://open.bycdao.com.
 */

package com.wx.house.core.pojo.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Result<T> {
    @ApiModelProperty(value = "返回编号")
    private Integer code;
    @ApiModelProperty(value = "返回信息")
    private String msg;
    @ApiModelProperty(value = "返回对象")
    private T data;

    public static <T> Result<T> success(T d) {
        return new Result<>(200, d, "success");
    }

    public static <T> Result<T> success(String msg, T d) {
        return new Result<>(200, d, msg);
    }

    public static <T> Result<T> success() {
        return new Result<>(200, null, "success");
    }

    public static <T> Result<T> error(int exception, T o, String msg) {
        return new Result<>(exception, o, msg);
    }

    public static <T> Result<T> error(T d) {
        return new Result<>(201, d, "error");
    }

    public static <T> Result<T> error(String msg) {
        return new Result<>(201, null, msg);
    }

    public Result(Integer code, T data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
