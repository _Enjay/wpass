package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@TableName(value = "e_auto_meter_read")
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EAutoMeterRead {
    /**
     * id
     */
     @TableId(value = "id", type = IdType.INPUT)
    private Long id;


    /**
     * 自动抄表数据频率
     */
    @TableField(value = "meter_read_num")
    private Integer meterReadNum;

    /**
     * 提前通知收房租天数
     */
    @TableField(value = "advance_days")
    private Integer advanceDays;

    /**
     * 操作人
     */
    @TableField(value = "opreation")
    private String opreation;

    /**
     * 操作时间
     */
    @TableField(value = "opreation_time")
    private Date opreationTime;

    /**
     * 主id
     */
    @TableField(value = "m_acc_id")
    private String mAccId;




}