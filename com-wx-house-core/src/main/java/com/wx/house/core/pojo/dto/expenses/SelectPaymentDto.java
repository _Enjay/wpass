package com.wx.house.core.pojo.dto.expenses;

import com.wx.house.core.pojo.base.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.dto.expenses
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 15:14
 * @Version V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class SelectPaymentDto extends Page {
    @ApiModelProperty(value = "支付方式(字典查支付方式)", example = "支付方式", required = false)
    private String dicId;
}
