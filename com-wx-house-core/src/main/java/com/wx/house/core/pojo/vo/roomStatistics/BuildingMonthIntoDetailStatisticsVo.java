package com.wx.house.core.pojo.vo.roomStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.NumberFormat;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/17 下午 03:39
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildingMonthIntoDetailStatisticsVo {
@ApiModelProperty("公寓名称")
private String buildingName;
@ApiModelProperty("房间数量")
private Integer roomNumber;
@ApiModelProperty("入住数量")
private Integer usedNumber;
@ApiModelProperty("入住率")
private String usedPercentage;

	public String getUsedPercentage() {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMaximumFractionDigits(2);
		if (roomNumber != 0 && usedNumber != 0)
			return numberFormat.format((float) usedNumber / (float) roomNumber * 100) + "%";
		return "0%";
	}
}
