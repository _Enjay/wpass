package com.wx.house.core.mapper.permission;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdminServices;
import com.wx.house.core.pojo.vo.AdminServiceDetailVo;
import com.wx.house.core.pojo.vo.AdminServiceVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 04:15
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminServicesMapper extends BaseMapper<SysAdminServices> {
List<AdminServiceVo> selectAdminServiceVo(@Param("keyword") String keyword, @Param("state") Integer state);

AdminServiceDetailVo selectAdminServiceDetailVo(@Param("maccId") String maccId);

int updateById(@Param("updated")SysAdminServices updated,@Param("id")String id);
int updateByMaccId(@Param("updated")SysAdminServices updated,@Param("maccId")String maccId);



}