package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:05
 * @description：${description}
 * @version: ：V$version
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_admin_withdrawal")
public class SysAdminWithdrawal {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 申请账户
 */
@TableField(value = "account")
@ApiModelProperty(value = "申请账户")
private String account;

/**
 * 申请提现金额
 */
@TableField(value = "withdrawal_balance")
@ApiModelProperty(value = "申请提现金额")
private BigDecimal withdrawalBalance;

/**
 * 提现前余额
 */
@TableField(value = "before_withdrawal")
@ApiModelProperty(value = "提现前余额")
private BigDecimal beforeWithdrawal;

/**
 * 提现后余额
 */
@TableField(value = "after_withdrawal")
@ApiModelProperty(value = "提现后余额")
private BigDecimal afterWithdrawal;

/**
 * 提现类型: 0银行卡 1支付 2微信 3线下 4...
 */
@TableField(value = "withdrawal_type")
@ApiModelProperty(value = "提现类型: 0银行卡 1支付 2微信 3线下 4...")
private Integer withdrawalType;

/**
 * 机构     支付宝/微信/中国工商银行
 */
@TableField(value = "org_name")
@ApiModelProperty(value = "机构     支付宝/微信/中国工商银行")
private String orgName;

/**
 * 附加信息     渝北区金开大道xx支行
 */
@TableField(value = "org_attach")
@ApiModelProperty(value = "附加信息     渝北区金开大道xx支行")
private String orgAttach;

/**
 * 机构账户     支付宝账户/银行卡账号。。。
 */
@TableField(value = "org_account")
@ApiModelProperty(value = "机构账户     支付宝账户/银行卡账号。。。")
private String orgAccount;

/**
 * 开卡人名字
 */
@TableField(value = "org_card_user_name")
@ApiModelProperty(value = "开卡人名字")
private String orgCardUserName;
/**
 * 开卡人电话
 */
@TableField(value = "org_card_user_phone")
@ApiModelProperty(value = "开卡人电话")
private String orgCardUserPhone;

/**
 * 审核备注
 */
@TableField(value = "remark")
@ApiModelProperty(value = "审核备注")
private String remark;

/**
 * 提现状态: 0申请提现中 1提现成功 2提现失败
 */
@TableField(value = "state")
@ApiModelProperty(value = "提现状态: 0申请提现中 1提现成功 2提现失败")
private Integer state;

/**
 * 审核时间
 */
@TableField(value = "update_time")
@ApiModelProperty(value = "审核时间")
private Date update_time;

/**
 * 创建时间
 */
@TableField(value = "create_time")
@ApiModelProperty(value = "创建时间")
private Date create_time;

/**
 * 已删除  0否 1已删除
 */
@TableField(value = "deleted")
@ApiModelProperty(value = "已删除  0否 1已删除")
private Integer deleted;

/**
 * 主账号ID
 */
@TableField(value = "macc_id")
@ApiModelProperty(value = "主账号ID")
private String maccId;

public static final String COL_ID = "id";

public static final String COL_ACCOUNT = "account";

public static final String COL_WITHDRAWAL_BLANCE = "withdrawal_blance";

public static final String COL_BEFORE_WITHDRAWAL = "before_withdrawal";

public static final String COL_AFTER_WITHDRAWAL = "after_withdrawal";

public static final String COL_WITHDRAWAL_TYPE = "withdrawal_type";

public static final String COL_ORG_NAME = "org_name";

public static final String COL_ORG_ATTACH = "org_attach";

public static final String COL_ORG_ACCOUNT = "org_account";

public static final String COL_REMARK = "remark";

public static final String COL_STATE = "state";

public static final String COL_UPDATTIME = "updatTime";

public static final String COL_CREATETIME = "createTime";

public static final String COL_DELETED = "deleted";
}