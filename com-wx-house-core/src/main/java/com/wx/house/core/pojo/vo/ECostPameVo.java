package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ECostPameVo {

	/**
	 * 合同id
	 */
	@ApiModelProperty(value = "合同id")
	private String contrId;
	/**
	 * 合同编号
	 */
	@ApiModelProperty(value = "合同编号")
	private String contrNumber;

	/**
	 * 合同名称
	 */
	@ApiModelProperty(value = "合同名称")
	private String contrName;
	
	
	/**
	 * 待处理费用创建时间
	 */
	@ApiModelProperty(value = "待处理费用创建时间")
	private Date creationTime;

	/**
	 * 收费月份
	 */
	@TableField(value = "charge_month")
	@ApiModelProperty(value = "收费月份")
	private String chargeMonth;


	/***
	 * 主账号id
	 */
	@TableField(value = "m_acc_id")
	private String mAccId;

	/**
	 * 费用类型: 0收费 1支出
	 */
	@ApiModelProperty("cost_type")
	private Integer costType;


	/**
	 * 费用类型: 0收费 1支出
	 */
	@ApiModelProperty("总计费用")
	private BigDecimal total;
	
	/**
	 * 待处理费用详情id
	 */
	@ApiModelProperty(value = "id")
	private String id;

	/**
	 * 待处理费用id
	 */
	@ApiModelProperty(value = "eCostId")
	private String eCostId;


	/**
	 * 收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他
	 */
	@ApiModelProperty("收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
	private Integer chargeType;

	/**
	 * 应收金额
	 */
	@ApiModelProperty("应收金额")
	private BigDecimal receiveMoney;

	/**
	 * 实收金额
	 */
	@ApiModelProperty("实收金额")
	private BigDecimal netMoney;

	/**
	 * 处理状态（0待处理 1已处理）
	 */
	@ApiModelProperty("处理状态（0待处理 1已处理）")
	private Integer costState;

	/**
	 * 备注
	 */
	@ApiModelProperty("备注")
	private String remarks;
	
	
}
