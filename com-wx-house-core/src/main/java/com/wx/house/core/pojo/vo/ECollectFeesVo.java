package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ECollectFeesVo {
	@ApiModelProperty(value = "收支类型 0收费  1退费")
	private Integer szType;
	@ApiModelProperty(value = "费用类型")
	private Integer feesType;
	@ApiModelProperty(value = "支付方式")
	private Integer paymentMethod;
	@ApiModelProperty(value = "收费年月")
	private String feeMonth;
	@ApiModelProperty(value = "应收费用")
	private BigDecimal receivable;
	@ApiModelProperty(value = "实收费用")
	private BigDecimal actual;
	@ApiModelProperty(value = "创建时间")
	private Date createTime;
	@ApiModelProperty(value = "收费时间")
	private Date chargeTime;

	@ApiModelProperty(value = "合同编号")
	private String contrNumber;

	
	@ApiModelProperty(value = "租户姓名")
	private String tenantName;
	@ApiModelProperty(value = "手机号")
	private String tenantPhone;
	@ApiModelProperty(value = "证件号")
	private String tenantCertiNumber;
	
	@ApiModelProperty(value = "备注")
	private String remarks;
	@ApiModelProperty(value = "房间号")
	private String roomNumber;
	@ApiModelProperty(value = "公寓名称")
	private String buildName;
}
