package com.wx.house.core.pojo.base;

import io.swagger.annotations.ApiModelProperty;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/29 上午 09:43
 * @description：
 * @version: ：V
 */
public class BasePage extends Page {
@ApiModelProperty(value = "查询的关键字", name = "keyWord", example = "模糊匹配的内容", required = false)
private String keyWord;

/**
 * @Description 状态
 */
@ApiModelProperty(value = "状态", name = "state", example = "0", required = false)
private Integer state;
}
