package com.wx.house.core.mapper.dic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.DicAreas;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface DicAreasMapper extends BaseMapper<DicAreas> {
    /**
     * 遍历出地区
     *
     * @param parentId
     * @return
     */
    List<DicAreas> findAreasByParentId(@Param("parentId") Integer parentId);
}