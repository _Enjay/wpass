package com.wx.house.core.pojo.dto.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 04:02
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminRealDto {
@ApiModelProperty("用户id")
private String adminId;
@ApiModelProperty("用户实名状态: 2未通过 3通过")
private Integer state;
@ApiModelProperty("备注")
private String remark;
}
