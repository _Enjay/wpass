package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/15 上午 09:41
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantContractDetailVo {
@ApiModelProperty(value = "租户id")
private String id;
@ApiModelProperty(value = "租户名称")
private String tenantName;

//租户有多个合同，每个合同有不同的期数，合同的起止时间和缴费情况
@ApiModelProperty(value = "合同详情")
private List<ContractPlanVo> contractPlanVo;

}
