package com.wx.house.core.mapper.dic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.DicWebDeploy;
import com.wx.house.core.pojo.po.EAutoMeterRead;
import com.wx.house.core.pojo.vo.DicWebDeployVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version:   ：V$version
 */
@Mapper
public interface DicWebDeployMapper extends BaseMapper<DicWebDeploy> {

    List<DicWebDeployVo> findWebDeployByMaccId(@Param("maccId")String maccId);


}