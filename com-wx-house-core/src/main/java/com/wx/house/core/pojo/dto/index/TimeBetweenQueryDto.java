package com.wx.house.core.pojo.dto.index;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 10:23
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeBetweenQueryDto {
@ApiModelProperty(value = "开始时间", required = false, example = "2019-10-10 00:00:00")
private Date startTime;
@ApiModelProperty(value = "结束时间", required = false, example = "2020-10-10 00:00:00")
private Date endTime;
}
