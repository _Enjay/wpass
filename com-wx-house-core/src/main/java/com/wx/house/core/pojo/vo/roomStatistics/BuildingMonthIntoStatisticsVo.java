package com.wx.house.core.pojo.vo.roomStatistics;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/17 下午 03:39
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildingMonthIntoStatisticsVo {
@ApiModelProperty("时间")
private String time;
private List<BuildingMonthIntoDetailStatisticsVo> buildingMonthIntoDetailStatisticsVos = new ArrayList<>();

}
