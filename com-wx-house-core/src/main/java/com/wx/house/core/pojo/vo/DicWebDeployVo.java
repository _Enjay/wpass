package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.wx.house.core.pojo.po.DicWebDeploy;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.vo
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/19 11:24
 * @Version V1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DicWebDeployVo extends DicWebDeploy {
    /**
     * 文件地址
     */
    @ApiModelProperty(value = "logo访问路径")
    private String url;
}
