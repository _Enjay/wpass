package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.sf.jsqlparser.expression.StringValue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 上午 10:56
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TUserContractVo {
@ApiModelProperty("合同id")
private String contractId;

@ApiModelProperty("合同编号")
private String contractNumber;

@ApiModelProperty("合同名称")
private String contractName;

@ApiModelProperty("公寓名称")
private String buildingName;

@ApiModelProperty("公寓地址")
private String buildingAddress;

@ApiModelProperty("楼层号")
private String floor;

@ApiModelProperty("房间号")
private String door;

@ApiModelProperty("用户名")
private String userName;

@ApiModelProperty("用户id")
private String userId;

@ApiModelProperty("用户手机号")
private String userPhone;

@ApiModelProperty("证件类型")
private String idCardType;

@ApiModelProperty("身份证")
private String idCardNumber;

@ApiModelProperty("备注")
private String remark;

@ApiModelProperty("合同开始时间")
private Date startTime;
@ApiModelProperty("合同结束时间")
private Date endTime;

@ApiModelProperty("合同押金")
private BigDecimal deposit;

@ApiModelProperty("租金类型:7月付 8季付 9半年付 10年付")
private String rentType;

public String getRentType() {
	// 7月付 8季付 9半年付 10年付
			return "7".equals(rentType) ? "月付"
					: "8".equals(rentType) ? "季付"
							: "9".equals(rentType) ? "半年付" 
									: "10".equals(rentType) ? "年付" :"未知";
}

@ApiModelProperty("物管费")
private Integer propertyManager;

@ApiModelProperty("预付房租")
private String prepayment;

@ApiModelProperty("时间: 4 月 5 季度 6 年 ")
private String contractTermUnit;

@ApiModelProperty("房租价格")
private String chargeMoney;


@ApiModelProperty("正面图片")
private String idCard;

@ApiModelProperty("反面图片")
private String idCard_r;

@ApiModelProperty("合同附件列表")
private List<String> contractFileList;
}
