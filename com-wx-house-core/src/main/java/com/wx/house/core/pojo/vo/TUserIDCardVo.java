package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/25 上午 09:50
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TUserIDCardVo {
@ApiModelProperty("姓名")
private String name;

@ApiModelProperty("身份证号码")
private String idCardNumber;

@ApiModelProperty("正面图片")
private String idCard;

@ApiModelProperty("反面图片")
private String idCard_r;
}
