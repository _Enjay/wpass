package com.wx.house.core.pojo.base;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/06/13 下午 02:28
 * @description：分页请求参数集合
 * @version: ：V
 */
@EqualsAndHashCode(callSuper = true)
@ApiModel
@Data
@NoArgsConstructor
public class DatePage extends Page {
public Integer page() {
	return this.getPage() != null ? this.getPage() : 1;
}

public Integer pageSize() {
	return this.getPageSize() != null ? this.getPageSize() : 10;
}


@ApiModelProperty(value = "开始时间", name = "startTime", example = "2010-07-19 11:42:36", required = false)
private String startTime;

@ApiModelProperty(value = "结束时间", name = "endTime", example = "2029-07-29 11:42:36", required = false)
private String endTime;

public Date getStartTime() {
	
	if (StringUtils.isEmpty(this.startTime)) {
		return null;
	}
	
	Calendar todayStart = Calendar.getInstance();
	try {
		//2018-10-17 10:17
		if (this.startTime.trim().length() == 16) {
			todayStart.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(this.startTime));
		} else {
			todayStart.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.startTime));
		}
	} catch (ParseException e) {
		e.printStackTrace();
		throw new RuntimeException("时间格式异常:" + "yyyy-MM-dd HH:mm:ss 或者 yyyy-MM-dd HH:mm ");
	}
	return todayStart.getTime();
}

public Date getEndTime() {
	if (StringUtils.isEmpty(this.endTime)) return null;
	Calendar todayEnd = Calendar.getInstance();
	try {
		if (this.startTime.trim().length() == 16) {
			todayEnd.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(this.endTime));
		} else {
			todayEnd.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(this.endTime));
		}
	} catch (ParseException e) {
		e.printStackTrace();
		throw new RuntimeException("时间格式异常:" + "yyyy-MM-dd HH:mm:ss 或者 yyyy-MM-dd HH:mm ");
	}
	return todayEnd.getTime();
}

public static void main(String[] args) {
	String s = "2018-10-17 10:17";
	System.out.println(s.trim().length());
}
}
