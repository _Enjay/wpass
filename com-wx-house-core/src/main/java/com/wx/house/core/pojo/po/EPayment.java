package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@TableName(value = "e_payment")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class EPayment {
    /**
     * 支付方式id
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 支付id
     */
    @TableField(value = "payment_id")
    private String paymentId;

    /**
     * 支付密钥
     */
    @TableField(value = "payment_secretkey")
    private String paymentSecretkey;
    
    /**
     * 商户id
     */
    @TableField(value = "merchant_id")
    private String merchantId;

    /**
     * 操作人
     */
    @TableField(value = "operation")
    private String operation;

    /**
     * 操作时间
     */
    @TableField(value = "operation_time")
    private Date operationTime;

    /**
     * 使用状态 1 使用 2 停用
     */
    @TableField(value = "logical_state")
    private Integer logicalState;

    /**
     * 字典id
     */
    @TableField(value = "dic_id")
    private Long dicId;

}