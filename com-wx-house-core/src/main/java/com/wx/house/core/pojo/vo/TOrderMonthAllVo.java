package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TOrderMonthAllVo {
	
	@ApiModelProperty("缴费月份")
	private String chargeMonth;
	
	private List<BammTorderTabelVo> bammTorderTabelVos ;
}
