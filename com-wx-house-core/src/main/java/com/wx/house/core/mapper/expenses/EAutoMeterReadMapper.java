package com.wx.house.core.mapper.expenses;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.EAutoMeterRead;

import java.util.List;

public interface EAutoMeterReadMapper extends BaseMapper<EAutoMeterRead> {
List<EAutoMeterRead> selectAll();
    List<EAutoMeterRead> selectAllByMAccId(@Param("mAccId")String mAccId);

}