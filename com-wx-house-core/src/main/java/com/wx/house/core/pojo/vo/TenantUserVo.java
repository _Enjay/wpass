package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:45
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantUserVo {

@ApiModelProperty(value = "id", example = "13S1EF3S21DFW65E1FWE1")
private String id;


@ApiModelProperty(value = "用户名称", example = "周小明")
private String name;


@ApiModelProperty(value = "用户昵称", example = "小明")
private String nickName;

@ApiModelProperty(value = "用户头像", example = "192.168.1.200:7741/1573459710786607.jpg")
private String avatar;


@ApiModelProperty(value = "手机号", example = "13996996969")
private String phoneNumber;


@ApiModelProperty(value = "证件号码", example = "500242199911445867")
private String idCardNumber;

@ApiModelProperty(value = "证件类型: 1身份证", example = "1")
private Integer idCardType;


@TableField(value = "state")
@ApiModelProperty(value = "状态: 0正常 1禁用", example = "0")
private Integer state;


@ApiModelProperty(value = "登陆ip", example = "219.153.47.208")
private String loginIp;

@ApiModelProperty(value = "最后一次登陆时间", example = "2019-01-01 15:33:27")
private Date loginTime;

private List<AttaUpload> attaUploadList;
}
