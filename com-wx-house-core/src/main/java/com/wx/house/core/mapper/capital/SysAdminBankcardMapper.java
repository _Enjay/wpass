package com.wx.house.core.mapper.capital;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdminBankcard;
import com.wx.house.core.pojo.vo.SysAdminBankcardVo;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 上午 10:15
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminBankcardMapper extends BaseMapper<SysAdminBankcard> {
SysAdminBankcard selectById(@Param("id") String id);

List<SysAdminBankcard> selectAll(@Param("maccId") String maccId, String keyword);

List<SysAdminBankcardVo> selectAllVo(@Param("maccId") String maccId);

int insertSelective(SysAdminBankcard sysAdminBankcard);


int updateById(@Param("updated") SysAdminBankcard updated, @Param("id") String id);

int updatePreferredByMaccId(@Param("updatedPreferred") Integer updatedPreferred, @Param("maccId") String maccId);


}