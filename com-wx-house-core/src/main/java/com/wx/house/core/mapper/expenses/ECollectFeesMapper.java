package com.wx.house.core.mapper.expenses;

import com.wx.house.core.pojo.vo.ECollectFeesDetailVo;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.vo.ECollectFeesVo;

import org.apache.ibatis.annotations.Mapper;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface ECollectFeesMapper extends BaseMapper<ECollectFees> {
    int insertList(@Param("list") List<ECollectFees> list);

    int deleteById(@Param("id") String id);

    /***
     * 查询收费
     * @param maccid
     * @param contrNumber
     * @param tenantKeyword
     * @param feesType
     * @return
     */
    List<ECollectFeesVo> selectECollectFees(
            @Param("maccid") String maccid,
            @Param("tenantKeyword") String tenantKeyword,
            @Param("paymentMethod") String paymentMethod,
            @Param("szType") String szType,
            @Param("feesType") String feesType,
            @Param("month") List<String> monthlist,
            @Param("startTime") Date startTime,
            @Param("endTime") Date endTime,
            @Param("buildingId") String buildingId,
            @Param("floorId") String floorId,
            @Param("doorNumber") String doorNumber);

    List<ECollectFees> selectExpense(@Param("mouth") String mouth, @Param("userId") String userId);

    /**
     * 查询订单详情
     *
     * @param mouth
     * @param userId
     * @return
     */
    List<ECollectFeesDetailVo> selectExpenseDetail(@Param("mouth") String mouth, @Param("userId") String userId);

    /***
     * 收费，退费时间集合
     * @param szType
     * @return
     */
    List<ECollectFees> selectFeeMonth(@Param("szType") String szType);

    List<ECollectFees> selectByMAccId(@Param("mAccId") String mAccId);

}