package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.validator.internal.IgnoreForbiddenApisErrors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "sys_admin")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysAdmin implements Serializable {
/**
 * 管理员id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "管理员id")
private String id;

/**
 * 操作人姓名
 */
@TableField(value = "real_name")
@ApiModelProperty(value = "操作人姓名")
private String realName;

/**
 * 管理员账号
 */
@TableField(value = "login_name")
@ApiModelProperty(value = "管理员账号")
private String loginName;
/**
 * 管理员密码
 */
@TableField(value = "login_pwd")
@ApiModelProperty(value = "管理员密码")
@JsonIgnore
private String loginPwd;

/**
 * 0超级管理员  1主账号  2子账号
 */
@TableField(value = "account_type")
@ApiModelProperty(value = "0超级管理员  1主账号  2子账号")
private Integer accountType;

/**
 * 主账号id
 */
@TableField(value = "m_acc_id")
@ApiModelProperty(value = "主账号id")
private String mAccId;

/**
 * 等级
 */
@TableField(value = "level")
@ApiModelProperty(value = "等级")
private String level;

/**
 * 手机号
 */
@TableField(value = "phone")
@ApiModelProperty(value = "手机号")
private Long phone;

/**
 * key
 */
@TableField(value = "safe_key")
@ApiModelProperty(value = "key")
@JsonIgnore
private Integer safeKey;

/**
 * 创建时间
 */
@TableField(value = "create_time")
@ApiModelProperty(value = "创建时间")
private Date createTime;

/**
 * 状态: 0正常,1禁用
 */
@TableField(value = "state")
@ApiModelProperty(value = "状态: 0正常,1禁用")
private Integer state;

/**
 * 删除状态:0正常,1禁用
 */
@TableField(value = "del_type")
@ApiModelProperty(value = "删除状态:0正常,1禁用")
private Integer delType;

/**
 * 登陆ip
 */
@TableField(value = "login_ip")
@ApiModelProperty(value = "登陆ip")
private String loginIp;

/**
 * 最后一次登陆时间
 */
@TableField(value = "login_time")
@ApiModelProperty(value = "最后一次登陆时间")
private Date loginTime;

/**
 * 操作人
 */
@TableField(value = "operation")
@ApiModelProperty(value = "操作人")
private String operation;

/**
 * 操作id
 */
@TableField(value = "operation_time")
@ApiModelProperty(value = "操作id")
private Date operationTime;

//private List<List<SysMenus>> sysMenus=new ArrayList<>();

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_REAL_NAME = "real_name";

public static final String COL_LOGIN_NAME = "login_name";

public static final String COL_LOGIN_PWD = "login_pwd";

public static final String COL_ACCOUNT_TYPE = "account_type";

public static final String COL_M_ACC_ID = "m_acc_id";

public static final String COL_LEVEL = "level";

public static final String COL_PHONE = "phone";

public static final String COL_SAFE_KEY = "safe_key";

public static final String COL_CREATE_TIME = "create_time";

public static final String COL_STATE = "state";

public static final String COL_DEL_TYPE = "del_type";

public static final String COL_LOGIN_IP = "login_ip";

public static final String COL_LOGIN_TIME = "login_time";

public static final String COL_OPERATION = "operation";

public static final String COL_OPERATION_TIME = "operation_time";
}