package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:39
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "t_user")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TUser implements Serializable {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 用户名称
 */
@TableField(value = "name")
@ApiModelProperty(value = "用户名称")
private String name;

/**
 * 用户昵称
 */
@TableField(value = "nick_name")
@ApiModelProperty(value = "用户昵称")
private String nickName;

/**
 * 用户头像
 */
@TableField(value = "avatar")
@ApiModelProperty(value = "用户头像")
private String avatar;

/**
 * 用户密码
 */
@TableField(value = "password")
@ApiModelProperty(value = "用户密码")
private String password;

@javax.persistence.Transient
private Boolean hasPassword;

public Boolean getHasPassword() {
	return !StringUtils.isEmpty(this.password);
}

/**
 * 盐
 */
@TableField(value = "safekey")
@ApiModelProperty(value = "盐")
@JsonIgnore
private String safekey;

/**
 * 支付密码
 */
@TableField(value = "pay_password")
@ApiModelProperty(value = "支付密码")
@JsonIgnore
private String payPassword;

/**
 * 手机号
 */
@TableField(value = "phone_number")
@ApiModelProperty(value = "手机号")
private String phoneNumber;

/**
 * 证件号码
 */
@TableField(value = "id_card_number")
@ApiModelProperty(value = "证件号码")
private String idCardNumber;

/**
 * 证件类型
 */
@TableField(value = "id_card_type")
@ApiModelProperty(value = "证件类型")
private Integer idCardType;

/**
 * 状态: 0正常,1禁用
 */
@TableField(value = "state")
@ApiModelProperty(value = "状态: 0正常,1禁用")
private Integer state;

/**
 * 删除状态:0正常,1删除
 */
@TableField(value = "del_type")
@ApiModelProperty(value = "删除状态:0正常,1删除")
@JsonIgnore
private Integer delType;

/**
 * 登陆ip
 */
@TableField(value = "login_ip")
@ApiModelProperty(value = "登陆ip")
private String loginIp;

/**
 * 最后一次登陆时间
 */
@TableField(value = "login_time")
@ApiModelProperty(value = "最后一次登陆时间")
private Date loginTime;

/**
 * 操作人
 */
@TableField(value = "operation")
@ApiModelProperty(value = "操作人")
@JsonIgnore
private String operation;

/**
 * 操作id
 */
@TableField(value = "operation_time")
@ApiModelProperty(value = "操作id")
@JsonIgnore
private Date operationTime;

@TableField(value = "主id")
@ApiModelProperty(value = "主id")
@JsonIgnore
private String maccid;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_NAME = "name";

public static final String COL_NICK_NAME = "nick_name";

public static final String COL_AVATAR = "avatar";

public static final String COL_PASSWORD = "password";

public static final String COL_SAFEKEY = "safekey";

public static final String COL_PAY_PASSWORD = "pay_password";

public static final String COL_PHONE_NUMBER = "phone_number";

public static final String COL_ID_CARD_NUMBER = "id_card_number";

public static final String COL_ID_CARD_TYPE = "id_card_type";

public static final String COL_STATE = "state";

public static final String COL_DEL_TYPE = "del_type";

public static final String COL_LOGIN_IP = "login_ip";

public static final String COL_LOGIN_TIME = "login_time";

public static final String COL_OPERATION = "operation";

public static final String COL_OPERATION_TIME = "operation_time";
}