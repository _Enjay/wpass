package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:06
 * @description：
 * @version: ：V
 */
@Getter
@AllArgsConstructor
public enum TenantSignTypeEnum {
	PASSWORD(1, "密码登陆"),
	VALID_CODE(2, "手机验证码登陆");
public Integer type;
public String description;
}
