package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "e_collect_fees")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ECollectFees implements Serializable {
@ApiModelProperty(value = "id")
private String id;

/**
 * 收支类型 收费  退费
 */
@TableField(value = "sz_type")
@ApiModelProperty(value = "收支类型 收费  退费")
private Integer szType;

/**
 * 费用类型
 */
@TableField(value = "fees_type")
@ApiModelProperty(value = "收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
private Integer feesType;

/**
 * 支付方式
 */
@TableField(value = "payment_method")
@ApiModelProperty(value = "支付方式")
private Integer paymentMethod;
/**
 * 合同id
 */
@TableField(value = "m_acc_id")
@ApiModelProperty(value = "合同id")
private String mAccId;
/**
 * 合同id
 */
@TableField(value = "contract_id")
@ApiModelProperty(value = "合同id")
private String contractId;

/**
 * 收费年月
 */
@TableField(value = "fee_month")
@ApiModelProperty(value = "收费年月")
private String feeMonth;

/**
 * 应收费用
 */
@TableField(value = "receivable")
@ApiModelProperty(value = "应收费用")
private BigDecimal receivable;

/**
 * 实收费用
 */
@TableField(value = "actual")
@ApiModelProperty(value = "实收费用")
private BigDecimal actual;

/**
 * 创建时间
 */
@TableField(value = "create_time")
@ApiModelProperty(value = "创建时间")
private Date createTime;

/**
 * 收费时间
 */
@TableField(value = "charge_time")
@ApiModelProperty(value = "收费时间")
private Date chargeTime;

/**
 * 备注
 */
@TableField(value = "remarks")
@ApiModelProperty(value = "备注")
private String remarks;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_SZ_TYPE = "sz_type";

public static final String COL_FEES_TYPE = "fees_type";

public static final String COL_CONTRACT_ID = "contract_id";

public static final String COL_FEE_MONTH = "fee_month";

public static final String COL_RECEIVABLE = "receivable";

public static final String COL_ACTUAL = "actual";

public static final String COL_CREATE_TIME = "create_time";

public static final String COL_CHARGE_TIME = "charge_time";
}