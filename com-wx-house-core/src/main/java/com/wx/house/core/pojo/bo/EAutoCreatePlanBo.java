package com.wx.house.core.pojo.bo;


import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class EAutoCreatePlanBo {
	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.INPUT)
	@ApiModelProperty(value = "id")
	private String id;

	/**
	 * 主id
	 */
	@TableField(value = "m_acc_id")
	@ApiModelProperty(value = "主id")
	private String mAccId;

	/**
	 * 合同id
	 */
	@TableField(value = "contract_id")
	@ApiModelProperty(value = "合同id")
	private String contractId;

	/**
	 * 计划收费时间
	 */
	@TableField(value = "plan_charge_time")
	@ApiModelProperty(value = "计划收费时间")
	private Date planChargeTime;

	/**
	 * 是否已经生成:   0已经生成   1待生成
	 */
	@TableField(value = "created")
	@ApiModelProperty(value = "是否已经生成:   0已经生成   1待生成")
	private Integer created;

	/**
	 * 缴费状态:  0已缴费  1待缴费  2未缴清
	 */
	@TableField(value = "payed")
	@ApiModelProperty(value = "缴费状态:  0已缴费  1待缴费  2未缴清")
	private Integer payed;

	/**
	 * 缴费时间
	 */
	@TableField(value = "pay_time")
	@ApiModelProperty(value = "缴费时间")
	private Date payTime;


	@TableField(value = "pay_type")
	@ApiModelProperty(value = "待缴费类型: 1房租 2水费 3电费 4气费 5物管费   [房租费固定,所以可以分批缴费] [水电费根据抄表数据来生成]")
	private String payType;

	@TableField(value = "rent_price")
	@ApiModelProperty(value = "房租价格")
	private BigDecimal rentPrice;
	@TableField(value = "rent_actual")
	@ApiModelProperty(value = "收到房租")
	private BigDecimal rentActual;

	@TableField(value = "property_manager_price")
	@ApiModelProperty(value = "物管费价格")
	private BigDecimal propertyManagerPrice;
	@TableField(value = "property_manager_actual")
	@ApiModelProperty(value = "收到物管费")
	private BigDecimal propertyManagerActual;

	@TableField(value = "routine_price")
	@ApiModelProperty(value = "水电气费用")
	private BigDecimal routinePrice;
	@TableField(value = "routine_actual")
	@ApiModelProperty(value = "收到水电气费用")
	private BigDecimal routineActual;
	
	@TableField(value = "contr_number")
	@ApiModelProperty(value = "收到水电气费用")
	private String contrNumber;
	@TableField(value = "contr_name")
	@ApiModelProperty(value = "收到水电气费用")
	private String contrName;
}