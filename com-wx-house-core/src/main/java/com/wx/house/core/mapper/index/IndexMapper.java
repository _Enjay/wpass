package com.wx.house.core.mapper.index;

import com.wx.house.core.pojo.vo.ContractTimeVo;
import com.wx.house.core.pojo.vo.checkInStatistics.CheckInStatisticsVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsManager.ExpensesAndReceiptsManagerVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics.ExpensesAndReceiptsStatisticsVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics.ExpensesStatisticsDetailsVo;
import com.wx.house.core.pojo.vo.houseManager.HouseManagerVo;
import com.wx.house.core.pojo.vo.leaseManager.LeaseManagerVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 09:32
 * @description：
 * @version: ：V
 */
public interface IndexMapper {

    ExpensesAndReceiptsManagerVo selectExpensesAndReceiptsManagerVo(@Param("maccids") List<String> maccids, @Param("startTime") Date startTime, @Param("endTime") Date endTime);

    LeaseManagerVo selectLeaseManagerVo(@Param("maccids") List<String> maccids);

    List<ExpensesStatisticsDetailsVo> selectExpensesStatisticsDetailsVo(@Param("maccids") List<String> maccids, @Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("szType") Integer szType);

    HouseManagerVo selectHouseManagerVo(@Param("maccids") List<String> maccids);

    CheckInStatisticsVo selectCheckInStatisticsVo(@Param("maccid") String maccid);

    List<ContractTimeVo> selectAllContractTimeVo(@Param("maccids") List<String> maccids, @Param("timeList") List<String> timeList);

    List<String> selectMaccIds();
}
