package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 下午 04:45
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum ChargeDirectionEnum {
	CHARGE(0, "收费"),
	REFUND(1, "退费");
private int code;
private String msg;
}
