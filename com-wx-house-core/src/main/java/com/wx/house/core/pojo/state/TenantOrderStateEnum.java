package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 11:31
 * @description：.
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum TenantOrderStateEnum {
	NORMAL(0, "待付款"),
	FREEZE(1, "已付款");
public Integer state;
public String description;
}
