package com.wx.house.core.mapper.expenses;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.dto.expenses.SelectPaymentDto;
import com.wx.house.core.pojo.po.EPayment;
import com.wx.house.core.pojo.vo.EPaymentVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EPaymentMapper extends BaseMapper<EPayment> {

    /**
     * 查看支付方式列表
     * @param mAccId
     * @return
     */
    List<EPaymentVo> selectPayMentList(@Param("pay") SelectPaymentDto selectPaymentDto);


}