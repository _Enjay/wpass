package com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 11:15
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpensesAndReceiptsStatisticsVo {

@ApiModelProperty("总计收费")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal totalExpenses;

public BigDecimal getTotalExpenses() {
	try {
		return this.expensesStatisticsDetailsVoList
				       .stream()
				       .map(ExpensesStatisticsDetailsVo::getMoney)
				       .reduce(BigDecimal.ZERO, BigDecimal::add);
		
	} catch (Exception ignored) {
	}
	return new BigDecimal(0);
}

@ApiModelProperty("总计退费")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal totalReceipts;

public BigDecimal getTotalReceipts() {
	try {
		return this.receiptsStatisticsDetailsVoList
				       .stream()
				       .map(ExpensesStatisticsDetailsVo::getMoney)
				       .reduce(BigDecimal.ZERO, BigDecimal::add);
	} catch (Exception ignored) {
	}
	return new BigDecimal(0);
}


@ApiModelProperty("总计收费详情")
private List<ExpensesStatisticsDetailsVo> expensesStatisticsDetailsVoList = new ArrayList<>();

@ApiModelProperty("总计退费详情")
private List<ExpensesStatisticsDetailsVo> receiptsStatisticsDetailsVoList = new ArrayList<>();

}
