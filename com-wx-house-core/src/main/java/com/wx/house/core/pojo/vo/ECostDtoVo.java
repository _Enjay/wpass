package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class ECostDtoVo {
	private List<ECostVo> List;
	@ApiModelProperty("应收金额")
	private BigDecimal receiveWater;
	@ApiModelProperty("应收水费")
	private BigDecimal receiveElectric;
	@ApiModelProperty("应收电费")
	private BigDecimal receiveGas;
}
