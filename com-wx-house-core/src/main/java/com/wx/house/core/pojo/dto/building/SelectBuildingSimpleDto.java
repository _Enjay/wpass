package com.wx.house.core.pojo.dto.building;

import com.wx.house.core.pojo.base.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 03:20
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class SelectBuildingSimpleDto {
@ApiModelProperty(value = "建筑名称", example = "v客部落", required = false)
private String buildingName;
@ApiModelProperty(value = "省份ID", example = "110000", required = false)
private Integer province;
@ApiModelProperty(value = "城市ID", example = "110100", required = false)
private Integer city;
@ApiModelProperty(value = "区县ID", example = "110101", required = false)
private Integer county;
}
