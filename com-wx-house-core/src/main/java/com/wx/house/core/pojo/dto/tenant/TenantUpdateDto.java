package com.wx.house.core.pojo.dto.tenant;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:45
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantUpdateDto {

@ApiModelProperty(value = "id", example = "13S1EF3S21DFW65E1FWE1", required = true)
@NotBlank
private String id;


@ApiModelProperty(value = "用户名称", example = "周小明", required = false)
private String name;


@ApiModelProperty(value = "用户昵称", example = "小明", required = false)
private String nickName;

@ApiModelProperty(value = "用户头像", example = "192.168.1.200:7741/1573459710786607.jpg", required = false)
private String avatar;

@ApiModelProperty(value = "手机号", example = "13996996969", required = false)
private String phoneNumber;

@ApiModelProperty(value = "证件号码", example = "500242199911445867", required = false)
private String idCardNumber;

@ApiModelProperty(value = "证件类型: 1身份证", example = "1", required = false)
private Integer idCardType;

@TableField(value = "state")
@ApiModelProperty(value = "状态: 0正常 1禁用", example = "0", required = false)
private Integer state;

@TableField(value = "del_type")
@ApiModelProperty(value = "删除状态:0正常,1删除", example = "0", required = false)
private Integer delType;

@ApiModelProperty(value = "附件列表", example = "[11111,22222,33333]", required = true)
private List<String> attachList;
}
