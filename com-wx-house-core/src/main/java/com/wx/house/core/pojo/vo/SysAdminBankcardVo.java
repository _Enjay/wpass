package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:44
 * @description：${description}
 * @version: ：V$version
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_admin_bankcard")
public class SysAdminBankcardVo {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 主id
 */
@TableField(value = "macc_id")
@ApiModelProperty(value = "主id")
private String maccId;

/**
 * 卡类型: 1银行卡 2支付 3微信 4....
 */
@TableField(value = "card_type")
@ApiModelProperty(value = "卡类型: 1银行卡 2支付 3微信 4....")
private Integer cardType;

/**
 * 发卡行名称
 */
@TableField(value = "card_org_name")
@ApiModelProperty(value = "发卡行名称")
private String cardOrgName;

/**
 * 发卡行附加信息
 */
@TableField(value = "card_org_attach")
@ApiModelProperty(value = "发卡行附加信息")
private String cardOrgAttach;

/**
 * 卡号
 */
@TableField(value = "card_number")
@ApiModelProperty(value = "卡号")
private String cardNumber;

/**
 * 开户人
 */
@TableField(value = "card_user_name")
@ApiModelProperty(value = "开户人")
private String cardUserName;

/**
 * 开户人电话
 */
@TableField(value = "card_user_phone")
@ApiModelProperty(value = "开户人电话")
private String cardUserPhone;

/**
 * 状态: 0正常 2禁用
 */
@TableField(value = "state")
@ApiModelProperty(value = "状态: 0正常 2禁用")
private Integer state;

/**
 * 时间
 */
@TableField(value = "create_time")
@ApiModelProperty(value = "时间")
private Date createTime;

/**
 * 删除状态: 0正常 1被删除
 */
@TableField(value = "deleted")
@ApiModelProperty(value = "删除状态: 0正常 1被删除")
private Integer deleted;

/**
 * 默认: 0正常 1默认
 */
@TableField(value = "preferred")
@ApiModelProperty(value = "默认: 0正常 1默认")
private Integer preferred;

@ApiModelProperty(value = "银行图片")
private String bankImg;

public static final String COL_ID = "id";

public static final String COL_MACC_ID = "macc_id";

public static final String COL_CARD_TYPE = "card_type";

public static final String COL_CARD_ORG_NAME = "card_org_name";

public static final String COL_CARD_ORG_ATTACH = "card_org_attach";

public static final String COL_CARD_NUMBER = "card_number";

public static final String COL_CARD_USER_NAME = "card_user_name";

public static final String COL_CARD_USER_PHONE = "card_user_phone";

public static final String COL_STATE = "state";

public static final String COL_CREATE_TIME = "create_time";

public static final String COL_DELETED = "deleted";

public static final String COL_PREFERRED = "preferred";
}