package com.wx.house.core.mapper.fileupload;

import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import com.wx.house.core.pojo.vo.SysAdminDetailVo;

public interface AttaUploadMapper extends BaseMapper<AttaUpload> {
	
	int updateResourceIdByIdIn(@Param("updatedResourceId") String updatedResourceId,
			@Param("idCollection") Collection<String> idCollection);
	/***
	 * 根据ResourceId查询附件表
	 * @param updatedResourceId
	 * @return
	 */
	List<AttaUpload> selectResourceId(@Param("resourceId") String resourceId);
	/***
	 * 根据ResourceId删除附件
	 * @param resourceId
	 * @return
	 */
	int deleteResourceId(@Param("resourceId") String resourceId);
}