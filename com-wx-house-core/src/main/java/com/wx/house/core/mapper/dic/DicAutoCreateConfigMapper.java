package com.wx.house.core.mapper.dic;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.DicAutoCreateConfig;

public interface DicAutoCreateConfigMapper extends BaseMapper<DicAutoCreateConfig> {
	int insertList(@Param("list")List<DicAutoCreateConfig> list);

	
}