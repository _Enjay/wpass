package com.wx.house.core.mapper.order;

import java.util.Collection;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.TOrderDetail;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 11:27
 * @description：${description}
 * @version: ：V$version
 */
public interface TOrderDetailMapper extends BaseMapper<TOrderDetail> {

    int insertList(@Param("list") List<TOrderDetail> list);

    List<TOrderDetail> selectCreatedOrderDetail(@Param("contractId") String contractId, @Param("paymentMonth") String paymentMonth, @Param("chargeTypeCollection") Collection<Integer> chargeTypeCollection);

    int insertTOrderDetail(@Param("tOrderDetail") List<TOrderDetail> tOrderDetail);

    //查询订单编号是否相等
    List<TOrderDetail> selectTOrferIds(@Param("detailList") List<TOrderDetail> detailList);
}