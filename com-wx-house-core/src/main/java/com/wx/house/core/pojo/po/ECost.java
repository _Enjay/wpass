package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "e_cost")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ECost implements Serializable {
    /**
     * 待处理费用id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "id")
    private String id;

    /**
     * 合同id
     */
    @TableField(value = "contr_id")
    @ApiModelProperty(value = "合同id")
    private String contrId;
    /**
     * 合同编号
     */
    @TableField(value = "contr_number")
    @ApiModelProperty(value = "合同编号")
    private String contrNumber;

    /**
     * 合同名称
     */
    @TableField(value = "contr_name")
    @ApiModelProperty(value = "合同名称")
    private String contrName;

    /**
     * 费用记录创建时间
     */
    @TableField(value = "creation_time")
    @ApiModelProperty(value = "费用记录创建时间")
    private Date creationTime;

    /**
     * 收费月份
     */
    @TableField(value = "charge_month")
    @ApiModelProperty(value = "收费月份")
    private String chargeMonth;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    @ApiModelProperty(value = "备注")
    private String remarks;

    /***
     * 主账号id
     */
    @TableField(value = "m_acc_id")
    @ApiModelProperty(value = "主账号id")
    private String mAccId;

    /**
     * 收费类型: 0保证金 1房租 2 物业管理费 3水,电,气费 6其他
     */
    @TableField(value = "pay_types")
    @ApiModelProperty("收费类型: 0保证金 1房租 2 物业管理费 3水,电,气费 6其他")
    private String payTypes;

    /**
     * 费用类型: 0收费 1支出
     */
    @TableField(value = "cost_type")
    @ApiModelProperty("cost_type")
    private Integer costType;

/**
 * 费用类型: 0收费 1支出
 */
@TableField(value = "total")
@ApiModelProperty("总计费用")
private BigDecimal total;

/**
 * 实际收费
 */
@TableField(value = "actual_pay")
@ApiModelProperty("实际收费")
private BigDecimal actualPay;

/**
 * 收费时间
 */
@TableField(value = "pay_time")
@ApiModelProperty(value = "收费时间")
private Date payTime;
}