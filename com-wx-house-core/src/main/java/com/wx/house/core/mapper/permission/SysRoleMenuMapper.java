package com.wx.house.core.mapper.permission;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysRoleMenu;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
	/***
	 * 删除角色菜单
	 * @param roleid
	 * @return
	 */
	int dateRolemenu(@Param("roleid")String roleid);
}