package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */

@Data
@TableName(value = "sys_role")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysRole implements Serializable {
    /**
     * 角色id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="角色id")
    private String id;

    /**
     * 角色名称
     */
    @TableField(value = "role_name")
    @ApiModelProperty(value="角色名称")
    private String roleName;

    /**
     * 角色状态 0正常 1禁用
     */
    @TableField(value = "macc_type")
    @ApiModelProperty(value="角色查看权限")
    private Integer maccType;
    
    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 创建时间
     */
    @TableField(value = "creat_time")
    @ApiModelProperty(value="创建时间")
    private Date creatTime;

    /**
     * 角色状态 0正常 1禁用
     */
    @TableField(value = "state")
    @ApiModelProperty(value="角色状态 0正常 1禁用")
    private Integer state;

    /**
     * 删除状态 0正常 1逻辑删除
     */
    @TableField(value = "del_type")
    @ApiModelProperty(value="删除状态 0正常 1逻辑删除")
    private Integer delType;

    /**
     * 操作人员
     */
    @TableField(value = "operation")
    @ApiModelProperty(value="操作人员")
    private String operation;

    /**
     * 操作时间
     */
    @TableField(value = "operation_time")
    @ApiModelProperty(value="操作时间")
    private Date operationTime;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_ROLE_NAME = "role_name";

    public static final String COL_REMARK = "remark";

    public static final String COL_CREAT_TIME = "creat_time";

    public static final String COL_STATE = "state";

    public static final String COL_DEL_TYPE = "del_type";

    public static final String COL_OPERATION = "operation";

    public static final String COL_OPERATION_TIME = "operation_time";
}