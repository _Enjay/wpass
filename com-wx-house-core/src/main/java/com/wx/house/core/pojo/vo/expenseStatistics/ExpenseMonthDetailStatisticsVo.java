package com.wx.house.core.pojo.vo.expenseStatistics;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 04:07
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseMonthDetailStatisticsVo {
@ApiModelProperty(value = "费用类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他", example = "1")
private Integer expenseType;

@ApiModelProperty(value = "共计收费", example = "1000.54")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal chargePrice;

@ApiModelProperty(value = "共计退费", example = "200.64")
@JsonSerialize(using = ToStringSerializer.class)
private BigDecimal refundPrice;
}
