package com.wx.house.core.mapper.expenses;

import java.util.Collection;
import java.util.Date;

import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.vo.TenantPendingMonthPaymentVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.po.TOrder;
import com.wx.house.core.pojo.vo.CcontractNextChargeVo;
import com.wx.house.core.pojo.vo.ECostPameVo;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 下午 02:27
 * @description：${description}
 * @version: ：V$version
 */
public interface ECostDetailMapper extends BaseMapper<ECostDetail> {
    int insertList(@Param("list") List<ECostDetail> list);

    int insertSelective(ECostDetail eCostDetail);

    List<ECostDetail> selectByIdIn(@Param("idCollection") Collection<String> idCollection);

    /***
     * 根据合同编号，收费年月查询
     *
     * @param contrNumber
     * @param chargeMonth
     * @return
     */
    ECostDetail selectMonth(@Param("contrNumber") String contrNumber, @Param("chargeMonth") String chargeMonth,
                            @Param("chargeType") String chargeType);

    /***
     * 根据主id，当前时间查询合同生成房租数据
     * @param maccid
     * @param NextCharge
     * @return
     */
    List<CcontractNextChargeVo> selectNextCharge(@Param("maccId") String maccId, @Param("advanceDays") Date advanceDays);

    /***
     * 根据待处理id查询待处理详情
     * @param eCostId
     * @return
     */
    List<ECostPameVo> selectECostID(@Param("eCostId") String eCostId);

    List<ECostDetail> selectCreatedECostDetail(@Param("contractId") String contractId, @Param("chargeMonth") String chargeMonth, @Param("chargeTypeCollection") Collection<Integer> chargeTypeCollection);


    List<TenantPendingMonthPaymentVo> selectTenantPendingPaymentVo(@Param("userId") String userId);

    //判断金额是否正确
    TOrder selectEcostPrice(@Param("selectBatchIds") List<ECost> selectBatchIds);

    /***
     * 订单缴费完成后批量更新待处理详情状态
     */
    int updateCostState(@Param("eCostId") List<String> eCostId);

    /***
     * 批量查詢
     */
    List<ECostDetail> selectEcostId(@Param("eCostId") List<String> eCostId);
}