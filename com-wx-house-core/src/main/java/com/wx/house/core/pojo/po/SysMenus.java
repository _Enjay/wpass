package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */

@Data
@TableName(value = "sys_menus")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysMenus implements Serializable {
    /**
     * 菜单ID_UUID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="菜单ID_UUID")
    private String id;

    /**
     * 父菜单ID
     */
    @TableField(value = "p_id")
    @ApiModelProperty(value="父菜单ID")
    private String pId;

    /**
     * 菜单名称
     */
    @TableField(value = "menu_name")
    @ApiModelProperty(value="菜单名称")
    private String menuName;

    /**
     * 菜单类型，1 菜单 2，链接
     */
    @TableField(value = "menu_type")
    @ApiModelProperty(value="菜单类型，1 菜单 2，链接")
    private Integer menuType;

    /**
     * 菜单链接
     */
    @TableField(value = "url")
    @ApiModelProperty(value="菜单链接")
    private String url;

    /**
     * 菜单图标
     */
    @TableField(value = "ico")
    @ApiModelProperty(value="菜单图标")
    private String ico;

    /**
     * 备注
     */
    @TableField(value = "remark")
    @ApiModelProperty(value="备注")
    private String remark;

    /**
     * 状态 ：-1被删除 0正常 1禁用
     */
    @TableField(value = "state")
    @ApiModelProperty(value="状态 ：-1被删除 0正常 1禁用")
    private Integer state;

    /**
     * 顺序
     */
    @TableField(value = "menu_order")
    @ApiModelProperty(value="顺序")
    private Integer menuOrder;

    /**
     * 操作人
     */
    @TableField(value = "operation")
    @ApiModelProperty(value="操作人")
    private String operation;

    /**
     * 操作时间
     */
    @TableField(value = "operation_time")
    @ApiModelProperty(value="操作时间")
    private Date operationTime;

    @Transient
    private List<SysMenus> child = new LinkedList<>();
    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_P_ID = "p_id";

    public static final String COL_MENU_NAME = "menu_name";

    public static final String COL_MENU_TYPE = "menu_type";

    public static final String COL_URL = "url";

    public static final String COL_ICO = "ico";

    public static final String COL_REMARK = "remark";

    public static final String COL_STATE = "state";

    public static final String COL_MENU_ORDER = "menu_order";

    public static final String COL_OPERATION = "operation";

    public static final String COL_OPERATION_TIME = "operation_time";
}