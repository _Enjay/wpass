package com.wx.house.core.pojo.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.wx.house.core.pojo.po.ECost;

import lombok.Data;

@Data
public class TOrderDetailAllVo {
	private String orderNumber; 
	private String state;
	private Date createTime;
	private List<ECost> eCostList=new ArrayList<ECost>();
}
