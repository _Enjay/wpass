package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 04:21
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
public enum BuildHireEnum {
	WAITHIRE(0, "待出租"),
	HIRED(1, "已出租");
public Integer state;
public String description;
}
