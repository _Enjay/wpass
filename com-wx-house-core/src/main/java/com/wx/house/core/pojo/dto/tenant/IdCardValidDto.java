package com.wx.house.core.pojo.dto.tenant;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 上午 10:35
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IdCardValidDto {
@ApiModelProperty("姓名")
@NotNull
private String name;

@ApiModelProperty("身份证号码")
@NotNull
private String idCardNumber;

@ApiModelProperty("正面图片")
@NotNull
private String idCard;

@ApiModelProperty("反面图片")
@NotNull
private String idCard_r;
}
