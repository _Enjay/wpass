package com.wx.house.core.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/17 下午 04:37
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractStatisticsVo {
private String buildingName;
private String contractName;
@JsonFormat(pattern="yyyy-MM")
private Date startTime;
@JsonFormat(pattern="yyyy-MM")
private Date endTime;
}
