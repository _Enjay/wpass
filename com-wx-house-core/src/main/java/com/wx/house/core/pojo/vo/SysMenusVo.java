package com.wx.house.core.pojo.vo;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.wx.house.core.pojo.po.SysMenus;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysMenusVo {

    @ApiModelProperty(value="菜单ID_UUID")
    private String id;

    @ApiModelProperty(value="父菜单ID")
    private String pId;

    @ApiModelProperty(value="菜单名称")
    private String menuName;

    @ApiModelProperty(value="菜单类型，1 菜单 2，链接")
    private Integer menuType;

    @ApiModelProperty(value="菜单链接")
    private String url;

    @ApiModelProperty(value="菜单图标")
    private String ico;

    @ApiModelProperty(value="备注")
    private String remark;

    @ApiModelProperty(value="状态 ：-1被删除 0正常 1禁用")
    private Integer state;
    
    @ApiModelProperty(value="顺序")
    private Integer menuOrder;

    @ApiModelProperty(value="操作人")
    private String operation;

    @ApiModelProperty(value="操作时间")
    private Date operationTime;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String permissionId;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @Transient
    private List<SysMenusVo> child = new LinkedList<>();
}
