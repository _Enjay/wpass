package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/11/29 上午 10:23
 * @description：${description}
 * @version:   ：V$version
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "t_order")
public class TOrder {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="id")
    private String id;

    /**
     * 订单编号
     */
    @TableField(value = "order_number")
    @ApiModelProperty(value="系统订单编号")
    private String orderNumber;
    /**
     * 订单编号
     */
    @TableField(value = "transaction_id")
    @ApiModelProperty(value="微信订单编号")
    private String transactionId;

    /**
     * 租户id
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value="租户id")
    private String userId;

    /**
     * 订单金额
     */
    @TableField(value = "price")
    @ApiModelProperty(value="订单金额")
    private BigDecimal price;

    /**
     * 实收金额
     */
    @TableField(value = "actual_price")
    @ApiModelProperty(value="实收金额")
    private BigDecimal actualPrice;

    /**
     * 订单状态: 0待付款 1已付款
     */
    @TableField(value = "state")
    @ApiModelProperty(value="订单状态: 0待付款 1已付款")
    private Integer state;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @ApiModelProperty(value="创建时间")
    private Date createTime;
    
    /**
     * 最近一次发起支付时间
     */
    @TableField(value = "latest_time")
    @ApiModelProperty(value="创建时间")
    private Date latestTime;
    
    /**
     * 支付时间
     */
    @TableField(value = "pay_time")
    @ApiModelProperty(value="支付时间")
    private Date payTime;

    /**
     * 删除状态: 0正常 1已删除
     */
    @TableField(value = "deleted")
    @ApiModelProperty(value="删除状态: 0正常 1已删除")
    private Integer deleted;

    /**
     * 支付方式: 1微信 2支付宝
     */
    @TableField(value = "pay_type")
    @ApiModelProperty(value="支付方式: 1微信 2支付宝")
    private Integer payType;
    
    /**
     * 扩展字段，如果是微信支付表示预付订单号
     */
    @TableField(value = "extension")
    @ApiModelProperty(value="扩展字段，如果是微信支付表示预付订单号")
    private String extension;

}