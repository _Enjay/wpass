package com.wx.house.core.mapper.building;

import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.building.BChargeTemplate;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface BChargeTemplateMapper extends BaseMapper<BChargeTemplate> {
int insertSelective(BChargeTemplate bChargeTemplate);

int insertList(@Param("list") List<BChargeTemplate> list);

int deleteById(@Param("id") Integer id);

int updateById(@Param("updated") BChargeTemplate updated, @Param("id") Integer id);

BChargeTemplate selectById(@Param("id") Integer id);
int deleteByBuildId(@Param("buildId")String buildId);

int updateByBuildId(@Param("updated")BChargeTemplate updated,@Param("buildId")String buildId);


}