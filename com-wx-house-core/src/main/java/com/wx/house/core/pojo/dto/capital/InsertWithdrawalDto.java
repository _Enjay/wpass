package com.wx.house.core.pojo.dto.capital;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:44
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsertWithdrawalDto {
@ApiModelProperty(value = "提现金额", required = true, example = "200.00")
@NotNull
private BigDecimal WithdrawalBalance;

@ApiModelProperty(value = "支付方式id", required = true, example = "123123")
@NotBlank
private String payModeId;
}
