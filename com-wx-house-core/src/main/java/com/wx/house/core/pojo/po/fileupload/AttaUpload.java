package com.wx.house.core.pojo.po.fileupload;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@TableName(value = "atta_upload")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AttaUpload {
/**
 * 附件id
 */
@TableId(value = "id")
@ApiModelProperty(value = "附件id")
private String id;

/**
 * 附件类型
 */
@TableField(value = "`type`")
@ApiModelProperty(value = "附件类型")
private String type;

/**
 * 附件名称
 */
@TableField(value = "atta_name")
@ApiModelProperty(value = "附件名称")
private String attaName;

/**
 * 文件后缀名
 */
@TableField(value = "file_suffix")
@ApiModelProperty(value = "文件后缀名")
private String fileSuffix;

/**
 * 文件地址
 */
@TableField(value = "url")
@ApiModelProperty(value = "文件地址")
private String url;

/**
 * 创建时间
 */
@TableField(value = "creat_time")
@ApiModelProperty(value = "创建时间")
private Date creatTime;

/**
 * 备注
 */
@TableField(value = "remark")
@ApiModelProperty(value = "备注")
private String remark;

/**
 * 附件路径
 */
@TableField(value = "path")
@ApiModelProperty(value = " 附件路径")
private String path;

/**
 * 附件来源id
 */
@TableField(value = "resource_id")
@ApiModelProperty(value = "附件来源id")
private String resourceId;
}