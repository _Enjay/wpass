package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class CcontractBuildVo {
	@ApiModelProperty(value = "合同Id")
	private String id;
	@ApiModelProperty(value = "合同编号")
	private String contrNumber;
	@ApiModelProperty(value = "合同姓名")
	private String contrName;
	@ApiModelProperty(value = "租户姓名")
	private String tenantName;
	@ApiModelProperty(value = "联系方式")
	private String tenantPhone;
}
