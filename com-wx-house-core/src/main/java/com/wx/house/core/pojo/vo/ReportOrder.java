package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 下午 03:38
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReportOrder {
    @ApiModelProperty("缴费合同id")
    private String contractId;

    @ApiModelProperty(value = "收费月份")
    private String payMonth;

    @ApiModelProperty(value = "收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
    private String payType;

    @ApiModelProperty(value="系统订单编号")
    private String orderNumber;
}
