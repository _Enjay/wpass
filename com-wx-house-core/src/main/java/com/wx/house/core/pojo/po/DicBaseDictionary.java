package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "dic_base_dictionary")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DicBaseDictionary implements Serializable {
	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty(value = "id")
	private Long id;

	/**
	 * 字典名称
	 */
	@TableField(value = "dic_name")
	@ApiModelProperty(value = "字典名称")
	private String dicName;

	/**
	 * 父级字典id
	 */
	@TableField(value = "parent_id")
	@ApiModelProperty(value = "父级字典id")
	private Long parentId;

	/**
	 * 字典编号
	 */
	@TableField(value = "dic_num")
	@ApiModelProperty(value = "字典编号")
	private String dicNum;

	/**
	 * 排序
	 */
	@TableField(value = "`order`")
	@ApiModelProperty(value = "排序")
	private Integer order;

	/**
	 * 数值
	 */
	@TableField(value = "`value`")
	@ApiModelProperty(value = "数值")
	private String value;

	/**
	 * 层级
	 */
	@TableField(value = "`level`")
	@ApiModelProperty(value = "层级")
	private String level;

	/**
	 * 操作人
	 */
	@TableField(value = "operation")
	@ApiModelProperty(value = "操作人")
	private String operation;

	/**
	 * 操作时间
	 */
	@TableField(value = "operation_time")
	@ApiModelProperty(value = "操作时间")
	private Date operationTime;

	/**
	 * 逻辑状态 1未删除 2已删除
	 */
	@TableField(value = "logical_state")
	@ApiModelProperty(value = "逻辑状态 1未删除  2已删除")
	private Long logicalState;

	/**
	 * 主账户id
	 */
	@TableField(value = "m_acc_id")
	@ApiModelProperty(value = "主账户id")
	private String mAccId;
	/**
	 * 子菜单
	 */
	private List<DicBaseDictionary> children;
	private static final long serialVersionUID = 1L;

	public static final String COL_ID = "id";

	public static final String COL_DIC_NAME = "dic_name";

	public static final String COL_PARENT_ID = "parent_id";

	public static final String COL_DIC_NUM = "dic_num";

	public static final String COL_ORDER = "order";

	public static final String COL_VALUE = "value";

	public static final String COL_LEVEL = "level";

	public static final String COL_OPERATION = "operation";

	public static final String COL_OPERATION_TIME = "operation_time";

	public static final String COL_LOGICAL_STATE = "logical_state";
}