package com.wx.house.core.pojo.dto.tenant;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:00
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantCertificationUpDto {
@ApiModelProperty(name = "phone", value = "手机号", required = true)
@NotBlank
private String phone;

@NotBlank
@ApiModelProperty(name = "name", value = "用户真实姓名", required = true)
private String name;

@NotBlank
@ApiModelProperty(name = "idCard", value = "身份证号码", required = true)
private String idCard;

@ApiModelProperty(name = "idCard", value = "身份证图片信息", required = true)
private List<String> fileList;
}
