package com.wx.house.core.pojo.vo.houseManager;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 下午 02:37
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HouseManagerVo {
@ApiModelProperty("总公寓数")
private Integer buildNumber;
@ApiModelProperty("总房间数")
private Integer roomNumber;

@ApiModelProperty("待出租房间")
private Integer waitHireNumber;
@ApiModelProperty("已出租房间")
private Integer hiredNumber;

@ApiModelProperty("停用房间")
private Integer freezeNumber;
}
