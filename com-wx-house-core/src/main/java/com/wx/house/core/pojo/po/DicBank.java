package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 下午 02:00
 * @description：${description}
 * @version: ：V$version
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "dic_bank")
public class DicBank {
/**
 * id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "id")
private String id;

/**
 * 银行名字
 */
@TableField(value = "bank_name")
@ApiModelProperty(value = "银行名字")
private String bankName;

@TableField(value = "bank_abbr")
@ApiModelProperty(value = "银行编码")
private String bankAbbr;

/**
 * 排序
 */
@TableField(value = "order")
@ApiModelProperty(value = "排序")
private Integer order;

/**
 * 附件id
 */
@TableField(value = "file_id")
@ApiModelProperty(value = "附件id")
private String fileId;

public static final String COL_ID = "id";

public static final String COL_BANK_NAME = "bank_name";

public static final String COL_ORDER = "order";

public static final String COL_FILE_ID = "file_id";
}