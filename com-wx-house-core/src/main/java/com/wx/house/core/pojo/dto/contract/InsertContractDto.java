package com.wx.house.core.pojo.dto.contract;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/04 下午 02:13
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsertContractDto {
@ApiModelProperty(value = "合同编号", required = true, example = "#1#114996")
@NotBlank
private String contrNumber;

@ApiModelProperty(value = "合同名称", required = true, example = "金坷垃一号")
@NotBlank
private String contrName;

@ApiModelProperty(value = "租房地址", required = true, example = "渝兴v客部落d5 601 稳信科技")
@NotBlank
private String address;

@ApiModelProperty(value = "房间id", required = true, example = "605231")
@NotBlank
private String roomId;

/**
 * 截止水表读数(立方米)
 */
@TableField(value = "end_water")
@ApiModelProperty(value = "截止水表读数(立方米)", required = true, example = "60.0")
private Integer endWater;

/**
 * 截止电表读数(千瓦时)
 */
@TableField(value = "end_electric")
@ApiModelProperty(value = "截止电表读数(千瓦时)", required = true, example = "72.0")
private Integer endElectric;

/**
 * 截止气表读数(立方米)
 */
@TableField(value = "end_gas")
@ApiModelProperty(value = "截止气表读数(立方米)", required = true, example = "89.0")
private Integer endGas;

@ApiModelProperty(value = "租户姓名", required = true, example = "小明")
@NotBlank
private String tenantName;

@ApiModelProperty(value = "手机号", required = true, example = "13996995959")
@NotBlank
private String tenantPhone;

@ApiModelProperty(value = "证件类型: 0身份证", required = true, example = "0")
@NotNull
private Integer tenantCertificates;


@ApiModelProperty(value = "证件号", required = true, example = "5002221999666655551396")
@NotBlank
private String tenantCertiNumber;


@ApiModelProperty(value = "备注", example = "这个娃儿皮的很，等他把房子租了就打死他狗日的")
private String remarks;

@ApiModelProperty(value = "合同期限", required = true, example = "6")
@NotNull
private Integer contractTerm;

@ApiModelProperty(value = "时间代码: 4小时 5天 6周 7月 8季度 9半年 10年", required = true, example = "7")
@NotNull
private Integer contractTermUnit;

@ApiModelProperty(value = "开始时间", example = "2010-07-19 11:42:36", required = true)
@NotNull
private String startTime;

@ApiModelProperty(value = "结束时间", example = "2029-07-29 11:42:36", required = true)
@NotNull
private String endTime;

public Date getStartTime() {
	if (StringUtils.isEmpty(this.startTime)) {
		return null;
	}
	Calendar todayStart = Calendar.getInstance();
	try {
		if (this.startTime.trim().length() == 16) {
			todayStart.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(this.startTime));
		} else {
			todayStart.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(this.startTime));
		}
	} catch (ParseException e) {
		e.printStackTrace();
		throw new RuntimeException("时间格式异常:" + "yyyy-MM-dd 或者 yyyy-MM-dd");
	}
	return todayStart.getTime();
}

public Date getEndTime() {
	if (StringUtils.isEmpty(this.endTime)) return null;
	Calendar todayEnd = Calendar.getInstance();
	try {
		if (this.startTime.trim().length() == 16) {
			todayEnd.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(this.endTime));
		} else {
			todayEnd.setTime(new SimpleDateFormat("yyyy-MM-dd").parse(this.endTime));
		}
	} catch (ParseException e) {
		e.printStackTrace();
		throw new RuntimeException("时间格式异常:" + "yyyy-MM-dd 或者 yyyy-MM-dd");
	}
	return todayEnd.getTime();
}

@ApiModelProperty(value = "合同押金", example = "300.00", required = false)
private BigDecimal deposit;

@ApiModelProperty(value = "预付房租", example = "900.00", required = false)
private BigDecimal prepayment;

@ApiModelProperty(value = "物管费/月", example = "20.00", required = true)
private BigDecimal propertyManager;

@ApiModelProperty(value = "付费类型 5[日付] 6[周付] 7[月付] 8[季付] 9[半年付] 10[年付]", example = "7", required = true)
@NotNull
private Integer rentType;

@ApiModelProperty(value = "房租价格", example = "300.00", required = true)
@NotNull
private BigDecimal chargeMoney;

@ApiModelProperty(value = "合同附件列表", example = "12312312312,12121521,12312313", required = true)
private List<String> contractFileIdList = new ArrayList<>();

@ApiModelProperty(value = "押金和预付房租支付方式: 0支付宝 1微信 2银行卡 3线下[默认]", example = "3", required = false)
private Integer depositPayType;
}
