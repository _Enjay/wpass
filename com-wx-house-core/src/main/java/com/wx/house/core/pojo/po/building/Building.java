package com.wx.house.core.pojo.po.building;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:56
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "b_building")
public class Building implements Serializable {
/**
 * 楼栋id
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "楼栋id")
private String id;

/**
 * 楼栋名称
 */
@TableField(value = "build_name")
@ApiModelProperty(value = "楼栋名称")
private String buildName;

/**
 * 省份id
 */
@TableField(value = "province")
@ApiModelProperty(value = "省份id")
private Integer province;

/**
 * 市id
 */
@TableField(value = "city")
@ApiModelProperty(value = "市id")
private Integer city;

/**
 * 区县id
 */
@TableField(value = "county")
@ApiModelProperty(value = "区县id")
private Integer county;

/**
 * 详细地址
 */
@TableField(value = "address")
@ApiModelProperty(value = "详细地址")
private String address;

/**
 * 添加时间
 */
@TableField(value = "add_time")
@ApiModelProperty(value = "添加时间")
private Date addTime;

/**
 * 操作人
 */
@TableField(value = "operation")
@ApiModelProperty(value = "操作人")
private String operation;

/**
 * 操作时间
 */
@TableField(value = "operation_time")
@ApiModelProperty(value = "操作时间")
private Date operationTime;

/**
 * 逻辑删除状态0 启用 1禁用
 */
@TableField(value = "logical_state")
@ApiModelProperty(value = "逻辑删除状态0 启用 1禁用")
private Integer logicalState;


/**
 * 主账号id
 */
@TableField(value = "m_acc_id")
@ApiModelProperty(value = "主账号id")
private String mAccId;

/**
 * 经度
 */
@TableField(value = "longitude")
@ApiModelProperty(value = "经度")
private String longitude;

/**
 * 纬度
 */
@TableField(value = "latitude")
@ApiModelProperty(value = "纬度")
private String latitude;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_BUILD_NAME = "build_name";

public static final String COL_PROVINCE = "province";

public static final String COL_CITY = "city";

public static final String COL_COUNTY = "county";

public static final String COL_ADDRESS = "address";

public static final String COL_ADD_TIME = "add_time";

public static final String COL_OPERATION = "operation";

public static final String COL_OPERATION_TIME = "operation_time";

public static final String COL_LOGICAL_STATE = "logical_state";

public static final String COL_PERMISSION_USER_ID = "permission_user_id";

public static final String COL_M_ACC_ID = "m_acc_id";

public static final String COL_LONGITUDE = "longitude";

public static final String COL_LATITUDE = "latitude";
}