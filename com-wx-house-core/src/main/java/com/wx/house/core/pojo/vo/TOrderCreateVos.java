package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * 订单管理
 *
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/02 上午 09:22
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TOrderCreateVos {
    @ApiModelProperty( value = "订单id")
    private String id;
    
    @ApiModelProperty("系统订单编号")
    private String orderNumber;
    
    @ApiModelProperty("订单总金额")
    private BigDecimal price;

    @ApiModelProperty("订单状态: 0待缴费 1已缴费")
    private Integer state;
    
    @ApiModelProperty("订单创建时间")
    private Date createTime;

    @ApiModelProperty("房间楼宇月份")
    private List<TOrderMonthAllVo> tOrderMonthAllVoList;
    
    @JsonIgnore
    @ApiModelProperty("明细id拼接字符串")
    private String todString;
    
}
