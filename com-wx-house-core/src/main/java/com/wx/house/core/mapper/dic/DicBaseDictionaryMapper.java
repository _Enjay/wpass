package com.wx.house.core.mapper.dic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.DicBaseDictionary;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Mapper
public interface DicBaseDictionaryMapper extends BaseMapper<DicBaseDictionary> {
    /**
     * 根据节点id查询下面目录
     * @param id
     * @return
     */
    List<DicBaseDictionary> selectAllByCondion(@Param("id") Integer id,
                                               @Param("mAccId") String mAccId);

    /**
     * 根據id查詢字典表的详情
     * @param id
     * @return
     */
    DicBaseDictionary selectDicBaseById(@Param("id") Long id);

    /**
     * 删除字典表的数据（根据id）
     * @param
     * @return
     */
    int deleteDicBaseById(@Param("ids")List<String> ids);

    /**
     *根据id查找父类id
     */
    DicBaseDictionary selectParentIdById(@Param("id") Integer id);

    /**
     * 根据id获取下面所有的子集id
     * @param id
     * @return
     */
    List<String> selectIds(@Param("id") Integer id);


}