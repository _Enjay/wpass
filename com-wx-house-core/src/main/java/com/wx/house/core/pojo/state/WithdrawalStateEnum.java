package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 下午 02:34
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum WithdrawalStateEnum {
	ACCEPT(1, "审核通过"),
	REJECT(2, "审核拒绝");

public Integer state;
public String description;
}
