package com.wx.house.core.pojo.dto.tenant;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:00
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantSignUpDto {
@ApiModelProperty(name = "phone", value = "手机号", required = true)
@NotBlank
private String phone;

@NotBlank
@ApiModelProperty(name = "code", value = "密码", required = true)
private String code;
}
