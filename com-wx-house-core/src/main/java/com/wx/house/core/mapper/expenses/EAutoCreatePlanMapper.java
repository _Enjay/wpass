package com.wx.house.core.mapper.expenses;

import java.util.Collection;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.bo.EAutoCreatePlanBo;
import com.wx.house.core.pojo.po.EAutoCreatePlan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/06 上午 11:37
 * @description：${description}
 * @version: ：V$version
 */
public interface EAutoCreatePlanMapper extends BaseMapper<EAutoCreatePlan> {
int insertList(@Param("list") List<EAutoCreatePlan> list);

List<EAutoCreatePlanBo> selectAllAutoPlanByExample();

int updateByIdIn(@Param("updated") EAutoCreatePlan updated, @Param("idCollection") Collection<String> idCollection);

}