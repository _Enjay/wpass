package com.wx.house.core.pojo.dto.capital;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:44
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewWithdrawalDto {
@ApiModelProperty(value = "提现表id", required = true, example = "200.00")
@NotBlank
private String WithdrawalId;

@ApiModelProperty(value = "状态: 1提现通过 2提现失败", required = true, example = "1")
@NotNull
private Integer state;

@ApiModelProperty(value = "备注", required = false, example = "小老弟你的钱快提没了啊，注意点")
private String remark;
}
