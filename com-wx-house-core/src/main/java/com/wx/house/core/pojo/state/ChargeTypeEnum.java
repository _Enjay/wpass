package com.wx.house.core.pojo.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 下午 04:50
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
public enum ChargeTypeEnum {
	DEPOSIT(0, "保证金"), RENT(1, "房租"), PROPERTY_MANAGEMENT(2, "物管费"), WATER(3, "水费"), ELECT(4, "电费"), GAS(5, "气费"), OTHER(6, "其他");
private int code;
private String msg;

public static String chargeTypeByCode(Integer code) {
	switch (code) {
		case 0:
			return DEPOSIT.msg;
		case 1:
			return RENT.msg;
		case 2:
			return PROPERTY_MANAGEMENT.msg;
		case 3:
			return WATER.msg;
		case 4:
			return ELECT.msg;
		case 5:
			return GAS.msg;
		case 6:
			return OTHER.msg;
	}
	return "";
}
}
