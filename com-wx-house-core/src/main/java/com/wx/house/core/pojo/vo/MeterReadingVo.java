package com.wx.house.core.pojo.vo;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class MeterReadingVo {
	   /**
     * 抄表id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="抄表id")
    private String id;

    /**
     * 楼栋id
     */
    @TableField(value = "building_id")
    @ApiModelProperty(value="楼栋id")
    private String buildingId;

    /**
     * 门牌号
     */
    @TableField(value = "door_number")
    @ApiModelProperty(value="房间号")
    private String doorNumber;

    /**
     * 合同编号
     */
    @TableField(value = "contr_number")
    @ApiModelProperty(value="合同编号")
    private String contrNumber;

    /**
     * 租户姓名
     */
    @TableField(value = "tenant_name")
    @ApiModelProperty(value="租户姓名")
    private String tenantName;

    /**
     * 电话号码
     */
    @TableField(value = "phone")
    @ApiModelProperty(value="电话号码")
    private String phone;

    /**
     * 抄表月份
     */
    @TableField(value = "month")
    @ApiModelProperty(value="抄表月份")
    private String month;

    /**
     * 水表读数(立方米)
     */
    @TableField(value = "month_water")
    @ApiModelProperty(value="水表读数(立方米)")
    private BigDecimal monthWater;

    /**
     * 电表读数(千瓦时)
     */
    @TableField(value = "month_electric")
    @ApiModelProperty(value="电表读数(千瓦时)")
    private BigDecimal monthElectric;

    /**
     * 气表读数(立方米)
     */
    @TableField(value = "month_gas")
    @ApiModelProperty(value="气表读数(立方米)")
    private BigDecimal monthGas;

    /**
     * 抄表状态 0待抄表 1已抄表
     */
    @TableField(value = "month_state")
    @ApiModelProperty(value="抄表状态 （0待抄表 1已抄表）")
    private Integer monthState;
    
    /**
     * 当月水表读数(立方米)
     */
    @TableField(value = "same_water")
    @ApiModelProperty(value="当月水表读数(立方米)")
    private BigDecimal sameWater;

    /**
     *当月 电表读数(千瓦时)
     */
    @TableField(value = "same_electric")
    @ApiModelProperty(value="当月电表读数(千瓦时)")
    private BigDecimal sameElectric;

    /**
     * 当月气表读数(立方米)
     */
    @TableField(value = "same_gas")
    @ApiModelProperty(value="当月气表读数(立方米)")
    private BigDecimal sameGas;
    
    private String buildName;
    
	private String address;
}
