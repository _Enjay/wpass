package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 下午 02:27
 * @description：${description}
 * @version: ：V$version
 */
@TableName(value = "e_cost_detail")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ECostDetail {
    /**
     * id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty("id")
    private String id;

    /**
     * 待处理表id
     */
    @TableField(value = "e_cost_id")
    @ApiModelProperty("待处理表id")
    private String eCostId;

    /**
     * 收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他
     */
    @TableField(value = "charge_type")
    @ApiModelProperty("收费类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
    private Integer chargeType;

    /**
     * 费用记录创建时间
     */
    @TableField(value = "creation_time")
    @ApiModelProperty("费用记录创建时间")
    private Date creationTime;

    /**
     * 应收金额
     */
    @TableField(value = "receive_money")
    @ApiModelProperty("应收金额")
    private BigDecimal receiveMoney;

    /**
     * 实收金额
     */
    @TableField(value = "net_money")
    @ApiModelProperty("实收金额")
    private BigDecimal netMoney;

    /**
     * 缴费金额
     */
    @TableField(value = "payment_money")
    @ApiModelProperty(value = "缴费金额")
    private BigDecimal paymentMoney;


    /**
     * 处理状态（0待处理 1已处理）
     */
    @TableField(value = "cost_state")
    @ApiModelProperty("处理状态（0待处理 1已处理）")
    private Integer costState;

    /**
     * 收费时间
     */
    @TableField(value = "charge_time")
    @ApiModelProperty("收费时间")
    private Date chargeTime;

    /**
     * 备注
     */
    @TableField(value = "remarks")
    @ApiModelProperty("备注")
    private String remarks;



}