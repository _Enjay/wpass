package com.wx.house.core.pojo.dto.order;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Auther: 杰
 * @Date: 2020/1/6 12:08
 * @Description:
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class E_UserContractDto {

    @ApiModelProperty("订单金额")
    private BigDecimal price;
    
    @ApiModelProperty("缴费详情")
    private List<C_UserOrderDetail> e_userContractDto;
}