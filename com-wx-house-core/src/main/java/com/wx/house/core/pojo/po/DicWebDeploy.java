package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@TableName(value = "dic_web_deploy")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DicWebDeploy {
    /**
     * id
     */
     @TableId(value = "id", type = IdType.INPUT)
     @ApiModelProperty(value = "id")
    private String id;

    /**
     * 网站名称
     */
    @TableField(value = "web_name")
    @ApiModelProperty(value = "网站名称")
    private String webName;

    /**
     * 版权信息
     */
    @TableField(value = "copyright")
    @ApiModelProperty(value = "版权信息")
    private String copyright;

    /**
     * 附件id
     */
    @TableField(value = "file_id")
    @ApiModelProperty(value = "附件id")
    private String fileId;

    /**
     * 网站域名
     */
    @TableField(value = "domain_name")
    @ApiModelProperty(value = "网站域名")
    private String domainName;

    /**
     * 操作人
     */
    @TableField(value = "operation")
    @ApiModelProperty(value = "操作人")
    private String operation;

    /**
     * 操作时间
     */
    @TableField(value = "opreation_time")
    @ApiModelProperty(value = "操作时间")
    private Date opreationTime;

    /**
     * 主id
     */
    @TableField(value = "m_acc_id")
    @ApiModelProperty(value = "主id")
    private String mAccId;

    public static final String COL_WEB_NAME = "web_name";

    public static final String COL_COPYRIGHT = "copyright";

    public static final String COL_FILE_ID = "file_id";

    public static final String COL_DOMAIN_NAME = "domain_name";

    public static final String COL_OPERATION = "operation";

    public static final String COL_OPREATION_TIME = "opreation_time";

    public static final String COL_M_ACC_ID = "m_acc_id";


}