package com.wx.house.core.pojo.dto.tenant;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:00
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantSignInDto {
@ApiModelProperty(name = "signType", value = "登陆类型: 1密码登陆[默认] 2验证码登陆", required = false)
private Integer signType;

@NotBlank
@ApiModelProperty(name = "phone", value = "手机号", required = true)
private String phone;

@NotBlank
@ApiModelProperty(name = "code", value = "密码 / 验证码", required = true)
private String code;
}
