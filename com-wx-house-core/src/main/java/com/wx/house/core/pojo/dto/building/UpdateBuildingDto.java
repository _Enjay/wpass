package com.wx.house.core.pojo.dto.building;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 03:24
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class UpdateBuildingDto {
@ApiModelProperty(value = "id", example = "a583c75c333048979711f9ac05674491", required = false)
@NotBlank
private String id;

@ApiModelProperty(value = "建筑名称", example = "v客部落", required = false)
private String buildingName;

@ApiModelProperty(value = "省份ID", example = "110000", required = false)
private Integer province;
@ApiModelProperty(value = "城市ID", example = "110100", required = false)
private Integer city;
@ApiModelProperty(value = "区县ID", example = "110101", required = false)
private Integer county;
@ApiModelProperty(value = "详细地址", example = "d5 601 稳信科技", required = false)
private String address;

@ApiModelProperty(value = "经度", example = "37.2303283876", required = false)
private String longitude;
@ApiModelProperty(value = "纬度", example = "105.6884765625", required = false)
private String latitude;

@ApiModelProperty(value = "水费 x元/1m³", example = "0.10", required = false)
private BigDecimal waterRate;
@ApiModelProperty(value = "电费 x元/1千瓦时", example = "0.88", required = false)
private BigDecimal powerRate;
@ApiModelProperty(value = "电费 x元/1m³", example = "0.23", required = false)
private BigDecimal gasRate;
@ApiModelProperty(value = "物管费 x元/月", example = "30", required = false)
private BigDecimal propertyRateRate;

@ApiModelProperty(value = "楼宇略缩图id", example = "111", required = true)
private String buildImgId;
}
