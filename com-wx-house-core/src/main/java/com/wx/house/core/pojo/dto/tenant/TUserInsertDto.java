package com.wx.house.core.pojo.dto.tenant;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


@Data
@TableName(value = "t_user")
public class TUserInsertDto implements Serializable {

@ApiModelProperty(value = "用户名字", example = "小明", required = true)
@NotBlank
private String name;


@ApiModelProperty(value = "手机号", example = "13996996969", required = true)
@NotBlank
private String phoneNumber;


@ApiModelProperty(value = "证件号码", example = "500242199911445867", required = true)
@NotBlank
private String idCardNumber;

@ApiModelProperty(value = "证件类型: 1身份证", example = "1", required = true)
@NotBlank
private Integer idCardType;

@ApiModelProperty(value = "附件列表", example = "[11111,22222,33333]", required = true)
private List<String> attachList;
}