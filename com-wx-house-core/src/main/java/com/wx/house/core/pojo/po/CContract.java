package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/04 上午 10:52
 * @description：${description}
 * @version: ：V$version
 */

@Data
@TableName(value = "c_contract")
public class CContract implements Serializable {
/**
 * 合同
 */
@TableId(value = "id", type = IdType.INPUT)
@ApiModelProperty(value = "合同")
private String id;

/**
 * 合同编号
 */
@TableField(value = "contr_number")
@ApiModelProperty(value = "合同编号")
private String contrNumber;

/**
 * 合同名称
 */
@TableField(value = "contr_name")
@ApiModelProperty(value = "合同名称")
private String contrName;

/**
 * 房间门牌号
 */
@TableField(value = "room_id")
@ApiModelProperty(value = "房间门牌号")
private String roomId;

/**
 * 租户姓名
 */
@TableField(value = "tenant_name")
@ApiModelProperty(value = "租户姓名")
private String tenantName;

/**
 * 手机号
 */
@TableField(value = "tenant_phone")
@ApiModelProperty(value = "手机号")
private String tenantPhone;

/**
 * 证件类型
 */
@TableField(value = "tenant_certificates")
@ApiModelProperty(value = "证件类型")
private Integer tenantCertificates;

/**
 * 证件号
 */
@TableField(value = "tenant_certi_number")
@ApiModelProperty(value = "证件号")
private String tenantCertiNumber;

/**
 * 备注
 */
@TableField(value = "remarks")
@ApiModelProperty(value = "备注")
private String remarks;

/**
 * 添加时间
 */
@TableField(value = "add_time")
@ApiModelProperty(value = "添加时间")
private Date addTime;

/**
 * 操作人
 */
@TableField(value = "operation")
@ApiModelProperty(value = "操作人")
private String operation;

/**
 * 操作时间
 */
@TableField(value = "operation_time")
@ApiModelProperty(value = "操作时间")
private Date operationTime;

/**
 * 逻辑状态0 启用 1禁用
 */
@TableField(value = "logical_state")
@ApiModelProperty(value = "逻辑状态0 正常 1废弃 2到期 3归档")
private Integer logicalState;

/**
 * 主账号id
 */
@TableField(value = "m_acc_id")
@ApiModelProperty(value = "主账号id")
private String mAccId;

private static final long serialVersionUID = 1L;

public static final String COL_ID = "id";

public static final String COL_CONTR_NUMBER = "contr_number";

public static final String COL_CONTR_NAME = "contr_name";

public static final String COL_ROOM_ID = "room_id";

public static final String COL_END_WATER = "end_water";

public static final String COL_END_ELECTRIC = "end_electric";

public static final String COL_END_GAS = "end_gas";

public static final String COL_TENANT_NAME = "tenant_name";

public static final String COL_TENANT_PHONE = "tenant_phone";

public static final String COL_TENANT_CERTIFICATES = "tenant_certificates";

public static final String COL_TENANT_CERTI_NUMBER = "tenant_certi_number";

public static final String COL_REMARKS = "remarks";

public static final String COL_ADD_TIME = "add_time";

public static final String COL_OPERATION = "operation";

public static final String COL_OPERATION_TIME = "operation_time";

public static final String COL_LOGICAL_STATE = "logical_state";

public static final String COL_M_ACC_ID = "m_acc_id";
}