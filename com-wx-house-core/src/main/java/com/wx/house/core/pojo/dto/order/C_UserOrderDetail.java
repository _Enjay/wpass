package com.wx.house.core.pojo.dto.order;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:26
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class C_UserOrderDetail {

    @ApiModelProperty("待缴费id")
    private String eCostId;

    @ApiModelProperty("总计费用")
    private String total;
}
