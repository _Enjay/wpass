package com.wx.house.core.pojo.vo.expenseStatistics;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 04:07
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseYearStatisticsTotalVo {
@JsonSerialize(using = ToStringSerializer.class)
@ApiModelProperty("总计收费")
private BigDecimal totalExpense;

public BigDecimal getTotalExpense() {
	return this.getExpenseYearStatisticsVoList().stream().map(ExpenseYearStatisticsVo::getExpenditure).reduce(BigDecimal.ZERO, BigDecimal::add);
}

public BigDecimal getTotalRefund() {
	return this.getExpenseYearStatisticsVoList().stream().map(ExpenseYearStatisticsVo::getIncome).reduce(BigDecimal.ZERO, BigDecimal::add);
}

@JsonSerialize(using = ToStringSerializer.class)
@ApiModelProperty("总计退费")
private BigDecimal totalRefund;

private List<ExpenseYearStatisticsVo> expenseYearStatisticsVoList = new ArrayList<>();

}
