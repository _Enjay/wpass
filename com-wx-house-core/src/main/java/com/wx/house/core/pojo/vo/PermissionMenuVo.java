package com.wx.house.core.pojo.vo;

import lombok.Data;

@Data
public class PermissionMenuVo {
	private String id;
	private String pId;
	private String menuName;
	private String url;
	private String permissionId;
}
