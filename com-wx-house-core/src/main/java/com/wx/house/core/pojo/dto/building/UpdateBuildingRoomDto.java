package com.wx.house.core.pojo.dto.building;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 03:46
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public
class UpdateBuildingRoomDto {
@ApiModelProperty(value = "id", example = "a583c75c333048979711f9ac05674491", required = true)
@NotBlank
private String id;
@ApiModelProperty(value = "楼层", example = "6", required = false)
private String floorId;
@ApiModelProperty(value = "门牌", example = "601", required = false)
private String doorNumber;
@ApiModelProperty(value = "房屋面积 m³", example = "68.33", required = false)
private BigDecimal roomSpace;
}
