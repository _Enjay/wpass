package com.wx.house.core.pojo.vo.checkInStatistics;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 下午 05:14
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckInStatisticsVo {
private List<String> monthList = new LinkedList<>();
private List<Integer> validTenantNumberList = new LinkedList<>();
}
