package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.core.pojo.vo
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 14:36
 * @Version V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EPaymentVo {
    /**
     * 支付方式id
     */
    @ApiModelProperty(value = "支付方式id")
    private String id;

    /**
     * 支付id
     */
    @ApiModelProperty(value = "支付id")
    private String paymentId;

    /**
     * 支付密钥
     */
    @ApiModelProperty(value = "支付密钥")
    private String paymentSecretkey;

    /**
     * 商户id
     */
    @ApiModelProperty(value = "商户id")
    private String merchantId;
    
    /**
     * 操作人
     */
    @ApiModelProperty(value = "操作人")
    private String operation;

    /**
     * 操作时间
     */
    @ApiModelProperty(value = "操作时间")
    private Date operationTime;



    /**
     * 字典id
     */
    @ApiModelProperty(value = "字典id")
    private Long dicId;

    /**
     * (字典名称)支付公司名称
     */
    @ApiModelProperty(value = "(字典名称)支付公司名称")
    private String dicName;

    /**
     * 使用状态 1 使用 2 停用
     */
    @ApiModelProperty(value = "使用状态 1 使用 2 停用")
    private Integer state;
}
