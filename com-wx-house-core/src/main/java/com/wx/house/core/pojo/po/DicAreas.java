package com.wx.house.core.pojo.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version:   ：V$version
 */

@Data
@TableName(value = "dic_areas")
@Builder
@AllArgsConstructor
public class DicAreas implements Serializable {
    /**
     * 区域id
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value="区域id")
    private Integer id;

    /**
     * 区域名称
     */
    @TableField(value = "areaname")
    @ApiModelProperty(value="区域名称")
    private String areaname;

    /**
     * 父级id
     */
    @TableField(value = "parentid")
    @ApiModelProperty(value="父级id")
    private Integer parentid;

    /**
     * 简称
     */
    @TableField(value = "shortname")
    @ApiModelProperty(value="简称")
    private String shortname;

    /**
     * 经度
     */
    @TableField(value = "lng")
    @ApiModelProperty(value="经度")
    private BigDecimal lng;

    /**
     * 纬度
     */
    @TableField(value = "lat")
    @ApiModelProperty(value="纬度")
    private BigDecimal lat;

    /**
     * 层级
     */
    @TableField(value = "level")
    @ApiModelProperty(value="层级")
    private Integer level;

    /**
     * 定位层级结构
     */
    @TableField(value = "position")
    @ApiModelProperty(value="定位层级结构")
    private String position;

    /**
     * 排序序号
     */
    @TableField(value = "sort")
    @ApiModelProperty(value="排序序号")
    private Integer sort;

    private static final long serialVersionUID = 1L;

    public static final String COL_ID = "id";

    public static final String COL_AREANAME = "areaname";

    public static final String COL_PARENTID = "parentid";

    public static final String COL_SHORTNAME = "shortname";

    public static final String COL_LNG = "lng";

    public static final String COL_LAT = "lat";

    public static final String COL_LEVEL = "level";

    public static final String COL_POSITION = "position";

    public static final String COL_SORT = "sort";
}