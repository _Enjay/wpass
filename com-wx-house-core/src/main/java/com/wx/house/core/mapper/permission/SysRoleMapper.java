package com.wx.house.core.mapper.permission;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wx.house.core.pojo.po.SysAdmin;
import com.wx.house.core.pojo.po.SysRole;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
	/***
	 * 根据角色名查询是否重复
	 */
	SysRole getRoleName(
			@Param("roleName") String roleName);
	/***
	 * 查询全部(模糊查询角色名，状态)
	 * @param id
	 * @param id
	 * @return
	 */
	List<SysRole> selctRole(
			@Param("roleName") String roleName,
			@Param("state") String state,
			@Param("maccType") String maccType);
	/***
	 * 分配角色查询(模糊查询角色名)
	 * @param id
	 * @param id
	 * @return
	 */
	List<SysRole> matchRole(
			@Param("roleName") String roleName);
	/***
	 * 删除角色（禁用）
	 * @param id
	 * @param state
	 * @return
	 */
	int DeleteRole(
			@Param("roleid") String roleid,
			@Param("state") Integer state);
	/***
	 * 删除角色（禁用）
	 * @param id
	 * @param state
	 * @return
	 */
	int DeleteDelType(
			@Param("roleid") String roleid);
	/***
	 * 查询全部(模糊查询角色名，状态)
	 * @param id
	 * @param id
	 * @return
	 */
	List<SysRole> AdminRole(
			@Param("adminId") String adminId);
	
}