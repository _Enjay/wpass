package com.wx.house.core.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/15 上午 09:44
 * @description：
 * @version: ：V
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractPlanDetailVo {

@ApiModelProperty(value = "计划收费时间")
private Date planChargeTime;

@ApiModelProperty(value = "缴费状态: 0已缴费 1待缴费 2未缴清")
private Integer payed;

@ApiModelProperty(value = "缴费时间")
private Date payTime;

@ApiModelProperty(value = "待缴费类型: 1房租 2水费 3电费 4气费 5物管费   [房租费固定,所以可以分批缴费] [水电费根据抄表数据来生成]")
private String payType;

@ApiModelProperty(value = "房租价格")
private BigDecimal rentPrice;

@ApiModelProperty(value = "收到房租")
private BigDecimal rentActual;

@ApiModelProperty(value = "物管费价格")
private BigDecimal propertyManagerPrice;

@ApiModelProperty(value = "收到物管费")
private BigDecimal propertyManagerActual;

@ApiModelProperty(value = "水电气费用")
private BigDecimal routinePrice;

@ApiModelProperty(value = "收到水电气费用")
private BigDecimal routineActual;
}
