package com.wx.house.core.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TOrderBuildingVo {

	@TableField(value = "contr_id")
    @ApiModelProperty("合同id")
    private String contractId;

	@TableField(value = "contr_name")
    @ApiModelProperty("合同名字")
    private String contractName;

    @ApiModelProperty("公寓地址")
    private String buildingAddress;

	@TableField(value = "build_name")
    @ApiModelProperty("公寓名称")
    private String buildingName;

    @TableField(value = "bbr_floor_id")
    @ApiModelProperty(value = "公寓楼层")
    private String buildingroomfioorid;

    @TableField(value = "bbr_door_number")
    @ApiModelProperty(value = "公寓门牌号")
    private String buildingdoornumber;

    @ApiModelProperty("公寓图片")
    private String buildingImg;

    @ApiModelProperty("该合同共计需要缴费多少")
    private BigDecimal expense;

    @ApiModelProperty(value = "水费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildWater;

    @ApiModelProperty(value = "电费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildElectric;

    @ApiModelProperty(value = "气费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildGas;

    @ApiModelProperty(value = "物管费单价", hidden = true)
    @JsonIgnore
    private BigDecimal buildProperty;

    @ApiModelProperty(value = "当月水用量", hidden = true)
    @JsonIgnore
    private BigDecimal thisMonthWater;

    public BigDecimal getThisMonthWater() {
        List<TOrderExpenseVo> collect = this.tOrderExpenseVoList.stream().filter(e -> e.getExpenseType().equals("水费")).collect(Collectors.toList());
        if (collect.size() == 0) return new BigDecimal(0);
        TOrderExpenseVo tOrderExpenseVo = collect.get(0);
        return tOrderExpenseVo.getTotalExpense().divide(this.getBuildWater());
    }


    @ApiModelProperty(value = "当月电用量", hidden = true)
    @JsonIgnore
    private BigDecimal thisMonthElectric;

    public BigDecimal getThisMonthElectric() {
        List<TOrderExpenseVo> collect = this.tOrderExpenseVoList.stream().filter(e -> e.getExpenseType().equals("电费")).collect(Collectors.toList());
        if (collect.size() == 0) return new BigDecimal(0);
        TOrderExpenseVo tOrderExpenseVo = collect.get(0);
        return tOrderExpenseVo.getTotalExpense().divide(this.getBuildElectric());
    }

    @ApiModelProperty(value = "当月气用量", hidden = true)
    @JsonIgnore
    private BigDecimal thisMonthGas;

    public BigDecimal getThisMonthGas() {
        List<TOrderExpenseVo> collect = this.tOrderExpenseVoList.stream().filter(e -> e.getExpenseType().equals("气费")).collect(Collectors.toList());
        if (collect.size() == 0) return new BigDecimal(0);
        TOrderExpenseVo tOrderExpenseVo = collect.get(0);
        return tOrderExpenseVo.getTotalExpense().divide(this.getBuildGas());
    }

    @ApiModelProperty(value = "当月物管费", hidden = true)
    @JsonIgnore
    private BigDecimal thisMonthProperty;

    public BigDecimal getThisMonthProperty() {
        return this.buildProperty;
    }


    public BigDecimal getExpense() {
        //统计该月，该合同，总计需要缴费多少
        if (tOrderExpenseVoList.size() > 0) {
            BigDecimal total = new BigDecimal(0);
            List<BigDecimal> expenseList = tOrderExpenseVoList.stream().map(TOrderExpenseVo::getTotalExpense).collect(Collectors.toList());
            for (BigDecimal e : expenseList) {
                total = total.add(e);
            }
            return total;
        }
        return expense;
    }

    @ApiModelProperty("真正缴费的详情")
    private List<TOrderExpenseVo> tOrderExpenseVoList = new ArrayList<>();

}
