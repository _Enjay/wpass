package com.wx.house.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

/**
 * @Author Json工具类
 * @description：
 * @version: ：V
 */
public class MapUtil {

/**
 * 反射Mapper  数据对象
 *
 * @param map
 * @param clazz
 * @return
 * @throws Exception
 */
public static <T> T mapToPojo(Map map, Class<T> clazz) {
	return new ObjectMapper().convertValue(map, clazz);
}
}
