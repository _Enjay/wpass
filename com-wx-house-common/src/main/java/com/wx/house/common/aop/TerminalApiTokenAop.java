package com.wx.house.common.aop;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.JwtUtils;
import io.jsonwebtoken.Claims;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 下午 02:58
 * @description：
 * @version: ：V
 */
@Service
@Aspect
public class TerminalApiTokenAop {

@Before("execution(public * com.wx.house.terminal.api.controller.*.*Controller.*(..))")
public void check() {
	HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
	Claims claims = null;
	try {
		claims = JwtUtils.builder().token(request.getHeader("authorization")).build().claims();
	} catch (Exception ej) {
		throw new BusinessException("登陆信息异常，请重新登陆");
	}
	System.out.println(claims);
}

public static void main(String[] args) {
	//0010 0100  1000
	System.out.println(1 << 4);
	
	//0001 0010 0100 1000 1111
}
}
