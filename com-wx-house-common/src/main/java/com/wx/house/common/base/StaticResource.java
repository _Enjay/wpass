package com.wx.house.common.base;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 上午 11:40
 * @description：
 * @version: ：V
 */
@Data
@Component
@ConfigurationProperties(prefix = "static-resource")
public class StaticResource {
/**
 * 服务器静态资源路径
 */
private String staticResourceUrl;


/**
 * 服务器静态资源存储路径
 */
private String staticResourcePath;

/**
 * 
 */
private String weixinpaySpbillCreateIp;
/**
 * 
 */
private String weixinpayNotifyUrl;
}
