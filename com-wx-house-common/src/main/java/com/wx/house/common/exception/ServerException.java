package com.wx.house.common.exception;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/25 下午 02:42
 * @description：
 * @version: ：V
 */
public class ServerException extends RuntimeException {
public ServerException() {
	super();
}

public ServerException(String message) {
	super(message);
}

public ServerException(String message, Throwable cause) {
	super(message, cause);
}

public ServerException(Throwable cause) {
	super(cause);
}

@Override
public String getMessage() {
	return super.getMessage();
}

@Override
public void printStackTrace() {
	super.printStackTrace();
}
}
