package com.wx.house.common.aop;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.exception.UserException;
import com.wx.house.core.pojo.base.Result;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Objects;

/**
 * @author yangxiusen 2019年7月18日
 */
@EnableAspectJAutoProxy(proxyTargetClass = true)
@Aspect
@Configuration
@Order(Ordered.LOWEST_PRECEDENCE)
public class PageHelperAspect extends BaseController {
	/**
	 * 此处的切点是注解的方式，也可以用包名的方式达到相同的效果 '@Pointcut("execution(*
	 * com.wwj.springboot.service.impl.*.*(..))")'
	 */
	@Pointcut("execution(* *..*controller..*.*(..)) && @annotation(com.wx.house.common.annotations.Paging)")
	public void pointcut() {
	}
	@Around(value = "pointcut()")
	public Object arround(ProceedingJoinPoint joinPoint) {
		HttpServletRequest request = ((ServletRequestAttributes) Objects
				.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
		int page = 1;
		int row = 10;
		if (StringUtils.isNotBlank(request.getParameter("page"))) {
			page = Integer.parseInt(request.getParameter("page"));
		}
		if (StringUtils.isNotBlank(request.getParameter("pageSize"))) {
			row = Integer.parseInt(request.getParameter("pageSize"));
		}
		PageHelper.startPage(page, row);
		Object o = null;
		PageInfo<Object> pageInfo = null;
		Result rs = null;
		try {
			o = joinPoint.proceed();
			rs = (Result)o;
			List<Object> list = (List<Object>) rs.getData();
			pageInfo = new PageInfo<Object>(list);
		} catch (Throwable e) {
			ExceptionHander(e);
		}
		return Result.success(rs.getMsg(),pageInfo);
	}
	private void ExceptionHander(Throwable e) {
		String msg="";
		if(e instanceof UserException) {
			msg=((UserException)e).toString()+"---"+((UserException)e).getMessage();
		}else {
			msg=e.toString()+"---"+e.getMessage();
		}
		throw new UserException(msg,602);
	}
//  @SuppressWarnings("unused")
//	@Before(value = "pointcut()")
//  public void pagehelper(JoinPoint joinPoint) {
//  	System.out.println("进入分页");
//  	HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
//  	String page = request.getParameter("page");
//  	String row = request.getParameter("row");
//  	//String nameString = joinPoint.getSignature().getName();
//  	//Object[] argsObjects = joinPoint.getArgs();
//  	PageHelper.startPage(Integer.parseInt(page),Integer.parseInt(row));
//  }
//    /**
//     * 处理完请求，返回内容
//     * @param ret
//     */
//    @AfterReturning(returning = "ret", pointcut = "operationLog()")
//    public void doAfterReturning(Object ret) {
//        System.out.println("方法的返回值 : " + ret);
//    }
//
//    /**
//     * 后置异常通知
//     */
//    @AfterThrowing("operationLog()")
//    public void throwss(JoinPoint jp){
//        System.out.println("方法异常时执行.....");
//    }
//
//
//    /**
//     * 后置最终通知,final增强，不管是抛出异常或者正常退出都会执行
//     */
//    @After("operationLog()")
//    public void after(JoinPoint jp){
//        System.out.println("方法最后执行.....");
//    }

}
