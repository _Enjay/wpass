package com.wx.house.common.config;

import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.MultipartConfigElement;
import java.io.File;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/07/08 下午 07:54
 * @description：
 * @version: ：V
 */
@Configuration
public class MultipartConfig {

@Bean
MultipartConfigElement multipartConfigElement() {
	MultipartConfigFactory factory = new MultipartConfigFactory();
	String location = System.getProperty("user.dir") + "/temp/";
	File tmpFile = new File(location);
	if (!tmpFile.exists()) {
		tmpFile.mkdirs();
	}
	
	factory.setLocation(location);
	return factory.createMultipartConfig();
}

}
