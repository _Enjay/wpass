package com.wx.house.common.assembly.alyidcard;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IDCardResult {
@ApiModelProperty("用户名称")
private String name;
@ApiModelProperty("身份证号")
private String idNo;
@ApiModelProperty(hidden = true)
private String respMessage;
@ApiModelProperty(hidden = true)
private String respCode;

@ApiModelProperty("省份")
private String province;
@ApiModelProperty("城市")
private String city;
@ApiModelProperty("区县")
private String county;

@ApiModelProperty("生日")
private String birthday;

@ApiModelProperty("性别")
private String sex;

@ApiModelProperty("年龄")
private String age;
}
