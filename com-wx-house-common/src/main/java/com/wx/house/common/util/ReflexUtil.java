package com.wx.house.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @date ：Created in 2019/10/25 下午 04:07
 * @description：
 * @version: ：V
 */
public class ReflexUtil {
private String aa;

public static <T> Set<Field> ergodicFiled(T t) {
	Class<?> clazz = t.getClass();
	
	Set<Field> fieldList = new HashSet<>(Arrays.asList(clazz.getDeclaredFields()));
	while (clazz != null && !clazz.getName().toLowerCase().equals("java.lang.object")) {
		fieldList.addAll(Arrays.asList(clazz.getDeclaredFields()));
		clazz = clazz.getSuperclass();
	}
	
	return fieldList;
}

public static <T> List<Method> ergodicMethod(T t) {
	Class<?> clazz = t.getClass();
	return new ArrayList<>(Arrays.asList(clazz.getMethods()));
}

}
