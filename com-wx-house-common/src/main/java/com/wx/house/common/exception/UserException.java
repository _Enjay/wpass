package com.wx.house.common.exception;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/25 下午 02:42
 * @description：
 * @version: ：V
 */
public class UserException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private Integer code;

    public UserException(){
        super();
    }
    public UserException(String msg,Integer errorCode){
        super(msg);
        code=errorCode;
    }
    public Integer getCode() {
        return code;
    }

}
