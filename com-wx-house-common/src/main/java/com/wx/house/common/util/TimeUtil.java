package com.wx.house.common.util;

import com.wx.house.common.exception.BusinessException;
import org.omg.CORBA.ObjectHelper;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 日期时间-工具类
 *
 * @since 2015-16-30
 */
public class TimeUtil {

/**
 * 标准:yyyy-MM-dd HH:mm:ss
 */
public static String STANDARD = "yyyy-MM-dd HH:mm:ss";

private static final long ONE_MINUTE = 60000L;

private static final long ONE_HOUR = 3600000L;

private static final long ONE_DAY = 86400000L;


private static final long ONE_WEEK = 604800000L;


private static final String ONE_SECOND_AGO = "秒前";

private static final String ONE_MINUTE_AGO = "分钟前";

private static final String ONE_HOUR_AGO = "小时前";

private static final String ONE_DAY_AGO = "天前";

private static final String ONE_MONTH_AGO = "月前";

private static final String ONE_YEAR_AGO = "年前";

/***
 * 格式化时间为 几秒前 分钟前 小时前。。。。
 * @param date
 * @return
 */
public static String format(Date date) {
	long delta = new Date().getTime() - date.getTime();
	if (delta < ONE_MINUTE) {
		long seconds = toSeconds(delta);
		return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;
	}
	if (delta < 45L * ONE_MINUTE) {
		long minutes = toMinutes(delta);
		return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;
	}
	if (delta < 24L * ONE_HOUR) {
		long hours = toHours(delta);
		return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;
	}
	if (delta < 48L * ONE_HOUR) {
		return "昨天";
	}
	if (delta < 30L * ONE_DAY) {
		long days = toDays(delta);
		return (days <= 0 ? 1 : days) + ONE_DAY_AGO;
	}
	if (delta < 12L * 4L * ONE_WEEK) {
		long months = toMonths(delta);
		return (months <= 0 ? 1 : months) + ONE_MONTH_AGO;
	} else {
		long years = toYears(delta);
		return (years <= 0 ? 1 : years) + ONE_YEAR_AGO;
	}
}

private static long toSeconds(long date) {
	return date / 1000L;
}

private static long toMinutes(long date) {
	return toSeconds(date) / 60L;
}

private static long toHours(long date) {
	return toMinutes(date) / 60L;
}

private static long toDays(long date) {
	return toHours(date) / 24L;
}

private static long toMonths(long date) {
	return toDays(date) / 30L;
}

private static long toYears(long date) {
	return toMonths(date) / 365L;
}

/**
 * 获取格式化字符串
 *
 * @param date        日期
 * @param formatStyle 格式化样式
 * @return 格式化字符串
 */
public static String format(Date date, String formatStyle) {
	SimpleDateFormat time = new SimpleDateFormat(formatStyle);
	return time.format(date);
}

/**
 * 获取当前时间点格式化字符串
 *
 * @param formatStyle 格式化样式
 * @return 格式化字符串
 */
public static String format(String formatStyle) {
	SimpleDateFormat time = new SimpleDateFormat(formatStyle);
	return time.format(new Date());
}

public static Date parse(String timeStr, String formatStyle) {
	if (StringUtils.isEmpty(timeStr)) return null;
	SimpleDateFormat format = new SimpleDateFormat(formatStyle);
	Date date = null;
	try {
		date = format.parse(timeStr);
	} catch (Exception e) {
		return null;
	}
	return date;
}

public static Date parse(String timeStr) {
	return parse(timeStr, "yyyy-MM-dd");
}

/**
 * 计算前10天日期  2018-04-16，2018-04-10 。。。。。。
 *
 * @param year
 * @param month
 * @param day
 * @param beDay
 * @return
 */
public static String getCurrentDate(int year, int month, int day, int beDay) {
	GregorianCalendar newCal = new GregorianCalendar(year, month, day);
	long milSec = newCal.getTimeInMillis() - beDay * 24 * 3600 * 1000;
	GregorianCalendar other = new GregorianCalendar();
	other.setTimeInMillis(milSec);
	String newYear = String.valueOf(other.get(GregorianCalendar.YEAR));
	String newMonth = String.valueOf(other.get(GregorianCalendar.MONTH) + 1);
	newMonth = newMonth.length() == 1 ? "0" + newMonth : newMonth;
	String newDay = String.valueOf(other.get(GregorianCalendar.DAY_OF_MONTH));
	newDay = newDay.length() == 1 ? "0" + newDay : newDay;
	String date = newYear + "-" + newMonth + "-" + newDay;
	return date;
}

public static Date getCurrentQuarterStartTime() {
	Calendar c = Calendar.getInstance();
	int currentMonth = c.get(Calendar.MONTH) + 1;
	SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
	Date now = null;
	try {
		if (currentMonth <= 3)
			c.set(Calendar.MONTH, 0);
		else if (currentMonth <= 6)
			c.set(Calendar.MONTH, 3);
		else if (currentMonth <= 9)
			c.set(Calendar.MONTH, 4);
		else if (currentMonth <= 12)
			c.set(Calendar.MONTH, 9);
		c.set(Calendar.DATE, 1);
		now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");
	} catch (Exception e) {
		e.printStackTrace();
	}
	return now;
}

/**
 * 当前季度的结束时间，即2012-03-31 23:59:59
 *
 * @return
 */
public static Date getCurrentQuarterEndTime() {
	Calendar cal = Calendar.getInstance();
	cal.setTime(getCurrentQuarterStartTime());
	cal.add(Calendar.MONTH, 3);
	return cal.getTime();
}


public static Date getCurrentYearStartTime() {
	Calendar cal = Calendar.getInstance();
	cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
	cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.YEAR));
	return cal.getTime();
}

public static Date getCurrentYearEndTime() {
	Calendar cal = Calendar.getInstance();
	cal.setTime(getCurrentYearStartTime());
	cal.add(Calendar.YEAR, 1);
	return cal.getTime();
}

public static Date getLastYearStartTime() {
	Calendar cal = Calendar.getInstance();
	cal.setTime(getCurrentYearStartTime());
	cal.add(Calendar.YEAR, -1);
	return cal.getTime();
}


/**
 * @Description 获取时间相差几天
 * @Param startDate
 * @Param endDate
 * @Return java.lang.Long
 * @Author 罗棋
 * @Date 2019/08/14 下午 02:49
 */
public static Long getDaysBetween(Date startDate, Date endDate) {
	if (startDate == null || endDate == null) return 31L;
	Calendar fromCalendar = Calendar.getInstance();
	fromCalendar.setTime(startDate);
	fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
	fromCalendar.set(Calendar.MINUTE, 0);
	fromCalendar.set(Calendar.SECOND, 0);
	fromCalendar.set(Calendar.MILLISECOND, 0);
	
	Calendar toCalendar = Calendar.getInstance();
	toCalendar.setTime(endDate);
	toCalendar.set(Calendar.HOUR_OF_DAY, 0);
	toCalendar.set(Calendar.MINUTE, 0);
	toCalendar.set(Calendar.SECOND, 0);
	toCalendar.set(Calendar.MILLISECOND, 0);
	
	return (toCalendar.getTime().getTime() - fromCalendar.getTime().getTime()) / (1000 * 60 * 60 * 24);
}

/**
 * @Description 时间间隔字符串
 * @Param startTime
 * @Param endTime
 * @Return java.util.List<java.lang.String>
 * @Author 罗棋
 * @Date 2019/12/17 下午 05:32
 */
public static List<String> createTimeList(String startTime, String endTime) {
	Date startDate = null;
	try {
		startDate = new SimpleDateFormat("yyyy-MM").parse(startTime);
	} catch (ParseException e) {
	}
	Date endDate = null;
	try {
		endDate = new SimpleDateFormat("yyyy-MM").parse(endTime);
	} catch (ParseException e) {
	}
	
	//如果开始时间大于结束时间则会陷入死循环
	if (startDate.after(endDate))
		throw new BusinessException("开始时间不能大于结束时间");
	
	List<String> resultList = new ArrayList<>();
	
	//如果开始时间等于结束时间
	if (startTime.equals(endTime)) {
		resultList.add(startTime);
		return resultList;
	}
	
	Calendar calendar = Calendar.getInstance();
	Date tempDate = startDate;
	
	calendar.setTime(tempDate);
	while (true) {
		resultList.add(new SimpleDateFormat("yyyy-MM").format(calendar.getTime()));
		calendar.add(Calendar.MONTH, 1);
		
		if (new SimpleDateFormat("yyyy-MM").format(calendar.getTime()).equals(endTime)) {
			resultList.add(endTime);
			break;
		}
	}
	
	return resultList;
}

public static List<String> createTimeListByYear(String year) {
	return createTimeList(year + "-01", year + "-12");
}
}
