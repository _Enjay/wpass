package com.wx.house.common.base;

import com.wx.house.common.util.IpHelp;
import com.wx.house.common.util.JwtUtils;
import com.wx.house.common.util.RedisUtil;
import com.wx.house.common.util.StringUtil;
import com.wx.house.core.pojo.base.UserInfo;
import io.jsonwebtoken.Claims;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/25 下午 04:14
 * @description：
 * @version: ：V
 */
@Validated
@Component
@RestController
public class BaseController {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    Environment environment;

    @Resource
    ProjectConfiguration projectConfiguration;

    public static final String VALIDATECODE = "VALIDATECODE:%s";

    /**
     * @Description 从请求头中获取token
     * @Param
     * @Return java.lang.String
     * @Author 罗棋
     * @Date 2019/10/29 上午 09:27
     */
    protected final String getToken() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        return request.getHeader("authorization");
    }

    /**
     * @Description 从请求头中token获取用户id
     * @Param
     * @Return java.lang.String
     * @Author 罗棋
     * @Date 2019/10/29 上午 09:27
     */
    protected final String getUserId() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        return JwtUtils.builder().token(request.getHeader("authorization")).build().claims().get("id").toString();
    }

    protected final String getUserPhone() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        return JwtUtils.builder().token(request.getHeader("authorization")).build().claims().get("phone").toString();
    }

    protected final String getMaccId() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        UserInfo userInfo = userInfo();

        if (userInfo.getAccountType().equals("1")) {
            return userInfo.getId();
        }
        return JwtUtils.builder().token(request.getHeader("authorization")).build().claims().get("mAccId").toString();
    }

    protected final UserInfo getMaccId1() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        UserInfo userInfo = userInfo();
        return userInfo;
	/*	if (userInfo.getAccountType().equals("1")) {
			return userInfo.getId();
		}
		return JwtUtils.builder().token(request.getHeader("authorization")).build().claims().get("mAccId").toString();*/
    }

    /**
     * @Description 从请求头中获取token的Claims
     * @Param
     * @Return io.jsonwebtoken.Claims
     * @Author 罗棋
     * @Date 2019/10/29 上午 09:27
     */
    protected final Claims getClaims() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        return JwtUtils.builder().token(request.getHeader("authorization")).build().claims();
    }

    protected final UserInfo userInfo() {
        return UserInfoUtil.userInfo();
    }

    /**
     * @Description 获取redis工具类
     * @Param
     * @Return com.wx.house.common.util.RedisUtil
     * @Author 罗棋
     * @Date 2019/10/29 上午 09:28
     */
    protected final RedisUtil redisUtil() {
        return this.redisUtil;
    }

    /**
     * @Description 获取配置文件
     * @Param server.port
     * @Return java.lang.String
     * @Author 罗棋
     * @Date 2019/10/29 上午 09:29
     */
    protected final String config(@NotBlank String path) {
        return this.environment.getProperty(path);
    }

    protected final String ip() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        return new IpHelp().getIpAddr(request);
    }


    protected final ProjectConfiguration globalConfig() {
        return this.projectConfiguration;
    }

    protected final boolean comparePassword(String userPassword, String dbPassword, Integer safeKey) {
        String HASHITERATIONS = this.config("common.HASHITERATIONS");
        if (StringUtil.isNotEmpty(HASHITERATIONS)) {
            HASHITERATIONS = "1024";
        }
        String s = new Sha256Hash(userPassword, safeKey.toString(), Integer.parseInt(HASHITERATIONS)).toString();
        return s.equals(dbPassword);
    }

    protected final String createPassword(String userPassword, Integer safeKey) {
        String HASHITERATIONS = this.config("common.HASHITERATIONS");
        if (StringUtil.isNotEmpty(HASHITERATIONS)) {
            HASHITERATIONS = "1024";
        }
        return new Sha256Hash(userPassword, safeKey.toString(), Integer.parseInt(HASHITERATIONS)).toString();
    }


}
