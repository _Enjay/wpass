package com.wx.house.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/25 下午 02:33
 * @description：
 * @version: ：V
 */
@Getter
@AllArgsConstructor
public class ExceptionType {
//前端导致异常
public final static int JSON_ERROR_EXCEPTION = 700;
public final static int PARAM_ERROR_EXCEPTION = 701;
public final static int MAX_UPLOAD_SIZE_EXCEPTION = 702;

//服务端异常
public final static int BUSINESS_EXCEPTION = 600;
public final static int EXCEPTION = 601;
public final static int SERVER_EXCEPTION = 602;
}
