package com.wx.house.common.assembly.alysms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/08/20 下午 04:55
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@Getter
@ToString
public enum SmsEnums {
	register("TP19111811");
private String template;

}
