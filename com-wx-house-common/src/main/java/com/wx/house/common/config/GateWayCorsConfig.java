package com.wx.house.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Component
@Configuration
public class GateWayCorsConfig {

@Bean
public CorsFilter corsFilter() {
	final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	final CorsConfiguration corsConfiguration = new CorsConfiguration();
	corsConfiguration.addAllowedHeader("*");
	corsConfiguration.addAllowedOrigin("*");
	corsConfiguration.addAllowedMethod("*");
	corsConfiguration.setMaxAge(3600L);
	//是否支持安全凭证
	corsConfiguration.setAllowCredentials(true);
	source.registerCorsConfiguration("/**", corsConfiguration);
	return new CorsFilter(source);
}
}
