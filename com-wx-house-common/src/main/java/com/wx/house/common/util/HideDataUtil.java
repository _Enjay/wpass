package com.wx.house.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.stream.IntStream;

/**
 * @date ：Created in 2019/11/29 下午 05:40
 * @description：
 * @version: ：V
 */
public class HideDataUtil {
/**
 * 方法描述 隐藏银行卡号中间的字符串（使用*号），显示前四后四
 *
 * @param cardNo
 * @return
 * @author 罗棋
 * @date 2019年11月29日 上午10:38:51
 */
public static String hideCardNo(String cardNo) {
	if (StringUtils.isBlank(cardNo)) {
		return cardNo;
	}
	
	int length = cardNo.length();
	int beforeLength = 4;
	int afterLength = 4;
	//替换字符串，当前使用“*”
	String replaceSymbol = "*";
	StringBuffer sb = new StringBuffer();
	IntStream.range(0, length).forEachOrdered(i -> {
		if (i < beforeLength || i >= (length - afterLength)) {
			sb.append(cardNo.charAt(i));
		} else {
			sb.append(replaceSymbol);
		}
	});
	
	return sb.toString();
}

/**
 * 方法描述 隐藏手机号中间位置字符，显示前三后三个字符
 *
 * @param phoneNo
 * @return
 * @author 罗棋
 * @date 2019年11月29日 上午10:38:51
 */
public static String hidePhoneNo(String phoneNo) {
	if (StringUtils.isBlank(phoneNo)) {
		return phoneNo;
	}
	
	int length = phoneNo.length();
	int beforeLength = 3;
	int afterLength = 3;
	//替换字符串，当前使用“*”
	String replaceSymbol = "*";
	StringBuilder sb = new StringBuilder();
	IntStream.range(0, length).forEachOrdered(i -> {
		if (i < beforeLength || i >= (length - afterLength)) {
			sb.append(phoneNo.charAt(i));
		} else {
			sb.append(replaceSymbol);
		}
	});
	return sb.toString();
}
}
