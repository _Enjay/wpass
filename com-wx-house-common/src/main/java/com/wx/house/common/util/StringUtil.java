package com.wx.house.common.util;

import org.springframework.lang.Nullable;

import java.util.*;

/**
 * @date ：Created in 2019/10/25 下午 03:52
 * @description：
 * @version: ：V
 */
public class StringUtil {

public static List strToList(String str) {
	return StringUtil.strToList(str, ",");
}


public static List strToList(String str, String cutChar) {
	return new ArrayList<>(Arrays.asList(str.split(cutChar)));
}

public static String listToStr(List<String> strList, String cutChar) {
	return String.join(cutChar, strList);
}

public static String listToStr(List<String> strList) {
	return String.join(",", strList);
}

public static <T> void outPrint(@Nullable List<T> objList) {
	Iterator<T> iterator = objList.iterator();
	for (; iterator.hasNext(); ) {
		T next = iterator.next();
		System.out.println(next);
	}
}

public static <T> void outPrint(@Nullable Set<T> objList) {
	Iterator<T> iterator = objList.iterator();
	for (; iterator.hasNext(); ) {
		T next = iterator.next();
		System.out.println(next);
	}
}

public static <T> void errPrint(@Nullable List<T> objList) {
	Iterator<T> iterator = objList.iterator();
	for (; iterator.hasNext(); ) {
		T next = iterator.next();
		System.err.println(next);
	}
}

public static <T> void errPrint(@Nullable Set<T> objList) {
	Iterator<T> iterator = objList.iterator();
	for (; iterator.hasNext(); ) {
		T next = iterator.next();
		System.err.println(next);
	}
}


public static boolean isEmpty(@Nullable Object str) {
	return str == null || "".equals(str);
}

public static boolean isNotEmpty(@Nullable Object str) {
	return str != null && !"".equals(str);
}

public static boolean hasLength(@Nullable CharSequence str) {
	return str != null && str.length() > 0;
}

public static boolean hasLength(@Nullable String str) {
	return str != null && !str.isEmpty();
}

public static boolean hasText(@Nullable CharSequence str) {
	return str != null && str.length() > 0 && containsText(str);
}

public static boolean hasText(@Nullable String str) {
	return str != null && !str.isEmpty() && containsText(str);
}

private static boolean containsText(CharSequence str) {
	int strLen = str.length();
	
	for (int i = 0; i < strLen; ++i) {
		if (!Character.isWhitespace(str.charAt(i))) {
			return true;
		}
	}
	
	return false;
}

public static boolean containsWhitespace(@Nullable CharSequence str) {
	if (!hasLength(str)) {
		return false;
	} else {
		int strLen = str.length();
		
		for (int i = 0; i < strLen; ++i) {
			if (Character.isWhitespace(str.charAt(i))) {
				return true;
			}
		}
		
		return false;
	}
}
}
