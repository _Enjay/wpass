package com.wx.house.common.base;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.stereotype.Component;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 上午 11:15
 * @description：
 * @version: ：V
 */
@Component
@Data
@ConfigurationProperties(prefix = "wx-house-config")
@EnableConfigurationProperties({StaticResource.class})
public class ProjectConfiguration {

/**
 * 静态资源相关
 */
@NestedConfigurationProperty
private StaticResource staticResource;


}
