package com.wx.house.common.assembly.alyidcard;

import com.google.common.primitives.Bytes;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.AliyunHttpUtils;
import com.wx.house.common.util.JsonUtils;
import com.wx.house.common.util.RedisUtil;
import com.wx.house.common.util.StringUtil;
import net.sf.json.JSON;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import springfox.documentation.spring.web.json.Json;

import javax.annotation.Resource;

import java.net.URLEncoder;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class IDCardUtil extends BaseController {
	private static final String SEND = "ALY:ID_CARD:SEND:%s";
	@Resource
	RedisUtil redisUtil;
	@Resource
	private ProjectConfiguration projectConfiguration;

//身份验证
	private String host = "https://idenauthen.market.alicloudapi.com";

	private String path = "/idenAuthentication";

	private String method = "POST";

	private String appcode = "310b0912c8314978a43865e1e0f854cc";

//图像识别OCR
	private String Ocrhost = "https://ocridcard.market.alicloudapi.com";

	private String Ocrpath = "/idCardAuto";

	private String Ocrmethod = "POST";

	private String Ocrappcode = "310b0912c8314978a43865e1e0f854cc";

	Map<String, String> headers = new HashMap<>();

	Map<String, String> params = new HashMap<>();

	Map<String, String> bodys = new HashMap<>();

	Map<String, String> querys = new HashMap<>();

	/***
	 * 根据身份证姓名实名检验
	 * 
	 * @param idCard
	 * @param name
	 * @return
	 */
	public IDCardResult validated(String idCard, String name) {

		if (StringUtil.isEmpty(idCard))
			throw new BusinessException("身份证不能为空");
		if (StringUtil.isEmpty(name))
			throw new BusinessException("姓名不能为空");

		// Authorization
		headers.put("Authorization", "APPCODE " + appcode);
		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

		// 请求参数
		bodys.put("idNo", idCard);
		bodys.put("name", name);

		IDCardResult idCardResult = null;
		try {
			HttpResponse httpResponse = AliyunHttpUtils.doPost(host, path, method, headers, params, bodys);
			idCardResult = JsonUtils.jsonToPojo(EntityUtils.toString(httpResponse.getEntity()), IDCardResult.class);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (idCardResult.getRespCode().equals("0000")) {
			redisUtil.set(String.format(SEND, idCardResult.getIdNo()), JsonUtils.objectToJson(idCardResult), 600L,
					TimeUnit.SECONDS);
			return idCardResult;
		}
		if (idCardResult.getRespCode().equals("0001"))
			throw new BusinessException("姓名不能为空");
		if (idCardResult.getRespCode().equals("0002"))
			throw new BusinessException("姓名不能包含特殊字符");
		if (idCardResult.getRespCode().equals("0003"))
			throw new BusinessException("身份证号不能为空");
		if (idCardResult.getRespCode().equals("0004"))
			throw new BusinessException("身份证号格式错误");
		if (idCardResult.getRespCode().equals("0007"))
			throw new BusinessException("无此身份证号码");
		if (idCardResult.getRespCode().equals("0008"))
			throw new BusinessException("身份证信息不匹配");
		if (idCardResult.getRespCode().equals("0010"))
			throw new BusinessException("阿里云身份证服务维护中，请稍后再试");
		return null;
	}
	//api链接：https://market.aliyun.com/products/57124001/cmapi028676.html?spm=5176.730005.productlist.d_cmapi028676.742a3524DUsnb5&innerSource=search_%E8%BA%AB%E4%BB%BD%E8%AF%81OCR%E8%AF%86%E5%88%AB#sku=yuncode2267600005
	public IDocrResult OcrValidated(byte[] bytes) {
		IDocrResult idr = new IDocrResult();
		// 最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
		headers.put("Authorization", "APPCODE " + Ocrappcode);
		// 根据API的要求，定义相对应的Content-Type
		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		Map<String, String> bodys = new HashMap<String, String>();
		// bodys.put("image", "http://"+img);
		bodys.put("image", "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(bytes));
		try {
			HttpResponse response = HttpUtils.doPost(Ocrhost, Ocrpath, Ocrmethod, headers, querys, bodys);
			String result = EntityUtils.toString(response.getEntity());
			idr = JsonUtils.jsonToPojo(result, IDocrResult.class);
			String state = String.valueOf(idr.getCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idr;
	}
	public IDocrResult OcrValidatedBase64(String bytes) {
		IDocrResult idr = new IDocrResult();
		// 最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
		headers.put("Authorization", "APPCODE " + Ocrappcode);
		// 根据API的要求，定义相对应的Content-Type
		headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		Map<String, String> bodys = new HashMap<String, String>();
		// bodys.put("image", "http://"+img);
		bodys.put("image",bytes);
		try {
			HttpResponse response = HttpUtils.doPost(Ocrhost, Ocrpath, Ocrmethod, headers, querys, bodys);
			String result = EntityUtils.toString(response.getEntity());
			idr = JsonUtils.jsonToPojo(result, IDocrResult.class);
			String state = String.valueOf(idr.getCode());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idr;
	}
}
