package com.wx.house.common.exception;

/**
 * @date ：Created in 2019/10/25 下午 02:42
 * @description：
 * @version: ：V
 */
public class BusinessException extends RuntimeException {
public BusinessException() {
	super();
}

public BusinessException(String message) {
	super(message);
}

public BusinessException(String message, Throwable cause) {
	super(message, cause);
}

public BusinessException(Throwable cause) {
	super(cause);
}

@Override
public String getMessage() {
	return super.getMessage();
}

@Override
public void printStackTrace() {
	super.printStackTrace();
}
}
