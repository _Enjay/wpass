package com.wx.house.common.assembly.alyidcard;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IDocrResult {
	private String code;
	private String msg;
	private Object result;
}
