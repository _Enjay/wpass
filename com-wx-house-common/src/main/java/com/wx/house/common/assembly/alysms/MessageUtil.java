package com.wx.house.common.assembly.alysms;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.AliyunHttpUtils;
import com.wx.house.common.util.JsonUtils;
import com.wx.house.common.util.RedisUtil;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Component
public class MessageUtil {
@Resource
RedisUtil redisUtil;
private static final String SEND = "ALY:SMS:SEND:%s";
private static final String SEND_NUMBER = "ALY:SMS:SEND_NUMBER:%s";
public static final String SEND_CODE = "ALY:SMS:CODE:%s";
private String host = "http://dingxin.market.alicloudapi.com";

private String path = "/dx/sendSms";

private String method = "POST";

private String appcode = "310b0912c8314978a43865e1e0f854cc";


Map<String, String> headers = new HashMap<>();

Map<String, String> params = new HashMap<>();

Map<String, String> bodys = new HashMap<>();


private String template = SmsEnums.register.getTemplate();


private String getCode() {
	return "code:" + (int) ((Math.random() * 9 + 1) * 100000);
}

private void setTemplate(SmsEnums smsEnums) {
	this.template = smsEnums.getTemplate();
}


public void send(String phone) {//验证此手机号是否可以发送，验证次手机号是否已发送上限
	Matcher matcher = Pattern.compile("^1[3|4|5|6|7|8|9]\\d{9}+$").matcher(phone);
	if (!matcher.find()) throw new BusinessException("手机号格式不正确");
	
	//Authorization
	headers.put("Authorization", "APPCODE " + appcode);
	
	//1分钟只能发送一次
	String s = redisUtil.get(String.format(SEND, phone));
	if (s != null)
		throw new BusinessException("验证码发送过于频繁，请稍后");
	
	//15分钟最多发送3次
	String s1 = redisUtil.get(String.format(SEND_NUMBER, phone));
	if (!StringUtils.isEmpty(s1))
		if (Integer.parseInt(redisUtil.get(String.format(SEND_NUMBER, phone))) >= 3)
			throw new BusinessException("验证码发送过于频繁，请稍后");
	
	//随机验证码
	String code = this.getCode();
	
	//请求参数
	params.put("mobile", phone);
	params.put("param", code);
	params.put("tpl_id", this.template);
	SmsResult smsResult = null;
	try {
		HttpResponse httpResponse = AliyunHttpUtils.doPost(host, path, method, headers, params, bodys);
		smsResult = JsonUtils.jsonToPojo(EntityUtils.toString(httpResponse.getEntity()), SmsResult.class);
	} catch (Exception e) {
		e.printStackTrace();
	}
	assert smsResult != null;
	switch (smsResult.getReturn_code()) {
		case "00000":
			redisUtil.set(String.format(SEND_CODE, phone), code.split(":")[1], 600L, TimeUnit.SECONDS);
			//开始发送计时，过了60秒才能发送
			redisUtil.set(String.format(SEND, phone), "0", 60L, TimeUnit.SECONDS);
			int num = 0;
			try {
				num = Integer.parseInt(redisUtil.get(String.format(SEND_NUMBER, phone))) + 1;
			} catch (Exception ignored) {
			}
			
			redisUtil.set(String.format(SEND_NUMBER, phone), String.valueOf(num), 900L, TimeUnit.SECONDS);
			break;
		case "10001":
			throw new BusinessException("手机号格式错误");
		case "10002":
		case "10003":
		case "10004":
		case "10005":
		case "10006":
			throw new BusinessException("阿里云短信设置错误(模板，配置)");
		case "10008":
		case "10009":
		case "10010":
		case "99999":
			throw new BusinessException("阿里云服务器网络异常");
		case "10007":
			throw new BusinessException("手机号查询不到归属地");
		default:
			throw new BusinessException("短信服务异常");
	}
}
}
