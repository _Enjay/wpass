package com.wx.house.terminal.api.client.service.impl;

import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.mapper.expenses.ECollectFeesMapper;
import com.wx.house.core.mapper.tenant.UserCustomMapper;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.vo.*;
import com.wx.house.terminal.api.client.service.IndexService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/21 上午 09:26
 * @description：
 * @version: ：V
 */
@Service
@RequiredArgsConstructor
public class IndexServiceImpl implements IndexService {
    private final UserCustomMapper userCustomMapper;
    private final ECollectFeesMapper eCollectFeesMapper;
    private final ProjectConfiguration projectConfiguration;

    @Override
    public TerminalIndexVo selectTerminalIndex(String tUserId) {
        //查询用户id，用户时间和名字，缴费金额，总费用，合同到期时间
        TerminalIndexVo terminalIndexVo = userCustomMapper.selectTerminalIndex(tUserId, new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        try {
            //判断已处理的缴费费用
            terminalIndexVo.setExpenseHistoryVoList(userCustomMapper.selectPayHistory(tUserId));
        } catch (Exception e) {
            e.getMessage();
        }

        return terminalIndexVo;
    }

    @Override
    public List<IndexContractVo> selectIndexContractVo(String tUserId, String state) {
        return userCustomMapper.selectIndexContractVo(tUserId, state);
    }

    @Override
    public TUserIDCardVo selectTUserIDCardVo(String tUserId) {
        TUserIDCardVo tUserIDCardVo = userCustomMapper.selectTUserIDCardVo(tUserId);
        String staticResourceUrl = projectConfiguration.getStaticResource().getStaticResourceUrl();
        tUserIDCardVo.setIdCard(staticResourceUrl + tUserIDCardVo.getIdCard());
        tUserIDCardVo.setIdCard_r(staticResourceUrl + tUserIDCardVo.getIdCard_r());
        return userCustomMapper.selectTUserIDCardVo(tUserId);
    }

    @Override
    public List<ECollectFees> selectExpense(String mouth, String userId) {
        return eCollectFeesMapper.selectExpense(mouth, userId);
    }

    @Override
    public List<ECollectFeesDetailVo> selectExpenseDetail(String mouth, String userId) {
        return eCollectFeesMapper.selectExpenseDetail(mouth, userId);
    }

    @Override
    public List<TenantPendingMonthPaymentVo> selectTenantPendingPaymentVo(String userId) {
        return userCustomMapper.selectTenantPendingPaymentVo(userId);
    }

    /**
     * app首页-待缴收费基本信息实现
     *
     * @param userId
     * @return
     */
    @Override
    public List<TenantPendingMonthPaymentVo> selectTenantPendingIndexPayFees(String userId) {
        return userCustomMapper.selectTenantPendingPaymentVo(userId);
    }

}
