package com.wx.house.terminal.api.client.service;

import com.wx.house.core.pojo.dto.tenant.IdCardValidDto;
import com.wx.house.core.pojo.vo.TUserContractVo;
import com.wx.house.core.pojo.vo.TUserIDCardVo;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/21 上午 09:26
 * @description：
 * @version: ：V
 */
public interface UserService {

    /**
     * 查询用户实名信息
     * @param userId
     * @return
     */
    TUserIDCardVo selectTUserIDCardVo(String userId);

    /**
     * 修改用户提交实名认证
     * @param idCardValidDto
     * @param userId
     */
    void updateUserIdCard(IdCardValidDto idCardValidDto, String userId);

    //查询用户合同详情
    TUserContractVo selectTenantContractDetail(String contractId);

}
