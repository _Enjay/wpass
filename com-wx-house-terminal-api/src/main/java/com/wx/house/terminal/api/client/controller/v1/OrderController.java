package com.wx.house.terminal.api.client.controller.v1;

import com.wx.house.common.annotations.Paging;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.pojo.base.BasePage;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.order.OrderParam;
import com.wx.house.core.pojo.vo.*;
import com.wx.house.terminal.api.client.service.IndexService;
import com.wx.house.terminal.api.client.service.TOrderDetailService;
import com.wx.house.terminal.api.client.service.TUserOrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:14 @description：
 * @version: ：V
 */
@RestController
@RequestMapping("tenant/v1.0.0/order")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "订单管理接口 [v.1.0.0]")
@Validated
public class OrderController extends BaseController {
	private final TUserOrderService tUserOrderService;
	private final TOrderDetailService trderDetailService;
	private final ProjectConfiguration projectConfiguration;
	private final IndexService indexService;

	@ApiOperation("用户申请创建订单")
	@PostMapping()
	public Result createOrder(@RequestBody @Validated OrderParam orderParam) {
		if (ObjectUtils.isEmpty(orderParam.getECostId())) {
			return Result.error("无订单提交");
		}
		tUserOrderService.createOrder(orderParam, this.getUserId());
		return Result.success("创建订单成功");
	}

	@ApiOperation("查询所有的订单")
	@GetMapping("selectallordermap/")
	@ApiImplicitParams({ @ApiImplicitParam(name = "state", value = "状态: 0待缴费 1已缴费 ", example = "0"), })
	@Paging
	public Result<List<TOrderCreateVos>> selectAllOrderMap(String state,BasePage page) {
		//查询list数组里的四个参数
		List<TOrderCreateVos> selectTOrderCreateAllVoFY = tUserOrderService.selectTOrderCreateAllVoFY(state, this.getUserId());
		StringBuilder str= new StringBuilder();
		//for循环list数组，根据id查找到todString里的具体数据
		for (TOrderCreateVos tOrderCreateVos : selectTOrderCreateAllVoFY) {
			if(str.toString().equals("")) {
				str = new StringBuilder(tOrderCreateVos.getTodString());
			}else {
				str.append(tOrderCreateVos.getTodString());
			}
		}
		// 查询所有订单信息-list里todString的数据
		List<TOrderCreateVos> orderCreateVos = tUserOrderService.selectDetailAllVo(str.toString());
		for (TOrderCreateVos TOrderCreateVos : selectTOrderCreateAllVoFY) {
			orderCreateVos.stream().filter(bean->{
				System.out.println(TOrderCreateVos.getId()+">>>"+bean.getId()+"____"+bean.getId().equals(TOrderCreateVos.getId()));
				if(bean.getId().equals(TOrderCreateVos.getId())) {
					TOrderCreateVos.setTOrderMonthAllVoList(bean.getTOrderMonthAllVoList().stream().filter(bean2 -> {
						bean2.getBammTorderTabelVos().stream().filter(bean3->{
							bean3.setUrl(projectConfiguration.getStaticResource().getStaticResourceUrl() + bean3.getUrl());
							return true;
						}).collect(Collectors.toList());
						return true;
					}).collect(Collectors.toList()));
				}
				return true;
			}).collect(Collectors.toList());
		}
		return Result.success(selectTOrderCreateAllVoFY);
	}

	@ApiOperation("取消订单")
	@DeleteMapping("{id}")
	public Result deleteOrder(@PathVariable("id") String id) {
		tUserOrderService.deleteOrder(id);
		return Result.success();
	}

	@ApiOperation("缴费订单详情")
	@GetMapping("detail/{id}")
	public Result<List<ECollectFeesDetailVo>> selectExpense(String mouth) {
		String userId = this.getUserId();
		List<ECollectFeesDetailVo> eCollectFeesDetailVos = indexService.selectExpenseDetail(mouth, userId);
		return Result.success(eCollectFeesDetailVos);
	}
}
