package com.wx.house.terminal.api.client.service.impl;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.core.mapper.tenant.TUserMapper;
import com.wx.house.core.pojo.dto.tenant.TenantCertificationUpDto;
import com.wx.house.core.pojo.po.TUser;
import com.wx.house.core.pojo.state.CertificationTypeEnum;
import com.wx.house.core.pojo.vo.TUserVo;
import com.wx.house.terminal.api.client.service.TenantSignService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class TenantSignServiceImpl implements TenantSignService {
private final TUserMapper tUserMapper;
private final AttaUploadMapper attaUploadMapper;

@Override
public TUserVo selectByPhoneNumber(String phoneNumber) {
	return tUserMapper.selectByPhoneNumber(phoneNumber);
}

@Override
public int insertSelective(TUser tUser) {
	return tUserMapper.insertSelective(tUser);
}

@Override
public int updatePassword(String userId, String newPassword,Integer safeKey) {
	return tUserMapper.updateById(TUser.builder().password(newPassword).safekey(safeKey.toString()).build(), userId);
}

@Override
public void updateCertification(TenantCertificationUpDto tenantCertificationUpDto, TUser signUser) {
	tUserMapper.updateById(TUser
			                       .builder()
			                       .name(tenantCertificationUpDto.getName())
			                       .idCardNumber(tenantCertificationUpDto.getIdCard())
			                       .idCardType(CertificationTypeEnum.ID_CARD.getType())
			                       .build(),
			signUser.getId());
	if (tenantCertificationUpDto.getFileList().size() <= 0) {
		throw new BusinessException("身份证附件不能为空");
	}
	attaUploadMapper.updateResourceIdByIdIn(signUser.getId(), tenantCertificationUpDto.getFileList());
}
}
