package com.wx.house.terminal.api.client.controller.v1;

import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.vo.IndexContractVo;
import com.wx.house.core.pojo.vo.TUserContractVo;
import com.wx.house.terminal.api.client.service.IndexService;
import com.wx.house.terminal.api.client.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.IntStream;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:31
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("tenant/v1.0.0/contract")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "合同接口 [v.1.0.0]")
@Validated
public class ContractController extends BaseController {
    private final IndexService indexService;
    private final UserService userService;
    private final ProjectConfiguration projectConfiguration;

    @ApiImplicitParam(name = "state", value = "状态: 0正常合同 1过期合同 不传-全部合同[默认]", required = false, example = "0")
    @ApiOperation("用户合同列表")
    @GetMapping
    public Result<PageInfo<IndexContractVo>> userContract(String state) {
        List<IndexContractVo> indexContractVos = indexService.selectIndexContractVo(this.getUserId(), state);
        for (IndexContractVo e : indexContractVos) {
            e.setIdCard(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getIdCard());
            e.setIdCard_r(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getIdCard_r());
            if (e.getContractFileList() != null)
                for (String file : e.getContractFileList()) {
                    file = projectConfiguration.getStaticResource().getStaticResourceUrl() + file;
                }
        }

        PageInfo<IndexContractVo> pageInfo = new PageInfo<>(indexContractVos);
        return Result.success(pageInfo);
    }

    @ApiImplicitParam(name = "contractId", value = "合同id", required = true, example = "6ee4b4c8d7074b8eb8abd17dc3ff0ff0")
    @ApiOperation("用户合同详情")
    @GetMapping("detail")
    public Result<TUserContractVo> userContractDetail(String contractId) {
        TUserContractVo tUserContractVo = userService.selectTenantContractDetail(contractId);
        tUserContractVo.setIdCard(projectConfiguration.getStaticResource().getStaticResourceUrl() + tUserContractVo.getIdCard());
        tUserContractVo.setIdCard_r(projectConfiguration.getStaticResource().getStaticResourceUrl() + tUserContractVo.getIdCard_r());
        IntStream
                .range(0, tUserContractVo.getContractFileList().size())
                .forEachOrdered(i -> tUserContractVo.getContractFileList().set(i, projectConfiguration.getStaticResource().getStaticResourceUrl() + tUserContractVo.getContractFileList().get(i)));
        return Result.success(tUserContractVo);
    }

}
