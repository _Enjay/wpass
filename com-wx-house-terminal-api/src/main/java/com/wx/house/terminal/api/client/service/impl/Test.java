package com.wx.house.terminal.api.client.service.impl;

import java.io.PrintStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/02 下午 12:24
 * @description：
 * @version: ：V
 */
public class Test {
//main方法下面编写代码使a输出100 b输出200
public static void main(String[] args) {
	int a = 10;
	int b = 10;
	method(a, b);
	
	System.out.println("a = " + a);
	System.out.println("b = " + b);
}

//原始method方法
private static void method(int a, int b) {
	MainProxy mainProxy = new MainProxy();
	Object o = Proxy.newProxyInstance(mainProxy.getClass().getClassLoader(), mainProxy.getClass().getInterfaces(), mainProxy);
	String s = o.toString();
}

//jdk动态代理-代理method方法
public static class MainProxy implements InvocationHandler {
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) {
		//重写print
		PrintStream p = new PrintStream(System.out) {
			@Override
			public void print(String s) {
				//正则表达式
				Matcher matcher = Pattern.compile("\\d+").matcher(s);
				while (matcher.find()) {
					String group = matcher.group();
					int i = Integer.parseInt(group);
					
					String o = s.replace(group, "");
					//位运算
					long result = Long.parseLong(String.valueOf(Long.parseLong(String.valueOf(i << 4 & 100)) << 1), 16);
					MainParamContextFactory mainParamContextFactory = new MainParamContextFactory();
					MainParamContext mainParamContext = mainParamContextFactory.produceStrategy(o);
					s = o + mainParamContext.run(result);
				}
				super.print(s);
			}
		};
		System.setOut(p);
		return null;
	}
}

//静态工厂
public static class MainParamContextFactory {
	MainParamContext produceStrategy(String o) {
		
		String path = this.getClass().getTypeName();
		path = path.replace("$MainParamContextFactory", "");
		
		try {
			String s = "$Param" + o.replace("=", "").toUpperCase().replace(" ", "") + "Strategy";
			Object o1 = Class.forName(path + s).newInstance();
			return new MainParamContext(o1.getClass());
		} catch (InstantiationException | ClassNotFoundException | IllegalAccessException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("还未实现策略:" + o);
	}
}

//策略模式
public static class MainParamContext {
	public MainParamContext(Class<?> sign) {
		try {
			this.paramAStrategy = (ParamStrategy) sign.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	private ParamStrategy paramAStrategy;
	
	public Integer run(long o) {
		return this.paramAStrategy.cont(o);
	}
}

//策略接口
public interface ParamStrategy {
	Integer cont(long o);
}

//具体策略
public static class ParamAStrategy implements ParamStrategy {
	@Override
	public Integer cont(long o) {
		return Integer.parseInt(String.valueOf(o));
	}
}

//具体策略
public static class ParamBStrategy implements ParamStrategy {
	@Override
	public Integer cont(long o) {
		return Integer.parseInt(String.valueOf(o)) * 2;
	}
}


}
