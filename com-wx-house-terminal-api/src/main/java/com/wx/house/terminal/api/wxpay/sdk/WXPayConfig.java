package com.wx.house.terminal.api.wxpay.sdk;

import java.io.InputStream;

public interface WXPayConfig {
    String getAppID();

    String getMchID();

    String getKey();

    InputStream getCertStream();

    int getHttpConnectTimeoutMs();

    int getHttpReadTimeoutMs();
}
