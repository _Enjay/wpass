package com.wx.house.terminal.api.client.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.dto.order.E_UserContractDto;
import com.wx.house.core.pojo.dto.order.OrderParam;
import com.wx.house.core.pojo.po.TOrder;
import com.wx.house.core.pojo.vo.TOrderCreateVo;
import com.wx.house.core.pojo.vo.TOrderCreateVos;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:23
 * @description：${description}
 * @version: ：V$version
 */
public interface TUserOrderService extends IService<TOrder> {
    //创建订单
    void createOrder(OrderParam orderParam, String userId);

	/**
	 * @param id
	 * 取消订单
	 */
    void deleteOrder(String id);


    // 查询所有订单信息-list里的数据
    List<TOrderCreateVos> selectTOrderCreateAllVoFY(String state, String userId);

    // 查询所有订单信息-list里todString的数据
    List<TOrderCreateVos> selectDetailAllVo(String todString);
}
