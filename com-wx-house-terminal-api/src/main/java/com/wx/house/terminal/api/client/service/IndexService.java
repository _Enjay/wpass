package com.wx.house.terminal.api.client.service;

import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.vo.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/21 上午 09:26
 * @description：
 * @version: ：V
 */
public interface IndexService {

    /**
     * 查询用户id，用户时间和名字，缴费金额，总费用，合同到期时间
     *
     * @param tUserId
     * @return
     */
    TerminalIndexVo selectTerminalIndex(String tUserId);

    /**
     * app查询合同列表
     * @param tUserId
     * @param state
     * @return
     */
    List<IndexContractVo> selectIndexContractVo(String tUserId, String state);

    TUserIDCardVo selectTUserIDCardVo(String tUserId);

    List<ECollectFees> selectExpense(String mouth, String userId);

    /**
     * 根据id和月份查询水电气和物管费
     * @param mouth
     * @param userId
     * @return
     */
    List<ECollectFeesDetailVo> selectExpenseDetail(String mouth, String userId);

    /**
     * 查询代缴费基本信息
     * @param userId
     * @return
     */
    List<TenantPendingMonthPaymentVo> selectTenantPendingPaymentVo(String userId);

    /**
     * app首页-待缴收费基本信息
     * @param userId
     * @return
     */
    List<TenantPendingMonthPaymentVo> selectTenantPendingIndexPayFees(String userId);

}
