package com.wx.house.terminal.api;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@ComponentScan({
		"com.wx.house.terminal.api",
		"com.wx.house.common",
		"com.wx.house.core"
})
@EnableTransactionManagement(proxyTargetClass = true)
@MapperScan("com.wx.house.core.mapper")
@EnableSwaggerBootstrapUI
@EnableScheduling
public class WxHouseTerminalApiApplication {

public static void main(String[] args) {
	SpringApplication.run(WxHouseTerminalApiApplication.class, args);
}
}
