package com.wx.house.terminal.api.wxpay.conf;

import com.wx.house.terminal.api.wxpay.sdk.WXPayConfig;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLDecoder;


public class WXConfigUtil implements WXPayConfig {
    private byte[] certData;
    public static final String APP_ID = "wx1533fe63d0e1c96d";//appid
    public static final String KEY = "3be86b6faf8829609de79d86537d788b";//秘钥5bda04b0ff38fa9ea5e6f9cd34d110d7
    public static final String MCH_ID = "1568110581";//商户id

    public WXConfigUtil() throws Exception {
        //String certPath = this.getClass().getResource("/").getPath()+"/cert/apiclient_cert.p12";//从微信商户平台下载的安全证书存放的路径
    	String certPath = "C:/root/wxcert/apiclient_cert.p12" ;
        certPath=URLDecoder.decode(certPath, "utf-8");
        System.out.println(certPath+"<<<<<<<<<<<<<<<<<<<<<<<");
        File file = new File(certPath);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    @Override
    public String getAppID() {
        return APP_ID;
    }

    //parnerid，商户号
    @Override
    public String getMchID() {
        return MCH_ID;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 10000;
    }
}