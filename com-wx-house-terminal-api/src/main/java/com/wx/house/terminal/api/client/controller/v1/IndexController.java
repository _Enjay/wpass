package com.wx.house.terminal.api.client.controller.v1;

import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.vo.ECollectFeesDetailVo;
import com.wx.house.core.pojo.vo.TenantPendingMonthPaymentVo;
import com.wx.house.core.pojo.vo.TerminalIndexVo;
import com.wx.house.core.pojo.vo.TenantPendingPaymentVo;
import com.wx.house.terminal.api.client.service.IndexService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:31
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("tenant/v1.0.0/index")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "首页接口 [v.1.0.0]")
@Validated
public class IndexController extends BaseController {
    private final IndexService indexService;
    private final ProjectConfiguration projectConfiguration;

    @ApiOperation("app首页")
    @GetMapping
    public Result<TerminalIndexVo> index() {
        String userId = this.getUserId();
        TerminalIndexVo terminalIndexVo = indexService.selectTerminalIndex(userId);
        return Result.success(terminalIndexVo);
    }

    /*@ApiOperation("app首页-待缴收费")
    @GetMapping("indexPayFees/")
    public Result<List<TenantPendingMonthPaymentVo>> indexPayFees() {
        String userId = this.getUserId();
        List<TenantPendingMonthPaymentVo> tenantPendingPaymentVos = indexService.selectTenantPendingIndexPayFees(userId);
        tenantPendingPaymentVos.forEach(e -> e.getPendingPaymentVoList().forEach(m -> m.setBuildingImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + m.getBuildingImg())));
        return Result.success(tenantPendingPaymentVos);
    }*/

    @GetMapping("expense")
    @ApiOperation("缴费记录明细")
    @ApiImplicitParam(value = "月份", example = "2019-01")
    public Result<List<ECollectFeesDetailVo>> selectExpense(String mouth) {
        String userId = this.getUserId();
        List<ECollectFeesDetailVo> eCollectFeesDetailVos = indexService.selectExpenseDetail(mouth, userId);
        return Result.success(eCollectFeesDetailVos);
    }

    //待缴费/按月份分组
    //如果往期没交，往期的也需要查出来
    //查收费月份小于等于本月
    @ApiOperation("订单管理-缴费订单[如果往期没交,会有多条数据]")
    @GetMapping("pendingPayment/")
    public Result<List<TenantPendingMonthPaymentVo>> selectTenantPendingPayment() {
        String userId = this.getUserId();
        List<TenantPendingMonthPaymentVo> tenantPendingPaymentVos = indexService.selectTenantPendingPaymentVo(userId);
        tenantPendingPaymentVos.forEach(e -> e.getPendingPaymentVoList().forEach(m -> m.setBuildingImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + m.getBuildingImg())));
        return Result.success(tenantPendingPaymentVos);
    }

}
