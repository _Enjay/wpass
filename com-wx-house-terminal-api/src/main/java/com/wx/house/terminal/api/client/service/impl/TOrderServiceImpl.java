package com.wx.house.terminal.api.client.service.impl;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.exception.UserException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.expenses.ECostDetailMapper;
import com.wx.house.core.mapper.expenses.ECostMapper;
import com.wx.house.core.mapper.order.TOrderDetailMapper;
import com.wx.house.core.mapper.order.TOrderMapper;
import com.wx.house.core.mapper.tenant.UserCustomMapper;
import com.wx.house.core.pojo.dto.order.OrderParam;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.TOrder;
import com.wx.house.core.pojo.po.TOrderDetail;
import com.wx.house.core.pojo.vo.*;
import com.wx.house.terminal.api.client.service.TUserOrderService;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:23
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TUserOrderService {
    private final TOrderMapper tOrderMapper;
    private final TOrderDetailMapper tOrderDetailMapper;
    private final UserCustomMapper userCustomMapper;
    private final ProjectConfiguration projectConfiguration;
    private final ECostDetailMapper eCostDetailMapper;
    private final ECostMapper eCostMapper;

    @Override
    public void createOrder(OrderParam orderParam, String userId) {
    	//将数据存入TOrder订单表
        String orderId = IDUtils.createUUId();
        TOrder tOrder = new TOrder();
        tOrder.setId(orderId);
        tOrder.setUserId(userId);
        tOrder.setOrderNumber(String.valueOf(IDUtils.genItemId(16)));
        tOrder.setPrice(orderParam.getTotal());
        tOrder.setState(0);
        tOrder.setCreateTime(new Date());
        tOrder.setPayType(0);
        //将多条数据存入LIST<TOrderDetail>集合
        String ids = String.join(",", orderParam.getECostId());
        List<ECost> selectBatchIds = eCostMapper.selectListId(ids);
        List<TOrderDetail> detailList = new ArrayList<TOrderDetail>();
        TOrderDetail tod = null;
        for (ECost eCost : selectBatchIds) {
            tod = new TOrderDetail();
            tod.setId(IDUtils.createUUId());
            tod.setOrderId(orderId);
            tod.setECostId(eCost.getId());
            tod.setCContractId(eCost.getContrId());
            detailList.add(tod);
        }
        //查询订单编号是否相等
        List<TOrderDetail> tOrderDetails = tOrderDetailMapper.selectTOrferIds(detailList);
        if(!ObjectUtils.isEmpty(tOrderDetails)) {
            //判断订单是否已经生成过
            for (TOrderDetail e : tOrderDetails) {
                /*for (int i = 0; i < orderParam.getECostId().size(); i++) {
                  if (e.getECostId().equals(orderParam.getECostId().get(i))) {
                	  throw new UserException("订单" + orderParam.getECostId().get(i) + "已存在，请到付款处付费", 201);
                    }
                }*/
                if (orderParam.getECostId().stream().anyMatch(e.getECostId()::contains)) {
                    throw new UserException("订单" + orderParam.getECostId() + "已存在，请到付款处付费", 201);
                }
            }
        }
        //判断金额是否正确
        TOrder tOrders = eCostDetailMapper.selectEcostPrice(selectBatchIds);
        if(tOrders.getPrice().compareTo(orderParam.getTotal())!=0) {
        	throw new UserException("总金额不相等，请确认订单后付费" + tOrders.getPrice(), 201);
        }
        if (tOrderMapper.insert(tOrder) == 0) {
        	throw new UserException("订单创建失败", 201);
        }
        if (detailList.size() != 0) {
        	if(tOrderDetailMapper.insertTOrderDetail(detailList)==0) {
        		throw new UserException("订单详情创建失败", 201);
        	}
        }
    }

    @Override
    public void deleteOrder(String id) {
        tOrderMapper.updateById(TOrder.builder().deleted(1).build(), id);
    }

    @Override
    public List<TOrderCreateVos> selectTOrderCreateAllVoFY(String state, String userId) {
        return tOrderMapper.selectTOrderCreateAllVoFY(state, userId);
    }
    @Override
    public List<TOrderCreateVos> selectDetailAllVo(String todString) {
        return tOrderMapper.selectDetailAllVo(todString);
    }


}
