package com.wx.house.terminal.api.client.controller.v1;

import com.github.pagehelper.util.StringUtil;
import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.tenant.IdCardValidDto;
import com.wx.house.core.pojo.po.TUser;
import com.wx.house.core.pojo.vo.TUserIDCardVo;
import com.wx.house.terminal.api.client.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:31
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("tenant/v1.0.0/user")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "用户接口 [v.1.0.0]")
@Validated
public class UserController extends BaseController {
    private static final String CODE = "ALY:ID_CODE:%s";
    private final UserService userService;

    @ApiOperation("用户实名信息")
    @GetMapping
    public Result<TUserIDCardVo> index() {
        String userId = this.getUserId();
        TUserIDCardVo tUserIDCardVo = userService.selectTUserIDCardVo(userId);
        return Result.success(tUserIDCardVo);
    }

    @ApiOperation("用户提交实名认证请求")
    @PutMapping("idCard/")
    public Result validIdCard(@RequestBody @Validated IdCardValidDto idCardValidDto) {
        String userId = this.getUserId();
        String name = redisUtil().get(String.format(CODE, idCardValidDto.getIdCardNumber()));
        if (StringUtil.isEmpty(name)) {
            return Result.error("提交的身份证图片已过期");
        }
        if (!name.equals(idCardValidDto.getName())) {
            return Result.error("提交的身份证与提交的身份证不符");
        }
        userService.updateUserIdCard(idCardValidDto, userId);
        return Result.success();
    }

}
