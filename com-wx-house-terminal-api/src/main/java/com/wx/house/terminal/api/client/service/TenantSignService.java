package com.wx.house.terminal.api.client.service;

import com.wx.house.core.pojo.dto.tenant.TenantCertificationUpDto;
import com.wx.house.core.pojo.po.TUser;
import com.wx.house.core.pojo.vo.TUserVo;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 04:31
 * @description：
 * @version: ：V
 */
public interface TenantSignService {
    //查询登录信息
    TUserVo selectByPhoneNumber(String phoneNumber);

    /**
     * 添加注册用户
     * @param tUser
     * @return
     */
    int insertSelective(TUser tUser);


    /**
     * 修改账户密码
     * @param userId
     * @param newPassword
     * @return
     */
    int updatePassword(String userId, String newPassword,Integer safeKey);

    void updateCertification(TenantCertificationUpDto tenantCertificationUpDto, TUser signUser);

}
