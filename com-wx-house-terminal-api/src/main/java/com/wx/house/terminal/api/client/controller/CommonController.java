package com.wx.house.terminal.api.client.controller;

import com.wx.house.common.assembly.alyidcard.IDCardResult;
import com.wx.house.common.assembly.alyidcard.IDCardUtil;
import com.wx.house.common.assembly.alysms.MessageUtil;
import com.wx.house.core.pojo.base.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 02:27
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("common/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "其他接口")
@Validated
public class CommonController {
    private final MessageUtil messageUtil;
    private final IDCardUtil idCardUtil;

    @ApiOperation("短信验证码")
    @GetMapping("sms/")
    public Result sms(String phone) {
        messageUtil.send(phone);
        return Result.success("success");
    }

    public static void main(String[] args) {
        System.out.println("String= " + String.valueOf(new Random().nextInt(8999999) + 100000));

    }

    @ApiOperation("实名认证")
    @GetMapping("idCard/")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cardId", value = "身份证号码"),
            @ApiImplicitParam(name = "realName", value = "真实名称")
    })
    public Result idCard(String cardId, String realName) {
        IDCardResult idCardResult = idCardUtil.validated(cardId, realName);
        return Result.success(idCardResult);
    }


}
