package com.wx.house.terminal.api.wxpay.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.po.TOrder;
import com.wx.house.terminal.api.wxpay.vo.PayResultVo;

import java.util.Map;

public interface WxService extends IService<TOrder> {
    Map<String, String> dounifiedOrder(String attach, String total_fee, String outTradeNo) throws Exception;
    String payBack(String notifyData);
    PayResultVo payResult(String OrderNumber) throws Exception ;
    void test() ;

    /***
     * 付款成功后更新支付后操作
     * @param notifyMap
     * @param out_trade_no
     */
     void updatePaySuccess(Map<String, String> notifyMap, String out_trade_no) throws Exception;
}