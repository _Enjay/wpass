package com.wx.house.terminal.api.client.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.order.TOrderDetailMapper;
import com.wx.house.core.pojo.po.TOrderDetail;
import com.wx.house.terminal.api.client.service.TOrderDetailService;
import org.springframework.stereotype.Service;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/11/29 上午 11:27
 * @description：${description}
 * @version:   ：V$version
 */
@Service
public class TOrderDetailServiceImpl extends ServiceImpl<TOrderDetailMapper, TOrderDetail> implements TOrderDetailService {

}
