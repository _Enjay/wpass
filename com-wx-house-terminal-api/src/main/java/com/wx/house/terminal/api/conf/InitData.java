package com.wx.house.terminal.api.conf;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import com.wx.house.common.base.ProjectConfiguration;
/**
 * @author yangxiusen
 *2019年7月31日
 */
@Configuration
public class InitData implements CommandLineRunner {

	@Resource
    private ProjectConfiguration projectConfiguration;
	public static String ImageUrl;
	@Override
	public void run(String... args) throws Exception {

		initImageUrl();
	}
	//
	private void initImageUrl(){
		ImageUrl=projectConfiguration.getStaticResource().getStaticResourceUrl();
	}
}
