package com.wx.house.terminal.api.wxpay.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class PayResultVo {
    private BigDecimal total_fee;
    private String transaction_id;
    private String  out_trade_no;
    private Date time_end;
}
