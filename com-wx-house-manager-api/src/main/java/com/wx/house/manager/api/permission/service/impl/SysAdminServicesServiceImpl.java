package com.wx.house.manager.api.permission.service.impl;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.StringUtil;
import com.wx.house.core.mapper.capital.SysAdminDetailMapper;
import com.wx.house.core.pojo.dto.admin.AdminRealDto;
import com.wx.house.core.pojo.dto.admin.UpgradeAdminServiceDto;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.vo.AdminServiceDetailVo;
import com.wx.house.core.pojo.vo.AdminServiceVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.permission.SysAdminServicesMapper;
import com.wx.house.core.pojo.po.SysAdminServices;
import com.wx.house.manager.api.permission.service.SysAdminServicesService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 03:13
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class SysAdminServicesServiceImpl extends ServiceImpl<SysAdminServicesMapper, SysAdminServices> implements SysAdminServicesService {
private final SysAdminServicesMapper sysAdminServicesMapper;
private final SysAdminDetailMapper sysAdminDetailMapper;


@Override
public List<AdminServiceVo> selectAdminServiceVo(String keyword, Integer state) {
	return sysAdminServicesMapper.selectAdminServiceVo(keyword, state);
}

@Override
public AdminServiceDetailVo selectAdminServiceDetailVo(String maccId) {
	return sysAdminServicesMapper.selectAdminServiceDetailVo(maccId);
}

@Override
public void reviewAdmin(AdminRealDto adminRealDto) {
	SysAdminDetail sysAdminDetail = new SysAdminDetail();
	sysAdminDetail.setId(adminRealDto.getAdminId());
	sysAdminDetail.setRealState(adminRealDto.getState());
	
	
	if (StringUtil.isNotEmpty(adminRealDto.getRemark())) {
		sysAdminDetail.setRemark(adminRealDto.getRemark());
	}
	if (sysAdminDetailMapper.updateByMaccId(sysAdminDetail, adminRealDto.getAdminId()) != 1) {
		throw new BusinessException("更新失败，请联系管理员");
	}
}

@Override
public void upgradeAdminService(UpgradeAdminServiceDto upgradeAdminServiceDto) {
	SysAdminServices sysAdminServices = new SysAdminServices();
	sysAdminServices.setAccountLevel(1);
	if (StringUtil.isNotEmpty(upgradeAdminServiceDto.getRemark())) {
		sysAdminServices.setRemark(upgradeAdminServiceDto.getRemark());
	}
	Date date = new Date();
	Calendar c = Calendar.getInstance();
	c.setTime(date);
	c.add(Calendar.MONTH, upgradeAdminServiceDto.getMonth());
	sysAdminServices.setServiceEndTime(c.getTime());
	
	//TODO 服务升级以后为房东新增角色
	
	String id = upgradeAdminServiceDto.getId();
	//step1. 用户和角色关系表
	
	//
	
	if (sysAdminServicesMapper.updateByMaccId(sysAdminServices, upgradeAdminServiceDto.getId()) != 1) {
		throw new BusinessException("更新失败，请联系管理员");
	}
}
}

