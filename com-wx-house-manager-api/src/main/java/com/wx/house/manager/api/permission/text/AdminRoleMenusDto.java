package com.wx.house.manager.api.permission.text;

import java.util.List;

import com.wx.house.core.pojo.po.SysAdmin;
import com.wx.house.core.pojo.po.SysMenus;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AdminRoleMenusDto {
	private SysAdmin sysAdmin;
	private List<SysMenus> ListsysMenus;
}
