package com.wx.house.manager.api.expenses.service.impl;

import com.wx.house.core.mapper.expenses.EPaymentMapper;
import com.wx.house.core.pojo.dto.expenses.SelectPaymentDto;
import com.wx.house.core.pojo.vo.EPaymentVo;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.EPayment;
import com.wx.house.manager.api.expenses.service.EPaymentService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class EPaymentServiceImpl extends ServiceImpl<EPaymentMapper, EPayment> implements EPaymentService {
@Resource
 private EPaymentMapper ePaymentMapper;
    @Override
    public void deletePaymentType(String id) {
        ePaymentMapper.deleteById(id);
    }

    @Override
    public List<EPaymentVo> selectPayMentList(SelectPaymentDto selectPaymentDto) {
        return ePaymentMapper.selectPayMentList(selectPaymentDto);
    }
}
