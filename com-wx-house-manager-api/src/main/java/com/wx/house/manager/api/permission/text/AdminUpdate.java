package com.wx.house.manager.api.permission.text;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class AdminUpdate {
	@ApiModelProperty(value = "AdminId", name = "id",  required = true)
	private String id;
	/**
	 * 管理员姓名
	 */
	@ApiModelProperty(value = "管理员姓名", name = "realName",  required = false)
	private String realName;
    /***
     * 手机号
     */
    @ApiModelProperty(value = "手机号", name = "phone",  required = false)
	private String phone; 
}
