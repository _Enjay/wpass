package com.wx.house.manager.api.permission.service.impl;

import com.wx.house.manager.api.permission.service.SysRoleMenuService;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.permission.SysRoleMenuMapper;
import com.wx.house.core.pojo.po.SysRoleMenu;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {
	@Resource
	private SysRoleMenuMapper sysRoleMenuMapper;
	
	@Override
	public int addRoleMenu(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuMapper.insert(sysRoleMenu);
	}

	@Override
	public int dateRoleMenu(String roleid) {
		return sysRoleMenuMapper.dateRolemenu(roleid);
	}
}
