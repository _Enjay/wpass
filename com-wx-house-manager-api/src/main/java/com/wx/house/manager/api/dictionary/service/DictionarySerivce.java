package com.wx.house.manager.api.dictionary.service;

import com.wx.house.core.pojo.po.DicBaseDictionary;

import java.util.List;
import java.util.Map;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.dictionary.service
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/11 15:16
 * @Version V1.0
 */
public interface DictionarySerivce {
    /**
     *查询基础字典表的列表(根据父节点)
     * @return
     */
    List<DicBaseDictionary> searchAllByCondion(Integer id,String mAccId);
    /**
     * 增加基础字典表
     */

    void addDicBase(DicBaseDictionary dicBaseDictionary);

    /**
     * 根据id查看当前的字典的详情
     */
    DicBaseDictionary searchDicBaseById(Long id);

    /**
     * 更新字典表
     */
    void updateDicBaseById(DicBaseDictionary dicBaseDictionary);
    /**
     * 删除字典表的数据
     */
    void deleteDicBaseById(Integer id);

    /**
     * 根据id查找父类id
     * @param id
     * @return
     */
    DicBaseDictionary selectParentIdById(Long id);
    Map<String,Object> findDicTree(Integer id,String mAccId);
}
