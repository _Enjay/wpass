package com.wx.house.manager.api.dic.service.impl;

import com.wx.house.core.mapper.dic.DicWebDeployMapper;
import com.wx.house.core.pojo.po.EAutoMeterRead;
import com.wx.house.core.pojo.vo.DicWebDeployVo;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.DicWebDeploy;
import com.wx.house.manager.api.dic.service.DicWebDeployService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class DicWebDeployServiceImpl extends ServiceImpl<DicWebDeployMapper, DicWebDeploy> implements DicWebDeployService {
  @Resource
    private  DicWebDeployMapper dicWebDeployMapper;
    @Override
    public List<DicWebDeployVo> findWebDeployByMaccId(String maccId) {

        return dicWebDeployMapper.findWebDeployByMaccId(maccId);
    }
}
