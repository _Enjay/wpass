package com.wx.house.manager.api.order.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.order.TOrderMapper;
import com.wx.house.core.pojo.po.TOrder;
import com.wx.house.manager.api.order.service.TOrderService;
/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/11/29 上午 10:23
 * @description：${description}
 * @version:   ：V$version
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TOrderService{

}
