package com.wx.house.manager.api.common;

import com.wx.house.common.assembly.alyidcard.IDCardUtil;
import com.wx.house.core.pojo.base.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/23 下午 01:54
 * @description：
 * @version: ：V
 */
@AllArgsConstructor
@RestController
@RequestMapping("base/validate/")
@Api(value = "实名认证", tags = "实名认证")
public class ValidController {
private final IDCardUtil idCardUtil;

@PostMapping
@ApiOperation("实名认证")
public Result valid(@RequestBody @Validated User user) {
	idCardUtil.validated(user.getIdCardNumber(), user.getUsername());
	return Result.success();
}

@Data
@AllArgsConstructor
@NoArgsConstructor
public static class User {
	@ApiModelProperty("用户名")
	private String username;
	@ApiModelProperty("身份证")
	private String idCardNumber;
}
}
