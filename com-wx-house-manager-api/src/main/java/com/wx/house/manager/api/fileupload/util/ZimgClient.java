package com.wx.house.manager.api.fileupload.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


/**
 * @author yangxiusen 2019年6月11日
 */
@Component
@Slf4j
@PropertySource("classpath:resources.properties")
public class ZimgClient {
	private static volatile ZimgClient zimgClient;
	public static String imgLocalpath;

	@Value("${imgLocalpath}")
	public void setimgLocalpath(String imgLocalpath) {
		ZimgClient.imgLocalpath = imgLocalpath;
	}

	public static ZimgClient getInstance() {
		if (zimgClient == null) {
			synchronized (ZimgClient.class) {
				if (zimgClient == null) {
					zimgClient = new ZimgClient();
				}
			}
		}
		return zimgClient;
	}






}
