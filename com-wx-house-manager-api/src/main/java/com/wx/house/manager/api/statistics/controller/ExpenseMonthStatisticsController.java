package com.wx.house.manager.api.statistics.controller;

import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.vo.expenseStatistics.ExpenseMonthStatisticsVo;
import com.wx.house.manager.api.statistics.services.ExpenseStatisticsServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 02:04
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("statistics/expense/month/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "收支统计报表")
@Validated
public class ExpenseMonthStatisticsController extends BaseController {
private final ExpenseStatisticsServices expenseStatisticsServices;

@ApiOperation("获取月度收支报表")
@GetMapping
@ApiImplicitParams({
		@ApiImplicitParam(name = "yearMonth", value = "年月", required = false)
})
public Result<ExpenseMonthStatisticsVo> expenseMonthStatistics(String yearMonth) {
	ExpenseMonthStatisticsVo expenseMonthStatisticsVo = expenseStatisticsServices.selectExpenseMonthStatisticsVo(this.getMaccId(), yearMonth);
	return Result.success(expenseMonthStatisticsVo);
}


//TODO export
}
