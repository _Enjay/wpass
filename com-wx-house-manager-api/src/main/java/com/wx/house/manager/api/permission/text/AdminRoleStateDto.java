package com.wx.house.manager.api.permission.text;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminRoleStateDto {
	private String sysRoleId;
	private String state;
}
