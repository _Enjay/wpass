package com.wx.house.manager.api.fileupload.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.core.pojo.dto.fileload.AttaUploadDto;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;


/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.fileupload.service
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/11 9:25
 * @Version V1.0
 */

public interface FileUploadService extends IService<AttaUpload> {
    AttaUpload multiUploads(MultipartFile file,String type,String id);

    /**
     * 获取本地图片
     * @param name
     */
  void getImg(String name, HttpServletResponse response);

    /**
     * 手机上传图片
     * @param name
     * @param response
     * @param bytes
     */
    AttaUpload upload(AttaUploadDto attaUploadDto);
}
