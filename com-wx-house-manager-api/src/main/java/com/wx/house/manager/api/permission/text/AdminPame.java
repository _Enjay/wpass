package com.wx.house.manager.api.permission.text;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class AdminPame {
	/***
	 * 管理员账号
	 */
    @ApiModelProperty(value = "管理员账号", name = "loginName",  required = true)
    @Length(max = 128)
    @NotBlank(message ="账号不能为空")
	private String loginName;
    /***
     * 管理员密码
     */
    @ApiModelProperty(value = "管理员密码", name = "loginPwd",  required = true)
    @Pattern(regexp = "^.{6,16}$", message = "用户密码:6-16位")
    @NotBlank(message ="密码不能为空")
	private String loginPwd;
	/**
	 * 管理员姓名
	 */
	@ApiModelProperty(value = "管理员姓名", name = "realName",  required = true)
	@NotBlank(message ="姓名不能为空")
	private String realName;
    /***
     * 手机号
     */
    @ApiModelProperty(value = "手机号", name = "phone",  required = true)
    @NotBlank(message ="手机号不能为空")
	private String phone;
    /***
     * 管理员类别0超级管理员 1主账号
     */
    @ApiModelProperty(value = "管理员类别0超级管理员 1主账号", name = "accoutype")
	private String accoutype; 
}
