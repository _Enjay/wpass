package com.wx.house.manager.api.building.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.StringUtil;
import com.wx.house.core.pojo.vo.HiredRoomVo;
import com.wx.house.core.pojo.vo.RoomNumberVo;
import com.wx.house.manager.api.building.service.BBuildingRoomService;
import com.wx.house.manager.api.building.service.BuildingService;
import com.wx.house.manager.api.building.text.BBuildingBaseDto;
import com.wx.house.manager.api.building.text.RoomDiscardDto;
import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.dto.building.*;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.wx.house.core.pojo.vo.BuildingRoomVo;
import com.wx.house.core.pojo.vo.BuildingSimpleVo;
import com.wx.house.core.pojo.vo.BuildingVo;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/01 下午 01:22
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("building/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "楼宇管理")
@Validated
public class BuildingController extends BaseController {
private final BuildingService buildingService;
private final BBuildingRoomService bBuildingRoomService;
private final ProjectConfiguration projectConfiguration;

@ApiOperation("楼宇列表")
@GetMapping
public Result<PageInfo<BuildingVo>> selectBuildingList(SelectBuildingDto selectBuildingDto) {
	UserInfo userInfo = this.userInfo();
	List<BuildingVo> buildingVoList = buildingService.selectBuildingVoList(userInfo.getMAccId(), selectBuildingDto.getBuildingName(), selectBuildingDto.getProvince(), selectBuildingDto.getCity(), selectBuildingDto.getCounty());
	buildingVoList.forEach(e -> e.setBuildImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getBuildImg()));
	PageInfo<BuildingVo> pageInfo = new PageInfo<>(buildingVoList);
	return Result.success(pageInfo);
}


@ApiOperation("简单楼宇列表")
@GetMapping("simple")
public Result<List<BuildingSimpleVo>> selectBuildingSimpleList(SelectBuildingSimpleDto selectBuildingDto) {
	UserInfo userInfo = this.userInfo();
	List<BuildingSimpleVo> buildingSimpleVoList = buildingService.selectBuildingSimpleVoList(userInfo.getMAccId(), selectBuildingDto.getBuildingName(), selectBuildingDto.getProvince(), selectBuildingDto.getCity(), selectBuildingDto.getCounty());
	
	return Result.success(buildingSimpleVoList);
}

@ApiOperation("根据楼宇id查询下面的房间[待出租房间]")
@GetMapping("room/{buildingId}")
@ApiImplicitParams({
		@ApiImplicitParam(name = "buildingId", value = "楼宇id"),
		@ApiImplicitParam(name = "room", value = "房间信息 [楼层]-[房间]  或者 [楼层]", example = "13-4    14")
})
public Result<PageInfo<BuildingRoomVo>> selectBuildingSimpleRoomList(@PathVariable("buildingId") String buildingId, Page page, String room) {
	String floor = "";
	String door = "";
	if (StringUtil.isEmpty(room)) {
		//do nothing
	} else if (room.contains("-")) {
		floor = room.split("-")[0];
		door = room.split("-")[1];
	} else {
		floor = room;
	}
	List<BuildingRoomVo> buildingRoomVos = bBuildingRoomService.selectSimpleWaitHireRoom(buildingId, floor, door,String.valueOf(0));
	buildingRoomVos.forEach(e -> e.setBuildingImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getBuildingImg()));
	PageInfo<BuildingRoomVo> pageInfo = new PageInfo<>(buildingRoomVos);
	return Result.success(pageInfo);
}

@ApiOperation("新增楼宇")
@PostMapping
public Result insertBuilding(@RequestBody @Validated InsertBuildingDto dto) {
	buildingService.insertBuilding(dto);
	return Result.success();
}

@ApiOperation("删除楼宇,如果楼宇所有房间都是待出租")
@DeleteMapping("{id}")
public Result deleteBuilding(@PathVariable String id) {
	try {
		buildingService.deleteBuilding(id);
		return Result.success();
	} catch (Exception e) {
		e.printStackTrace();
	}
	return Result.error("还有出租房间，删除失败");
}

@ApiOperation("为楼宇新增房间,如果有添加失败的，通过buildingRooms返回")
@PostMapping("room/")
public Result insertBuildingRoom(@RequestBody @Validated List<InsertBuildingRoomDto> insertBuildingRoomDtoList) {
	List<BuildingRoom> buildingRooms = buildingService.insertBuildingRoom(insertBuildingRoomDtoList);
	return Result.success(buildingRooms);
}

@ApiOperation("根据楼宇id查询下面的房间[全部房间]")
@GetMapping("room/list/{buildingId}")
@ApiImplicitParams({
		@ApiImplicitParam(name = "buildingId", value = "楼宇id"),
		@ApiImplicitParam(name = "floor", value = "楼层", example = "14")
})
public Result<PageInfo<BuildingRoomVo>> selectBuildingRoomList(@PathVariable("buildingId") String buildingId, Page page, String floor) {
	List<BuildingRoomVo> buildingRoomVos = bBuildingRoomService.selectSimpleWaitHireRoom(buildingId, floor, null, null);
	buildingRoomVos.forEach(e -> e.setBuildingImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getBuildingImg()));
	PageInfo<BuildingRoomVo> pageInfo = new PageInfo<>(buildingRoomVos);
	return Result.success(pageInfo);
}

@ApiOperation("为楼宇删除房间,如果房间状态为待出租")
@DeleteMapping("room/")
public Result deleteBuildingRoom(@RequestParam(value = "idList") @ApiParam(value = "id列表,删除的房间必须为待出租状态") List<String> idList) {
	List<BuildingRoom> buildingRooms = buildingService.deleteBuildingRoom(idList);
	return Result.success("删除成功", buildingRooms);
}

@ApiOperation("修改多个房间的属性")
@PutMapping("room/")
public Result updateBuildingRoom(@RequestBody @Validated List<UpdateBuildingRoomDto> updateBuildingRoomDtoList) {
	buildingService.updateBuildingRoom(updateBuildingRoomDtoList);
	return Result.success();
}

@ApiOperation("修改楼宇信息")
@PutMapping
public Result updateBuilding(@RequestBody @Validated UpdateBuildingDto updateBuildingDto) {
	buildingService.updateBuilding(updateBuildingDto);
	return Result.success();
}

@ApiOperation("查询已出租房间")
@GetMapping("hiredRoom/")
@ApiImplicitParams({
		@ApiImplicitParam(name = "buildingId", value = "楼宇id"),
		@ApiImplicitParam(name = "room", value = "房间信息 [楼层]-[房间]  或者 [楼层]", example = "13-4    14")
})
public Result<PageInfo<HiredRoomVo>> selectHiredRoomVo(Page page, String buildingId, String room) {
	String floor = "";
	String door = "";
	if (StringUtil.isEmpty(room)) {
		//do nothing
	} else if (room.contains("-")) {
		floor = room.split("-")[0];
		door = room.split("-")[1];
	} else {
		floor = room;
	}
	List<HiredRoomVo> hiredRoomVos = bBuildingRoomService.selectHiredRoomVo(buildingId, floor, door);
	hiredRoomVos.forEach(e -> e.setBuildingImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getBuildingImg()));
	return Result.success(new PageInfo<>(hiredRoomVos));
}

	@ApiOperation("根据合同编号房间读数信息比较")
	@GetMapping("roomHistory/")
	public Result selectroomHistory(BBuildingBaseDto bBuildingBaseDto) {
		try {
			if (StringUtil.isEmpty(bBuildingBaseDto.getContrNumber())) {
				return Result.error("请选择公寓");
			}
			BuildingRoom buildingRoom = bBuildingRoomService.selectNumberRoom(bBuildingBaseDto.getContrNumber());
			System.out.println("--------"+buildingRoom);
			if (bBuildingBaseDto.getWater() != null) {
				if (bBuildingBaseDto.getWater()<buildingRoom.getHistoryWater()) {
					return Result.error("上个月使用水为：" + buildingRoom.getHistoryWater() + ",填入读数过小");
				}
			}
			if (bBuildingBaseDto.getElectric() != null) {
				if (bBuildingBaseDto.getElectric()<buildingRoom.getHistoryElectric()) {
					return Result.error("最近历史使用电为：" + buildingRoom.getHistoryElectric() + ",填入读数过小");
				}
			}
			if (bBuildingBaseDto.getGas() != null) {
				if (bBuildingBaseDto.getGas()<buildingRoom.getHistoryGas()) {
					return Result.error("最近历史使用气为：" + buildingRoom.getHistoryGas() + ",填入读数过小");
				}
			}
			return Result.success();
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("房间查询失败");
		}
	}
	@ApiOperation("房间（停用/启用）")
	@PutMapping("roomDiscard/")
	public Result discardBuildingRoom(@RequestBody RoomDiscardDto roomDiscardDto) {
		try {
			if("1".equals(roomDiscardDto.getBuildType())) {
				return Result.error("不能手动将房间出租");
			}
			buildingService.discardBuildingRoom(roomDiscardDto.getRoomId(),roomDiscardDto.getBuildType());
			return Result.success();
		} catch (Exception e) {
			return Result.error("废弃房间失败");
		}
	}
	@ApiOperation("根据楼宇id查询房间号")
	@GetMapping("selectRoomNumber/")
	@ApiImplicitParams({
		@ApiImplicitParam(name = "buildingId", value = "楼宇id")
	})
	public Result<List<RoomNumberVo>> selectRoomNumber(String buildingId) {
		try {
			List<RoomNumberVo> list=bBuildingRoomService.selectRoomNumber(buildingId);
			return Result.success(list);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("房间更新失败");
		}
	}
	@ApiOperation("根据房间id房间读数信息比较")
	@GetMapping("roomIdHistory/")
	public Result selectroomIdHistory(BBuildingBaseDto bBuildingBaseDto) {
		try {
			BuildingRoom buildingRoom = bBuildingRoomService.selectNumberIdRoom(bBuildingBaseDto.getRoomId());
			System.out.println("--------"+buildingRoom);
			if (bBuildingBaseDto.getWater() != null) {
				if (bBuildingBaseDto.getWater()<buildingRoom.getHistoryWater()) {
					return Result.error("上个月使用水为：" + buildingRoom.getHistoryWater() + ",填入读数过小");
				}
			}
			if (bBuildingBaseDto.getElectric() != null) {
				if (bBuildingBaseDto.getElectric()<buildingRoom.getHistoryElectric()) {
					return Result.error("最近历史使用电为：" + buildingRoom.getHistoryElectric() + ",填入读数过小");
				}
			}
			if (bBuildingBaseDto.getGas() != null) {
				if (bBuildingBaseDto.getGas()<buildingRoom.getHistoryGas()) {
					return Result.error("最近历史使用气为：" + buildingRoom.getHistoryGas() + ",填入读数过小");
				}
			}
			return Result.success();
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("房间查询失败");
		}
		
	}
}
