package com.wx.house.manager.api.statistics.services;

import com.wx.house.core.pojo.vo.expenseStatistics.ExpenseMonthStatisticsVo;
import com.wx.house.core.pojo.vo.expenseStatistics.ExpenseYearStatisticsVo;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 02:05
 * @description：
 * @version: ：V
 */
public interface ExpenseStatisticsServices {
List<ExpenseYearStatisticsVo> selectExpenseYearStatisticsVo(String maccid, String year);

ExpenseMonthStatisticsVo selectExpenseMonthStatisticsVo(String maccid, String yearMonth);

}
