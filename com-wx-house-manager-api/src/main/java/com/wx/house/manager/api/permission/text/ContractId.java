package com.wx.house.manager.api.permission.text;



import javax.validation.constraints.NotBlank;


import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContractId {
	@ApiModelProperty(value = "合同id", required = true)
	@NotBlank
	private String contractId;
}
