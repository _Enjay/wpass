package com.wx.house.manager.api.dic.service.impl;

import com.wx.house.manager.api.dic.service.DicAreasService;
import com.wx.house.core.mapper.dic.DicAreasMapper;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.DicAreas;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class DicAreasServiceImpl extends ServiceImpl<DicAreasMapper, DicAreas> implements DicAreasService {
	@Resource
	private DicAreasMapper areasMapper;
	
	@Override
	public List<DicAreas> findAreasByParentId(Integer parentId) {
		if (parentId == null) {
			throw new RuntimeException("必须知道父id");
		}
		List<DicAreas> areas = areasMapper.findAreasByParentId(parentId);
		System.out.println("xxx" + areas);
		if (!areas.isEmpty()) {
			return areas;
		}
		return null;
	}
}
