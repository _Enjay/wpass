package com.wx.house.manager.api.permission.service.impl;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.wx.house.manager.api.permission.service.SysAdminService;
import com.wx.house.manager.api.permission.text.AdminPame;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.capital.SysAdminDetailMapper;
import com.wx.house.core.mapper.permission.SysAdminMapper;
import com.wx.house.core.mapper.permission.SysAdminRoleMapper;
import com.wx.house.core.mapper.permission.SysAdminServicesMapper;
import com.wx.house.core.pojo.po.SysAdmin;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.po.SysAdminRole;
import com.wx.house.core.pojo.po.SysAdminServices;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version:   ：V$version
 */
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin> implements SysAdminService {
	@Resource
	private SysAdminMapper sysAdminMapper;
	@Resource
	private SysAdminDetailMapper sysAdminDetailMapper;
	@Resource
	private SysAdminRoleMapper sysAdminRoleMapper;
	@Resource
	private SysAdminServicesMapper sysAdminServicesMapper;

	@Override
	public SysAdmin loginAdmin(String loginName) {
		return sysAdminMapper.loginAdmin(loginName);
	}
	@Override
	public SysAdmin loginPhone(String phone) {
		return sysAdminMapper.getPhone(phone);
	}
	@Override
	public SysAdmin loginAdminId(String adminId) {
		return sysAdminMapper.loginAdminId(adminId);
	}

	@Override
	public int updateAdminip(String id,String loginIp, Date loginTime) {
		return sysAdminMapper.updetaAdminIp( id, loginIp, loginTime);
	}
	
	@Override
	public int addAdmin(AdminPame adminPame,String pwd,Integer safeKey,String accoutype,String level,String id) {
		//随机生成uuid
		String uuid=IDUtils.genItemId(16) + "";
		//step6. 存值id,姓名,账号,密码,手机号
		SysAdmin admin = SysAdmin.builder().id(uuid)
					   .realName(adminPame.getRealName())
					   .loginName(adminPame.getLoginName())
					   .loginPwd(pwd)
					   .phone(Long.valueOf(adminPame.getPhone()))
					   .safeKey(safeKey)
					   .createTime(new Date())
					   .state(0)
					   .delType(0)
					   .build();
		
		SysAdminRole adminRole =null;
		//根据前台传值判断是否是建立超级管理员
		if ("0".equals(adminPame.getAccoutype())) {
			admin.setAccountType(0);
			admin.setLevel(0 + "");
			adminRole = SysAdminRole.builder().id(IDUtils.genItemId(8) + "").adminId(admin.getId())
					.roleId(1 + "").operation("李好").operationTime(new Date()).build();
			if(sysAdminRoleMapper.insert(adminRole)!=1) {
				throw new BusinessException("分配角色失败！请联系管理员");
			}
			return sysAdminMapper.insert(admin);
		} else {
			// step7. 账号为0超级管理员，1为主账号，2为子账号
			if ("0".equals(accoutype)) {
				// 赋值为主账号
				admin.setAccountType(1);
				// 等级拼接存入
				admin.setLevel(level + "." + uuid);
				adminRole = SysAdminRole.builder().id(IDUtils.genItemId(8) + "").adminId(admin.getId())
						.roleId(2 + "").operation("李好").operationTime(new Date()).build();
				if(sysAdminRoleMapper.insert(adminRole)!=1) {
					throw new BusinessException("分配角色失败！请联系管理员");
				}
				SysAdminDetail adminCapital = SysAdminDetail.builder()
						.id(String.valueOf(IDUtils.genItemId(16)))
						.totalIncome(new BigDecimal(0.00))
						.balance(new BigDecimal(0.00))
						.withdrawals(new BigDecimal(0.00))
						.freezeBalance(new BigDecimal(0.00))
						.realState(0)
						.maccId(uuid)
						.build();
				if(sysAdminDetailMapper.insert(adminCapital)!=1) {
					throw new BusinessException("创建账户详细失败！请联系管理员");
				}
			}
			if ("1".equals(accoutype)) {
				// 赋值为子账号
				admin.setAccountType(2);
				// 存入主id
				admin.setMAccId(id);
				// 等级拼接存入
				admin.setLevel(level + "." + uuid);
				adminRole = SysAdminRole.builder().id(IDUtils.genItemId(8) + "").adminId(admin.getId())
						.roleId(3 + "").operation("李好").operationTime(new Date()).build();
				if(sysAdminRoleMapper.insert(adminRole)!=1) {
					throw new BusinessException("分配角色失败！请联系管理员");
				}
			}
			if ("2".equals(accoutype)) {
				throw new BusinessException("你没有权限");
			}
		}
		return sysAdminMapper.insert(admin);
}
	@Override
	public SysAdmin addAdminAndDetail(String phone,String uuid) {
		SysAdmin sysAdmin = SysAdmin.builder().id(uuid).realName("新用户").accountType(1).mAccId(0 + "").level("0." + uuid)
				.phone(Long.valueOf(phone)).createTime(new Date()).state(0).delType(0)
				.build();
		if(sysAdminMapper.insert(sysAdmin)!=1){
			throw new BusinessException("创建账户失败！请联系管理员");
		}
		SysAdminDetail adminCapital = SysAdminDetail.builder()
				.id(String.valueOf(IDUtils.genItemId(16)))
				.totalIncome(new BigDecimal(0.00))
				.balance(new BigDecimal(0.00))
				.withdrawals(new BigDecimal(0.00))
				.freezeBalance(new BigDecimal(0.00))
				.realState(0)
				.maccId(uuid)
				.build();
		if(sysAdminDetailMapper.insert(adminCapital)!=1) {
			throw new BusinessException("创建账户详细失败！请联系管理员");
		}
		SysAdminRole sysAdminRole=SysAdminRole.builder()
				.id(String.valueOf(IDUtils.genItemId(16)))
				.adminId(uuid)
				.roleId(String.valueOf(2))
				.build();
		if(sysAdminRoleMapper.insert(sysAdminRole)!=1) {
			throw new BusinessException("创建账户失败！请联系管理员");
		}
		SysAdminServices sysAdminServices=SysAdminServices.builder()
				.id(String.valueOf(IDUtils.genItemId(16)))
				.maccId(uuid)
				.accountLevel(0)
				.build();
		if(sysAdminServicesMapper.insert(sysAdminServices)!=1) {
			throw new BusinessException("创建账户失败！请联系管理员");
		}
		return 	sysAdmin;
	}
	@Override
	public List<SysAdmin> selectAdmin(String level,String accountType) {
		return sysAdminMapper.selctAdmin(level, accountType);
	}
	
	@Override
	public List<SysAdmin> selectAll() {
		return sysAdminMapper.selectList(null);
	}

	@Override
	public int UpdateAdmin(SysAdmin sysAdmin) {
		return sysAdminMapper.updateById(sysAdmin);
	}

	@Override
	public int DeleteAdmin(String id,Integer state) {
		return sysAdminMapper.DeleteAdmin(id ,state );
	}

	@Override
	public int deleteDelType(String id) {
		return sysAdminMapper.deleteDelType(id);
	}
	@Override
	public int updateAdminLog(String adminId,String loginPwd, Integer safeKey) {
		return sysAdminMapper.updateAdminLog(adminId, loginPwd, safeKey);
	}
	@Override
	public int updateAdminPhone(String adminId,String newphone) {
		SysAdmin sysAdmin=SysAdmin.builder()
				.id(adminId)
				.loginName(newphone)
				.phone(Long.valueOf(newphone))
				.build();
		return sysAdminMapper.updateById(sysAdmin);
	}
}

