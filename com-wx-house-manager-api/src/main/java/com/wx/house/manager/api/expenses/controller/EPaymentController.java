package com.wx.house.manager.api.expenses.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.manager.api.expenses.service.EPaymentService;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.UserInfoUtil;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.dto.expenses.EPaymentDto;
import com.wx.house.core.pojo.dto.expenses.SelectPaymentDto;
import com.wx.house.core.pojo.dto.expenses.UpdateStateDto;
import com.wx.house.core.pojo.po.EPayment;
import com.wx.house.core.pojo.vo.EPaymentVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.expenses.controller
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 14:05
 * @Version V1.0
 */
@RestController
@Slf4j
@Api(value = "支付管理", tags = "支付管理")
@RequestMapping("EPayment/")
public class EPaymentController extends BaseController {
    @Resource
    private EPaymentService ePaymentService;

    @PostMapping("addPayment/")
    @ApiOperation(value = "新增支付方式", notes = "新增抄表配置")
    public Result addPaymentType(@RequestBody @Validated EPaymentDto ePaymentDto) {
        EPayment ePayment = new EPayment();
        BeanUtils.copyProperties(ePaymentDto,ePayment);
        ePayment.setId(IDUtils.genItemId(12)+"");
        ePayment.setOperation("jack");
        ePayment.setOperationTime(new Date());
        ePaymentService.save(ePayment);
        return Result.success("新增成功");
    }

    @PutMapping("updatePayment/")
    @ApiOperation(value = "更新支付方式", notes = "更新支付方式")
    public Result updatePaymentType(@RequestBody @Validated EPaymentDto ePaymentDto) {
        EPayment ePayment = new EPayment();
        BeanUtils.copyProperties(ePaymentDto,ePayment);
        ePayment.setOperation("jack");
        ePayment.setOperationTime(new Date());
        ePaymentService.updateById(ePayment);
        return Result.success("更新成功");
    }

    @PutMapping("updateState/")
    @ApiOperation(value = "启动/禁用支付", notes = "启动/禁用支付")
    public Result updatePaymentType(@RequestBody UpdateStateDto updateStateDto) {
        EPayment ePayment = new EPayment();
        ePayment.setId(updateStateDto.getId());
        ePayment.setLogicalState(updateStateDto.getLogicalState());
        ePaymentService.updateById(ePayment);
        return Result.success("操作成功");
    }


    @DeleteMapping("deleteState/{id}")
    @ApiOperation(value = "删除支付方式", notes = "更新支付方式")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "id",required = true, dataType = "String", paramType = "path")
    })
    public Result deletePaymentType(@PathVariable String id) {
        ePaymentService.deletePaymentType(id);
        return Result.success("删除成功");
    }


    @GetMapping("list")
    @ApiOperation(value = "支付管理列表", notes = "支付管理列表")
    public Result<PageInfo<EPaymentVo>> paymentTypeList(SelectPaymentDto selectPaymentDto) {
        UserInfo userInfo = this.userInfo();
        List<EPaymentVo> buildingVoList =   ePaymentService.selectPayMentList(selectPaymentDto);
        PageInfo<EPaymentVo> pageInfo = new PageInfo<>(buildingVoList);
        return Result.success(pageInfo);
    }
}
