package com.wx.house.manager.api.dic.service.impl;

import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.util.IDUtils;
import com.wx.house.common.util.StringUtil;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.core.pojo.dto.dic.C_DicBankDto;
import com.wx.house.core.pojo.dto.dic.U_DicBankDto;
import com.wx.house.core.pojo.vo.DicBankVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.dic.DicBankMapper;
import com.wx.house.core.pojo.po.DicBank;
import com.wx.house.manager.api.dic.service.DicBankService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 下午 02:00
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class DicBankServiceImpl extends ServiceImpl<DicBankMapper, DicBank> implements DicBankService {
private final DicBankMapper dicBankMapper;
private final AttaUploadMapper attaUploadMapper;
private final ProjectConfiguration projectConfiguration;


@Override
public List<DicBankVo> selectAll(String keyword) {
	List<DicBankVo> dicBankVos = dicBankMapper.selectAll(keyword);
	dicBankVos.forEach(e -> {
		e.setUrl(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getUrl());
	});
	return dicBankVos;
}

@Override
public void addBank(C_DicBankDto PDicBankDto) {
	DicBank dicBank = new DicBank();
	dicBank.setId(IDUtils.createUUId());
	dicBank.setBankName(PDicBankDto.getBankName());
	dicBank.setBankAbbr(PDicBankDto.getBankAbbr());
	if (PDicBankDto.getOrder() == null) {
		dicBank.setOrder(PDicBankDto.getOrder());
	} else {
		dicBank.setOrder(0);
	}
	dicBank.setFileId(PDicBankDto.getFileId());
	dicBankMapper.insertSelective(dicBank);
}

@Override
public void deleteBank(String id) {
	dicBankMapper.deleteById(id);
}

@Override
public void updateBank(U_DicBankDto dicBankDto) {
	DicBank dicBank = new DicBank();
	dicBank.setId(dicBankDto.getId());
	if (StringUtil.isNotEmpty(dicBankDto.getBankName()))
		dicBank.setBankName(dicBankDto.getBankName());
	if (StringUtil.isNotEmpty(dicBankDto.getBankAbbr()))
		dicBank.setBankAbbr(dicBankDto.getBankAbbr());
	if (StringUtil.isNotEmpty(dicBankDto.getFileId()))
		dicBank.setFileId(dicBankDto.getFileId());
	if (dicBankDto.getOrder() != null)
		dicBank.setOrder(dicBankDto.getOrder());
	
	dicBankMapper.updateById(dicBank, dicBankDto.getId());
}
}
