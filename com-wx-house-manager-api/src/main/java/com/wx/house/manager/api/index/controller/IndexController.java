package com.wx.house.manager.api.index.controller;

import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.index.TimeBetweenQueryDto;
import com.wx.house.core.pojo.vo.checkInStatistics.CheckInStatisticsVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsManager.ExpensesAndReceiptsManagerVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics.ExpensesAndReceiptsStatisticsVo;
import com.wx.house.core.pojo.vo.houseManager.HouseManagerVo;
import com.wx.house.core.pojo.vo.leaseManager.LeaseManagerVo;
import com.wx.house.manager.api.index.services.IndexServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 09:30
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("index/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "后台管理系统首页接口i")
@Validated
public class IndexController extends BaseController {
private final IndexServices indexServices;

@ApiOperation("收支管理")
@GetMapping("expensesAndReceiptsManager")
public Result<ExpensesAndReceiptsManagerVo> expensesAndReceiptsManager(@Validated TimeBetweenQueryDto dto) {
	String maccIds = indexServices.selectMaccIds(this.getMaccId1());
	ExpensesAndReceiptsManagerVo expensesAndReceiptsManagerVo = indexServices.selectExpensesAndReceiptsManagerVo(maccIds, dto.getStartTime(), dto.getEndTime());
	return Result.success(expensesAndReceiptsManagerVo);
}

@ApiOperation("租约管理")
@GetMapping("leaseManager")
public Result<LeaseManagerVo> leaseManager() {
	String maccIds = indexServices.selectMaccIds(this.getMaccId1());
	LeaseManagerVo leaseManagerVo = indexServices.selectLeaseManagerVo(maccIds);
	if (leaseManagerVo.getLeaseManagerDetailVoList().size() > 1) {
		leaseManagerVo.setLeaseManagerDetailVoList(leaseManagerVo.getLeaseManagerDetailVoList().stream().filter(e -> e.getExpireTime().before(new Date())).collect(Collectors.toList()));
	}
	return Result.success(leaseManagerVo);
}

@ApiOperation("收支统计")
@GetMapping("expensesAndReceiptsStatistics")
public Result<ExpensesAndReceiptsStatisticsVo> expensesAndReceiptsStatistics(@Validated TimeBetweenQueryDto dto) {
	String maccIds = indexServices.selectMaccIds(this.getMaccId1());
	ExpensesAndReceiptsStatisticsVo expensesAndReceiptsStatisticsVo = indexServices.selectExpensesAndReceiptsStatisticsVo(maccIds, dto.getStartTime(), dto.getEndTime());
	return Result.success(expensesAndReceiptsStatisticsVo);
}


@ApiOperation("房屋管理")
@GetMapping("houseManager")
public Result<HouseManagerVo> houseManager() {
	String maccIds = indexServices.selectMaccIds(this.getMaccId1());
	HouseManagerVo houseManagerVo = indexServices.selectHouseManagerVo(maccIds);
	return Result.success(houseManagerVo);
}

@ApiOperation("入住统计")
@GetMapping("checkInStatistics")
@ApiImplicitParams({
		@ApiImplicitParam(name = "startTime", value = "开始时间", example = "2019-06"),
		@ApiImplicitParam(name = "endTime", value = "结束时间", example = "2019-12")
})
public Result<CheckInStatisticsVo> checkInStatistics(String startTime, String endTime) {
	String maccIds = indexServices.selectMaccIds(this.getMaccId1());
	CheckInStatisticsVo checkInStatisticsVo = indexServices.selectCheckInStatisticsVo(maccIds, startTime, endTime);
	return Result.success(checkInStatisticsVo);
}


}
