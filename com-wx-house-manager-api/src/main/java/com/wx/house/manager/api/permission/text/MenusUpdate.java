package com.wx.house.manager.api.permission.text;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class MenusUpdate {
	@ApiModelProperty(value = "节点id", name = "id", example = "0", required = true)
	@NotNull(message = "菜单id不能为空")
	private String id;

	/**
	 * 父菜单ID
	 */
	@ApiModelProperty(value = "父节点ID,创建顶级菜单的时候可以为0", name = "pId", example = "0", required = true)
	private String pId;

	/**
	 * 菜单名称
	 */
	@ApiModelProperty(value = "菜单名称", name = "menuName", example = "权限管理", required = true)
	private String menuName;

	/**
	 * 菜单链接
	 */
	@ApiModelProperty(value = "菜单链接", name = "url", example = "/system/", required = false)
	private String url;

	/**
	 * 菜单图标
	 */
	@ApiModelProperty(value = "菜单图标", name = "ico", example = "xxxxx", required = false)
	private String ico;

	/**
	 * 备注
	 */
	@ApiModelProperty(value = "备注", name = "remark", example = "权限管理系统", required = false)
	private String remark;

	/**
	 * 菜单类型，1 菜单 2，链接
	 */
	@ApiModelProperty(value = "菜单类型 1 菜单 2，链接", name = "menuType", example = "1", required = true)
	private Integer menuType;

	/**
	 * 顺序
	 */
	@ApiModelProperty(value = "顺序", name = "menuOrder", example = "0", required = true)
	private Integer menuOrder;
}
