package com.wx.house.manager.api.dic.controller;

/**
 * @description:
 * @author: 喜胖装机
 * @date: 2019-06-12 17:35
 */

import com.wx.house.manager.api.dic.service.DicAreasService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.po.DicAreas;

import javax.annotation.Resource;
import java.util.List;

/**
 * 公司加载区域
 */

@RestController
@RequestMapping("company/")
@Api(value = "区域模块", tags = "省市区")
@Slf4j
public class AreasController extends BaseController {
@Resource
private DicAreasService areasService;

@GetMapping("areas/parent/{id}")
@ApiOperation(value = "查询区域", notes = "根据父id查找区域")
@ApiImplicitParams({
		@ApiImplicitParam(name = "id", value = "父节点id", required = true, dataType = "Integer", paramType = "path"),
})
public Result searchAreasById(@PathVariable Integer id) {
	List<DicAreas> areas = areasService.findAreasByParentId(id);
	return Result.success(areas);
}
}
