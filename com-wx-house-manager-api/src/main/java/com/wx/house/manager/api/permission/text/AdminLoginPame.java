package com.wx.house.manager.api.permission.text;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class AdminLoginPame {
	@ApiModelProperty(value = "新密码", example = "v客部落", required = false)
	private String newPwd;
	@ApiModelProperty(value = "旧密码", example = "v客部落", required = false)
	private String usedPwd;
}
