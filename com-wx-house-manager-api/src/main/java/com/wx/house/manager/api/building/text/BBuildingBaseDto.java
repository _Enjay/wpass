package com.wx.house.manager.api.building.text;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BBuildingBaseDto {
	@ApiModelProperty(value = "合同编号", name = "contrNumber", example = "abcdefg", required = false)
	private String contrNumber;
	@ApiModelProperty(value = "房间id", name = "roomId", example = "abcdefg", required = false)
	private String roomId;
	@ApiModelProperty(value = "水表读数", name = "Water", example = "0123456789", required = false)
	private Integer water;
	@ApiModelProperty(value = "电表读数", name = "Electric", example = "0123456789", required = false)
	private Integer electric;
	@ApiModelProperty(value = "气表读数", name = "Gas", example = "0123456789", required = false)
	private Integer gas;
}
