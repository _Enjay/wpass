package com.wx.house.manager.api.contract.service.impl;

import com.wx.house.manager.api.contract.service.CContractDetailService;
import com.wx.house.core.mapper.contract.CContractDetailMapper;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.ContractDetail;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class CContractDetailServiceImpl extends ServiceImpl<CContractDetailMapper, ContractDetail> implements CContractDetailService {

}
