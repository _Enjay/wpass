package com.wx.house.manager.api.permission.service.impl;

import com.wx.house.manager.api.permission.service.SysMenusService;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.permission.SysMenusMapper;
import com.wx.house.core.pojo.po.SysMenus;
import com.wx.house.core.pojo.vo.PermissionMenuVo;
import com.wx.house.core.pojo.vo.SysMenusVo;
import com.wx.house.core.pojo.vo.SysPermissionVo;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version:   ：V$version
 */
@Service
public class SysMenusServiceImpl extends ServiceImpl<SysMenusMapper, SysMenus> implements SysMenusService {
	@Resource
	private SysMenusMapper sysMenusMapper;
	
	@Override
	public int addMenu(SysMenus sysMenus) {
		return sysMenusMapper.insert(sysMenus);
	}

	@Override
	public SysMenus getMenusName(String MenuName) {
		return sysMenusMapper.getMenusName(MenuName);
	}

	@Override
	public int updateMenu(SysMenus sysMenus) {
		return sysMenusMapper.updateMenu(sysMenus);
	}

	@Override
	public int DeleteMenus(String sysMenusId, Integer state) {
		return sysMenusMapper.DeleteMenus(sysMenusId, state);
	}

	@Override
	public List<SysMenus> selectMenus(String MenuName) {
		return sysMenusMapper.selectMenus(MenuName);
	}

	@Override
	public int DeleteMenuseId(String sysMenusId) {
		return sysMenusMapper.DeleteMenuseId(sysMenusId);
	}
	
	@Override
	public List<SysMenusVo> sysMenuslist() {
		return sysMenusMapper.sysMenuslist();
	}

	@Override
	public List<SysMenusVo> sysRoleMenuslist(String roleId) {
		return sysMenusMapper.sysRoleMenuslist(roleId);
	}

	@Override
	public List<SysMenusVo> selectRoleMenusAll(String adminId) {
		return sysMenusMapper.sysMenusId(adminId);
	}

	@Override
	public List<SysMenusVo> sysAdmin() {
		return sysMenusMapper.sysAdmin();
	}

	@Override
	public SysPermissionVo SelectSysPermission(String name) {
		return sysMenusMapper.SelectSysPermission(name);
	}


	@Override
	public Set<SysMenusVo> findAdminMenu() {
		return sysMenusMapper.selectByAdmin();
	}

	@Override
	public Set<SysMenusVo> findMenuByUserId(String uid) {
		Set<SysMenusVo> sysMenus = sysMenusMapper.selectByUserId(uid);
		return sysMenus;
	}

	@Override
	public List<SysMenusVo> findAllByIdList(List<String> idList) {
		return sysMenusMapper.selectMenuById(idList);
	}

	@Override
	public List<SysMenus> findAdminMenulist() {
		return sysMenusMapper.selectByAdminlist();
	}

	@Override
	public List<PermissionMenuVo> selectMenuPermission(String adminId) {
		return sysMenusMapper.selectMenuPermission(adminId);
	}

	@Override
	public PermissionMenuVo listPidMenus(String id) {
		return sysMenusMapper.listPidMenus(id);
	}

}
