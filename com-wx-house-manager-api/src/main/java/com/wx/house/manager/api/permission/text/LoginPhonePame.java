package com.wx.house.manager.api.permission.text;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class LoginPhonePame {
	/***
	 * 用户账号
	 */
	@ApiModelProperty(value = "手机号", name = "phone", example = "abcdefg", required = true)
	@Length(max = 11)
	@NotBlank
	private String phone;
	/***
	 * 验证码
	 */
	@ApiModelProperty(value = "验证码", name = "code", example = "1234", required = true)
	@NotNull
	private String code;
}
