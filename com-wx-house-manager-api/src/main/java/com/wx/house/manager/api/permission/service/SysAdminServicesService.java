package com.wx.house.manager.api.permission.service;

import com.wx.house.core.pojo.dto.admin.AdminRealDto;
import com.wx.house.core.pojo.dto.admin.UpgradeAdminServiceDto;
import com.wx.house.core.pojo.po.SysAdminServices;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.AdminServiceDetailVo;
import com.wx.house.core.pojo.vo.AdminServiceVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 03:13
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminServicesService extends IService<SysAdminServices> {
List<AdminServiceVo> selectAdminServiceVo(String keyword, Integer state);

AdminServiceDetailVo selectAdminServiceDetailVo(String maccId);


void reviewAdmin(AdminRealDto adminRealDto);

void upgradeAdminService(UpgradeAdminServiceDto upgradeAdminServiceDto);
}

