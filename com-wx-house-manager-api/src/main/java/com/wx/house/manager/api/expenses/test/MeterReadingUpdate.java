package com.wx.house.manager.api.expenses.test;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeterReadingUpdate{
	
	@ApiModelProperty(value = "抄表ID" )
	private String id;

	@ApiModelProperty(value = "水表读数(立方米)" )
    private Integer monthWater;

	@ApiModelProperty(value = "电表读数(千瓦时)" )
    private Integer monthElectric;

	@ApiModelProperty(value = "气表读数(立方米)" )
    private Integer monthGas;
}
