package com.wx.house.manager.api.tenant.controller;

import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.wx.house.common.assembly.alyidcard.IDCardUtil;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.manager.api.tenant.service.TUserService;
import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.tenant.TUserInsertDto;
import com.wx.house.core.pojo.dto.tenant.TenantUpdateDto;
import com.wx.house.core.pojo.vo.TenantContractDetailVo;
import com.wx.house.core.pojo.vo.TenantUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:40
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("tenant/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "租户管理")
@Validated
public class TenantUserController extends BaseController {
private static final String CODE = "ALY:ID_CODE:%s";
private final TUserService tUserService;
private final ProjectConfiguration projectConfiguration;
private final IDCardUtil idCardUtil;

@ApiOperation("查询所有租户")
@GetMapping
@ApiImplicitParams({
		@ApiImplicitParam(name = "keyword", value = "关键字: 用户昵称，名称，手机号，身份证号", required = false, paramType = "query", dataType = "String"),
})
public Result<PageInfo<TenantUserVo>> selectAllTenant(String keyword) {
	String maccId = this.getMaccId();
	List<TenantUserVo> tenantUserVos = tUserService.selectAll(keyword, maccId);
	tenantUserVos.forEach(e -> {
		e.getAttaUploadList().forEach(f -> {
			f.setUrl(projectConfiguration.getStaticResource().getStaticResourceUrl() + f.getUrl());
		});
	});
	PageInfo<TenantUserVo> pageInfo = new PageInfo<>(tenantUserVos);
	return Result.success(pageInfo);
}

@ApiOperation("查询所有租户[无分页")
@GetMapping("npg")
@ApiImplicitParams({
		@ApiImplicitParam(name = "idNumber", value = "关键字: 用户昵称，名称，手机号，身份证号", required = false, paramType = "query", dataType = "String"),
})
public Result<List<TenantUserVo>> selectAllTenantNpg(String idNumber) {
	String maccId = this.getMaccId();
	List<TenantUserVo> tenantUserVos =new ArrayList<TenantUserVo>();
	if(StringUtil.isEmpty(idNumber)) {
	 tenantUserVos = tUserService.selectIDAll(null, maccId);
	}
	if(!StringUtil.isEmpty(idNumber)) {
		 tenantUserVos = tUserService.selectIDAll(idNumber, null);
	}
	tenantUserVos.forEach(e -> {
		e.getAttaUploadList().forEach(f -> {
			f.setUrl(projectConfiguration.getStaticResource().getStaticResourceUrl() + f.getUrl());
		});
	});
	return Result.success(tenantUserVos);
}

@ApiOperation("新增租户")
@PostMapping
public Result insertTenant(@RequestBody TUserInsertDto tUserInsertDto) {
	  String maccId = this.getMaccId();
	  String name=redisUtil().get(String.format(CODE,tUserInsertDto.getIdCardNumber()));
      if(StringUtil.isEmpty(name)) {
      	return Result.error("提交的身份证图片已过期");
      }
      if(!name.equals(tUserInsertDto.getName())) {
      	return Result.error("提交的身份证与提交的身份证不符");
      }
	idCardUtil.validated(tUserInsertDto.getIdCardNumber(), tUserInsertDto.getName());
	tUserService.insertTenant(tUserInsertDto, maccId);
	return Result.success();
}

@ApiOperation("更新租户")
@PutMapping
public Result updateTenant(@RequestBody TenantUpdateDto tenantUpdateDto) {
	tUserService.updateTenant(tenantUpdateDto);
	return Result.success();
}

@ApiOperation("删除租户")
@DeleteMapping("{id}")
public Result deleteTenant(@PathVariable String id) {
	tUserService.deleteTenant(id);
	return Result.success();
}

//租户查看自己的合同
@GetMapping("contractDetail/{id}")
public Result<TenantContractDetailVo> tenantContractDetail(@PathVariable("id") String id) {
	//租户有多个合同，每个合同有不同的期数，合同的起止时间和缴费情况
	return Result.success(tUserService.selectTenantContractDetail(id));
}
}
