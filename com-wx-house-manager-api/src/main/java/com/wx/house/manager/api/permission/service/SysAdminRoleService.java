package com.wx.house.manager.api.permission.service;

import com.wx.house.core.pojo.po.SysAdminRole;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminRoleService extends IService<SysAdminRole> {
	/***
	 * 分配菜单
	 * @param adminRole
	 * @return
	 */
	int addAdminRole(SysAdminRole adminRole);
	/***
	 * 删除角色菜单
	 * @param adminid
	 */
	void dateAdminRole(String adminid);
	/***
	 * 用户下的角色
	 */
	List<SysAdminRole> selectAdminIdRole(String adminId);
}
