package com.wx.house.manager.api.building.service.impl;

import com.wx.house.core.pojo.vo.HiredRoomVo;
import com.wx.house.core.pojo.vo.RoomNumberVo;
import com.wx.house.manager.api.building.service.BBuildingRoomService;
import com.wx.house.core.mapper.building.BuildingRoomMapper;
import com.wx.house.core.pojo.vo.BuildingRoomVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.building.BuildingRoom;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class BBuildingRoomServiceImpl extends ServiceImpl<BuildingRoomMapper, BuildingRoom> implements BBuildingRoomService {
private final BuildingRoomMapper buildingRoomMapper;

@Override
public List<BuildingRoomVo> selectSimpleWaitHireRoom(String buildingId, String floor, String door,String buildType) {
	return buildingRoomMapper.selectSimpleWaitHireRoom(buildingId,floor,door, buildType);
}

@Override
public List<BuildingRoomVo> selectWaitHireRoom(String buildingId, String floor) {
	return buildingRoomMapper.selectWaitHireRoom(buildingId, floor);
}

@Override
public List<HiredRoomVo> selectHiredRoomVo(String buildingId, String floor, String door) {
	return buildingRoomMapper.selectHiredRoomVo(buildingId, floor, door);
}

@Override
public BuildingRoom selectNumberRoom(String contrNumber) {
	return buildingRoomMapper.selectNumberRoom(contrNumber);
}

@Override
public List<RoomNumberVo> selectRoomNumber(String buildingId) {
	return buildingRoomMapper.selectRoomNumber(buildingId);
}

@Override
public BuildingRoom selectNumberIdRoom(String roomId) {
	return buildingRoomMapper.selectNumberIdRoom(roomId);
}
}
