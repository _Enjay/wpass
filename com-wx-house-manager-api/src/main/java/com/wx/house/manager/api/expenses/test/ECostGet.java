package com.wx.house.manager.api.expenses.test;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class ECostGet {
	@ApiModelProperty("根据租户搜索: 租户性名 租户手机 租户证件号码 合同编号")
	private String tenantKeyword;
	@ApiModelProperty("收费类型: 0保证金 1房租 2 物业管理费 3,4,5水费,电费,气费 6其他")
	private String chargeType;
	@ApiModelProperty("开始时间")
	private Date startTime;
	@ApiModelProperty("结束时间")
	private Date endTime;
	@ApiModelProperty("楼宇id")
	private String buildingId;
	@ApiModelProperty("房间号")
	private String RoomNumber;
}
