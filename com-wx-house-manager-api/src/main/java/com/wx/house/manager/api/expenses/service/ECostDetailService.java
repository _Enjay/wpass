package com.wx.house.manager.api.expenses.service;

import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.vo.CcontractNextChargeVo;


import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 下午 02:27
 * @description：${description}
 * @version: ：V$version
 */
public interface ECostDetailService extends IService<ECostDetail> {

int insertList(List<ECostDetail> list);
	/***
	 * 根据合同编号，收费月份查询记录
	 * @param contrNumber
	 * @param chargeMonth
	 * @return
	 */
	ECostDetail selectMonth(String contrNumber,String chargeMonth,String chargeType);
	/***
	 * 添加ECostDetail
	 */
	int insertECostDetail(ECostDetail costDetail);
	/***
	 * 根据当前时间查询需要生成房租的合同
	 * @param NextCharge
	 * @return
	 */
	List<CcontractNextChargeVo> selectNextCharge(String maccId,Date advanceDays);
}
