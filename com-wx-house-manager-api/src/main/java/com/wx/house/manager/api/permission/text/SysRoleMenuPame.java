package com.wx.house.manager.api.permission.text;

import java.util.List;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class SysRoleMenuPame {
	private String RoleId;
	private List<MenuId> MenuId;
}
