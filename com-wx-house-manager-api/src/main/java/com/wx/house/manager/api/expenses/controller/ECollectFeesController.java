package com.wx.house.manager.api.expenses.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.github.pagehelper.PageInfo;
import com.wx.house.manager.api.expenses.service.ECollectFeesService;
import com.wx.house.manager.api.expenses.test.ECollectFeesAdd;
import com.wx.house.manager.api.expenses.test.ECollectFeesGet;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.exception.UserException;
import com.wx.house.common.util.JwtUtils;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.vo.ECollectFeesVo;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Api(value = "模块:收费(退费)", tags = "收费(退费)")
@RequestMapping("ECollectFees/")
public class ECollectFeesController extends BaseController {
	  @Resource
	  private ECollectFeesService eCollectFeesService;
	  
	  @GetMapping("chargeList")
	  @ApiOperation(value = "收费（退费）管理列表", notes = "收费（退费）管理列表")
	  public Result<PageInfo<ECollectFeesVo>> getECollectFees(Page page, @Validated ECollectFeesGet eCollectFeesGet ,HttpServletRequest request) {
		  try {
				List<ECollectFeesVo> eCollectFees=null;
				eCollectFees= eCollectFeesService.selectECollectFees( eCollectFeesGet,this.getMaccId());
	    		PageInfo<ECollectFeesVo> pageInfo = new PageInfo<>(eCollectFees);
		    	return Result.success(pageInfo);
		} catch (Exception e) {
			 throw new UserException("查询收费失败", 201);
		}
	  }
	  @ApiOperation(value = "新增（退费）收费项", notes = "新增（退费）收费项")
	  @PostMapping("addECollectFees/")
		public Result addChargeECollectFees(@RequestBody @Validated ECollectFeesAdd eCollectFeesAdd) {
			try {
				eCollectFeesService.addECollectFees(eCollectFeesAdd,this.getMaccId());
				return Result.success();
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException("新增收费失败");
			}
		}
	  @GetMapping("feeMonth/")
	  @ApiOperation(value = "收费退费的年月集合", notes = "收费退费的年月集合")
		public Result selectFeeMonth(String SzType) {
			try {
				List<ECollectFees> collectFees=eCollectFeesService.selectFessMonth(SzType);
				List<String> ridList = collectFees.stream().map(ECollectFees::getFeeMonth).distinct().collect(Collectors.toList());
				System.out.println("---------"+ridList);
				return Result.success(ridList);
			} catch (Exception e) {
				e.printStackTrace();
				throw new BusinessException("新增收费失败");
			}
		}
}
