package com.wx.house.manager.api.expenses.test;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeterReadingModify{
	@ApiModelProperty(value = "id" )
	private String id;
	
	@ApiModelProperty(value = "合同编号" )
	private String contrNumber;
	
	@ApiModelProperty(value = "抄表月份" )
	private String feeMonth;

	@ApiModelProperty(value = "修改项：3水费 4电费 5气费" )
    private String feesType;

	@ApiModelProperty(value = "修改值" )
    private Integer monthvalue;
}
