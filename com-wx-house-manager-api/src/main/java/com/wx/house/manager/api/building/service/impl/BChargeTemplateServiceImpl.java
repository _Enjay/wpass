package com.wx.house.manager.api.building.service.impl;

import com.wx.house.manager.api.building.service.BChargeTemplateService;
import com.wx.house.core.mapper.building.BChargeTemplateMapper;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.building.BChargeTemplate;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class BChargeTemplateServiceImpl extends ServiceImpl<BChargeTemplateMapper, BChargeTemplate> implements BChargeTemplateService {

}
