package com.wx.house.manager.api.capital.service.impl;

import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.capital.SysAdminBankcardMapper;
import com.wx.house.core.mapper.capital.SysAdminDetailMapper;
import com.wx.house.core.mapper.permission.SysAdminMapper;
import com.wx.house.core.pojo.dto.capital.InsertWithdrawalDto;
import com.wx.house.core.pojo.dto.capital.ReviewWithdrawalDto;
import com.wx.house.core.pojo.po.SysAdmin;
import com.wx.house.core.pojo.po.SysAdminBankcard;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.state.WithdrawalStateEnum;
import com.wx.house.core.pojo.vo.SysAdminWithdrawalVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.SysAdminWithdrawal;
import com.wx.house.core.mapper.capital.SysAdminWithdrawalMapper;
import com.wx.house.manager.api.capital.service.SysAdminWithdrawalService;
import org.springframework.util.StringUtils;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:12
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class SysAdminWithdrawalServiceImpl extends ServiceImpl<SysAdminWithdrawalMapper, SysAdminWithdrawal> implements SysAdminWithdrawalService {
private final SysAdminWithdrawalMapper sysAdminWithdrawalMapper;
private final SysAdminBankcardMapper sysAdminBankcardMapper;
private final SysAdminMapper sysAdminMapper;
private final SysAdminDetailMapper sysAdminDetailMapper;
private final ProjectConfiguration projectConfiguration;

@Override
public List<SysAdminWithdrawalVo> selectByMaccId(String maccId, Integer state, String keyword) {
	List<SysAdminWithdrawalVo> sysAdminWithdrawals = sysAdminWithdrawalMapper.selectByMaccId(maccId, state, keyword);
	sysAdminWithdrawals.forEach(e -> e.setBankImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getBankImg()));
	return sysAdminWithdrawals;
}

@Override
public SysAdminWithdrawal selectById(String id) {
	return sysAdminWithdrawalMapper.selectById(id);
}

@Override
public void insertWithdrawal(InsertWithdrawalDto insertWithdrawalDto, String maccId) {
	SysAdminBankcard sysAdminBankcard = sysAdminBankcardMapper.selectById(insertWithdrawalDto.getPayModeId());
	SysAdmin sysAdmin = sysAdminMapper.selectById(maccId);
	SysAdminDetail sysAdminDetail = sysAdminDetailMapper.selectByMaccId(maccId);
	
	BigDecimal balance = sysAdminDetail.getBalance();
	BigDecimal withdrawal = insertWithdrawalDto.getWithdrawalBalance();
	
	BigDecimal afterWithdrawal = balance.subtract(withdrawal);
	BigDecimal freezeBalance = sysAdminDetail.getFreezeBalance().add(withdrawal);
	
	//余额小于要提现金额
	if (afterWithdrawal.compareTo(new BigDecimal(0)) < 0) {
		throw new BusinessException("余额不足，无法提现");
	}
	
	//1 创建提现记录
	SysAdminWithdrawal sysAdminWithdrawal = new SysAdminWithdrawal();
	sysAdminWithdrawal.setId(IDUtils.createUUId());
	sysAdminWithdrawal.setAccount(sysAdmin.getPhone().toString());
	sysAdminWithdrawal.setWithdrawalBalance(insertWithdrawalDto.getWithdrawalBalance());
	sysAdminWithdrawal.setWithdrawalType(sysAdminBankcard.getCardType());
	sysAdminWithdrawal.setOrgName(sysAdminBankcard.getCardOrgName());
	sysAdminWithdrawal.setOrgAttach(sysAdminBankcard.getCardOrgAttach());
	sysAdminWithdrawal.setOrgAccount(sysAdminBankcard.getCardNumber());
	sysAdminWithdrawal.setOrgCardUserName(sysAdminBankcard.getCardUserName());
	sysAdminWithdrawal.setOrgCardUserPhone(sysAdminBankcard.getCardUserPhone());
	sysAdminWithdrawal.setState(0);
	sysAdminWithdrawal.setCreate_time(new Date());
	sysAdminWithdrawal.setDeleted(0);
	sysAdminWithdrawal.setMaccId(maccId);
	sysAdminWithdrawal.setRemark("");
	sysAdminWithdrawal.setWithdrawalBalance(withdrawal);
	sysAdminWithdrawal.setBeforeWithdrawal(balance);
	sysAdminWithdrawal.setAfterWithdrawal(afterWithdrawal);
	
	//2 余额表扣除提现余额，新增冻结余额
	sysAdminDetail.setBalance(afterWithdrawal);
	sysAdminDetail.setFreezeBalance(freezeBalance);
	
	if (sysAdminDetailMapper.updateById(
			SysAdminDetail
					.builder()
					.balance(afterWithdrawal)
					.freezeBalance(freezeBalance)
					.build(),
			sysAdminDetail.getId()) != 1) {
		throw new BusinessException("扣除余额异常");
	}
	if (sysAdminWithdrawalMapper.insertSelective(sysAdminWithdrawal) != 1) {
		throw new BusinessException("生成提现记录失败");
	}
}

@Override
public void reviewWithdrawal(ReviewWithdrawalDto reviewWithdrawalDto) {
	SysAdminWithdrawal sysAdminWithdrawal = new SysAdminWithdrawal();
	sysAdminWithdrawal.setState(reviewWithdrawalDto.getState());
	if (!StringUtils.isEmpty(reviewWithdrawalDto.getRemark())) {
		sysAdminWithdrawal.setRemark(reviewWithdrawalDto.getRemark());
	}
	sysAdminWithdrawal.setUpdate_time(new Date());
	
	SysAdminWithdrawal withdrawal = sysAdminWithdrawalMapper.selectById(reviewWithdrawalDto.getWithdrawalId());
	if (withdrawal == null) throw new BusinessException("提现记录异常，请联系管理员");
	SysAdminDetail sysAdminDetail = sysAdminDetailMapper.selectByMaccId(withdrawal.getMaccId());
	if (sysAdminDetail == null) throw new BusinessException("用户信息异常，请联系管理员");
	
	if (reviewWithdrawalDto.getState().equals(WithdrawalStateEnum.ACCEPT.state)) {
		//审核成功，扣除冻结金额，增加已提现余额
		
		//扣除冻结金额
		BigDecimal freezeBalance = sysAdminDetail.getFreezeBalance().subtract(withdrawal.getWithdrawalBalance());
		//增加已提现金额
		BigDecimal withdrawalBalance = sysAdminDetail.getWithdrawals().add(withdrawal.getWithdrawalBalance());
		
		sysAdminDetailMapper.updateById(SysAdminDetail.builder()
				                                .freezeBalance(freezeBalance)
				                                .withdrawals(withdrawalBalance)
				                                .build(),
				withdrawal.getMaccId());
	} else if (reviewWithdrawalDto.getState().equals(WithdrawalStateEnum.REJECT.state)) {
		//审核失败，退还余额，扣除冻结金额
		
		//扣除冻结金额
		BigDecimal freezeBalance = sysAdminDetail.getFreezeBalance().subtract(withdrawal.getWithdrawalBalance());
		//退还余额
		BigDecimal balance = sysAdminDetail.getBalance().add(withdrawal.getWithdrawalBalance());
		
		sysAdminDetailMapper.updateById(SysAdminDetail.builder()
				                                .freezeBalance(freezeBalance)
				                                .balance(balance)
				                                .build(),
				withdrawal.getMaccId());
	}
	
	
	sysAdminWithdrawalMapper.updateById(sysAdminWithdrawal, reviewWithdrawalDto.getWithdrawalId());
}

@Override
public void deleteWithdrawal(String id) {
	sysAdminWithdrawalMapper.updateById(SysAdminWithdrawal.builder().deleted(1).build(), id);
}
}
