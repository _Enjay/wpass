package com.wx.house.manager.api.permission.text;

import lombok.Data;

@Data
public class RoleAddPame {
	private String roleid;
	private String roleName;
	private String remark;
	private String maccType;
}
