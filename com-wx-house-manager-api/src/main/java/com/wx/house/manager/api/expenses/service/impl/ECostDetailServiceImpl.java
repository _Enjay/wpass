package com.wx.house.manager.api.expenses.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.vo.CcontractNextChargeVo;
import com.wx.house.core.mapper.expenses.ECostDetailMapper;
import com.wx.house.manager.api.expenses.service.ECostDetailService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 下午 02:27
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class ECostDetailServiceImpl extends ServiceImpl<ECostDetailMapper, ECostDetail> implements ECostDetailService {
private final ECostDetailMapper eCostDetailMapper;

@Override
public int insertList(List<ECostDetail> list) {
	return eCostDetailMapper.insertList(list);
}

@Override
public ECostDetail selectMonth(String contrNumber, String chargeMonth,String chargeType) {
	return eCostDetailMapper.selectMonth(contrNumber, chargeMonth, chargeType);
}

@Override
public int insertECostDetail(ECostDetail costDetail) {
	return eCostDetailMapper.insert(costDetail);
}

@Override
public List<CcontractNextChargeVo> selectNextCharge(String maccId,Date advanceDays) {
	return eCostDetailMapper.selectNextCharge(maccId, advanceDays);
}
}
