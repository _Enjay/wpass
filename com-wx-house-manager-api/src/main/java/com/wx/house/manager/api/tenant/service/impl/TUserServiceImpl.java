package com.wx.house.manager.api.tenant.service.impl;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.common.util.StringUtil;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.core.pojo.dto.tenant.TUserInsertDto;
import com.wx.house.core.pojo.dto.tenant.TenantUpdateDto;
import com.wx.house.core.pojo.state.LogicalDeleteEnum;
import com.wx.house.core.pojo.state.TenantStateEnum;
import com.wx.house.core.pojo.vo.TenantContractDetailVo;
import com.wx.house.core.pojo.vo.TenantUserVo;
import com.wx.house.manager.api.tenant.service.TUserService;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.stereotype.Service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.tenant.TUserMapper;
import com.wx.house.core.pojo.po.TUser;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:39
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService {
private final TUserMapper tUserMapper;
private final AttaUploadMapper attaUploadMapper;

@Override
public List<TenantUserVo> selectAll(String keyword, String maccId) {
	return tUserMapper.selectAll(keyword,maccId);
}

@Override
public void deleteTenant(String id) {
	tUserMapper.deleteById(id);
}

@Override
public void insertTenant(TUserInsertDto tUserInsertDto, String maccId) {
	TUser tUser = new TUser();
	tUser.setId(IDUtils.createUUId());
	tUser.setName(tUserInsertDto.getName());
	tUser.setPhoneNumber(tUserInsertDto.getPhoneNumber());
	tUser.setIdCardNumber(tUserInsertDto.getIdCardNumber());
	tUser.setIdCardType(tUserInsertDto.getIdCardType());
	tUser.setDelType(LogicalDeleteEnum.NORMAL.state);
	tUser.setState(TenantStateEnum.NORMAL.state);
	tUser.setMaccid(maccId);
	String safekey = IDUtils.getSafeKey();
	tUser.setSafekey(safekey);
	String s = new Sha256Hash(tUserInsertDto.getPhoneNumber(), safekey, 1024).toString();
	tUser.setPassword(s);
	
	if (tUserMapper.insertSelective(tUser) != 1) throw new BusinessException("租户新增异常");
	
	if (tUserInsertDto.getAttachList().size() >= 1) {
		if(ObjectUtils.isNotEmpty(attaUploadMapper.selectResourceId(tUser.getId()))) {
			if((attaUploadMapper.deleteResourceId(tUser.getId()))==0) {
				throw new BusinessException("租户新增绑定附件异常,重新提交审核");
			}
		}
		if (attaUploadMapper.updateResourceIdByIdIn(tUser.getId(), tUserInsertDto.getAttachList()) != tUserInsertDto.getAttachList().size()) {
			throw new BusinessException("租户新增绑定附件异常");
		}
	} else throw new BusinessException("还没有上传证件图片信息");
	
}

@Override
public void updateTenant(TenantUpdateDto tenantUpdateDto) {
	TUser tUser = new TUser();
	tUser.setId(tenantUpdateDto.getId());
	if (StringUtil.isNotEmpty(tenantUpdateDto.getName())) {
		tUser.setName(tenantUpdateDto.getName());
	}
	if (StringUtil.isNotEmpty(tenantUpdateDto.getNickName())) {
		tUser.setNickName(tenantUpdateDto.getNickName());
	}
	if (StringUtil.isNotEmpty(tenantUpdateDto.getAvatar())) {
		tUser.setAvatar(tenantUpdateDto.getAvatar());
	}
	if (StringUtil.isNotEmpty(tenantUpdateDto.getPhoneNumber())) {
		tUser.setPhoneNumber(tenantUpdateDto.getPhoneNumber());
	}
	if (StringUtil.isNotEmpty(tenantUpdateDto.getIdCardNumber())) {
		tUser.setIdCardNumber(tenantUpdateDto.getIdCardNumber());
	}
	if (StringUtil.isNotEmpty(tenantUpdateDto.getIdCardType())) {
		tUser.setIdCardType(tenantUpdateDto.getIdCardType());
	}
	if (tenantUpdateDto.getState() != null) {
		tUser.setState(tenantUpdateDto.getState());
	}
	if (tenantUpdateDto.getDelType() != null) {
		tUser.setDelType(tenantUpdateDto.getDelType());
	}
	if (tenantUpdateDto.getAttachList().size() >= 1) {
		if (attaUploadMapper.updateResourceIdByIdIn(tUser.getId(), tenantUpdateDto.getAttachList()) != tenantUpdateDto.getAttachList().size()) {
			throw new BusinessException("租户新增绑定附件异常");
		}
	}
	tUserMapper.updateById(tUser, tUser.getId());
}

@Override
public TenantContractDetailVo selectTenantContractDetail(String tenantId) {
	return tUserMapper.selectTenantContractDetail(tenantId);
}

@Override
public List<TenantUserVo> selectIDAll(String idNumber, String maccId) {
	return tUserMapper.selectIDAll(idNumber,maccId);
}
}
