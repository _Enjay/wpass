package com.wx.house.manager.api.permission.text;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class loginPwdPame {
    @ApiModelProperty(value = "手机号", name = "phone",  required = true)
    @NotBlank(message ="手机号不能为空")
	private String phone;
	@ApiModelProperty(value = "密码", name = "loginPwd", example = "123456", required = true)
	@NotNull
	private String loginPwd;
}
