package com.wx.house.manager.api.building.service;

import com.wx.house.core.pojo.po.building.BChargeTemplate;
import com.baomidou.mybatisplus.extension.service.IService;
    /**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version:   ：V$version
 */
public interface BChargeTemplateService extends IService<BChargeTemplate>{


}
