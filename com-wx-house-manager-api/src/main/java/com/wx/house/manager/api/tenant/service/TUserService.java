package com.wx.house.manager.api.tenant.service;

import com.wx.house.core.pojo.dto.tenant.TUserInsertDto;
import com.wx.house.core.pojo.dto.tenant.TenantUpdateDto;
import com.wx.house.core.pojo.po.TUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.TenantContractDetailVo;
import com.wx.house.core.pojo.vo.TenantUserVo;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/12 上午 10:39
 * @description：${description}
 * @version: ：V$version
 */
public interface TUserService extends IService<TUser> {
List<TenantUserVo> selectAll(String keyword, String maccId);

List<TenantUserVo> selectIDAll(String idNumber, String maccId);

void deleteTenant(String id);

void insertTenant(TUserInsertDto tUserInsertDto, String maccId);

void updateTenant(TenantUpdateDto tenantUpdateDto);

TenantContractDetailVo selectTenantContractDetail(String tenantId);

}
