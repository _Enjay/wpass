package com.wx.house.manager.api.expenses.test;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MeterReadingGet{
	@ApiModelProperty("根据楼宇名称搜索")
	private String buildName;
	@ApiModelProperty("根据抄表月份搜索")
	private String month;
	@ApiModelProperty("根据抄表状态搜索")
	private String monthState;
}
