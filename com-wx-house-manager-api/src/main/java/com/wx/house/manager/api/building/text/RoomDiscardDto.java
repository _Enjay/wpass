package com.wx.house.manager.api.building.text;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel
public class RoomDiscardDto {
	private String roomId;
	private String buildType;
}
