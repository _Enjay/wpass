package com.wx.house.manager.api.permission.text;

import java.util.List;

import com.wx.house.core.pojo.po.SysAdminRole;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class SysAdminRolePame {
	private String adminId;
	private List<String> roleId;
}
