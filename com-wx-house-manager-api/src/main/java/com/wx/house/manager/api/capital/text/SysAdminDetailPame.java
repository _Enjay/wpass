package com.wx.house.manager.api.capital.text;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysAdminDetailPame {
	@ApiModelProperty(value = "真实姓名", name = "reallyName", example = "abcdefg", required = true)
	private String reallyName;
	@ApiModelProperty(value = "证件号码", name = "idCardNumber", example = "abcdefg", required = true)
	private String idCardNumber;
	@ApiModelProperty(value = "合同附件列表", example = "12312312312,12121521", required = true)
	private List<String> contractFileIdList = new ArrayList<>();
}
