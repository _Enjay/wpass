package com.wx.house.manager.api.expenses.service;

import com.wx.house.core.pojo.dto.expenses.SelectPaymentDto;
import com.wx.house.core.pojo.po.EPayment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.EPaymentVo;

import java.util.List;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version:   ：V$version
 */
public interface EPaymentService extends IService<EPayment>{
        /**
         * 删除支付方式
         * @param id
         */
      void  deletePaymentType(String id);

    /**
     * 查看支付方式列表
     * @param mAccId
     * @return
     */
     List<EPaymentVo> selectPayMentList(SelectPaymentDto selectPaymentDto);
}
