package com.wx.house.manager.api.task;

import com.wx.house.manager.api.contract.service.CContractService;
import com.wx.house.manager.api.expenses.service.EAutoMeterReadService;
import com.wx.house.manager.api.expenses.service.MeterReadingService;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.pojo.bo.CContractBo;
import com.wx.house.core.pojo.po.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.wx.house.core.pojo.state.MeterReadingEnum.WAIT_READING;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/06 上午 09:59
 * @description：
 * @version: ：V
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class ReadingMeterTask {
private final EAutoMeterReadService eAutoMeterReadService;
private final CContractService cContractService;
private final MeterReadingService meterReadingService;

/***
 * 每月自动生成抄表
 */
@Scheduled(cron = "0 23 10 * * ?")
@Transactional(rollbackFor = BusinessException.class)
public void rent() {
	List<EAutoMeterRead> eAutoMeterReads = eAutoMeterReadService.selectAll();
	Date date = new Date();
	String dd = new SimpleDateFormat("dd").format(date);
	for (int i = 0; i < eAutoMeterReads.size(); i++) {
		if(eAutoMeterReads.get(i).getMeterReadNum()==Integer.valueOf(dd)) {
			List<CContractBo> cContracts = cContractService.selectByMAccId(eAutoMeterReads.get(i).getMAccId());
			cContracts.forEach(cContract -> {
					String datetime=new SimpleDateFormat("yyyy-MM").format(date);
					MeterReading meterReading = new MeterReading();
					meterReading.setId(IDUtils.createUUId());
					meterReading.setBuildingId(cContract.getBuildingRoom().getBuildingId());
					meterReading.setDoorNumber(cContract.getBuildingRoom().getFloorId()+"-"+cContract.getBuildingRoom().getDoorNumber());
					meterReading.setContrNumber(cContract.getContrNumber());
					meterReading.setTenantName(cContract.getTenantName());
					meterReading.setPhone(cContract.getTenantPhone());
					meterReading.setMonth(datetime);
					meterReading.setMonthElectric(0);
					meterReading.setMonthGas(0);
					meterReading.setMonthWater(0);
					meterReading.setMonthElectric(0);
					meterReading.setMonthState(WAIT_READING.state);
					if(ObjectUtils.isEmpty(meterReadingService.selectNumberMonth(cContract.getContrNumber(), datetime))) {
						if(meterReadingService.addMeter(meterReading)!=1) {
							log.error("生成抄表计划异常:" + meterReading);
							throw new BusinessException("生成抄表计划异常：" + cContract);
						}
					}
			});
		}
	}
//	if (meterReadingList.size() > 1) {
//		meterReadingService.insertList(meterReadingList);
//	}
	
	
//	eAutoMeterReads.forEach(e -> {
//		if (e.getMeterReadNum().equals(dd)) {
//			List<CContractBo> cContracts = cContractService.selectByMAccId(e.getMAccId());
//			cContracts.forEach(cContract -> {
//				try {
//					MeterReading meterReading = new MeterReading();
//					meterReading.setId(IDUtils.createUUId());
//					meterReading.setBuildingId(cContract.getBuildingRoom().getBuildingId());
//					meterReading.setDoorNumber(cContract.getBuildingRoom().getDoorNumber());
//					meterReading.setContrNumber(cContract.getId());
//					meterReading.setTenantName(cContract.getTenantName());
//					meterReading.setPhone(cContract.getTenantPhone());
//					meterReading.setMonth(new SimpleDateFormat("yyyy-mm").format(date));
//					meterReading.setMonthElectric(new BigDecimal(0));
//					meterReading.setMonthGas(new BigDecimal(0));
//					meterReading.setMonthGas(new BigDecimal(0));
//					meterReading.setMonthElectric(new BigDecimal(0));
//					meterReading.setMonthState(WAIT_READING.state);
//					meterReadingList.add(meterReading);
//				} catch (Exception ex) {
//					log.error("异常生成计划:" + e.toString());
//					System.out.println("异常生成计划:" + e.toString());
//				}
//			});
//		}
//	});
//	if (meterReadingList.size() > 1)
//		meterReadingService.insertList(meterReadingList);
}
}
