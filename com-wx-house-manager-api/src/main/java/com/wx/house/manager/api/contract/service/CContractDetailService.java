package com.wx.house.manager.api.contract.service;

import com.wx.house.core.pojo.po.ContractDetail;
import com.baomidou.mybatisplus.extension.service.IService;
    /**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version:   ：V$version
 */
public interface CContractDetailService extends IService<ContractDetail>{


}
