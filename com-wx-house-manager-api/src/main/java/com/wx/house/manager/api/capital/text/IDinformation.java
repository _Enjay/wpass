package com.wx.house.manager.api.capital.text;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class IDinformation {
	@ApiModelProperty("用户地址")
	private String address;
	@ApiModelProperty("出生年月")
	private String birthday;
	@ApiModelProperty("姓名")
	private String name;
	@ApiModelProperty("身份证号")
	private String code;
	@ApiModelProperty("性别")
	private String sex;
	@ApiModelProperty("民族")
	private String nation;
}
