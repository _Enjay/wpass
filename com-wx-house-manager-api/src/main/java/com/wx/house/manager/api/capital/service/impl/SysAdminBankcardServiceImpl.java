package com.wx.house.manager.api.capital.service.impl;

import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.core.pojo.dto.capital.InsertBankcardDto;
import com.wx.house.core.pojo.state.LogicalDeleteEnum;
import com.wx.house.core.pojo.vo.SysAdminBankcardVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.capital.SysAdminBankcardMapper;
import com.wx.house.core.pojo.po.SysAdminBankcard;
import com.wx.house.manager.api.capital.service.SysAdminBankcardService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:44
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class SysAdminBankcardServiceImpl extends ServiceImpl<SysAdminBankcardMapper, SysAdminBankcard> implements SysAdminBankcardService {
private final SysAdminBankcardMapper sysAdminBankcardMapper;
private final AttaUploadMapper attaUploadMapper;
private final ProjectConfiguration projectConfiguration;

@Override
public List<SysAdminBankcard> selectAll(String maccId, String keyword) {
	return sysAdminBankcardMapper.selectAll(maccId, keyword);
}

@Override
public List<SysAdminBankcardVo> selectAllVo(String maccId) {
	List<SysAdminBankcardVo> sysAdminBankcardVos = sysAdminBankcardMapper.selectAllVo(maccId);
	sysAdminBankcardVos.forEach(e -> {
		e.setBankImg(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getBankImg());
	});
	return sysAdminBankcardVos;
}

@Override
public int insertSelective(SysAdminBankcard sysAdminBankcard) {
	return sysAdminBankcardMapper.insertSelective(sysAdminBankcard);
}

@Override
public void addBankCard(InsertBankcardDto bankcardDto, String maccId) {
	
	if (bankcardDto.getPreferred() != null) {//如果新增一个默认银行卡
		// 将其他的银行卡都设置成非默认
		sysAdminBankcardMapper.updatePreferredByMaccId(bankcardDto.getPreferred(), maccId);
	}
	
	SysAdminBankcard sysAdminBankcard = new SysAdminBankcard();
	sysAdminBankcard.setId(IDUtils.createUUId());
	sysAdminBankcard.setMaccId(maccId);
	sysAdminBankcard.setDeleted(LogicalDeleteEnum.NORMAL.state);
	sysAdminBankcard.setPreferred(bankcardDto.getPreferred());
	sysAdminBankcard.setCardOrgName(bankcardDto.getCardOrgName());
	sysAdminBankcard.setCardOrgAttach(bankcardDto.getCardOrgAttach());
	sysAdminBankcard.setCardNumber(bankcardDto.getCardNumber());
	sysAdminBankcard.setCardUserName(bankcardDto.getCardUserName());
	sysAdminBankcard.setCardUserPhone(bankcardDto.getCardUserPhone());
	sysAdminBankcard.setCardType(bankcardDto.getCardType());
	sysAdminBankcard.setState(0);
	sysAdminBankcard.setCreateTime(new Date());
	
	
	if (sysAdminBankcardMapper.insertSelective(sysAdminBankcard) != 1) {
		throw new BusinessException("银行卡新增失败");
	}
}

@Override
public void setDefault(String maccId, String id) {
	sysAdminBankcardMapper.updatePreferredByMaccId(0, maccId);
	sysAdminBankcardMapper.updateById(SysAdminBankcard.builder().preferred(1).build(), id);
}

@Override
public void deleteBankcard(String id) {
	sysAdminBankcardMapper.updateById(SysAdminBankcard.builder().deleted(1).build(), id);
}
}



