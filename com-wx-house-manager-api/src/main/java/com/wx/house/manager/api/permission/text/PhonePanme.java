package com.wx.house.manager.api.permission.text;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PhonePanme {
	@ApiModelProperty(value = "手机号", example = "13344445555", required = false)
	private String phone;
	@ApiModelProperty(value = "验证码", example = "xxxx", required = false)
	private String code;
}
