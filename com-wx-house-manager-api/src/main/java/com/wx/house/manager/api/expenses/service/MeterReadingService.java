package com.wx.house.manager.api.expenses.service;

import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.po.MeterReading;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.wx.house.core.pojo.vo.MeterReadingVo;
import com.wx.house.manager.api.expenses.test.MeterReadingModify;
import com.wx.house.manager.api.expenses.test.meterIdPame;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
public interface MeterReadingService extends IService<MeterReading> {
/***
 * 新增抄表
 * @param meterReading
 * @return
 */
int addMeter(MeterReading meterReading);

/***
 * 查询全部抄表记录
 * @param buildName
 * @param month
 * @return
 */
List<MeterReadingVo> AllMeter(String buildName, String month, String monthState, String maccid);

/***
 * 更新抄表
 * @param meterReading
 * @return
 */
int updateMeter(MeterReading meterReading);

/***
 * 完成抄表
 * @param meterId
 * @return
 */
int addcompleteMeter(meterIdPame meterIdPame);


int insertList(List<MeterReading> list);

void updateMeterReading(MeterReading meterReading,BuildingRoom buildingRoom,ECostDetail eCostDetail,ECost eCost);
/***
 * 根据合同编号，抄表月份判断是否存在
 * @param number
 * @param month
 * @return
 */
MeterReading selectNumberMonth(String number,String month);
}
