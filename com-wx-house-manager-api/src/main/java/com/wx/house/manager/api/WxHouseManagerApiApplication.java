package com.wx.house.manager.api;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;

import javax.annotation.Resource;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan({
		"com.wx.house.manager.api",
		"com.wx.house.common",
		"com.wx.house.core"
})
@EnableTransactionManagement(proxyTargetClass = true)
@MapperScan("com.wx.house.core.mapper")
@EnableSwaggerBootstrapUI
@EnableSwagger2
@EnableScheduling
public class WxHouseManagerApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(WxHouseManagerApiApplication.class, args);
	}
	
}