package com.wx.house.manager.api.expenses.test;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class meterIdPame {
	@ApiModelProperty(value = "抄表id" )
	private String meterId;
	@ApiModelProperty(value = "合同编号" )
	private String contrNumber;
	@ApiModelProperty(value = "抄表月份" )
	private String month;
	@ApiModelProperty(value = "水表读数(立方米)" )
	private Integer monthWater;
	@ApiModelProperty(value = "电表读数(千瓦时)" )
	private Integer monthElectric;
	@ApiModelProperty(value = "气表读数(立方米)" )
	private Integer monthGas;
}
