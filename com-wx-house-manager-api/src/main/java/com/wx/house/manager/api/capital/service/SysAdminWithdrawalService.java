package com.wx.house.manager.api.capital.service;

import com.wx.house.core.pojo.dto.capital.InsertWithdrawalDto;
import com.wx.house.core.pojo.dto.capital.ReviewWithdrawalDto;
import com.wx.house.core.pojo.po.SysAdminWithdrawal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.SysAdminWithdrawalVo;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:12
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminWithdrawalService extends IService<SysAdminWithdrawal> {
List<SysAdminWithdrawalVo> selectByMaccId(String maccId, Integer state, String keyword);

SysAdminWithdrawal selectById(String id);

void insertWithdrawal(InsertWithdrawalDto insertWithdrawalDto, String maccId);

void reviewWithdrawal(ReviewWithdrawalDto reviewWithdrawalDto);

void deleteWithdrawal(String id);
}
