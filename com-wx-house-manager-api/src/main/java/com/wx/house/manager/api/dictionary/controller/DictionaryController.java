package com.wx.house.manager.api.dictionary.controller;

import com.wx.house.manager.api.dictionary.service.DictionarySerivce;
import com.wx.house.manager.api.dictionary.test.DicBaseTesting;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.Constant;
import com.wx.house.common.util.LevelUtil;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.po.DicBaseDictionary;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.dictionary.controller
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/11 15:15
 * @Version V1.0
 */
@RestController
@RequestMapping("dicbase/")
@Api(value = "字典管理", tags = "字典管理")
@Slf4j
public class DictionaryController extends BaseController {
    @Resource
    private DictionarySerivce dictionarySerivce;
    public DictionaryController() {
    }
    @GetMapping("findDic/")
    @ApiOperation(value = "获取系统字典", notes = "获取系统字典" + "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "节点的id（根据节点的id查询树）", required = false, dataType = "Integer", paramType = "query"),
    })
    public Result findDic(@RequestParam(defaultValue = "0") Integer id) {
        UserInfo userInfo = this.userInfo();
        // 查询所有的分类
        Map<String, Object> tree = dictionarySerivce.findDicTree(id,userInfo.getMAccId());
        return Result.success(tree);
    }
    /**
     * 新增基础字典
     *
     * @param
     * @param
     * @return
     */
    @PostMapping("add/")
    @ApiOperation(value = "新增字典", notes = "添加一个字典")
    public Result addDicBase(@RequestBody @Validated DicBaseTesting dicBaseTesting, HttpServletRequest request) {
        UserInfo userInfo = this.userInfo();
        DicBaseDictionary.DicBaseDictionaryBuilder dicBaseDictionaryBuilder = DicBaseDictionary.builder();
        //给对象赋值
        dicBaseDictionaryBuilder.dicName(dicBaseTesting.getDicName());
        //添加顶级字典
        if (dicBaseTesting.getParentId()==0) {
            dicBaseDictionaryBuilder.parentId(Long.parseLong("0"))
                    .level("0")
                    .value("0")
                    .dicNum(dicBaseTesting.getDicNum());
        }else {
            //添加子目录
            //根据选中的id查询详情
            DicBaseDictionary dicBaseDictionary = dictionarySerivce.searchDicBaseById(dicBaseTesting.getParentId());
            dicBaseDictionaryBuilder.parentId(dicBaseTesting.getParentId())
                    .level(LevelUtil.calculateLevel(dicBaseDictionary.getLevel(), dicBaseTesting.getParentId().intValue()))
                    .value(dicBaseTesting.getValue())
                    .dicNum(dicBaseTesting.getDicNum());
        }
        dicBaseDictionaryBuilder.operationTime(new Date())
                .order(dicBaseTesting.getOrder())
                .logicalState(Long.parseLong("1"))
                .mAccId(userInfo.getMAccId())
                .operation("jack").build();


        dictionarySerivce.addDicBase(dicBaseDictionaryBuilder.build());

        return  Result.success("新增成功");
    }

    /**
     * 更新字典表
     */

    @PutMapping("update/")
    @ApiOperation(value = "基础字典表的更新", notes = "更新字典")
    public Result updateDicBase(@RequestBody @Validated  DicBaseTesting dicBaseTesting,HttpServletRequest request) {
        DicBaseDictionary.DicBaseDictionaryBuilder dicBuilder = DicBaseDictionary.builder();
        dicBuilder.id(dicBaseTesting.getId())
                .dicNum(dicBaseTesting.getDicNum())
                .order(dicBaseTesting.getOrder())
                .parentId(dicBaseTesting.getParentId())
                .value(dicBaseTesting.getValue())
                //获取当前操作人
                .operation("jack")
                .operationTime(new Date())
                .dicName(dicBaseTesting.getDicName());
        dictionarySerivce.updateDicBaseById(dicBuilder.build());
        return Result.success("更新成功");
    }

    /**
     * 删除字典表数据
     */

    @DeleteMapping("delete/{id}")
    @ApiOperation(value = "基础字典表的删除", notes = "删除字典")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "Integer", paramType = "path"),
    })
    public Result deleteDicBase(@PathVariable Integer id) {
//        //系统字典的字典不能删除（1-12）
//        if(id<=12){
//            throw  new BusinessException("系统自带字典不能删除！！！");
//        }else {
            dictionarySerivce.deleteDicBaseById(id);
     //   }
        return Result.success("删除成功");
    }

  /**
     * 遍历字典表数据
     *
     * @param
     * @return
     */
    @GetMapping("list/")
    @ApiOperation(value = "查询字典下级目录", notes = "根据父id，名称查找字典下级目录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", value = "类别(0支付方式 1收费类型 2角色类型 3支付公司,4收费类型单位,5租金类型,6修改抄表类型,7待处理费用类型,8收支类型)", required = true, dataType = "Integer", paramType = "query"),
    })
    public Result searchDicBaseCondion(Integer type) {
        UserInfo userInfo = this.userInfo();
        Map<String, Object> tree=dictionarySerivce.findDicTree(Constant.IDS[type],userInfo.getMAccId());
      return  Result.success(tree);
    }

    /**
     * 根据id查询出字典表的详情
     */
    @GetMapping("search/{id}")
    @ApiOperation(value = "查询字典详情", notes = "根据id查找字典详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String", paramType = "path"),
    })
    public Result searchDicById(@PathVariable Long id) {
        DicBaseDictionary dicBase = dictionarySerivce.searchDicBaseById(id);
        return  Result.success(dicBase);
    }

  /*  *//**
     * 根据id查询出父id的详情
     *//*
    @GetMapping("{id}")
    @ApiOperation(value = "查询字典父id", notes = "根据id查找字典父id的详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", required = true, dataType = "String", paramType = "path"),
    })
    public Result searchParentIdById(@PathVariable Long id) {
        DicBaseDictionary dicBase = dictionarySerivce.selectParentIdById(id);
        return  Result.success(dicBase);
    }*/
}
