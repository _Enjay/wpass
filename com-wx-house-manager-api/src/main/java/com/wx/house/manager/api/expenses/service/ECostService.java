package com.wx.house.manager.api.expenses.service;

import com.wx.house.core.pojo.dto.expenses.ECostDto;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.vo.ECostDetailVo;
import com.wx.house.core.pojo.vo.ECostVo;
import com.wx.house.manager.api.expenses.test.ECostGet;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version:   ：V$version
 */
public interface ECostService extends IService<ECost>{

	int insertList(List<ECost> list);
	/***
	 * 查询待处理费用列表
	 * @param mAccId
	 * @param contractKeyword
	 * @param tenantKeyword
	 * @param state
	 * @return
	 */
	List<ECostVo> selectAllEcost(ECostGet eCostGet,String maccid);
	/***
	 * 根据待处理id查询待处理详情
	 * @param eCostId
	 * @return
	 */
	ECostDetailVo selectAllEcostDetail(String eCostId);
	/***
	 * 确定待处理费用
	 * @param eCost
	 * @return
	 */
	void addEcost(ECostDto eCostDto);
	/***
	 * 根据合同，月份查询是否存在当月记录
	 * @param contrId
	 * @param month
	 * @return
	 */
	ECost selectMonth(String contrId,String month,String payTypes);
	/***
	 * 添加ECost
	 * @param eCost
	 * @return
	 */
	int insertECost(ECost eCost);
	/***
	 * 根据id查询
	 * @param id
	 * @return
	 */
	ECost selectId(String id);
}
