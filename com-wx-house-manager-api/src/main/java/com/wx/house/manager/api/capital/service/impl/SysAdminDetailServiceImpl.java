package com.wx.house.manager.api.capital.service.impl;

import com.wx.house.core.pojo.vo.SysAdminBalanceVo;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.vo.SysAdminDetailVo;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.core.mapper.building.BuildingRoomMapper;
import com.wx.house.core.mapper.capital.SysAdminDetailMapper;
import com.wx.house.core.mapper.contract.CContractDetailMapper;
import com.wx.house.core.mapper.contract.CContractMapper;
import com.wx.house.core.mapper.expenses.EAutoCreatePlanMapper;
import com.wx.house.core.mapper.expenses.ECollectFeesMapper;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.manager.api.capital.service.SysAdminDetailService;
import com.wx.house.manager.api.capital.text.SysAdminDetailPame;

import lombok.RequiredArgsConstructor;


/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 02:31
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class SysAdminDetailServiceImpl extends ServiceImpl<SysAdminDetailMapper, SysAdminDetail>
		implements SysAdminDetailService {
@Resource
private SysAdminDetailMapper sysAdminDetailMapper;
@Resource
private final AttaUploadMapper attaUploadMapper;

@Override
public SysAdminDetail selectByMaccId(String maccId) {
	return sysAdminDetailMapper.selectByMaccId(maccId);
}

@Override
public SysAdminBalanceVo selectBalance(String maccId) {
	return sysAdminDetailMapper.selectBalance(maccId);
}

@Override
public int addSysAdminDetail(SysAdminDetail adminCapital) {
	return sysAdminDetailMapper.insert(adminCapital);
}

@Override
public int updateCard(String adminId, SysAdminDetailPame sysAdminDetailPame) {
	if(ObjectUtils.isNotEmpty(attaUploadMapper.selectResourceId(adminId))) {
		if((attaUploadMapper.deleteResourceId(adminId))==0) {
			throw new BusinessException("租户新增绑定附件异常,重新提交审核");
		}
	}
	if(attaUploadMapper.updateResourceIdByIdIn(adminId, sysAdminDetailPame.getContractFileIdList())==0){
		throw new BusinessException("租户新增绑定附件异常");
	}
	return sysAdminDetailMapper.updateReal(adminId, sysAdminDetailPame.getReallyName(), sysAdminDetailPame.getIdCardNumber());
}

@Override
public SysAdminDetailVo selectAttaUpload(String adminId) {
	return sysAdminDetailMapper.selectAdminUpload(adminId);
}

@Override
public SysAdminDetail selectIdCardNumber(String idCardNumber) {
	return sysAdminDetailMapper.selectIdCardNumber(idCardNumber);
}
}
