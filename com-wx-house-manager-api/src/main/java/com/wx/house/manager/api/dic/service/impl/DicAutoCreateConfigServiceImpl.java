package com.wx.house.manager.api.dic.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.DicAutoCreateConfig;
import com.wx.house.core.mapper.dic.DicAutoCreateConfigMapper;
import com.wx.house.manager.api.dic.service.DicAutoCreateConfigService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 上午 11:03
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class DicAutoCreateConfigServiceImpl extends ServiceImpl<DicAutoCreateConfigMapper, DicAutoCreateConfig> implements DicAutoCreateConfigService {

}
