package com.wx.house.manager.api.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.po.TOrderDetail;

/**
* @Author     ：罗棋
* @Email      ：58428467@qq.com
* @date       ：Created in 2019/11/29 上午 11:27
* @description：${description}
* @version:   ：V$version
*/
public interface TOrderDetailService extends IService<TOrderDetail>{


}
