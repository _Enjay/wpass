package com.wx.house.manager.api.fileupload.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.pojo.dto.fileload.AttaUploadDto;
import com.wx.house.manager.api.fileupload.config.IpConfiguration;
import com.wx.house.manager.api.fileupload.service.FileUploadService;
import com.wx.house.manager.api.fileupload.util.ZimgClient;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.fileupload.AttaUploadMapper;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.fileupload.service.impl
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/11 9:26
 * @Version V1.0
 */
@Slf4j
@Service
public class FileUploadServiceImpl extends ServiceImpl<AttaUploadMapper, AttaUpload> implements FileUploadService {
@Resource
private AttaUploadMapper attaUploadMapper;
@Resource
private ProjectConfiguration projectConfiguration;

@Override
public AttaUpload multiUploads(MultipartFile file, String type, String id) {
	String fileName = null;
	AttaUpload upload = new AttaUpload();
	try {
		if (file.isEmpty()) {
			log.error("上传失败，上传文件为空");
		} else {
			Path path = Paths.get(projectConfiguration.getStaticResource().getStaticResourcePath());
			if (!Files.exists(path)) {
				Files.createDirectories(path);
			}
			//存入数据库
			upload.setId(id);
			upload.setType(type);
			//获取上传文件名，受浏览器影响
			String fileSuffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
			upload.setAttaName(file.getOriginalFilename());
			upload.setFileSuffix(fileSuffix);
			//生成文件名称
			fileName = IDUtils.genImageName();
			String pathName = projectConfiguration.getStaticResource().getStaticResourcePath() + fileName + fileSuffix;
			File dest = new File(pathName);
			file.transferTo(dest);
			dest.setWritable(true);
			upload.setPath(pathName);
			//设置文件预览路径
			upload.setUrl(fileName + fileSuffix);
			upload.setCreatTime(new Date());
			//存入数据库
			this.attaUploadMapper.insert(upload);
			System.gc();
		}
	} catch (IOException e) {
		log.error("上传失败，原因： " + e);
	}
	return upload;
}

@Override
public void getImg(String name, HttpServletResponse response) {
	response.setContentType("image/jpeg;charset=utf-8");
	response.setHeader("Content-Disposition", "inline; filename=girls.png");
	ServletOutputStream outputStream = null;
	try {
		outputStream = response.getOutputStream();
		outputStream.write(Files.readAllBytes(Paths.get(ZimgClient.imgLocalpath).resolve(name)));
		outputStream.flush();
		outputStream.close();
	} catch (IOException e) {
		e.printStackTrace();
	}
}
	@Override
	public AttaUpload upload(AttaUploadDto attaUploadDto) {
        //对字节数组字符串进行Base64解码并生成图片
		//从字符串中获取图片后缀
		AttaUpload upload = new AttaUpload();
		String fileName = null;
		String fileNameSuffix="."+attaUploadDto.getEncodedImageStr().split(";base64,")[0].split("/")[1];
		String imgStr=attaUploadDto.getEncodedImageStr().split(";base64,")[1];
		BASE64Decoder decoder = new BASE64Decoder();
		try
			{
					Path path = Paths.get(projectConfiguration.getStaticResource().getStaticResourcePath());
					if (!Files.exists(path)) {
						Files.createDirectories(path);
					}
			//Base64解码
			byte[] bytes = decoder.decodeBuffer(imgStr);
			//文件保存路径
			fileName=IDUtils.genImageName();
	        String pathName=projectConfiguration.getStaticResource().getStaticResourcePath() + fileName + fileNameSuffix;
			OutputStream out = new FileOutputStream(pathName);
		    IOUtils.write(bytes,out);
			out.flush();
			out.close();
			//保存数据库
				//存入数据库
				upload.setId(IDUtils.genItemId(13)+"");
				upload.setType(attaUploadDto.getType());
				//获取上传文件名，受浏览器影响
				upload.setAttaName(attaUploadDto.getFileUploadPath());
				upload.setFileSuffix(fileNameSuffix);
				upload.setPath(pathName);
				//设置文件预览路径
				upload.setUrl(fileName + fileNameSuffix);
				upload.setCreatTime(new Date());
				attaUploadMapper.insert(upload);
			}
			catch (Exception e)
			{
              e.printStackTrace();
              log.error("手机文件上传出错"+e);
				System.out.println("文件上传失败");
           }
       return upload;
	}
	public static void main(String[] args) throws IOException {
	//String s="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAPwAvQDASIAAhEBAxEB/8QAHwAAAQQCAwEBAAAAAAAAAAAABAIDBQYBBwAICQoL/8QASRAAAgICAQQBAwMDAwIDBAITAQIDEQQSBQYTISIHAAgxIzJBCRRRFUJhM3EWUoEKFyRDYpHwoRglU3KxwfE0gtHhGSY1Y5Ki/8QAHgEAAgIDAQEBAQAAAAAAAAAAAgMBBAAFBgcICQr/xABHEQEAAgEDAwIEBAMGBQMCAgsBAhEhAxIxACJBBFEFEzJhBkJxgVKRoQcUI2Kx8AhywdHhFTPxFiSCJUOSNFOiCRhjk8Ly/9oADAMBAAIRAxEAPwDzmbCxyoVoUCErKLBMgZo6WN2YiZmZXBljcgM6I82zoB9Xfp7FmSMR46JHC7IsJSR5FOQrQI8cjtGEMjG5pYtHCJNDv3GUxy1vBxBBjOyZEkveTZkEYOqJHMixvbIzxmJDs8MbxxyCcu76Fk2b0LJhyjHxp2kByMyJgjCRdez3HeVolU91qh7wDLZx4iGYEFV+afSSl/eJBKY7kKvBKVckqzWWizhikr9A1Zx20njkIlm2N3l2mZctbQk1am9Ok4Zcfj6VQkDk1E75BGNJqzuvs0ZllXeHfdRjlArLGsoUpeYCEqpQfDM4Jj7iK277AIlXY11ANKi7R1SxwXHwx46Fi5jAAjTd5pBCV1LhpWJYoupaN5LCKdESN5ChlYchWMUbmTY+Y6llVSolZRI9Kf7e1AXQswZH8MDRf0D0mhJ0olq7btlKrqL25UoVsrhoODQ+oY7qRtZRyUSEEw4Fw57vFc3Iu8bkdxQSGj1chmdWQAoY0KsQ4JHh/JDPSBVIduRUk7amNZtO/A7IzRxqzR1IyFRHKVDxpjqSqDVSrhgWSQSMuIu5IYbLeoO3aMCysWWnij0mjoxe6BwsrSyyBojGxEkG3btSjW4cgAgAuJklVwinuiRJTRYRjvrcQ0ZZnSJ6cZxmyJXmXd20xq9r5rcNt8X1UYxqKAlDtdraAGG2qUlbRmrTMnjuZ0jkRl/YBa2oZdlIZSUJkGmossFO23bFofp4ks5EZGoVkKg7AkSBRsWJcCI7qRbbXbDUeWMadTqp1AtlDdzxZciJURiN2kKOzuddD6x90UPpcqukmzKDEbjNln3DyqXbUEkIyggLKjlVUKohiLNIejYbllL6SlUuw45QOeYlAbaTqnKQy7fp5j3L7ItOSoqeHwO5U1WZTqL1H4LEP/3AsAjx5HjwSSSzDZkMRYBlKsWjIUGhI6rOfb9iuUjvtqY2I1eUkaKAFkSSBX7ZWFQSDIBsyKoYxnU9uypCmQdsqRUIDhnaMyEFo1YvG7qgDtsNi2qnRqULdAv6LGvsPTZlUWfmz2sN8qxQ3RWKBaCxKCsYoUQoM1VhV4bw1Ief3slIa+lp6OMTsAFADABm2IWgo9y7BtizaIrEAbSEo1p2ml047tg1NLEARHGwKMCNVVmbtyEHuEAEbRlBTMmpVQiNJIgHMY2lLPcaqCPcMY1kbyTflgGYEHtsW/aTw5CRrIrUQJFcozFQzOq2e2wpRGr90nVUYKyAFGNT5kyQ7hGrSS0ULQPdaU0WxFsp6zTlGUqYql5V2xrb3DZkHlCsVyoxNLRMhNKY9hIqMsi+qMYWqUuSY3MjIEVlAMkSvKAySeLOmRFaBijI6BZLUNVLTGmZgWLWxJKhv2hmKgGWIBNhofyIkDyHwjAOSN3cldghQKWZpC7GSXsqj4kAj8EaBRahGdWoFZEZkRdZFJMQGqNLVBVs6zqa2rpsU4U82gAYpu65SkGkeiMqNFxjlVGSkXx/pY+c9aU+dPjzP674CTj+MyTFKakfHIYRSqikBSipbs7shZiZZBqWRPDhfPzL+D8vhX5Dj58FYeRyJVPdWNnxsVRJpatjqxSKI2ixoGLvIsYWMWx9dsZI5Csu7MO2VBJjjjfu6yOzBSbA2pgzhQzbf7VP1H8nw+ByEjNkYyOrOCEkRXDTKFYFF0ZmDFbJk3ClCxVlFtkZak6kuFcblMVGzu4APJdcGBry0mU2RZYBmkwL4rMsljgy3nrxvHwr1Fi8iBPC2ThY+oyciXGeXZGYFn48zK4ZVSR0bIZ4hDE1SFJpJVETnfH/ACvLZj8RxHSOWuxk/txLg5bGX9Ru4zLHGESGMooGUxCRjttogZXPszJw/Dl3BxoFRI2tXSF4zIFPr2o0O4IUHYq5AuBFQsIw2uHxOAMvJVMPG7QMx7eIqBYqOjFVUutsUkVmBVe7GWZ3NCyiwDc3ecyk0hdOQQzSKe1WFDW9NulJsoRTAMjZ5aaA5xy82deZHSP2Z8cMSXl/kHPniEWNFmpjQpihYcQYzTTR5cuQmoIR0bWMxSpIHBdhqUv/AE19unTfyvm5XMyq/H9K8XmrxnG40CBZsuHEEMa7NNFf9uyCoiGUgNkRKwkDZQ2v8n/OfR55Jej8PKjnMnIDCz54ZsVcAtjzBpccqdolxUkuOTIQxoR2sfdVnjEnajpDjeHx+B4//TMbGgxJI42K44x0XeaBHmJeNTErtujGGxG501BMSMp6OpthukWm1ptlbUhVa7q3VyKoV0mEBWMaKfajBFB90uRdI8KVZ89/3AdB9Q/GXX/OdOYnO5C4GJkSZPHJhTS4sAxJbQxY4jkOjDbR4lJkkjeW43iCB6j0T8t/IXSrcfPw3VPLw9reVoTlzz4wd+2SwiZz2pirmS08khCgRYVVvUD71fhZ+o4P/GvHceGfsY+JmSRvKzxmN517jnHIl0jxlx3Mg hLSduSJpA0saxeQ+fwT47FZJpVhxlRZoy0wp5wsiL+qzhIWCxfqlQqaptE0ROt3T15sqJSijuVkhMKMpJXgLj9jwvVDWiaZLGFxTeCuLxJUZO4HLu3WB6pfA33o8j1Dy+L0v1xx8A/uNYYOSiylDOrTvssyzj1eOAwpGAsqtp3WkV46b07xuH4zlsDGyYu1lRSgyRyS9uZRHLGunaYxSpTIwkWRArAlNXXXx85vB4+HBi4EuIrx5S1LJMsgDCTUyYwD7xuHVCVXS1ZdjqN2Mfq59p/znPnZMfQHKxzZUpxmysKeWUdyF0/UeN3llmkaMosojgIEyEHIJcTKFzU1GSspLYbg+/GCyQba7eRUFllnpfVscSyHdBwmCIDebzRiyVBdHXefg+mOL4hpUwcDFxUJUoIoIkUKAVsBAGJ1aiJCx8Ahybb6tMMPZvRgBfkagEUKq1N+R59iSbBYEha1DxvzV0hndXTdJRZMq8hiztiTpkQPjxx5CiNkiRpNFmeQSIojhVtW2Rn3pDuZmDex2ooLDDwxFrsSQCBQJIs+wBJ1JBXGW0qC0XbdrLHsA1fIFBmlerWrqTnbuCKFALFiYW6atMGBVprHS5YY5dS5AoRtL241aoW/gsxJJG48MdQ8mpBql1zzfQfRHPTyJyfEcXlzESId44/7hu5GpbdSSH3jjUUykPCkiMrwyGJtgsFcOFZgCjaEKobyraMVZiFLGrtmC+aDVqfPiX4i+ZZ/m4dTScxm4nD/AOpJlY2Vk5EzwQwqzjsQywTzsI37k0r4jsrF2qMvjtslmOvML7rTMi7o8raZUvGffGFxm3kKPAfeLklfsAdsT8p7d3+k+hek+ke9NwPCYnHpkNrLLjQEWzMZGCMT4cn3dVKBvVGJEcYjz1Z1v050nx82dy/J4uFjxLtLvOinSjJUaijaRASds/ujUsu2yIeu33F/OzfEfTkCnIhPOZAQYYZZE7sioYlmUBkYRf3hhdtIpRVIGUfrjx3+RvmLrf5S5SLkOf5lLx0ycePBxmyYMOGEdte40LP2I5AHeB5DUssaP3mdQS5z9RKMNt4SPcMrWxxkFXdu+kAM011W1tZ0+0ARPyxKWr7qMBmkq2Rtzb6Ddff1B+leK5OTG6WwDzONjytCMmSZsSSd1Z4WcrPHMoxi+nbdotqb8BihHW3rH73uv+eVcnpwYnCo8bJKiLBky5E/cJmT/wCISQI4EcUcdQs5uUspEpjk8+c1EgyzDKiZDSAv5GodJHkk0cbxqwlDIHDFi5jZQu+Q7fW2ugOiM7q/KxcLDwJC+ZqYYcWBiiSgyRTQyRgRiR2Esiohtjk7oFZnEYqS3NMm+KVq4yWV+OABQROPfqnH1mrOREnKpVuI3VdoEb5FLpcu3kU6vGf8v/IHUWfh9RcnymXNm4uTj5GsYMMajFEYZZsfGMQnXWGAO0qyLHExWSId5dO0WT95fO8r0zBxGMv+k5iQJjNyAZJ5EbHWNHdY5pVJlcRkJkRxuGjdJGEkqKR2X+EftY4zg+lciPqzh2ys3PjUNDlRRBgJhHGXR3CyKz6FkGSsRx428IpdiKTxv2ccRzWL1HFDEONlE8hxKgQR9xWL+kZlMaq7RMjGJBqxeMJjLBHEacXXNROYc0rZiJl4sUWsod2LC9px1gLkq3cFyjtRdp7g5XA8111w6d5X5J6whysvH615ZuLSeGbJc5biWQZJ0lVd3S4jGh3CyIO3Ky2IIv1L51Lm8lxXR+Vk4olTlIVKNJK8xklYhzuFZ5e00pHbEkayszM27tEbj2lw/wAA9S/HXSvKYOPgvM0Sif8A+HCmRlhUZUKq8ZASOzIjBMeN1k1clWjJ+un/AMkYnV2WzxNJycGHLk9t8WSGdMZY1kXGkXvM0cTukjSiX1RpIwPEwZtLW9Ym5fOOY3doFtO0UGisIV1mqakBlId7FXkq63UZwOPF8bkOtb5PyBn4cubJjNIssqVLk7Ke3JuFLGRSNlTuEpswUMSjDu3HHeumPnT5M4v+yyMPqLLlxYsmBlxDO746ywSOGWWOOaJpBEJArJIHlZFhR5Jo0WGHXOZ0By+SO6W/tElfvsG70coOkirJJcNVuyFFUh1kkAKAqT9W/ov485ueUYuHDHl5EzSSBp1P9uuigSkARAqCQixypcgr3ikZ5IhEpEiOY2Fn7e1NKLy4vFXkRDW1Wd91Yqv2RpswpZG7twVt69j/ALcvlzN+U+kkz+Zw3w8nHWOFZSBeRKscZlCIgkBiWRz25F7imRJAH8ju7E646r5fpvsPx/Dycjjs4hkZJEIAZ4402jaORXtZLchlT+TSqR9ai+3bozO6H6Uig5GWMz5Gkk+KkZg7NNIZln92KmGXvR6sFkRUJlZzrK3YOXk8HLjZVZJkQAtJamxuoIESmGSRA0ZBOw/USyoZCxoa+vOMgi0cWSqgQrKVjOac4bOuo9JCc9OPzWp7bctEm1wKG2QVzwsgvoHGzpuQxo8zKh/t55oYZHRNpDCLEjFStE6+AW/cQNAGVi5A5bkHx4pGiCSP2iUa2USFh2i2oDhfRq8hzXmMxGRx9YnnJDCJVCsB5qMKjOwFdstGfy5JWMldVIUqzKGhMlGmSQGVSXk1JkcOpjWbtN+AgHgAqsZDNqyqxlIkOu1ddVB7gpkSwuLS6uKglomUJOXaenjVEh2mUtJSSqu0iSqXdJzWBOOobI5FXEz5CdrewY1CEsO2FRmkp1ZpfVIpAWWON2YFAe4aLNkSZebUSgL+ych1l3lmYkgpuVRXkGsq9uNtjLJIwVhJ9WbPmRNL7brLc0RAburGdUKSqvb7oZ1cjvOZGtxMqugRqzjhFd27TxMAAgDQ+qqoVWpbcGQoi/vZdMYVR1uh8yd7hcYRlKxAyKrj8v1FgKqjehoRoXncSjIHJjFlYEt9woTnqSixQz9xQyONh+oiuhIbRtUaOOJWZVAWRHCBAvbHaZ1+p5KidC5C+jF9dSW3jYxAW4VKl7YkBDFVEiMqnVhEY1qqIllP1GV/diqCStA0kIv2AU7M0ji5AW8EM5KsuSshWJl0KJGC3eliEUjzoY+2VlDNq6KSIx24x4eUuzJ68pAu5kBWW1YxkbDcWDd57f4toXT1YAkaA3Ukc1dFyy1eMZynOHqw5LM7IYVEabJsW0cmOgJT3NQbYW9lClgxoqdzuKwYARqAf9zE+FLEnYBihq1cs3nVQ5uMKACrOI79mEkEswCkyIYn2tVcMhHpZ9vFggLq7KFX6LViSwr2FMQK/BaiNiNGZApBAbYKVLqhZCZ3zak2KR5X7e74VMeLcW9VWLFE2PstZtiWlocZRoePD1E5+Q2PA66sukof9MeJFVVvuhkBMbu4DKjfqlgpkQSSFK8clnMao7s6iOMgiQNEN4VaMtLkE0GvzYYmQIShKrDY8+GSeLthoY+5sE7oLSbA9wxAqyeH1OzWZItLVXNulYJWFmVJCGVwpdFmQaq8YKyRMwchStOgcOXB2WQK0xOGtMbudLGglLBUS6HCZqNtJYWWPCLtyeN1y+puLJjeFDw4sla9qT2I0jiemWSRTJcaPrH6pJjQNNHIhBB1M0gVxKsUSCNd5S31MpjsWZSIzJbFjGVUP5JUFwXDuFBBKsRIxVkFyV9Q+DJkq7rLGX7ZWNgvcfQuyEbtKzbgDIbd1crHHDbFkeNmnknMIZ1dk0UfiiEIY3Z2AApRbeCyqoJO3iGcnObaKVayAAWB71Rd3TyDEJFFZvtiWSkhIpRtGrw+KHHSZDFAxidiGpwI6LspaKQgtR/TRyaSXuWpCGMgpEC5i915HXdkRxJ5B1T/AKakovctFjYIjOKAobsQzszREiSz5TZLtLIdlV0LGcA0361XUI9GI7INxxbkWHCTGM7kfoqkgCGS/OyvqrKhYPsqVQQLsJVLK2tymHD1WpHJNoY7DdIsrJlsMVTmhX2HGkXH6kcrKnb3QH9ViW15kOShyONlychQJ4zFtHStTNNHckcY8So8epCkJKrK1FdYwjEwXIx/6bky4qiV2Vo9pGGgAedYUqQ6qVY2yqysrLI7iyjfVqjmnWQHWTtlQF2VVdyyKG2FpqUYOGiaIob78cpVQqVTnMovllwN5WFMjaqr+wkP6qbOQEYSCEAsqUVhgY6Mufq9XUKZTzyb3aURv221T+UI0DJKre/DoxNQ7Qil25xYU2qqhw5JOLwwzYcKqWEaFZFkVDpHpYVo6EhKOQm5BkkkLxl5A2xDI0XO0hkJPaUHQxkBi41ZIj6fttGkYrswBAvU7DSwwYrTp4IcMDsSJYkJeNV2IePuKq6Go1k9ZGtlP6u8c6x92IzLUbypGrBkjjKqyqwLIxKmMmNGDBS6lrCkrI+u1NV2tSbE4azJ8haWYXGSijnsPTh2BHF3GwxWwkFlHcXVKRWrRqOihcsaGkR7SIEAcmMOAh/EdbPMpRVkLkupE0ZIAZlCghQHV1IJbU7KUZkDBVVFf0UIZFX9heEllC92QklWLMjSOpVIhdYXYGfZP7ewxJ7gkieJajjuKMkg7XJjsINppjvES0jF9nZbZQnmSTRisUkbSEkL3YUCjUtEAE12vqarFIakqI8bpIihRS4laWAgWcqbAIBxaN1UeQz4FxyXm8l9MCOMAyaBACo1YoAFDLuEVU01chirshkVWKNIGr6sPC56O39u0IiMzSvW5UbyjRS0oTts57asCHCaydtixKgwwgeOQpFZZzKrBSe5SItoyBm1UoPCKGXYupb1NESYyRgNGBJKzxyxmJZwFR2mjJU2VZZCVRH2LyK51JBRzqzX1hbnLtAtVYtF1XL93dwe3VeWhpzi9kUclxiCdvMfdbpM04s4swhdNyArKQ66BVZgIwStDdWKMHAihVTX7GKWZpI/Mx3hKSREtIZR3HkWKW2ijL6uKYtE7NJIGuSGgY22kTclYE2VIWadUIZWKujMxDmQaEGzGYViZlIhP5IuRnWZfqU7QmYxSKaY2thXRwCqGg26SbbPCLKNRkIYRq7/AFk9fV1SMZTlJsuN4qW15yxrji83bXQHp9PSLiBZ5Ba7RQwVV0eb922uQQPcRKlkeyiHRWZhtakOqFkIVmpT/wBP3D7AsLpw0n9uggZ1R5JVAQsGCstklhLBqpcN21VBjgCSUV6RVG9hoygcqppKHcZ1Nho7R3C0tqAHbUte2o3ck6NZoWd2jDEsVaMFPBWVEcdsKy+yyCNQq/8ATyAUBXcCsk4NjMY4d2pJ8xtzJCqKDLVLxToQhYhuMMljFviVNRjY09rdKhkxaJYf7tUkd1V0hDOLL0e6pcxu43BuNklIjLyKwBYAEnC4BlClTWiyMyKsh3PnYUXLCZe2gBrRmLIFGwdX8VWZWT1SwpZwSZGBD6qgG6kg2t+gFEMd6K2bBwJp5ljjSQRAMQA7kyOxEdqpLH9NY7kchR5DhGB7iHHUmgMpflJXOShRhyU+DKlcNB1ajoQZKxgxJRu4xlJFMRw1ebWrfDZ0Pg4mSVp8ibsuSqwAMT7IqqBEGrV9SjHVk9o2EcaAg7ClzcteOx8PGxmxJBLGzFSWLxsjySFo7Ql2lCIFA3Vi9rSKPorjOHjQI8qB5AVBUgkAp4oLRUKxYUtnwBTMC13p+nEzMcRxBldUBU6o8ocKSU3LEKsspUMLrdxq+ho0PUaGsBKOrNYrbvSosovLKlKxtlnPgOrnptTThMgRhVxjTEpXFNnnmXthq7ujcPmyQyDHZgjL5jFFSsij3UhTaAKVK0oRmDhSTa/WxeOkEkKMVYMGuQsoMblSEUHa9vMYq9tSutp+ko1hl48+BlMJY2jmV2UCRYscNrshepDqyaANHIY2jaSRQS2tT2Hg+T7rJTsHWu7EGUF2RQwMau5tHY6CyGZmYCpCrfWt+ZrQanqzxj65NfSjzS+AMg+/O6jo6U4wl8uAWP0mC4t2YX9PGL62phSsdIu4QQAAFDEtoCYwRHTFi9AlQaDBkG+wMvFkWF3ewQ4pFZmUIoa2JJYkE+SVoqrES+uv1";
		BASE64Encoder base64Encoder = new BASE64Encoder();
		File file = new File("C:\\root\\files\\upload\\012.jpg");
		String encode = base64Encoder.encode(FileUtils.readFileToByteArray(file)).replaceAll(" ","");
	//	String s1=s.split(";base64,")[1];
		System.out.println(encode);
}
}
