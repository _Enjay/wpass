package com.wx.house.manager.api.permission.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.admin.AdminRealDto;
import com.wx.house.core.pojo.dto.admin.UpgradeAdminServiceDto;
import com.wx.house.core.pojo.vo.AdminServiceDetailVo;
import com.wx.house.core.pojo.vo.AdminServiceVo;
import com.wx.house.manager.api.permission.service.SysAdminServicesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/03 下午 03:13
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("adminService/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "管理员服务信息和实名审核")
@Validated
public class SysAdminServicesController {
private final SysAdminServicesService adminServicesService;
private final ProjectConfiguration projectConfiguration;

@ApiOperation("客户列表")
@GetMapping
@ApiImplicitParams({
		@ApiImplicitParam(name = "keyword", value = "有效搜索范围: 姓名 手机号 证件号码"),
		@ApiImplicitParam(name = "state", value = "实名状态: 0未实名 1待审核 2拒绝 3成功")
})
public Result<PageInfo<AdminServiceVo>> selectAll(String keyword, Integer state, Page page) {
	List<AdminServiceVo> adminServiceVos = adminServicesService.selectAdminServiceVo(keyword, state);
	adminServiceVos.forEach(e -> {
		try {
			e.setIdCardImg(projectConfiguration.getStaticResource().getStaticResourceUrl().concat(e.getIdCardImg()));
			e.setIdCardImg_r(projectConfiguration.getStaticResource().getStaticResourceUrl().concat(e.getIdCardImg_r()));
		} catch (Exception ex) {
		}
	});
	PageInfo<AdminServiceVo> pageInfo = new PageInfo<>(adminServiceVos);
	return Result.success(pageInfo);
}

@ApiOperation("客户详情")
@GetMapping("detail/{id}")
public Result<AdminServiceDetailVo> detail(@PathVariable("id") String id) {
	AdminServiceDetailVo adminServiceDetailVo = adminServicesService
			                                            .selectAdminServiceDetailVo(id);
	adminServiceDetailVo.setIdCardImg(projectConfiguration.getStaticResource().getStaticResourceUrl().concat(adminServiceDetailVo.getIdCardImg()));
	adminServiceDetailVo.setIdCardImg_r(projectConfiguration.getStaticResource().getStaticResourceUrl().concat(adminServiceDetailVo.getIdCardImg_r()));
	return Result.success(adminServiceDetailVo);
}

@ApiOperation("实名审核")
@PutMapping
public Result reviewAdmin(@RequestBody @Validated AdminRealDto adminRealDto) {
	adminServicesService.reviewAdmin(adminRealDto);
	return Result.success();
}

@ApiOperation("为管理员修改服务[升级服务]")
@PutMapping("upgrade")
public Result upgradeAdminService(@RequestBody @Validated UpgradeAdminServiceDto upgradeAdminServiceDto) {
	adminServicesService.upgradeAdminService(upgradeAdminServiceDto);
	return Result.success();
}
}
