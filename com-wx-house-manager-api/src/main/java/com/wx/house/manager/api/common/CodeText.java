package com.wx.house.manager.api.common;

import com.wx.house.common.assembly.alysms.MessageUtil;
import com.wx.house.core.pojo.base.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/18 下午 02:27
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("common1/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "其他接口")
@Validated
public class CodeText {
private final MessageUtil messageUtil;

@ApiOperation("短信验证码")
@GetMapping("sms1/")
public Result sms1(String phone) {
	messageUtil.send(phone);
	return Result.success("success");
}


}
