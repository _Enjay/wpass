package com.wx.house.manager.api.contract.service;

import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.bo.CContractBo;
import com.wx.house.core.pojo.dto.contract.InsertContractDto;
import com.wx.house.core.pojo.po.CContract;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.po.ContractDetail;
import com.wx.house.core.pojo.vo.CcontractBuildVo;
import com.wx.house.core.pojo.vo.ContractBaseVo;
import com.wx.house.core.pojo.vo.ContractVo;
import com.wx.house.core.pojo.vo.HiredRoomVo;

import lombok.Data;

import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version: ：V$version
 */
public interface CContractService extends IService<CContract> {

List<ContractVo> selectAllContract(
		String mAccId
		, String contractKeyword
		, String tenantKeyword,
		Integer state,
		Integer daysTime);

void insertContract(InsertContractDto insertContractDto, UserInfo userInfo);


void createChargePlan(InsertContractDto insertContractDto,
                      UserInfo userInfo,
                      CContract cContract,
                      ContractDetail contractDetail);

void expenseCounter(String tenantPhone);

void deleteContract(String id);


List<CcontractBuildVo> selectNumber(String tenantKeyword);


List<CContractBo> selectByMAccId(String mAccId);

void validContractId(String id);
/***
 * 根据合同编号查询合同详情
 * @param contrNumber
 * @return
 */
ContractBaseVo selectIdBase(String contrNumber);
/***
 * 根据合同id查询合同状态
 */
CContract selectContractId(String contractId);
/***
 * 合同到期后归档
 */
void updateFile(String contractId);
}
