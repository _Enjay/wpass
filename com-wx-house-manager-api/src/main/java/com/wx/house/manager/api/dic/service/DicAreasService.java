package com.wx.house.manager.api.dic.service;

import com.wx.house.core.pojo.po.DicAreas;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
public interface DicAreasService extends IService<DicAreas> {
	/**
	 * 查找区域
	 *
	 * @param parentId
	 * @return
	 */

	List<DicAreas> findAreasByParentId(Integer parentId);
}
