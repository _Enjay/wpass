package com.wx.house.manager.api.order.service;

import com.wx.house.core.pojo.po.TOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/29 上午 10:23
 * @description：${description}
 * @version: ：V$version
 */
public interface TOrderService extends IService<TOrder> {


}
