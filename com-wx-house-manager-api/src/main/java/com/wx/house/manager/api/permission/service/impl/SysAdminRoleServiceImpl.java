package com.wx.house.manager.api.permission.service.impl;

import com.wx.house.manager.api.permission.service.SysAdminRoleService;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.permission.SysAdminRoleMapper;
import com.wx.house.core.pojo.po.SysAdminRole;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class SysAdminRoleServiceImpl extends ServiceImpl<SysAdminRoleMapper, SysAdminRole> implements SysAdminRoleService {
	@Resource
	private SysAdminRoleMapper sysAdminRoleMapper;
	
	@Override
	public int addAdminRole(SysAdminRole adminRole) {
		return sysAdminRoleMapper.insert(adminRole);
	}

	@Override
	public void dateAdminRole(String adminid) {
		sysAdminRoleMapper.dateAdmin(adminid);
	}

	@Override
	public List<SysAdminRole> selectAdminIdRole(String adminId) {
		return sysAdminRoleMapper.selectAdminIdRole(adminId);
	}

}
