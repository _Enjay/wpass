package com.wx.house.manager.api.capital.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.capital.InsertWithdrawalDto;
import com.wx.house.core.pojo.dto.capital.ReviewWithdrawalDto;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.po.SysAdminWithdrawal;
import com.wx.house.core.pojo.vo.SysAdminBalanceVo;
import com.wx.house.core.pojo.vo.SysAdminWithdrawalVo;
import com.wx.house.manager.api.capital.service.SysAdminDetailService;
import com.wx.house.manager.api.capital.service.SysAdminWithdrawalService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/26 下午 03:40
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("cashWithdrawal/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "余额提现")
@Validated
public class CashWithdrawalController extends BaseController {
private final SysAdminWithdrawalService sysAdminWithdrawalService;
private final SysAdminDetailService sysAdminCapitalService;

@ApiOperation("用户余额信息")
@GetMapping("balance")
public Result<SysAdminBalanceVo> adminBalance() {
	String maccId = this.getMaccId();
	SysAdminBalanceVo sysAdminBalanceVo = sysAdminCapitalService.selectBalance(maccId);
	return Result.success(sysAdminBalanceVo);
}

@GetMapping
@ApiOperation("提现记录")
@ApiImplicitParams({
		@ApiImplicitParam(name = "state", value = "提现状态: 0申请提现中 1提现成功 2提现失败 null全部[默认] 3已审核", required = false, example = "0"),
})
public Result<PageInfo<SysAdminWithdrawalVo>> selectWithdrawal(Page page, Integer state, String keyword) {
	String maccId = this.getMaccId();
	System.out.println(maccId);
	List<SysAdminWithdrawalVo> sysAdminWithdrawals = null;
	if ("0".equals(maccId)) {
		sysAdminWithdrawals = sysAdminWithdrawalService.selectByMaccId(null, state, keyword);
	} else {
		sysAdminWithdrawals = sysAdminWithdrawalService.selectByMaccId(maccId, state, keyword);
	}
	PageInfo<SysAdminWithdrawalVo> pageInfo = new PageInfo<>(sysAdminWithdrawals);
	return Result.success(pageInfo);
}

@GetMapping("detail/{id}")
@ApiOperation("提现记录详情")
@ApiImplicitParam(name = "id", value = "提现记录表id", required = true, example = "0")
public Result<SysAdminWithdrawal> selectWithdrawalDetail(@PathVariable("id") String id) {
	return Result.success(sysAdminWithdrawalService.selectById(id));
}

@PostMapping
@ApiOperation("发起提现")
public Result insertWithdrawal(@RequestBody @Validated InsertWithdrawalDto insertWithdrawalDto) {
	String maccId = this.getMaccId();
	sysAdminWithdrawalService.insertWithdrawal(insertWithdrawalDto, maccId);
	return Result.success();
}

@PutMapping
@ApiOperation("审核提现")
public Result reviewWithdrawal(@RequestBody @Validated ReviewWithdrawalDto reviewWithdrawalDto) {
	sysAdminWithdrawalService.reviewWithdrawal(reviewWithdrawalDto);
	return Result.success();
}

@DeleteMapping("{id}")
@ApiOperation("删除提现记录")
public Result deleteWithdrawal(@PathVariable("id") String id) {
	sysAdminWithdrawalService.deleteWithdrawal(id);
	return Result.success();
}

}
