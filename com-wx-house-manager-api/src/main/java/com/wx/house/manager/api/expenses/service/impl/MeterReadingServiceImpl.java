package com.wx.house.manager.api.expenses.service.impl;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.exception.UserException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.building.BuildingRoomMapper;
import com.wx.house.core.mapper.contract.CContractMapper;
import com.wx.house.core.mapper.expenses.ECostDetailMapper;
import com.wx.house.core.mapper.expenses.ECostMapper;
import com.wx.house.core.mapper.expenses.MeterReadingMapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.po.MeterReading;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.wx.house.core.pojo.vo.BammTorderVo;
import com.wx.house.core.pojo.vo.ContractBaseVo;
import com.wx.house.core.pojo.vo.ECostPameVo;
import com.wx.house.core.pojo.vo.MeterReadingVo;
import com.wx.house.manager.api.building.service.BBuildingRoomService;
import com.wx.house.manager.api.expenses.service.MeterReadingService;
import com.wx.house.manager.api.expenses.test.MeterReadingModify;
import com.wx.house.manager.api.expenses.test.meterIdPame;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class MeterReadingServiceImpl extends ServiceImpl<MeterReadingMapper, MeterReading> implements MeterReadingService {
@Resource
private MeterReadingMapper meterReadingMapper;
@Resource
private ECostMapper eCostMapper;
@Resource
private CContractMapper cContractMapper;
@Resource
private ECostDetailMapper eCostDetailMapper;
@Resource
private BuildingRoomMapper buildingRoomMapper;

@Override
public int addMeter(MeterReading meterReading) {
	return meterReadingMapper.insert(meterReading);
}

@Override
public List<MeterReadingVo> AllMeter(String buildName, String month, String monthState, String maccid) {
	return meterReadingMapper.AllMeter(maccid, buildName, month, monthState);
}

@Override
public int updateMeter(MeterReading meterReading) {
	return meterReadingMapper.updateById(meterReading);
}

@Override
public int addcompleteMeter(meterIdPame meterIdPame) {
	//根据合同编号查询合同id,合同编号，合同名称
	ContractBaseVo contractBaseVo = cContractMapper.selectIdBase(meterIdPame.getContrNumber());
	//判断是否有房间读数历史信息
	BuildingRoom buildingRoom=buildingRoomMapper.selectNumberRoom(meterIdPame.getContrNumber());
	//水费
	BigDecimal aWater=null; 
	//电费
	BigDecimal aElectric=null; 
	//气费
	BigDecimal aGas=null;
	List<ECostDetail> costDetails=new ArrayList<>();
	
	if(ObjectUtils.isEmpty(buildingRoom)) {
			aWater = big(meterIdPame.getMonthWater() - contractBaseVo.getEndWater())
					.multiply(contractBaseVo.getBuildWater());
			aElectric = big(meterIdPame.getMonthElectric() - contractBaseVo.getEndElectric())
					.multiply(contractBaseVo.getBuildElectric());
			aGas = big(meterIdPame.getMonthGas() - contractBaseVo.getEndGas()).multiply(contractBaseVo.getBuildGas());
	}else {
			aWater = big(meterIdPame.getMonthWater() - buildingRoom.getHistoryWater())
					.multiply(contractBaseVo.getBuildWater());
			aElectric = big(meterIdPame.getMonthElectric() - buildingRoom.getHistoryElectric())
					.multiply(contractBaseVo.getBuildElectric());
			aGas = big(meterIdPame.getMonthGas() - buildingRoom.getHistoryGas()).multiply(contractBaseVo.getBuildGas());
	}
	
	//判断待处理表是否已经有水电气当月记录
	ECost eCost =eCostMapper.selectMonth(contractBaseVo.getId(), meterIdPame.getMonth(), "3,4,5");
	//待处理表无水电气记录
	String ECostId=	null;
	if(ObjectUtils.isEmpty(eCost)){
		String ids=String.valueOf(IDUtils.genItemId(16));
		ECost eCost1=new ECost();
		eCost1.setId(ids);
		eCost1.setContrId(contractBaseVo.getId());
		eCost1.setContrNumber(contractBaseVo.getContrNumber());
		eCost1.setCreationTime(new Date());
		eCost1.setChargeMonth(meterIdPame.getMonth());
		eCost1.setMAccId(contractBaseVo.getMAccId());
		eCost1.setContrName(contractBaseVo.getContrName());
		eCost1.setPayTypes("3,4,5");
		eCost1.setCostType(0);
		eCost1.setTotal(aWater.add(aElectric).add(aGas));
		eCost1.setActualPay(new BigDecimal(0));
		if(eCostMapper.insert(eCost1)!=1) {
			throw new BusinessException("添加待处理费用,确定合同失败");
		}else {
		 ECostId=ids;
		}
	}else {
		ECostId=eCost.getId();
	}
	
	//判断待处理表是否已经有物业管理费当月记录
	ECost eCost1 =eCostMapper.selectMonth(contractBaseVo.getId(), meterIdPame.getMonth(), "2");
	//待处理表无物业管理费
	String ECostIds=null;
	if(ObjectUtils.isEmpty(eCost)){
		String ids=String.valueOf(IDUtils.genItemId(16));
		ECost eCost2=new ECost();
		eCost2.setId(ids);
		eCost2.setContrId(contractBaseVo.getId());
		eCost2.setContrNumber(contractBaseVo.getContrNumber());
		eCost2.setCreationTime(new Date());
		eCost2.setChargeMonth(meterIdPame.getMonth());
		eCost2.setMAccId(contractBaseVo.getMAccId());
		eCost2.setContrName(contractBaseVo.getContrName());
		eCost2.setPayTypes("2");
		eCost2.setCostType(0);
		eCost2.setTotal(contractBaseVo.getPropertyManager());
		if(eCostMapper.insert(eCost2)!=1) {
			throw new BusinessException("添加待处理费用,确定合同失败");
		}else {
		 ECostIds=ids;
		}
	}else {
		ECostIds=eCost1.getId();
	}
	
	//水
	ECostDetail detail = ECostDetail.builder().creationTime(new Date()).netMoney(new BigDecimal(0.00))
			.costState(0).build();
	detail.setChargeType(3);
	detail.setReceiveMoney(aWater);
	detail.setId(String.valueOf(IDUtils.genItemId(16)));
	detail.setECostId(ECostId);
	costDetails.add(detail);
	//电
	ECostDetail detail1 = ECostDetail.builder().creationTime(new Date()).netMoney(new BigDecimal(0.00))
			.costState(0).build();
	detail1.setChargeType(4);
	detail1.setReceiveMoney(aElectric);
	detail1.setId(String.valueOf(IDUtils.genItemId(16)));
	detail1.setECostId(ECostId);
	costDetails.add(detail1);
	//气
	ECostDetail detail2 = ECostDetail.builder().creationTime(new Date()).netMoney(new BigDecimal(0.00))
			.costState(0).build();
	detail2.setChargeType(5);
	detail2.setReceiveMoney(aGas);
	detail2.setId(String.valueOf(IDUtils.genItemId(16)));
	detail2.setECostId(ECostId);
	costDetails.add(detail2);
	//物业费
	ECostDetail detail3 = ECostDetail.builder().creationTime(new Date()).netMoney(new BigDecimal(0.00))
			.costState(0).build();
	detail3.setChargeType(2);
	detail3.setReceiveMoney(contractBaseVo.getPropertyManager());
	detail3.setId(String.valueOf(IDUtils.genItemId(16)));
	detail3.setECostId(ECostIds);
	costDetails.add(detail3);
	
	for (int i = 0; i < costDetails.size(); i++) {
		System.out.println("------"+costDetails.get(i));
		eCostDetailMapper.insert(costDetails.get(i));
	}
	//将最新的水电气读数存入
	BuildingRoom base =new BuildingRoom();
	base.setId(contractBaseVo.getRoomId());
	base.setHistoryWater(meterIdPame.getMonthWater());
	base.setHistoryElectric(meterIdPame.getMonthElectric());
	base.setHistoryGas(meterIdPame.getMonthGas());
	base.setUpdateTime(new Date());
	//判断房间读数信息是否为空
	base.setId(buildingRoom.getId());
	if(buildingRoomMapper.updateBuildingRoom(base)!=1) {
		throw new UserException("修改房间读数失败",201);
	}
	return meterReadingMapper.completeMeter(meterIdPame.getMeterId(),
				meterIdPame.getMonthWater()-buildingRoom.getHistoryWater(),
				meterIdPame.getMonthElectric()-buildingRoom.getHistoryElectric(),
				meterIdPame.getMonthGas()-buildingRoom.getHistoryGas());
}

@Override
public int insertList(List<MeterReading> list) {
	return meterReadingMapper.insertList(list);
}

@Override
public void updateMeterReading(MeterReading meterReading,BuildingRoom buildingRoom,ECostDetail eCostDetail,ECost eCost) {
	if(buildingRoomMapper.updateBuildingRoom(buildingRoom)!=1) {
		throw new BusinessException("修改历史读数失败");
	}
	if(eCostMapper.updateById(eCost)!=1) {
		throw new BusinessException("修改待处理费用失败");
	}
	if(eCostDetailMapper.updateById(eCostDetail)!=1) {
		throw new BusinessException("修改待处理费用详情失败");
	} 
	if(meterReadingMapper.updateById(meterReading)!=1) {
		throw new UserException("修改抄表数据失败",201);
	}
}

@Override
public MeterReading selectNumberMonth(String number, String month) {
	return meterReadingMapper.selectNumberMonth(number, month);
}
/***
 * Int转Big
 * @param i
 * @return
 */
private BigDecimal big(Integer i) {
	return new BigDecimal(Integer.toString(i));
}
}
