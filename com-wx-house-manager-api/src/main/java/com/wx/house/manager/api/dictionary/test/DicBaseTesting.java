package com.wx.house.manager.api.dictionary.test;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @description:
 * @author: 喜胖装机
 * @date: 2019-06-06 17:56
 */
@Data
@ApiModel
public class DicBaseTesting {
@ApiModelProperty(value = "id（用于更新）", name = "id", example = "123", required = false)
private Long id;

/**
 * 排序
 */
@NotNull
@ApiModelProperty(value = "排序", name = "order", example = "0", required = false)
private Integer order;

/**
 * 数值
 */
@ApiModelProperty(value = "数值", name = "value", example = "0", required = false)
private String value;

/**
 * 字典编号
 */
@ApiModelProperty(value = "字典编号", name = "dicNum", example = "0", required = false)
private String dicNum;

/**
 * 字典名称
 */
@NotNull
@ApiModelProperty(value = "字典名称", name = "dicName", example = "0", required = false)
private String dicName;

/**
 * 父节点id
 */
@ApiModelProperty(value = "父节点id", name = "parentId", example = "1235", required = false)
private Long parentId;

    public Long getParentId() {
        return parentId==null || parentId==0 ?0:parentId ;
    }

}
