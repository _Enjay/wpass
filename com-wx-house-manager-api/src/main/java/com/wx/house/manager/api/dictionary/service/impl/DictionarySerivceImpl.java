package com.wx.house.manager.api.dictionary.service.impl;

import com.wx.house.manager.api.dictionary.service.DictionarySerivce;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.core.mapper.dic.DicBaseDictionaryMapper;
import com.wx.house.core.pojo.po.DicBaseDictionary;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.dictionary.service.impl
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/11 15:16
 * @Version V1.0
 */
@Service
public class DictionarySerivceImpl implements DictionarySerivce {
    @Resource
    DicBaseDictionaryMapper dicBaseDictionaryMapper;

    @Override
    public List<DicBaseDictionary> searchAllByCondion(Integer id,String mAccId) {

        List<DicBaseDictionary> searchlist = dicBaseDictionaryMapper.selectAllByCondion(id,mAccId);
        return searchlist;

    }

    @Override
    public void addDicBase(DicBaseDictionary dicBaseDictionary) {
        try {
            dicBaseDictionaryMapper.insert(dicBaseDictionary);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("添加失败！！！");
        }

    }

    @Override
    public DicBaseDictionary searchDicBaseById(Long id) {
        try {
            DicBaseDictionary dictionary = dicBaseDictionaryMapper.selectDicBaseById(id);
            if(dictionary!=null){
                return dictionary;
            }
        }catch (Exception e){
            throw new BusinessException("查询失败！！！");
        }
        return  null;
    }

    @Override
    public void updateDicBaseById(DicBaseDictionary dicBaseDictionary) {
        try {
            dicBaseDictionaryMapper.updateById(dicBaseDictionary);
        }catch (Exception e){
            throw new BusinessException("更新失败！！！");
        }

    }

    @Override
    public void deleteDicBaseById(Integer id) {

        try {
            List<String> ids = dicBaseDictionaryMapper.selectIds(id);
            ids.add(id+"");
            System.out.println("list:"+ids);
            dicBaseDictionaryMapper.deleteDicBaseById(ids);
        }catch (Exception e){
            throw new RuntimeException("删除失败！！！");
        }

    }

    @Override
    public DicBaseDictionary selectParentIdById(Long id) {
        return dicBaseDictionaryMapper.selectParentIdById(id.intValue());
    }

    @Override
    public Map<String, Object> findDicTree(Integer id,String mAccId) {

            Map<String, Object> data = new HashMap<>();
            try {//查询所有菜单
                List<DicBaseDictionary> allMenu = dicBaseDictionaryMapper.selectAllByCondion(null,mAccId);
                //根节点
                List<DicBaseDictionary> rootMenu = new ArrayList<DicBaseDictionary>();
                for (DicBaseDictionary nav : allMenu) {
                    //父节点是0的，为根节点。
                    if (nav.getParentId() == (long)(id)|| nav.getParentId() == null) {
                        rootMenu.add(nav);
                    }
                }
                /* 根据Menu类的order排序 */
                Collections.sort(rootMenu, order());
                //为根菜单设置子菜单，getClild是递归调用的
                for (DicBaseDictionary nav : rootMenu) {
                    /* 获取根节点下的所有子节点 使用getChild方法*/
                    List<DicBaseDictionary> childList = getChild(nav.getId(), allMenu);
                    //给根节点设置子节点
                    nav.setChildren(childList);
                }
                /**
                 * 输出构建好的菜单数据。
                 *
                 */
                data.put("success", "true");
                data.put("list", rootMenu);

                return data;
            } catch (Exception e) {
                data.put("false", "父节点不能为空");
                e.printStackTrace();
                data.put("list", new ArrayList());
                return data;
            }
        }



        /**
         * 获取子节点
         *
         * @param id      父节点id
         * @param allMenu 所有菜单列表
         * @return 每个根节点下，所有子菜单列表
         */
        private List<DicBaseDictionary> getChild(Long id, List<DicBaseDictionary> allMenu) {
            //子菜单l;
            List<DicBaseDictionary> childList = new ArrayList<DicBaseDictionary>();
            for (DicBaseDictionary nav : allMenu) {
                // 遍历所有节点，将所有菜单的父id与传过来的根节点的id比较
                //相等说明：为该根节点的子节点。
                //ParentId()
                if (nav.getParentId().equals(id)) {
                    childList.add(nav);
                }
            }
            System.out.println("子集：" + childList);
            //递归
            for (DicBaseDictionary nav : childList) {
                nav.setChildren(getChild(nav.getId(), allMenu));
            }

            //排序
            Collections.sort(childList, order());
            //如果节点下没有子节点，返回一个空List（递归退出）
            if (childList.size() == 0) {
                return new ArrayList<DicBaseDictionary>();
            }
            return childList;

        }

/**
 * 排序
 *
 * @return
 */
        private Comparator<DicBaseDictionary> order() {
            Comparator<DicBaseDictionary> comparator = new Comparator<DicBaseDictionary>() {
                @Override
                public int compare(DicBaseDictionary o1, DicBaseDictionary o2) {
                    if (!o1.getOrder().equals(o2.getOrder()) && o1.getOrder() != 0 && o2.getOrder() != 0) {
                        return o1.getOrder() - o2.getOrder();
                    }
                    return 0;
                }
            };
            return comparator;
        }

}
