package com.wx.house.manager.api.expenses.controller;

import com.wx.house.manager.api.expenses.service.EAutoMeterReadService;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.UserInfoUtil;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.dto.expenses.EAutoMeterReadDto;
import com.wx.house.core.pojo.po.EAutoMeterRead;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.expenses.controller
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 10:36
 * @Version V1.0
 */
@RestController
@RequestMapping("meterReadConfig/")
@Api(value = "自动抄表配置", tags = "自动抄表配置")
@Slf4j
public class EAutoMeterReadController extends BaseController {
    @Resource
    private EAutoMeterReadService eAutoMeterReadService;

    @PostMapping("add/")
    @ApiOperation(value = "新增抄表配置", notes = "新增抄表配置")

    public Result addMeterRead(@RequestBody @Validated EAutoMeterReadDto eAutoMeterReadDto) {
        UserInfo userInfo = UserInfoUtil.userInfo();
       //判断此用户是否配置了自动抄表
        List<EAutoMeterRead> meterReads = eAutoMeterReadService.findMeterReadByMaccId(userInfo.getMAccId());
        EAutoMeterRead autoMeterRead = EAutoMeterRead.builder()
                .meterReadNum(eAutoMeterReadDto.getMeterReadNum())
                .advanceDays(eAutoMeterReadDto.getAdvanceDays())
                .opreation("jack")
                .opreationTime(new Date()).build();
        if(meterReads.size()!=0){
            autoMeterRead.setId(meterReads.get(0).getId());
            eAutoMeterReadService.updateById(autoMeterRead);
        }else {
            autoMeterRead.setId(IDUtils.genItemId(12));
            autoMeterRead.setMAccId(userInfo.getMAccId());
            eAutoMeterReadService.save(autoMeterRead);
        }
        return Result.success("操作成功成功");
    }

    @GetMapping("detail/")
    @ApiOperation(value = "查看自动抄表配置详情", notes = "查看自动抄表配置详情")

    public Result findMeterRead() {
        UserInfo userInfo = UserInfoUtil.userInfo();
        //判断此用户是否配置了自动抄表
        List<EAutoMeterRead> meterReads = eAutoMeterReadService.findMeterReadByMaccId(userInfo.getMAccId());
        if(meterReads.size()!=0){
            return Result.success(meterReads.get(0));
        }else {
            return Result.success(null);
        }

    }
}
