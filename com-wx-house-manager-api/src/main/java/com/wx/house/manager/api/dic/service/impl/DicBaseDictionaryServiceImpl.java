package com.wx.house.manager.api.dic.service.impl;

import com.wx.house.manager.api.dic.service.DicBaseDictionaryService;
import com.wx.house.core.mapper.dic.DicBaseDictionaryMapper;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.pojo.po.DicBaseDictionary;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class DicBaseDictionaryServiceImpl extends ServiceImpl<DicBaseDictionaryMapper, DicBaseDictionary> implements DicBaseDictionaryService {

}
