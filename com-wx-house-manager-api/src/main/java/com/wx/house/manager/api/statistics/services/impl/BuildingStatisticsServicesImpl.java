package com.wx.house.manager.api.statistics.services.impl;

import com.wx.house.common.util.StringUtil;
import com.wx.house.common.util.TimeUtil;
import com.wx.house.core.mapper.building.BuildingMapper;
import com.wx.house.core.mapper.building.BuildingRoomMapper;
import com.wx.house.core.mapper.contract.CContractMapper;
import com.wx.house.core.mapper.statistics.ExpenseStatisticsMapper;
import com.wx.house.core.pojo.po.building.Building;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.wx.house.core.pojo.vo.BuildingSimpleVo;
import com.wx.house.core.pojo.vo.ContractStatisticsVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingMonthIntoDetailStatisticsVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingMonthIntoStatisticsVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingYearIntoStatisticsVo;
import com.wx.house.manager.api.statistics.services.BuildingStatisticsServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/17 下午 03:46
 * @description：
 * @version: ：V
 */
@Service
@RequiredArgsConstructor
public class BuildingStatisticsServicesImpl implements BuildingStatisticsServices {
private final ExpenseStatisticsMapper expenseStatisticsMapper;
private final CContractMapper cContractMapper;
private final BuildingRoomMapper buildingRoomMapper;
private final BuildingMapper buildingMapper;

@Override
public BuildingMonthIntoStatisticsVo selectBuildingIntoStatisticsVo(String maccid, String yearMonth) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
	SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM");  
	Calendar cal = Calendar.getInstance(); 
	String start =null;
	String end =null;
	//判断是否传了时间
	if(!StringUtil.isEmpty(yearMonth)) {
		String[] strArray = yearMonth.split("-");
	    //设置年份  
	    cal.set(Calendar.YEAR, Integer.valueOf(strArray[0]));  
	    //设置月份  
	    cal.set(Calendar.MONTH, Integer.valueOf(strArray[1])-1); 
	    //获取某月最大天数
	    int lastDay = cal.getActualMaximum(Calendar.DATE);
	    //设置日历中月份的最大天数  
	    cal.set(Calendar.DAY_OF_MONTH, lastDay);  
	    //格式化日期
		System.out.println("当前时间:"+sdf.format(cal.getTime()));
		//获取到当前月份每个月的尾数
		 start =yearMonth+"-01";
		 end   =sdf.format(cal.getTime());
	}else {
		int lastDay = cal.getActualMaximum(Calendar.DATE);
		cal.set(Calendar.DAY_OF_MONTH, lastDay);
		yearMonth =sd.format(cal.getTime());
		start =yearMonth+"-01";
		end =sdf.format(cal.getTime());
		System.out.println("当前时间:"+sdf.format(cal.getTime()));
	}
	 BuildingMonthIntoStatisticsVo builVo = new BuildingMonthIntoStatisticsVo();
	 BuildingMonthIntoDetailStatisticsVo selectRoomNumber =new BuildingMonthIntoDetailStatisticsVo();
	 BuildingMonthIntoDetailStatisticsVo selcetNumber =new BuildingMonthIntoDetailStatisticsVo();
	 List<Building> selcetMaccId = buildingMapper.selcetMaccId(maccid);
	 builVo.setTime(yearMonth);
	 for (int i = 0; i < selcetMaccId.size(); i++) {
		 	selcetNumber = buildingMapper.selcetNumber(selcetMaccId.get(i).getBuildName());
			selectRoomNumber = expenseStatisticsMapper.selectRoomNumber(selcetMaccId.get(i).getBuildName(), start, end);
			selectRoomNumber.setRoomNumber(selcetNumber.getRoomNumber());
			selectRoomNumber.setBuildingName(selcetMaccId.get(i).getBuildName());
			builVo.getBuildingMonthIntoDetailStatisticsVos().add(selectRoomNumber);
	 }
	return builVo;
}

@Override
public List<BuildingYearIntoStatisticsVo> buildingYearStatistics(String maccid, String year, String buildingName) {
	year = StringUtil.isNotEmpty(year) ? year : new SimpleDateFormat("yyyy").format(new Date());
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
	SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM");  
	
	List<BuildingRoom> buildingRooms = buildingRoomMapper.selectByMaccid(maccid, buildingName);
	List<String> timeListByYear = TimeUtil.createTimeListByYear(year);
	
	List<BuildingYearIntoStatisticsVo> builVo = new ArrayList<>();
	
	
	Calendar cal = Calendar.getInstance(); 
	String start =null;
	String end =null;
	for (int i = 0; i < timeListByYear.size(); i++) {
		String[] strArray = timeListByYear.get(i).split("-");
	    //设置年份  
	    cal.set(Calendar.YEAR, Integer.valueOf(strArray[0]));  
	    //设置月份  
	    cal.set(Calendar.MONTH, Integer.valueOf(strArray[1])-1); 
	    //获取某月最大天数
	    int lastDay = cal.getActualMaximum(Calendar.DATE);
	    //设置日历中月份的最大天数  
	    cal.set(Calendar.DAY_OF_MONTH, lastDay);  
	    //格式化日期
		System.out.println("当前时间:"+sdf.format(cal.getTime()));
		//获取到当前月份每个月的尾数
		 start =timeListByYear.get(i)+"-01";
		 end   =sdf.format(cal.getTime());
		 
		 BuildingYearIntoStatisticsVo selectRoomNumber=new BuildingYearIntoStatisticsVo();
		 BuildingMonthIntoDetailStatisticsVo selcetNumber =new BuildingMonthIntoDetailStatisticsVo();
		 selectRoomNumber.setMonth(timeListByYear.get(i));
		 
		 if(StringUtil.isEmpty(buildingName)) {
			 selcetNumber= buildingMapper.selcetMaccIdNumber(maccid);
			 selectRoomNumber = expenseStatisticsMapper.selectYeaMRoomNumber( maccid,start, end);
			 selectRoomNumber.setRoomNumber(selcetNumber.getRoomNumber());
			 selectRoomNumber.setMonth(timeListByYear.get(i));
			 builVo.add(selectRoomNumber);
		 }else {
			selcetNumber = buildingMapper.selcetNumber(buildingName);
			selectRoomNumber = expenseStatisticsMapper.selectYeaRoomNumber(buildingName, start, end);
			selectRoomNumber.setRoomNumber(selcetNumber.getRoomNumber());
			selectRoomNumber.setMonth(timeListByYear.get(i));
			builVo.add(selectRoomNumber);
		}
	}
	return builVo;
}
}
