package com.wx.house.manager.api.permission.service;

import com.wx.house.core.pojo.po.SysAdmin;
import com.wx.house.manager.api.permission.text.AdminPame;

import java.util.Date;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminService extends IService<SysAdmin> {
	/***
	 * 根据账号密码查询管理员的记录
	 * @param loginName
	 * @param loginPwd
	 * @return
	 */
	SysAdmin loginAdmin(String loginName);
	/***
	 * 查询电话是否注册
	 * @param phone
	 * @return
	 */
	SysAdmin loginPhone(String phone);
	/***
	 * 根据账号ID查询管理员的记录
	 * @param loginName
	 * @param loginPwd
	 * @return
	 */
	SysAdmin loginAdminId(String adminId);
	/***
	 * 用户登录更新ip，更新登录时间
	 */
	int updateAdminip(String id,String loginIp,Date loginTime);
	/***
	 * 用户注册
	 * @return
	 */
	int addAdmin(AdminPame adminPame,String pwd,Integer safeKey,String accoutype,String level,String id);
	
	/***
	 * 添加用户，用户详细
	 * @return
	 */
	SysAdmin addAdminAndDetail(String phone,String uuid);
	
	/***
	 * 查询全部用户
	 * @return
	 */
	List<SysAdmin> selectAdmin(String level,String accountType);
	/***
	 * 修改用户
	 * @param sysAdmin
	 * @return
	 */
	int UpdateAdmin(SysAdmin sysAdmin);
	/***
	 * 禁用/啟用
	 * @param id
	 * @return
	 */
	int DeleteAdmin(String id,Integer state);
	/***
	 * 删除用户 del_type
	 * @param id
	 * @return
	 */
	int deleteDelType(String id);
	/***
	 * 测试使用
	 * @return
	 */
	List<SysAdmin> selectAll();
	/***
	 * 修改密码
	 */
	int updateAdminLog(String adminId,String loginPwd,Integer safeKey); 
	/***
	 * 修改手机号
	 * @param newPhone
	 * @param usedPhone
	 * @return
	 */
	int updateAdminPhone(String adminId,String newphone);
}
