package com.wx.house.manager.api.expenses.test;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Pattern;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ECollectFeesAdd {
	
	@ApiModelProperty(value = "合同id")
	private String contractId;

	@ApiModelProperty(value = "收支类型: 0收费  1退费")
	private String szType;

	@ApiModelProperty(value = "费用类型: 0保证金 1房租 2 物业管理费 3水费 4电费 5气费 6其他")
	private String feesType;
	
//	@ApiModelProperty(value = "支付方式:0支付宝 1微信 2银行卡 3线下")
//	private String paymentMethod;
	
	@ApiModelProperty(value = "收费年月(2019-09)")
	@Pattern(regexp = "2\\d+\\-(0[1-9]|1[012])",message = "时间格式不正常")
	private String feeMonth;
	
	@ApiModelProperty(value = "应收")
	private BigDecimal receivable;

	@ApiModelProperty(value = "实收")
	private BigDecimal actual;

	@ApiModelProperty(value = "备注")
	private String remarks;
	
}
