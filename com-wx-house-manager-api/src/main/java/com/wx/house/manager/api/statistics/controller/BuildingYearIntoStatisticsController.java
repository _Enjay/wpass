package com.wx.house.manager.api.statistics.controller;

import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingYearIntoStatisticsVo;
import com.wx.house.manager.api.statistics.services.BuildingStatisticsServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/17 下午 03:45
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("statistics/building/year/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "收支统计报表")
@Validated
public class BuildingYearIntoStatisticsController extends BaseController {
private final BuildingStatisticsServices buildingStatisticsServices;

@ApiOperation("年度入住报表")
@GetMapping
@ApiImplicitParams({
		@ApiImplicitParam(name = "year", value = "年", required = false)
})
public Result<List<BuildingYearIntoStatisticsVo>> expenseYearStatistics(String year, String buildingName) {
	List<BuildingYearIntoStatisticsVo> buildingYearIntoStatisticsVoList = buildingStatisticsServices.buildingYearStatistics(this.getMaccId(), year,buildingName);
	return Result.success(buildingYearIntoStatisticsVoList);
}


}
