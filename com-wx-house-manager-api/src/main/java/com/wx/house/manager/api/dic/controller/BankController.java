package com.wx.house.manager.api.dic.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.dic.C_DicBankDto;
import com.wx.house.core.pojo.dto.dic.U_DicBankDto;
import com.wx.house.core.pojo.vo.DicBankVo;
import com.wx.house.manager.api.dic.service.DicBankService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 下午 02:01
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("bank/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "银行信息管理")
@Validated
public class BankController {
private final DicBankService dicBankService;

@ApiOperation("银行信息列表")
@GetMapping
@ApiImplicitParam(name = "keyword", value = "搜索词: 银行", required = false)
public Result<PageInfo<DicBankVo>> selectAll(Page page, String keyword) {
	List<DicBankVo> dicBankVos = dicBankService.selectAll(keyword);
	PageInfo<DicBankVo> pageInfo = new PageInfo<>(dicBankVos);
	return Result.success(pageInfo);
}

@ApiOperation("新增银行信息")
@PostMapping
public Result addBank(@RequestBody @Validated C_DicBankDto PDicBankDto) {
	dicBankService.addBank(PDicBankDto);
	return Result.success();
}

@ApiOperation("修改银行信息")
@PutMapping
public Result updateBank(@RequestBody @Validated U_DicBankDto dicBankDto) {
	dicBankService.updateBank(dicBankDto);
	return Result.success();
}

@ApiOperation("删除银行信息")
@DeleteMapping("{id}")
public Result deleteBank(@PathVariable String id) {
	dicBankService.deleteBank(id);
	return Result.success();
}
}
