package com.wx.house.manager.api.task;

import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.manager.api.contract.service.CContractDetailService;
import com.wx.house.manager.api.contract.service.CContractService;
import com.wx.house.manager.api.expenses.service.EAutoCreatePlanService;
import com.wx.house.manager.api.expenses.service.EAutoMeterReadService;
import com.wx.house.manager.api.expenses.service.ECostDetailService;
import com.wx.house.manager.api.expenses.service.ECostService;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.expenses.ECostMapper;
import com.wx.house.core.pojo.bo.EAutoCreatePlanBo;
import com.wx.house.core.pojo.po.CContract;
import com.wx.house.core.pojo.po.ContractDetail;
import com.wx.house.core.pojo.po.EAutoCreatePlan;
import com.wx.house.core.pojo.po.EAutoMeterRead;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.state.PlanCreatedEnum;
import com.wx.house.core.pojo.vo.CcontractNextChargeVo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import javax.annotation.Resource;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/06 上午 09:59
 * @description：
 * @version: ：V
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class RentCreateTask {
private final EAutoMeterReadService eAutoMeterReadService;
private final ECostService eCostService;
private final ECostDetailService eCostDetailService;
private final CContractService cContractService;
private final CContractDetailService cContractDetailService;

/***
 * @Description 房租生成
 * @Param
 * @Return void
 * @Author 罗棋
 * @Date 2019/11/26 下午 02:34
 */
@Scheduled(cron = "0 23 10 * * ?")
@Transactional(rollbackFor = BusinessException.class)
public void rent() {
		//获取所有的其他参数
		List<EAutoMeterRead> eAutoMeterReads = eAutoMeterReadService.selectAll();
		for (int j = 0; j < eAutoMeterReads.size(); j++) {
			//设置时间格式
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			//获取当前时间
			Date date = new Date();
			//获取提前多久收费时间
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.add(Calendar.DATE, eAutoMeterReads.get(j).getAdvanceDays());
			
			//step1.查询当前时间需要生成房租的合同
			System.out.println("时间是："+format.format(calendar.getTime()));
			List<CcontractNextChargeVo> list=eCostDetailService.selectNextCharge(eAutoMeterReads.get(j).getMAccId(), calendar.getTime());
			//合同
			CContract cContract = new CContract();
			//合同详细
			ContractDetail contractDetail=new ContractDetail();
			//待处理费用详情
			ECostDetail eCostDetail = new ECostDetail();
			//循环
			for (int i = 0; i < list.size(); i++) {
				System.out.println("合同id+"+list.get(i).getContractId()+" 时间+"+format.format(list.get(i).getNextChargeTime()));
				ECost eCost =eCostService.selectMonth(list.get(i).getContractId(), format.format(list.get(i).getNextChargeTime()), "1");
				//判断待处理表是否已经有当月，月份记录
				if(ObjectUtils.isEmpty(eCost)){
					//待处理表
					eCost =new ECost();
					eCost.setId(IDUtils.createUUId());
					eCost.setContrId(list.get(i).getContractId());
					eCost.setContrNumber(list.get(i).getContrNumber());
					eCost.setContrName(list.get(i).getContrName());
					eCost.setCreationTime(date);
					eCost.setChargeMonth(format.format(list.get(i).getNextChargeTime()));
					eCost.setMAccId(list.get(i).getMAccId());
					eCost.setPayTypes("1");
					eCost.setCostType(0);
					eCost.setTotal(list.get(i).getChargeMoney());
					eCost.setActualPay(new BigDecimal(0));
					if(eCostService.insertECost(eCost)!=1) {
						log.error("待处理费用详情创建出错：" + eCost);
						throw new BusinessException("待处理费用详情创建出错：" + eCost);
					}
				}
				//房租
				eCostDetail.setId(IDUtils.createUUId());
				eCostDetail.setECostId(eCost.getId());
				eCostDetail.setChargeType(1);
				eCostDetail.setCreationTime(new Date());
				//存入当前合同当月房租
				eCostDetail.setReceiveMoney(list.get(i).getChargeMoney());
				eCostDetail.setNetMoney(new BigDecimal(0));
				eCostDetail.setCostState(0);
				if(eCostDetailService.insertECostDetail(eCostDetail)!=1) {
					log.error("添加待处理费用出错：" + eCostDetail);
					throw new BusinessException("添加待处理费用出错：" + eCostDetail);
				}
				//获取下次收费时间
				Calendar calendar1 = Calendar.getInstance();
				calendar1.setTime(date);
				System.out.println("时间是："+format1.format(calendar1.getTime()));
				// step2.判断付费类型
				if (list.get(i).getRentType() == 7) {
					calendar1.add(Calendar.MONTH, 1);
					contractDetail.setNextChargeTime(calendar1.getTime());
				}
				if (list.get(i).getRentType() == 8) {
					calendar1.add(Calendar.MONTH, 3); 
					contractDetail.setNextChargeTime(calendar1.getTime());
				}
				if (list.get(i).getRentType() == 9) {
					calendar1.add(Calendar.MONTH, 6); 
					contractDetail.setNextChargeTime(calendar1.getTime());
				}
				if (list.get(i).getRentType() == 10) {
					calendar1.add(Calendar.MONTH, 12); 
					contractDetail.setNextChargeTime(calendar1.getTime());
				}
				//合同到期
				if((format1.format(calendar.getTime())).equals(format1.format(list.get(i).getEndTime()))) {
					cContract.setId(list.get(i).getContractId());
					cContract.setLogicalState(2);
					if(!cContractService.updateById(cContract)) {
						throw new BusinessException("更新合同状态为到期失败");
					}
				}
				//合同未到期
				else {
					contractDetail.setId(list.get(i).getId());
					if(!cContractDetailService.updateById(contractDetail)) {
						throw new BusinessException("更新合同详细下次收费时间失败");
					}
				}
			}
		}
	}
}
