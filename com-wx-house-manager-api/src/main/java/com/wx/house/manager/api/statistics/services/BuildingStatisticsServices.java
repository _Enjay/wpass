package com.wx.house.manager.api.statistics.services;

import com.wx.house.core.pojo.vo.roomStatistics.BuildingMonthIntoStatisticsVo;
import com.wx.house.core.pojo.vo.roomStatistics.BuildingYearIntoStatisticsVo;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/13 下午 02:05
 * @description：
 * @version: ：V
 */
public interface BuildingStatisticsServices {
BuildingMonthIntoStatisticsVo selectBuildingIntoStatisticsVo(String maccid, String yearMonth);

List<BuildingYearIntoStatisticsVo> buildingYearStatistics(String maccid, String year, String buildingName);
}
