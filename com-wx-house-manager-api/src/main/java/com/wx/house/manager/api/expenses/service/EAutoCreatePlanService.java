package com.wx.house.manager.api.expenses.service;

import com.wx.house.core.pojo.bo.EAutoCreatePlanBo;
import com.wx.house.core.pojo.po.EAutoCreatePlan;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 上午 11:04
 * @description：${description}
 * @version: ：V$version
 */
public interface EAutoCreatePlanService extends IService<EAutoCreatePlan> {
List<EAutoCreatePlanBo> selectAllAutoPlanByExample();

int updateByIdIn(EAutoCreatePlan updated, Collection<String> idCollection);

}

