package com.wx.house.manager.api.expenses.service;

import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.vo.ECollectFeesVo;
import com.wx.house.manager.api.expenses.test.ECollectFeesAdd;
import com.wx.house.manager.api.expenses.test.ECollectFeesGet;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
    /**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version:   ：V$version
 */
public interface ECollectFeesService extends IService<ECollectFees>{
	
	/***
	 * 收费列表
	 * @param contrNumber
	 * @param tenantKeyword
	 * @param maccid
	 * @return
	 */
	List<ECollectFeesVo> selectECollectFees(ECollectFeesGet eCollectFeesGet,String maccid);
	/***
	 * 添加收支
	 * @param eCollectFees
	 * @return
	 */
	void addECollectFees(ECollectFeesAdd eCollectFeesAdd,String maccid);
	/***
	 * 收费，退费所有的年月集合
	 * @param szType
	 * @return
	 */
	List<ECollectFees> selectFessMonth(String szType);
}
