package com.wx.house.manager.api.index.services;

import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.vo.checkInStatistics.CheckInStatisticsVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsManager.ExpensesAndReceiptsManagerVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics.ExpensesAndReceiptsStatisticsVo;
import com.wx.house.core.pojo.vo.houseManager.HouseManagerVo;
import com.wx.house.core.pojo.vo.leaseManager.LeaseManagerVo;

import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 09:31
 * @description：
 * @version: ：V
 */
public interface IndexServices {
    ExpensesAndReceiptsManagerVo selectExpensesAndReceiptsManagerVo(String maccid, Date startTime, Date endTime);

    LeaseManagerVo selectLeaseManagerVo(String maccid);

    ExpensesAndReceiptsStatisticsVo selectExpensesAndReceiptsStatisticsVo(String maccid, Date startTime, Date endTime);

    HouseManagerVo selectHouseManagerVo(String maccid);

    CheckInStatisticsVo selectCheckInStatisticsVo(String maccid, String startTime, String endTime);

    String selectMaccIds(UserInfo userInfo);
}
