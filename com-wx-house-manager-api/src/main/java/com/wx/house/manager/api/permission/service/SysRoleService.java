package com.wx.house.manager.api.permission.service;

import com.wx.house.core.pojo.po.SysRole;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version: ：V$version
 */
public interface SysRoleService extends IService<SysRole> {
	/***
	 * 添加角色
	 * @return
	 */
	int AddRole(SysRole sysRole);
	/***
	 * 查询全部角色(角色名称（模糊），状态)
	 * @param roleName
	 * @param state
	 * @return
	 */
	List<SysRole> selectRole(String roleName,String state,String maccType);
	/***
	 * 分配角色查询(角色名称（模糊)
	 * @param roleName
	 * @param state
	 * @return
	 */
	List<SysRole> matchRole(String roleName);
	/***
	 * 根据角色名查询是否重复
	 * @param roleName
	 * @return
	 */
	SysRole getRoleName(String roleName);
	/***
	 * 修改用户
	 * @param sysAdmin
	 * @return
	 */
	int UpdateRole(SysRole sysRole);
	/***
	 * 禁用/啟用
	 * @param id
	 * @return
	 */
	int DeleteRole(String roleid,Integer state);
	/***
	 * 禁用/啟用
	 * @param id
	 * @return
	 */
	int DeleteDelType(String roleid);
	/***
	 * 根据用户查询角色
	 */
	List<SysRole> AdminRole(String adminId);
}
