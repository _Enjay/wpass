package com.wx.house.manager.api.dic.service;

import com.wx.house.core.pojo.dto.dic.C_DicBankDto;
import com.wx.house.core.pojo.dto.dic.U_DicBankDto;
import com.wx.house.core.pojo.po.DicBank;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.DicBankVo;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 下午 02:00
 * @description：${description}
 * @version: ：V$version
 */
public interface DicBankService extends IService<DicBank> {
List<DicBankVo> selectAll(String keyword);

void addBank(C_DicBankDto PDicBankDto);

void deleteBank(String id);

void updateBank(U_DicBankDto dicBankDto);
}
