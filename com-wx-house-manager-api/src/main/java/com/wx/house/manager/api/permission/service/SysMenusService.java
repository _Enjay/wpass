package com.wx.house.manager.api.permission.service;

import com.wx.house.core.pojo.po.SysMenus;
import com.wx.house.core.pojo.vo.PermissionMenuVo;
import com.wx.house.core.pojo.vo.SysMenusVo;
import com.wx.house.core.pojo.vo.SysPermissionVo;

import java.util.List;
import java.util.Set;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version: ：V$version
 */
public interface SysMenusService extends IService<SysMenus> {
	/***
	 * 添加菜单
	 * @param sysMenus
	 */
	int addMenu(SysMenus sysMenus);
	/***
	 * 根据菜单名查询是否重复
	 * @param roleName
	 * @return
	 */
	SysMenus getMenusName(String MenuName);
	/***
	 * 查询全部菜单
	 * @param roleName
	 * @return
	 */
	List<SysMenus> selectMenus(String MenuName);
	/***
	 * 更新
	 * @param sysMenus
	 * @return
	 */
	int updateMenu(SysMenus sysMenus);
	/***
	 * 删除(禁用/启用)
	 * @param sysMenusId
	 * @param state
	 * @return
	 */
	int DeleteMenus(String sysMenusId,Integer state);
	/***
	 * 删除
	 * @param sysMenusId
	 * @param state
	 * @return
	 */
	int DeleteMenuseId(String sysMenusId);
	/***
	 * 查询全部菜单(无条件)
	 */
	List<SysMenusVo> sysMenuslist();
	/***
	 * 查询全部菜单(角色id)
	 */
	List<SysMenusVo> sysRoleMenuslist(String roleId);
	/***
	 * 根据adminID查询菜单
	 * @param roleId
	 * @return
	 */
	List<SysMenusVo> selectRoleMenusAll(String adminId);
	/***
	 * 查询admin的菜单
	 */
	List<SysMenusVo> sysAdmin();
	
	SysPermissionVo SelectSysPermission(String name);
	/***
	 * 查询所有的api
	 * @return
	 */
	Set<SysMenusVo> findAdminMenu();
	/***
	 * 查询所有的api
	 * @return
	 */
	List<SysMenus> findAdminMenulist();
	/***
	 * 查询管理员下面的api
	 * @param uid
	 * @return
	 */
	Set<SysMenusVo> findMenuByUserId(String uid);
	/***
	 * 根据Menus的ID批量查询上级
	 * @param idList
	 * @return
	 */
	List<SysMenusVo> findAllByIdList(List<String> idList);
	/***
	 * 根据adminId单独查询拥有的角色数据
	 * @param adminId
	 * @return
	 */
	List<PermissionMenuVo> selectMenuPermission(String adminId);
	PermissionMenuVo listPidMenus(String id);
}
