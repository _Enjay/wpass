package com.wx.house.manager.api.expenses.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.po.EAutoMeterRead;

import java.util.List;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.expenses.service
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 10:30
 * @Version V1.0
 */
public interface EAutoMeterReadService extends IService<EAutoMeterRead> {
List<EAutoMeterRead> selectAll();

    /**
     * 根据maccid查询自动抄表
     * @param maccid
     * @return
     */
    List<EAutoMeterRead> findMeterReadByMaccId(String maccid);
}
