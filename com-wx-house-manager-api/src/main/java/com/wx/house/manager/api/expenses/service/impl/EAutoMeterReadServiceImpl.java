package com.wx.house.manager.api.expenses.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.manager.api.expenses.service.EAutoMeterReadService;
import com.wx.house.core.mapper.expenses.EAutoMeterReadMapper;
import com.wx.house.core.pojo.po.EAutoMeterRead;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.expenses.service.impl
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 10:31
 * @Version V1.0
 */
@Service
@RequiredArgsConstructor
public class EAutoMeterReadServiceImpl extends ServiceImpl<EAutoMeterReadMapper, EAutoMeterRead> implements EAutoMeterReadService {
private final EAutoMeterReadMapper eAutoMeterReadMapper;

@Override
public List<EAutoMeterRead> selectAll() {
	return eAutoMeterReadMapper.selectAll();
}

@Override
public List<EAutoMeterRead> findMeterReadByMaccId(String maccid) {
	return eAutoMeterReadMapper.selectAllByMAccId(maccid);
}
}
