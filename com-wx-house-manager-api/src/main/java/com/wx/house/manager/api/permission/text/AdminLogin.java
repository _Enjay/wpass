package com.wx.house.manager.api.permission.text;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.Length;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel
public class AdminLogin {
	/***
	 * 用户账号
	 */
	@ApiModelProperty(value = "账户(手机号)", name = "loginName", example = "abcdefg", required = true)
	@Length(max = 128)
	@NotBlank
	private String loginName;
	/***
	 * 用户密码
	 */
	@ApiModelProperty(value = "登陆密码", name = "loginPwd", example = "123456", required = true)
	@NotNull
	@Pattern(regexp = "^.{6,16}$", message = "用户密码:6-16位")
	private String loginPwd;
}
