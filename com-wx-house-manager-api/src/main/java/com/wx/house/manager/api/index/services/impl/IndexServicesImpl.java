package com.wx.house.manager.api.index.services.impl;

import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.StringUtil;
import com.wx.house.core.mapper.expenses.ECollectFeesMapper;
import com.wx.house.core.mapper.index.IndexMapper;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.vo.ContractTimeVo;
import com.wx.house.core.pojo.vo.checkInStatistics.CheckInStatisticsVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsManager.ExpensesAndReceiptsManagerVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsManager.ExpensesManagerDetailVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics.ExpensesAndReceiptsStatisticsVo;
import com.wx.house.core.pojo.vo.expensesAndReceiptsStatistics.ExpensesStatisticsDetailsVo;
import com.wx.house.core.pojo.vo.houseManager.HouseManagerVo;
import com.wx.house.core.pojo.vo.leaseManager.LeaseManagerVo;
import com.wx.house.manager.api.index.services.IndexServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/12/10 上午 09:31
 * @description：
 * @version: ：V
 */
@Service
@RequiredArgsConstructor
public class IndexServicesImpl implements IndexServices {
private final IndexMapper indexMapper;
private final ECollectFeesMapper eCollectFeesMapper;

@Override
public ExpensesAndReceiptsManagerVo selectExpensesAndReceiptsManagerVo(String maccid, Date startTime, Date endTime) {
	String[] split = maccid.split(",");
	List<String> maccIds = Stream.of(split).collect(Collectors.toList());
	ExpensesAndReceiptsManagerVo expensesAndReceiptsManagerVo = indexMapper.selectExpensesAndReceiptsManagerVo(maccIds, startTime, endTime);
	if (expensesAndReceiptsManagerVo == null) expensesAndReceiptsManagerVo = new ExpensesAndReceiptsManagerVo();
	List<ExpensesManagerDetailVo> expensesManagerDetailVoList = expensesAndReceiptsManagerVo.getExpensesManagerDetailVoList();
	
	List<ExpensesManagerDetailVo> newExpensesManagerDetailVoList = new ArrayList<>();
	for (int i = 0; i < 7; i++) {
		int finalI = i;
		List<ExpensesManagerDetailVo> collect = expensesManagerDetailVoList.stream().filter(e -> e.getWaitExpensesType() == finalI).collect(Collectors.toList());
		if (collect.size() >= 1) {
			newExpensesManagerDetailVoList.add(collect.get(0));
		} else {
			newExpensesManagerDetailVoList.add(new ExpensesManagerDetailVo(i, 0, new BigDecimal(0)));
		}
	}
	expensesAndReceiptsManagerVo.setExpensesManagerDetailVoList(newExpensesManagerDetailVoList);
	return expensesAndReceiptsManagerVo;
}

@Override
public LeaseManagerVo selectLeaseManagerVo(String maccid) {
	String[] split = maccid.split(",");
	List<String> maccIds = Stream.of(split).collect(Collectors.toList());

	LeaseManagerVo leaseManagerVo = indexMapper.selectLeaseManagerVo(maccIds);
	if (leaseManagerVo == null) return new LeaseManagerVo();
	leaseManagerVo.setLeaseManagerDetailVoList(leaseManagerVo.getLeaseManagerDetailVoList().stream().filter(e -> e.getExpireTime().before(new Date())).collect(Collectors.toList()));
	return leaseManagerVo;
}

@Override
public ExpensesAndReceiptsStatisticsVo selectExpensesAndReceiptsStatisticsVo(String maccid, Date startTime, Date endTime) {
	String[] split = maccid.split(",");
	List<String> maccIds = Stream.of(split).collect(Collectors.toList());

	//查询收费
	List<ExpensesStatisticsDetailsVo> expensesStatisticsDetailsVos = indexMapper.selectExpensesStatisticsDetailsVo(maccIds, startTime, endTime, 0);
	
	List<ExpensesStatisticsDetailsVo> newExpensesStatisticsDetailsVos = new ArrayList<>();
	for (int i = 0; i < 7; i++) {
		int finalI = i;
		List<ExpensesStatisticsDetailsVo> collect = expensesStatisticsDetailsVos.stream().filter(e -> e.getType() == finalI).collect(Collectors.toList());
		if (collect.size() >= 1) {
			newExpensesStatisticsDetailsVos.add(collect.get(0));
		} else {
			newExpensesStatisticsDetailsVos.add(new ExpensesStatisticsDetailsVo(i, new BigDecimal(0)));
		}
	}
	
	//查询退费
	List<ExpensesStatisticsDetailsVo> receiptsStatisticsDetailsVos = indexMapper.selectExpensesStatisticsDetailsVo(maccIds, startTime, endTime, 1);
	
	List<ExpensesStatisticsDetailsVo> newReceiptsStatisticsDetailsVos = new ArrayList<>();
	
	for (int i = 0; i < 7; i++) {
		int finalI = i;
		List<ExpensesStatisticsDetailsVo> collect = receiptsStatisticsDetailsVos.stream().filter(e -> e.getType() == finalI).collect(Collectors.toList());
		if (collect.size() >= 1) {
			newReceiptsStatisticsDetailsVos.add(collect.get(0));
		} else {
			newReceiptsStatisticsDetailsVos.add(new ExpensesStatisticsDetailsVo(i, new BigDecimal(0)));
		}
	}
	
	ExpensesAndReceiptsStatisticsVo expensesAndReceiptsStatisticsVo = new ExpensesAndReceiptsStatisticsVo();
	expensesAndReceiptsStatisticsVo.setExpensesStatisticsDetailsVoList(newExpensesStatisticsDetailsVos);
	expensesAndReceiptsStatisticsVo.setReceiptsStatisticsDetailsVoList(newReceiptsStatisticsDetailsVos);
	
	return expensesAndReceiptsStatisticsVo;
}

@Override
public HouseManagerVo selectHouseManagerVo(String maccid) {
	String[] split = maccid.split(",");
	List<String> maccIds = Stream.of(split).collect(Collectors.toList());
	return indexMapper.selectHouseManagerVo(maccIds);
}

@Override
public CheckInStatisticsVo selectCheckInStatisticsVo(String maccid, String startTime, String endTime) {
	//开始时间是过去六个月
	if (StringUtil.isEmpty(startTime)) {
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		calendar.add(Calendar.MONTH, -6);
		startTime = new SimpleDateFormat("yyyy-MM").format(calendar.getTime());
	}
	//结束时间默认是现在
	if (StringUtil.isEmpty(endTime)) {
		endTime = new SimpleDateFormat("yyyy-MM").format(new Date());
	}
	
	List<String> timeList = createTimeList(startTime, endTime);
	String[] split = maccid.split(",");
	List<String> maccIds = Stream.of(split).collect(Collectors.toList());
	List<ContractTimeVo> contractTimeVos = indexMapper.selectAllContractTimeVo(maccIds, timeList);
	contractTimeVos.forEach(System.err::println);
	
	List<Integer> validTenantNumberList = new LinkedList<>();
	
	//根据timeList进行区间统计
	for (String month : timeList) {
		//在这个month里的有效合同有多少个
		
		Date thisMonthDate = null;
		try {
			thisMonthDate = new SimpleDateFormat("yyyy-MM").parse(month);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Date finalThisMonthDate = thisMonthDate;
		
		//查询的月份在开始时间和结束时间之间
		long count = contractTimeVos
				             .stream()
				             .filter(e -> e.getStartTime().before(finalThisMonthDate))
				             .filter(e -> e.getEndTime().after(finalThisMonthDate))
				             .count();
		
		count += contractTimeVos
				         .stream()
				         .filter(e -> getMonth(e.getStartTime()).compareTo(finalThisMonthDate) == 0)
				         .count();
		
		validTenantNumberList.add((int) count);
	}
	CheckInStatisticsVo checkInStatisticsVo = new CheckInStatisticsVo();
	checkInStatisticsVo.setMonthList(timeList);
	checkInStatisticsVo.setValidTenantNumberList(validTenantNumberList);
	
	return checkInStatisticsVo;
}

	@Override
	public String selectMaccIds(UserInfo userInfo) {
		String  maccId = "";
		if(userInfo.getAccountType().equals("0")){
			maccId = String.join(",",indexMapper.selectMaccIds()) ;
		}else{
			maccId = userInfo.getId();
		}
		return  maccId;
	}

/*	@Override
	public List<String> selectMaccIds() {

		String  maccId = "";
		if(this.getMaccId1().getAccountType().equals("0")){
			maccId = String.join(",",indexMapper.selectMaccIds()) ;
		}else{
			maccId =  this.getMaccId1().getId();
		}

		return  indexMapper.selectMaccIds();
	}*/

	private Date getMonth(Date date) {
	String monthDate = new SimpleDateFormat("yyyy-MM").format(date);
	try {
		return new SimpleDateFormat("yyyy-MM").parse(monthDate);
	} catch (ParseException e) {
		e.printStackTrace();
	}
	return new Date();
}

private static List<String> createTimeList(String startTime, String endTime) {
	Date startDate = null;
	try {
		startDate = new SimpleDateFormat("yyyy-MM").parse(startTime);
	} catch (ParseException e) {
	}
	Date endDate = null;
	try {
		endDate = new SimpleDateFormat("yyyy-MM").parse(endTime);
	} catch (ParseException e) {
	}
	
	//如果开始时间大于结束时间则会陷入死循环
	if (startDate.after(endDate))
		throw new BusinessException("开始时间不能大于结束时间");
	
	List<String> resultList = new ArrayList<>();
	
	//如果开始时间等于结束时间
	if (startTime.equals(endTime)) {
		resultList.add(startTime);
		return resultList;
	}
	
	Calendar calendar = Calendar.getInstance();
	Date tempDate = startDate;
	
	calendar.setTime(tempDate);
	while (true) {
		resultList.add(new SimpleDateFormat("yyyy-MM").format(calendar.getTime()));
		calendar.add(Calendar.MONTH, 1);
		
		if (new SimpleDateFormat("yyyy-MM").format(calendar.getTime()).equals(endTime)) {
			resultList.add(endTime);
			break;
		}
	}
	
	return resultList;
}

}
