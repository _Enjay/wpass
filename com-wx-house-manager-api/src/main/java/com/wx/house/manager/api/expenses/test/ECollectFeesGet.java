package com.wx.house.manager.api.expenses.test;

import java.util.Date;

import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
@Data
public class ECollectFeesGet {
	@ApiModelProperty("根据租户搜索: 租户性名 租户手机 租户证件号码 合同编号")
	private String tenantKeyword;
	@ApiModelProperty("根据费用类型搜索:字典查询1 ")
	private String feesType;
	@ApiModelProperty("根据支付方式:字典查询0 ")
	private String paymentMethod;
	@ApiModelProperty("根据收支类型搜索0收费 1退费: ")
	private String szType;
	@ApiModelProperty("缴费月份:2019-9,2019-10")
	private String month;
	@ApiModelProperty("开始时间")
	private Date startTime;
	@ApiModelProperty("结束时间")
	private Date endTime;
	@ApiModelProperty("楼宇id")
	private String buildingId;
	@ApiModelProperty("房间号")
	private String RoomNumber;
	
}
