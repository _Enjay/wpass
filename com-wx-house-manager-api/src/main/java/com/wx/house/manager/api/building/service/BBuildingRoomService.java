package com.wx.house.manager.api.building.service;

import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.BuildingRoomVo;
import com.wx.house.core.pojo.vo.HiredRoomVo;
import com.wx.house.core.pojo.vo.RoomNumberVo;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 上午 11:57
 * @description：${description}
 * @version: ：V$version
 */
public interface BBuildingRoomService extends IService<BuildingRoom> {

List<BuildingRoomVo> selectSimpleWaitHireRoom(String buildingId, String floor, String door,String buildType);

List<BuildingRoomVo> selectWaitHireRoom(String buildingId, String floor);

List<HiredRoomVo> selectHiredRoomVo(String buildingId, String floor, String door);
/***
 * 根据合同查询房间详细表
 */
BuildingRoom selectNumberRoom(String contrNumber);
/***
 * 根据楼宇id查询房间号
 */
List<RoomNumberVo> selectRoomNumber(String buildingId);
/***
 * 根据房间号查询房间详细表
 */
BuildingRoom selectNumberIdRoom(String roomId);
}
