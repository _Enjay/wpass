package com.wx.house.manager.api.dic.controller;

import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.core.pojo.vo.DicWebDeployVo;
import com.wx.house.manager.api.dic.service.DicWebDeployService;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.UserInfoUtil;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.dto.expenses.DicWebDeployDto;
import com.wx.house.core.pojo.po.DicWebDeploy;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @Title:
 * @BelongProjecet com-wx-house
 * @BelongPackage com.wx.house.api.dic.controller
 * @Description:
 * @Author: yxc
 * @Date: 2019/11/12 13:44
 * @Version V1.0
 */
@RestController
@RequestMapping("DicWebDeploy/")
@Api(value = "网站配置", tags = "网站配置")
@Slf4j
public class DicWebDeployController extends BaseController {
    @Resource
    private DicWebDeployService dicWebDeployService;
    @Resource
    private ProjectConfiguration projectConfiguration;
    @PostMapping("add/")
    @ApiOperation(value = "新增网站", notes = "新增网站")
    public Result addDicWebDeploy(@RequestBody @Validated DicWebDeployDto dicWebDeployDto){
     //   UserInfo userInfo = UserInfoUtil.userInfo();
        //判断此用户是否配置了网站
        List<DicWebDeployVo> deploys = dicWebDeployService.findWebDeployByMaccId("0");
        DicWebDeploy dicWebDeploy = new DicWebDeploy();
        BeanUtils.copyProperties(dicWebDeployDto,dicWebDeploy);
        dicWebDeploy.setOperation("jack");
        dicWebDeploy.setOpreationTime(new Date());
      if(deploys.size()!=0){
          dicWebDeploy.setId(deploys.get(0).getId());
          dicWebDeployService.updateById(dicWebDeploy);
      }else {
          dicWebDeploy.setId(IDUtils.genItemId(16)+"");
          dicWebDeploy.setMAccId("0");
          dicWebDeployService.save(dicWebDeploy);
      }
        return Result.success("操作成功");
    }
    @GetMapping("findDicWebDeployDetail/")
    @ApiOperation(value = "查看网站配置详情", notes = "查看网站配置详情")
    public Result findDicWebDeployDetail(){
      //  UserInfo userInfo = UserInfoUtil.userInfo();
        List<DicWebDeployVo> deploys = dicWebDeployService.findWebDeployByMaccId("0");
       //超级管理员的logo
        if(deploys.size()!=0){
           //配置url路径
           deploys.get(0).setUrl(projectConfiguration.getStaticResource().getStaticResourceUrl() +deploys.get(0).getUrl());
           return Result.success(deploys.get(0));
       }
        return Result.success(null);
    }
}
