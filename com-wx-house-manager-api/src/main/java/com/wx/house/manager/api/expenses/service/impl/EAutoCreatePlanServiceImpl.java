package com.wx.house.manager.api.expenses.service.impl;

import com.wx.house.manager.api.expenses.service.EAutoCreatePlanService;
import com.wx.house.core.pojo.bo.EAutoCreatePlanBo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.expenses.EAutoCreatePlanMapper;
import com.wx.house.core.pojo.po.EAutoCreatePlan;

import java.util.Collection;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/05 上午 11:04
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class EAutoCreatePlanServiceImpl extends ServiceImpl<EAutoCreatePlanMapper, EAutoCreatePlan> implements EAutoCreatePlanService {
private final EAutoCreatePlanMapper eAutoCreatePlanMapper;

@Override
public List<EAutoCreatePlanBo> selectAllAutoPlanByExample() {
	return eAutoCreatePlanMapper.selectAllAutoPlanByExample();
}

@Override
public int updateByIdIn(EAutoCreatePlan updated, Collection<String> idCollection) {
	return eAutoCreatePlanMapper.updateByIdIn(updated, idCollection);
}
}
