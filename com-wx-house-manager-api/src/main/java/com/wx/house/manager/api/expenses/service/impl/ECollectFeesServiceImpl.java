package com.wx.house.manager.api.expenses.service.impl;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.util.StringUtil;
import com.wx.house.manager.api.expenses.service.ECollectFeesService;
import com.wx.house.manager.api.expenses.test.ECollectFeesAdd;
import com.wx.house.manager.api.expenses.test.ECollectFeesGet;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.exception.UserException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.capital.SysAdminDetailMapper;
import com.wx.house.core.mapper.expenses.ECollectFeesMapper;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.vo.ECollectFeesVo;

import lombok.RequiredArgsConstructor;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class ECollectFeesServiceImpl extends ServiceImpl<ECollectFeesMapper, ECollectFees> implements ECollectFeesService {
	private final ECollectFeesMapper eCollectFeesMapper;
	
//	private final SysAdminDetailMapper sysAdminDetailMapper;

	@Override
	public List<ECollectFeesVo> selectECollectFees(ECollectFeesGet eCollectFeesGet,String maccid) {
		List<String> monthlist=null;
		if(StringUtil.isNotEmpty(eCollectFeesGet.getMonth())) {
		 monthlist =Arrays.asList(eCollectFeesGet.getMonth().split(","));
		}
		String floorId=null;
		String doorNumber=null;
		if(!StringUtil.isEmpty(eCollectFeesGet.getRoomNumber())) {
			String[] s = eCollectFeesGet.getRoomNumber().split("-");
			 floorId=s[0];
			 doorNumber=s[1];
		}
		return eCollectFeesMapper.selectECollectFees(maccid,
				eCollectFeesGet.getTenantKeyword(),
				eCollectFeesGet.getPaymentMethod(), 
				eCollectFeesGet.getSzType(),
				eCollectFeesGet.getFeesType(),
				monthlist,
				eCollectFeesGet.getStartTime(),
				eCollectFeesGet.getEndTime(),eCollectFeesGet.getBuildingId(), floorId, doorNumber);
	}

	@Override
	public void addECollectFees(ECollectFeesAdd eCollectFeesAdd,String maccid) {
//		SysAdminDetail sysAdminDetail=sysAdminDetailMapper.selectContractId(eCollectFeesAdd.getContractId());
		ECollectFees eCollectFees=ECollectFees.builder()
				.id(IDUtils.createUUId().toString())
				.szType(Integer.valueOf(eCollectFeesAdd.getSzType()))
				.feesType(Integer.valueOf(eCollectFeesAdd.getFeesType()))
				.paymentMethod(3)
				.mAccId(maccid)
				.contractId(eCollectFeesAdd.getContractId())
				.feeMonth(eCollectFeesAdd.getFeeMonth())
				.receivable(eCollectFeesAdd.getReceivable())
				.actual(eCollectFeesAdd.getActual())
				.createTime(new Date())
				.chargeTime(new Date())
				.remarks(eCollectFeesAdd.getRemarks())
				.build();
//		BigDecimal a=null;
//		BigDecimal b=null;
		if(eCollectFeesMapper.insert(eCollectFees)!=1)
		{
			 throw new UserException("添加收支失败", 201);
		}
//		if(!"3".equals(eCollectFeesAdd.getPaymentMethod())){
//			if("0".equals(eCollectFeesAdd.getSzType())) {
//				 a= sysAdminDetail.getBalance().add(eCollectFeesAdd.getActual());
//				 b= sysAdminDetail.getTotalIncome().add(eCollectFeesAdd.getActual());
//				 sysAdminDetailMapper.updateDetail(sysAdminDetail.getMaccId(), a, b);
//			}
//			else {
//				 a= sysAdminDetail.getBalance().subtract(eCollectFeesAdd.getActual());
//				 sysAdminDetailMapper.updateDetail(sysAdminDetail.getMaccId(), a, null);
//			}
//		}
	}

	@Override
	public List<ECollectFees> selectFessMonth(String szType) {
		return eCollectFeesMapper.selectFeeMonth(szType);
	}

}
