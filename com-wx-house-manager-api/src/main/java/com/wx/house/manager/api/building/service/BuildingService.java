package com.wx.house.manager.api.building.service;

import com.wx.house.core.pojo.dto.building.*;
import com.wx.house.core.pojo.po.building.Building;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.po.building.BuildingRoom;
import com.wx.house.core.pojo.vo.BuildingSimpleVo;
import com.wx.house.core.pojo.vo.BuildingVo;

import java.util.List;


public interface BuildingService extends IService<Building> {

List<BuildingVo> selectBuildingVoList(String mAccId,
                                      String buildingName,
                                      Integer province,
                                      Integer city,
                                      Integer county);

List<BuildingSimpleVo> selectBuildingSimpleVoList(String mAccId,
                                                  String buildingName,
                                                  Integer province,
                                                  Integer city,
                                                  Integer county);

void insertBuilding(InsertBuildingDto dto);

Building selectByBuildName(String buildingName);

void deleteBuilding(String id);

List<BuildingRoom> insertBuildingRoom(List<InsertBuildingRoomDto> insertBuildingRoomDtoList);

List<BuildingRoom> deleteBuildingRoom(List<String> idList);

void updateBuildingRoom(List<UpdateBuildingRoomDto> updateBuildingRoomDtoList);

void discardBuildingRoom(String roomId,String buildType);

void updateBuilding(UpdateBuildingDto updateBuildingDto);
}
