package com.wx.house.manager.api.dic.service;

import com.wx.house.core.pojo.po.DicAutoCreateConfig;
import com.baomidou.mybatisplus.extension.service.IService;
    /**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/11/05 上午 11:03
 * @description：${description}
 * @version:   ：V$version
 */
public interface DicAutoCreateConfigService extends IService<DicAutoCreateConfig>{


}
