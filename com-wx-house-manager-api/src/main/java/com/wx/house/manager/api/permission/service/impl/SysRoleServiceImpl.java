package com.wx.house.manager.api.permission.service.impl;

import java.util.List;

import javax.annotation.Resource;

import com.wx.house.manager.api.permission.service.SysRoleService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wx.house.core.mapper.permission.SysRoleMapper;
import com.wx.house.core.pojo.po.SysRole;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version: ：V$version
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {
	@Resource
	private SysRoleMapper sysRoleMapper;
	
	@Override
	public int AddRole(SysRole sysRole) {
		return sysRoleMapper.insert(sysRole);
	}

	@Override
	public SysRole getRoleName(String roleName) {
		return sysRoleMapper.getRoleName(roleName);
	}

	@Override
	public List<SysRole> selectRole(String roleName, String state,String maccType) {
		return sysRoleMapper.selctRole(roleName, state,maccType);
	}
	
	@Override
	public List<SysRole> matchRole(String roleName) {
		return sysRoleMapper.matchRole(roleName);
	}

	@Override
	public int UpdateRole(SysRole sysRole) {
		return sysRoleMapper.updateById(sysRole);
	}

	@Override
	public int DeleteRole(String roleid, Integer state) {
		return sysRoleMapper.DeleteRole(roleid, state);
	}

	@Override
	public int DeleteDelType(String roleid) {
		return sysRoleMapper.DeleteDelType(roleid);
	}

	@Override
	public List<SysRole> AdminRole(String adminId) {
		return sysRoleMapper.AdminRole(adminId);
	}



}
