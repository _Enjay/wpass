package com.wx.house.manager.api.expenses.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.wx.house.manager.api.expenses.service.ECostService;
import com.wx.house.manager.api.expenses.test.ECostGet;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.expenses.ECostDto;
import com.wx.house.core.pojo.vo.ECostDetailVo;
import com.wx.house.core.pojo.vo.ECostVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@Api(value = "模块:待处理费用", tags = "模块:待处理费用")
@RequestMapping("Ecost/")
public class ECostController extends BaseController{
	@Resource
	private ECostService eCostService;
	
	@GetMapping("list")
	@ApiOperation(value = "待处理费用列表", notes = "待处理费用列表")
	public Result<PageInfo<ECostDetailVo>> getAllECost(Page page,@Validated ECostGet eCostGet, HttpServletRequest request) {
		try {
			List<ECostDetailVo> list=new ArrayList<ECostDetailVo>();
			List<ECostVo> eCosts = eCostService.selectAllEcost(eCostGet, this.getMaccId());
			for (int i = 0; i < eCosts.size(); i++) {
				ECostDetailVo eCostDetailVo = eCostService.selectAllEcostDetail(eCosts.get(i).getId());
				list.add(eCostDetailVo);
			}
			PageInfo<ECostDetailVo> pageInfo = new PageInfo<>(list);
			return Result.success(pageInfo);
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("查询失败");
		}
	}
	
	@PutMapping("addECost/")
	@ApiOperation(value = "确认待处理费用(更新)", notes = "确认待处理费用(更新)")
	public Result addECost(@RequestBody ECostDto eCostDto) {
		try {
			if(eCostDto.getReceiveMoney().compareTo(eCostDto.getNetMoney()) != 0) {
				return Result.error("应收和实收金额不对");
			}
			eCostService.addEcost(eCostDto);
			return Result.success();
		} catch (Exception e) {
			e.printStackTrace();
			throw new BusinessException("处理失败");
		}
	}
	
}
