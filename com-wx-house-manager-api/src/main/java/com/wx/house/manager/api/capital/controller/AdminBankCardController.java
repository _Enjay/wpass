package com.wx.house.manager.api.capital.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.BaseController;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.util.HideDataUtil;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.dto.capital.InsertBankcardDto;
import com.wx.house.core.pojo.po.SysAdminBankcard;
import com.wx.house.core.pojo.state.LogicalDeleteEnum;
import com.wx.house.core.pojo.vo.SysAdminBankcardVo;
import com.wx.house.manager.api.capital.service.SysAdminBankcardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/28 上午 10:15
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("bankcard/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "管理员银行卡管理")
@Validated
public class AdminBankCardController extends BaseController {
private final SysAdminBankcardService sysAdminBankcardService;

@ApiOperation("银行卡列表")
@GetMapping
@ApiImplicitParam(name = "keyword", value = "搜索词: 银行卡号，开户人，开户人手机号", required = false)
public Result<PageInfo<SysAdminBankcard>> selectAll(Page page, String keyword) {
	String maccId = this.getMaccId();
	List<SysAdminBankcard> sysAdminBankcards = sysAdminBankcardService.selectAll(maccId, keyword);
	sysAdminBankcards.forEach(e -> e.setCardNumber(HideDataUtil.hideCardNo(e.getCardNumber())));
	PageInfo<SysAdminBankcard> pageInfo = new PageInfo<>(sysAdminBankcards);
	return Result.success(pageInfo);
}

@ApiOperation("银行卡列表[全部]")
@GetMapping("all/")
public Result<List<SysAdminBankcardVo>> selectAll() {
	String maccId = this.getMaccId();
	List<SysAdminBankcardVo> sysAdminBankcardVos = sysAdminBankcardService.selectAllVo(maccId);
	sysAdminBankcardVos.forEach(e -> e.setCardNumber(HideDataUtil.hideCardNo(e.getCardNumber())));
	return Result.success(sysAdminBankcardVos);
}


@ApiOperation("管理员新增银行卡")
@PostMapping
public Result addBankCard(@RequestBody @Validated InsertBankcardDto bankcardDto) {
	String maccId = this.getMaccId();
	sysAdminBankcardService.addBankCard(bankcardDto, maccId);
	return Result.success();
}

@ApiOperation("将银行卡设置为默认")
@PutMapping("{id}")
public Result setDefault(@PathVariable("id") String id) {
	String maccId = this.getMaccId();
	sysAdminBankcardService.setDefault(maccId, id);
	return Result.success();
}

@ApiOperation("删除银行卡")
@DeleteMapping("{id}")
public Result deleteBankcard(@PathVariable("id") String id) {
	sysAdminBankcardService.deleteBankcard(id);
	return Result.success();
}

}
