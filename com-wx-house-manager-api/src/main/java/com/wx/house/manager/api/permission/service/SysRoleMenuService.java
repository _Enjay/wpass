package com.wx.house.manager.api.permission.service;

import com.wx.house.core.pojo.po.SysRoleMenu;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:02
 * @description：${description}
 * @version: ：V$version
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {
	/***
	 * 分配菜单
	 * @param sysRoleMenu
	 * @return
	 */
	int addRoleMenu(SysRoleMenu sysRoleMenu);
	/***
	 * 删除角色菜单
	 * @param roleid
	 * @return
	 */
	int dateRoleMenu(String roleid);
}
