package com.wx.house.manager.api.capital.service;

import com.wx.house.core.pojo.dto.capital.InsertBankcardDto;
import com.wx.house.core.pojo.po.SysAdminBankcard;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.vo.SysAdminBankcardVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 04:44
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminBankcardService extends IService<SysAdminBankcard> {
List<SysAdminBankcard> selectAll(String maccId, String keyword);

List<SysAdminBankcardVo> selectAllVo(String maccId);

int insertSelective(SysAdminBankcard sysAdminBankcard);


void addBankCard(InsertBankcardDto bankcardDto, String maccId);

void setDefault(String maccId, String id);

void deleteBankcard(String id);

}



