package com.wx.house.manager.api.common;


import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wx.house.common.base.BaseController;
import com.wx.house.common.util.IpHelp;
import com.wx.house.common.util.RandomValidateCodeUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
@RequestMapping("base/validateCode/")
@Api(value = "基础设施", tags = "基础设施")
public class ValidateCode extends BaseController{
	
	@GetMapping
	@ApiOperation("图形验证码")
	public void getValidateCode(HttpServletRequest request, HttpServletResponse response) {
		RandomValidateCodeUtil randomValidateCodeUtil = new RandomValidateCodeUtil();
		String randcode = randomValidateCodeUtil.getRandcode(request, response);
		String ipAddr = new IpHelp().getIpAddr(request);
		System.out.println("图片："+randcode);
		redisUtil().set(String.format(VALIDATECODE, ipAddr), randcode, 300L, TimeUnit.HOURS);
	}
}
