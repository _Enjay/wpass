package com.wx.house.manager.api.contract.controller;

import com.github.pagehelper.PageInfo;
import com.wx.house.common.base.ProjectConfiguration;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.core.pojo.po.CContract;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import com.wx.house.manager.api.contract.service.CContractService;
import com.wx.house.manager.api.permission.text.ContractId;
import com.wx.house.common.base.BaseController;
import com.wx.house.core.pojo.base.Page;
import com.wx.house.core.pojo.base.Result;
import com.wx.house.core.pojo.base.UserInfo;
import com.wx.house.core.pojo.dto.contract.InsertContractDto;
import com.wx.house.core.pojo.dto.contract.SelectContractDto;
import com.wx.house.core.pojo.vo.CcontractBuildVo;
import com.wx.house.core.pojo.vo.ContractVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author ：罗棋
 * @Email ：58428467@qq.com
 * @Date ：Created in 2019/11/04 上午 10:53
 * @description：
 * @version: ：V
 */
@RestController
@RequestMapping("contract/")
@Slf4j
@RequiredArgsConstructor
@Api(tags = "合同管理")
@Validated
public class ContractController extends BaseController {
private final CContractService cContractService;
private final ProjectConfiguration projectConfiguration;

@ApiOperation("合同列表")
@GetMapping
public Result<PageInfo<ContractVo>> selectAllContract(Page page, @Validated SelectContractDto selectContractDto) {
	UserInfo userInfo = this.userInfo();
	PageInfo<ContractVo> contractVoPageInfo = new PageInfo<>(cContractService.selectAllContract(
			userInfo.getMAccId(),
			selectContractDto.getContractKeyword(),
			selectContractDto.getTenantKeyword(),
			selectContractDto.getState(),
			selectContractDto.getDaysTime()
	));
	for (ContractVo contractVo : contractVoPageInfo.getList()) {
		for (AttaUpload e : contractVo.getAttaUploadList()) {
			e.setUrl(projectConfiguration.getStaticResource().getStaticResourceUrl() + e.getUrl());
		}
	}
	return Result.success(contractVoPageInfo);
}

@ApiOperation("新增合同")
@PostMapping
public Result insertContract(@RequestBody @Validated InsertContractDto insertContractDto) {
	UserInfo userInfo = this.userInfo();
	cContractService.insertContract(insertContractDto, userInfo);
	return Result.success();
}

@ApiOperation("合同删除")
@DeleteMapping("{id}")
public Result deleteContract(@PathVariable String id) {
	//合同删除以后，租的房间应该为待出租
	cContractService.deleteContract(id);
	return Result.success();
}

@ApiOperation("查询合同编号")
@GetMapping("contrNumber/")
@ApiImplicitParams({
		@ApiImplicitParam(name = "tenantKeyword", value = "合同编号，电话，出租人", required = false, dataType = "String", paramType = "query")
})
public Result<PageInfo<CcontractBuildVo>> selectNumber(Page page, String tenantKeyword) {
	List<CcontractBuildVo> buildVos = cContractService.selectNumber(tenantKeyword);
	PageInfo<CcontractBuildVo> pageInfo = new PageInfo<>(buildVos);
	return Result.success(pageInfo);
}


@ApiOperation("验证合同编号是否存字")
@PostMapping("validId/{number}")
public Result validContractId(@PathVariable("number") String number) {
	cContractService.validContractId(number);
	return Result.success();
}
@ApiOperation("合同到期后归档")
@PutMapping("updateFile/")
public Result updateFile(@RequestBody @Validated ContractId contractId) {
	try {
		//查询合同状态
		CContract cContract= cContractService.selectContractId(contractId.getContractId());
		if(cContract.getLogicalState()!=2) {
			return Result.error("合同状态不对，不能归档");
		}
 		cContractService.updateFile(contractId.getContractId());
		return Result.success();
	} catch (Exception e) {
		throw new BusinessException("合同到期后归档失败");
	}
}
}
