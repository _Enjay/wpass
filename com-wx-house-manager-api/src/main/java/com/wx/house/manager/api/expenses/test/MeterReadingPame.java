package com.wx.house.manager.api.expenses.test;

import java.math.BigDecimal;

import javax.validation.constraints.Pattern;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class MeterReadingPame{
    /**
     * 楼栋id
     */
	@ApiModelProperty(value = "楼栋id" )
    private String buildingId;

    /**
     * 门牌号
     */
	@ApiModelProperty(value = "门牌号" )
    private String doorNumber;

    /**
     * 合同编号
     */
	@ApiModelProperty(value = "合同编号" )
    private String contrNumber;

    /**
     * 租户姓名
     */
	@ApiModelProperty(value = " 租户姓名" )
    private String tenantName;

    /**
     * 电话号码
     */
	@ApiModelProperty(value = "电话号码" )
    private String phone;

    /**
     * 抄表月份
     */
	@ApiModelProperty(value = "抄表月份(2019.09)" )
	@Pattern(regexp = "2\\d+\\-(0[1-9]|1[012])",message = "时间格式不正常")
    private String month;

    /**
     * 水表读数(立方米)
     */
	@ApiModelProperty(value = "水表读数(立方米)" )
    private Integer monthWater;

    /**
     * 电表读数(千瓦时)
     */
	@ApiModelProperty(value = "电表读数(千瓦时)" )
    private Integer monthElectric;

    /**
     * 气表读数(立方米)
     */
	@ApiModelProperty(value = "气表读数(立方米)" )
    private Integer monthGas;
}
