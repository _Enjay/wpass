package com.wx.house.manager.api.dic.service;

import com.wx.house.core.pojo.po.DicWebDeploy;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wx.house.core.pojo.po.EAutoMeterRead;
import com.wx.house.core.pojo.vo.DicWebDeployVo;

import java.util.List;

/**
 * @Author     ：罗棋
 * @Email      ：58428467@qq.com
 * @date       ：Created in 2019/10/28 上午 11:58
 * @description：${description}
 * @version:   ：V$version
 */
public interface DicWebDeployService extends IService<DicWebDeploy>{
    /**
     * 根据主账号id查看账户的网站配置
     * @param maccId
     * @return
     */
    List<DicWebDeployVo>  findWebDeployByMaccId(String maccId);
}
