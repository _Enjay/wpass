//package com.wx.house.manager.api.contract.controller;
//
//import com.wx.house.common.util.RedisUtil;
//import com.wx.house.core.pojo.base.Result;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import lombok.RequiredArgsConstructor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.function.RouterFunction;
//import org.springframework.web.servlet.function.ServerResponse;
//
//import java.util.ArrayList;
//
//import static org.springframework.web.servlet.function.RequestPredicates.GET;
//import static org.springframework.web.servlet.function.RequestPredicates.POST;
//import static org.springframework.web.servlet.function.RouterFunctions.route;
//
///**
// * @Author ：罗棋
// * @Email ：58428467@qq.com
// * @date ：Created in 2019/11/05 下午 01:32
// * @description：
// * @version: ：V
// */
//@Api(tags = "测试函数式controller")
//@RequiredArgsConstructor
//@RestController
//public class ContractHandler {
//private final RedisUtil redisUtil;
//
//@GetMapping("")
//@ApiOperation("tessfas")
//public Object test1() {
//	return "2";
//}
//
//@Bean
//@ApiOperation("测试1")
//public RouterFunction<ServerResponse> routes2() {
//	return route(GET(""), e ->
//			                      ServerResponse.ok().body(Result.success())
//	).andRoute(POST(""), e ->
//			                     ServerResponse.ok().body(Result.success(new ArrayList<>().add("post接收参数")))
//	).andRoute(GET("test/"), e ->
//			                         ServerResponse.ok().body(Result.success(redisUtil.get("aaa")))
//	).andRoute(GET("test/add/{str}"), e -> {
//		redisUtil.set("aaa", e.pathVariable("str"));
//		return ServerResponse.ok().body(Result.success());
//	});
//}
//}
