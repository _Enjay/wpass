package com.wx.house.manager.api.expenses.service.impl;

import com.wx.house.manager.api.expenses.service.ECostService;
import com.wx.house.manager.api.expenses.test.ECostGet;
import com.wx.house.common.exception.BusinessException;
import com.wx.house.common.util.IDUtils;
import com.wx.house.core.mapper.capital.SysAdminDetailMapper;
import com.wx.house.core.mapper.expenses.ECollectFeesMapper;
import com.wx.house.core.mapper.expenses.ECostDetailMapper;
import com.wx.house.core.mapper.expenses.ECostMapper;
import lombok.RequiredArgsConstructor;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.util.StringUtil;
import com.wx.house.core.pojo.dto.expenses.ECostDto;
import com.wx.house.core.pojo.po.ECollectFees;
import com.wx.house.core.pojo.po.ECost;
import com.wx.house.core.pojo.po.ECostDetail;
import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.vo.ECostDetailVo;
import com.wx.house.core.pojo.vo.ECostPameVo;
import com.wx.house.core.pojo.vo.ECostVo;

import java.math.BigDecimal;
import java.nio.BufferOverflowException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/10/28 下午 12:01
 * @description：${description}
 * @version: ：V$version
 */
@Service
@RequiredArgsConstructor
public class ECostServiceImpl extends ServiceImpl<ECostMapper, ECost> implements ECostService {
private final ECostMapper eCostMapper;

private final ECostDetailMapper costDetailMapper;

private final ECollectFeesMapper eCollectFeesMapper;

@Override
public int insertList(List<ECost> list) {
	return eCostMapper.insertListRent(list);
}

@Override
public List<ECostVo> selectAllEcost(ECostGet eCostGet, String maccid) {
		String floorId=null;
		String doorNumber=null;
		if(!StringUtil.isEmpty(eCostGet.getRoomNumber())) {
			String[] s = eCostGet.getRoomNumber().split("-");
			 floorId=s[0];
			 doorNumber=s[1];
		}
		return eCostMapper.selectAllECost(maccid, eCostGet.getTenantKeyword(),
				eCostGet.getChargeType(),
				eCostGet.getStartTime(),
				eCostGet.getEndTime(), eCostGet.getBuildingId(),floorId, doorNumber);
}

@Override
public ECostDetailVo selectAllEcostDetail(String eCostId) {
	return eCostMapper.selectAllECostDetail(eCostId);
}

@Override
public void addEcost(ECostDto eCostDto) {
	//根据待处理id查询待处理详细
	List<ECostPameVo> list=costDetailMapper.selectECostID(eCostDto.getId());
	//将数据存入收取费用表
	ECollectFees eCollectFees=null;
	for (int i = 0; i < list.size(); i++) {
		eCollectFees = ECollectFees.builder()
                .id(IDUtils.createUUId())
                .szType(list.get(i).getCostType())
                .feesType(list.get(i).getChargeType().intValue())
                .paymentMethod(3)
                .mAccId(list.get(i).getMAccId())
                .contractId(list.get(i).getContrId())
                .feeMonth(list.get(i).getChargeMonth())
                .receivable(list.get(i).getReceiveMoney())
                .actual(list.get(i).getReceiveMoney())
                .createTime(list.get(i).getCreationTime())
                .chargeTime(new Date())
                .remarks(eCostDto.getRemarks())
                .build();
		if(eCollectFeesMapper.insert(eCollectFees)!=1) {
		throw new BusinessException("添加记录到收取费用表失败");
		}
		ECost eCost =new ECost();
		eCost.setId(list.get(i).getECostId());
		eCost.setActualPay(eCostDto.getNetMoney());
		eCost.setPayTime(new Date());
		if(eCostMapper.updateById(eCost)!=1) {
			throw new BusinessException("更新待处理费用处理状态失败");
		}
		ECostDetail eCostDetail = new ECostDetail();
		eCostDetail.setId(list.get(i).getId());
		eCostDetail.setNetMoney(list.get(i).getReceiveMoney());
		eCostDetail.setCostState(1);
		eCostDetail.setChargeTime(new Date());
		if(costDetailMapper.updateById(eCostDetail)!=1){
			throw new BusinessException("更新待处理费用详情处理状态失败");
		}
	}
//	if(ObjectUtils.isEmpty(sysAdminDetail)) {
//		throw new BusinessException("合同资金数据不存在");
//	}
//	else {
//		BigDecimal a= sysAdminDetail.getTotalIncome().add(eCostDto.getNetMoney());
//		return sysAdminDetailMapper.updateDetail(sysAdminDetail.getMaccId(), a);
//	}
}
	@Override
	public ECost selectMonth(String contrId, String month,String payTypes) {
		return eCostMapper.selectMonth(contrId, month ,payTypes);
	}

	@Override
	public int insertECost(ECost eCost) {
		return eCostMapper.insert(eCost);
	}

	@Override
	public ECost selectId(String id) {
		return eCostMapper.selectId(id);
	}

}
