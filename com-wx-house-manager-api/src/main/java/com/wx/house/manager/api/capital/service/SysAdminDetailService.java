package com.wx.house.manager.api.capital.service;

import com.wx.house.core.pojo.po.SysAdminDetail;
import com.wx.house.core.pojo.po.fileupload.AttaUpload;
import com.wx.house.core.pojo.vo.SysAdminBalanceVo;
import com.wx.house.core.pojo.vo.SysAdminDetailVo;
import com.wx.house.manager.api.capital.text.SysAdminDetailPame;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

/**
 * @Author ：罗棋
 * @Email ：58428467@qq.com
 * @date ：Created in 2019/11/27 下午 02:31
 * @description：${description}
 * @version: ：V$version
 */
public interface SysAdminDetailService extends IService<SysAdminDetail> {
SysAdminDetail selectByMaccId(String maccId);

SysAdminBalanceVo selectBalance(String maccId);

int addSysAdminDetail(SysAdminDetail adminCapital);

/***
 * 提交实名认证
 *
 * @param adminDetail
 * @return
 */
int updateCard(String adminId, SysAdminDetailPame sysAdminDetailPame);

/***
 * 查询用户资料
 *
 * @param adminId
 * @return
 */
SysAdminDetailVo selectAttaUpload(String adminId);
/***
 * 查询身份证是否已实名
 *
 * @param adminId
 * @return
 */
SysAdminDetail selectIdCardNumber(String idCardNumber);
}
