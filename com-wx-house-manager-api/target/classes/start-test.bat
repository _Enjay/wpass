setlocal enabledelayedexpansion
@echo off

rem 端口情况
set com-wx-house-manager-api=7741
set com-wx-house-terminal-api=7742


rem 发布到
set disk=D:
set basePath=%disk%\web\api\

rem 内部使用
set apiPath=%basePath%com-wx-house-manager-api\
set terminalPath=%basePath%com-wx-house-terminal-api\

echo =========================================================
echo 初始化工作
if not exist %apiPath%  md %apiPath%
if not exist %terminalPath%  md %terminalPath%
echo success

echo =========================================================
echo step1.执行清理工作
echo 为api服务清理%com-wx-house-api%端口
for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":%com-wx-house-manager-api%"') do taskkill /f /pid %%m
for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":%com-wx-house-terminal-api%"') do taskkill /f /pid %%m

echo 清理成功

echo step2.将jar包拷贝到相应的目录

echo ------------------------------
echo "%WORKSPACE%\com-wx-house-manager-api\target\com-wx-house-manager-api-0.0.1-SNAPSHOT.jar"
echo ↓↓↓↓↓
copy "%WORKSPACE%\com-wx-house-manager-api\target\com-wx-house-manager-api-0.0.1-SNAPSHOT.jar" "%apiPath%"
echo "%apiPath%"

echo "%WORKSPACE%\com-wx-house-terminal-api\target\com-wx-house-terminal-api-0.0.1-SNAPSHOT.jar"
echo ↓↓↓↓↓
copy "%WORKSPACE%\com-wx-house-terminal-api\target\com-wx-house-terminal-api-0.0.1-SNAPSHOT.jar" "%terminalPath%"
echo "%terminalPath%"
echo ------------------------------


echo =========================================================
echo 执行java包

cd \
cd %disk%
cd %basePath%

echo 执行 %apiPath%com-wx-house-manager-api-0.0.1-SNAPSHOT.jar
set BUILD_ID=dontKillMe
start javaw -server -Xms1024m -Xmx2048m -jar "%apiPath%com-wx-house-manager-api-0.0.1-SNAPSHOT.jar" --spring.profiles.active=test

echo 执行 %terminalPath%com-wx-house-terminal-api-0.0.1-SNAPSHOT.jar
set BUILD_ID=dontKillMe
start javaw -server -Xms1024m -Xmx2048m -jar "%terminalPath%com-wx-house-terminal-api-0.0.1-SNAPSHOT.jar" --spring.profiles.active=test

exit 0